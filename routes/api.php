<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('home', 'Api\HomeController@index');
Route::post('notification/test', 'Api\HomeController@testNotification');
Route::get('configurations', 'Api\HomeController@getConfigurationsData');
Route::get('all-categories', 'Api\ServiceController@getAllServiceCategories');
Route::get('all-services', 'Api\ServiceController@getAllServicesList');
Route::get('service-category/detail/{id}', 'Api\ServiceController@getServiceCategoryDetails');

Route::get('page/{page_type}', 'Api\CmsPageController@getPageDetailWebLink');
Route::get('faq-categories', 'Api\CmsPageController@getFaqCategories');
Route::get('faq-category/detail/{category}', 'Api\CmsPageController@getFaqsByCategories');

/*CUSTOMER AUTHENTICATION ROUTES*/
Route::post('customer/authenticate', 'Api\CustomerController@authenticate');
Route::post('customer/resend-otp', 'Api\CustomerController@resendOtp');
Route::post('customer/verify-otp', 'Api\CustomerController@verifyOtp');

Route::post('packages', 'Api\PackagesController@getPackagesList');
Route::post('contact-us/store', 'ContactUsController@storeContactUsData');

/*FOR CUSTOMERS */
Route::group(['middleware' => ['auth_customer_token'], 'prefix' => 'customer'], function () {
    Route::get('profile', 'Api\CustomerController@getCustomerDetail');
    Route::post('update-profile', 'Api\CustomerController@updateCustomerProfile');

    Route::post('update-number/send-otp', 'Api\CustomerController@sendUpdateNumberOTP');
    Route::post('update-number/verify-otp', 'Api\CustomerController@updateContactNumberVerification');

    /*CUSTOMER ADDRESS ROUTES*/
    Route::get('addresses', 'Api\CustomerAddressController@index');
    Route::post('address/store', 'Api\CustomerAddressController@storeAddress');
    Route::post('address/edit', 'Api\CustomerAddressController@getAddressDetail');
    Route::post('address/update', 'Api\CustomerAddressController@updateAddress');
    Route::post('address/delete', 'Api\CustomerAddressController@deleteAddress');

    /*BOOKING ROUTES*/
    Route::post('booking/create', 'Api\BookingController@bookServices');

    Route::post('bookings', 'Api\BookingController@getBookingsListForCustomer');
    Route::post('booking/details', 'Api\BookingController@getBookingDetailsForUsers');
    Route::post('booking/issue/store', 'Api\BookingController@generateServiceIssue');
    Route::post('booking/rating/store', 'Api\BookingController@updateBookingRatings');
    Route::post('booking/cancel', 'Api\BookingController@cancelBookingService');

    Route::post('notifications', 'Api\NotificationsController@getCustomerNotificationsList');
    Route::post('notification/delete', 'Api\NotificationsController@deleteNotification');
    Route::post('notification/read', 'Api\NotificationsController@readNotification');

    Route::post('contact-request/store', 'ContactRequestController@storeContactRequest');
    Route::post('conversations', 'Api\ConversationController@getConversationList');
    Route::post('conversation/cancel', 'Api\ConversationController@cancelConversation');
    Route::post('conversation/booking', 'Api\ConversationController@createBookingConversation');

    Route::post('package/purchase', 'Api\PackagesController@purchasePackage');
    Route::post('package/dopayment', 'Api\PackagesController@doPayment');

    Route::post('subscription-packages', 'Api\PackagesController@getCustomerSubscriptionsList');
});

/*VENDOR/MANAGER AUTHENTICATION ROUTES*/
Route::post('user/send-otp', 'Api\UserAuthController@sendOtp');
Route::post('user/verify-otp', 'Api\UserAuthController@verifyOtp');

Route::post('get-insurance-details', 'Api\HomeController@getInsuranceDetails');

/*FOR MANAGERS/VENDORS */
Route::group(['middleware' => ['auth_user_token']], function () {
    Route::get('user/profile', 'Api\UserController@getUserDetail');
    Route::post('user/update-profile', 'Api\UserController@updateUserProfile');
    Route::post('user/update-number/send-otp', 'Api\UserController@sendUpdateNumberOTP');
    Route::post('user/update-number/verify-otp', 'Api\UserController@updateContactNumberVerification');

    Route::post('user/bookings', 'Api\BookingController@getBookingsListForUsers');
    Route::post('user/reports', 'Api\BookingController@getBookingReports');
    Route::post('user/booking/details', 'Api\BookingController@getBookingDetailsForUsers');
    Route::post('vendor/booking/confirm', 'Api\BookingController@assignVendorForBookingService');
    Route::post('booking/service/start', 'Api\BookingController@serviceStartByVendor');
    Route::post('booking/completed/send-otp', 'Api\BookingController@sendServiceCompletedOTP');
    Route::post('booking/completed/confirm-otp', 'Api\BookingController@confirmServiceCompletedOTP');
    Route::post('user/booking/cancel', 'Api\BookingController@cancelBookingService');

    Route::post('user/payment-history', 'Api\BookingController@getBookingPaymentHistoryForUsers');
    Route::post('user/payout-history', 'Api\BookingController@getBookingPayoutHistoryForUsers');

    Route::post('user/notifications', 'Api\NotificationsController@getUserNotificationsList');
    Route::post('user/notification/delete', 'Api\NotificationsController@deleteNotification');
    Route::post('user/notification/read', 'Api\NotificationsController@readNotification');

    Route::post('user/get-services', 'Api\ServiceController@getServicesListByUser');
    Route::post('user/issue-requests-list', 'Api\BookingController@getIssuedBookingsListByUser');
    Route::post('user/conversations', 'Api\ConversationController@getConversationList');
    Route::post('user/conversation/cancel', 'Api\ConversationController@cancelConversation');
    Route::post('user/conversation/booking', 'Api\ConversationController@createBookingConversation');

    Route::post('vendor/get-work-schedule', 'Api\UserController@getVendorWorkSchedule');
    Route::post('vendor/work-schedule/store', 'Api\UserController@storeVendorWorkSchedule');
    Route::post('vendor/unavailability/store', 'Api\UserController@storeVendorUnavailability');
    Route::post('vendor/unavailability', 'Api\UserController@getVendorUnavailabilityList');
    Route::post('vendor/unavailability/cancel', 'Api\UserController@cancelVendorUnavailability');

    Route::group(['middleware' => ['check_manager']], function () {
        Route::post('manager/service-categories', 'Api\ServiceController@getServiceCategoriesListByUser');
        Route::post('manager/get-vendors', 'Api\UserController@getVendorsListByManager');

        Route::get('vendor/list', 'Api\UserController@getVendorList');
        
        Route::post('search-customers', 'Api\CustomerController@searchCustomers');
        Route::post('customer/create', 'Api\CustomerController@createCustomerByManager');
        Route::post('customer/detail', 'Api\CustomerController@getCustomerDetail');
        Route::post('customer-address/create', 'Api\CustomerAddressController@storeAddress');

        Route::post('manager/booking/create', 'Api\BookingController@createBookingByManager');
        Route::post('booking/get-vendors-by-service', 'Api\BookingController@getVendorsListByService');

        Route::post('manager/issue-request/approved', 'Api\BookingController@approveIssueRequest');
        Route::post('manager/issue-request/rejected', 'Api\BookingController@rejectIssueRequest');

        Route::post('manager/contact-request/approved', 'Api\BookingController@convertQuoteRequestToBooking');
        Route::post('manager/vendor-unavailability', 'Api\UserController@getVendorUnavailabilityList');

        Route::post('manager/payout/completed', 'Api\BookingController@bookingPayoutCompleted');

    });
});