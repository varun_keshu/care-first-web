<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'web'], function () {
    Route::get('/', 'Front\HomeController@index')->name('home');
    Route::get('/features', 'Front\HomeController@getFeaturePageData')->name('feature');
    Route::get('/services','Front\ServiceController@index')->name('service');
    Route::get('/detail/{id}','Front\ServiceController@getServiceCategoryDetail')->name('serviceCategory.detail');
    Route::get('/prices','Front\PricingController@index')->name('price');
    Route::get('/contact','Front\ContactUsController@index')->name('contact');
    Route::post('/contact/store','Front\ContactUsController@store')->name('contact.store');
    Route::get('/page/{type?}', 'Admin\PageController@getPageDetailByType')->name('page_detail');
    Route::get('/faqs', 'Admin\FaqsController@getFrontFaqsList')->name('faq');

    Route::get('admin/login', 'Admin\LoginController@index')->name('login');
    Route::post('admin/login/authenticate', 'Admin\LoginController@authenticateAdmin')->name('admin.authenticate');

    Route::get('admin/password/forgot', 'Admin\ForgotPasswordController@forgotPassword')->name('forgot_password');
    Route::post('admin/password/forgot', 'Admin\ForgotPasswordController@sendPasswordResetInstructions')->name('sent_reset_password_link');
    Route::get('admin/password/reset/{token}', 'Admin\ForgotPasswordController@resetPasswordLink')->name('reset_password');
    Route::post('admin/password/reset', 'Admin\ForgotPasswordController@resetPassword')->name('reset_admin_password');

    Route::get('admin-user/add-password/{token}', 'Admin\AdminUserController@setPasswordLink')->name('admin.set_password');
    Route::post('admin-user/add-password', 'Admin\AdminUserController@resetPassword')->name('admin-user.password.store');

    Route::get('app-view/messages/by-conversation', ['as' => 'app_view.getMessagesByConversation', 'uses' => 'AppViews\MessagesController@getMessagesByConversation']);
    Route::post('app-view/check-new-message', ['as' => 'app_view.checkNewMessage', 'uses' => 'AppViews\MessagesController@checkNewMessage']);
    Route::post('app-view/message/send', ['as' => 'app_view.message.send', 'uses' => 'AppViews\MessagesController@sendMessage']);

    Route::get('dopayment', ['as' => 'app.dopayment', 'uses' => 'AppViews\PaymentController@dopayment']);
    Route::get('payment/success', ['as' => 'app.payment.success', 'uses' => 'AppViews\PaymentController@paymentSuccess']);

    Route::group(['middleware' => ['admins'], 'prefix' => 'admin'], function () {
        Route::get('/logout', 'Admin\LoginController@logout')->name('logout');
        Route::get('/profile', 'Admin\AdminUserController@getProfile')->name('admin.profile');
        Route::post('/profile/update', 'Admin\AdminUserController@updateProfile')->name('admin.update_profile');

        Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');

        Route::get('/admin-users', 'Admin\AdminUserController@index')->name('admin.admin_users');
        Route::get('/admin-user/create', 'Admin\AdminUserController@createNewAdmin')->name('admin.create');
        Route::post('admin-user/store', 'Admin\AdminUserController@store')->name('admin.store');
        Route::get('/admin-user/delete/{id}', 'Admin\AdminUserController@delete')->name('admin.admin-user.delete');

        Route::get('/email-templates', 'Admin\EmailTemplateController@index')->name('email_templates');
        Route::get('/email-templates/create', 'Admin\EmailTemplateController@create')->name('email_templates.create');
        Route::post('/email-templates/store', 'Admin\EmailTemplateController@store')->name('email_templates.store');
        Route::get('/email-templates/edit/{id}', 'Admin\EmailTemplateController@edit')->name('email_templates.edit');
        Route::post('/email-templates/update/{id}', 'Admin\EmailTemplateController@update')->name('email_templates.update');
        Route::get('/email-templates/delete/{id}', 'Admin\EmailTemplateController@delete')->name('email_templates.delete');

        /*SLIDERS ROUTES*/
        Route::get('/banners', 'Admin\SliderController@index')->name('sliders');
        Route::get('/banners/create', 'Admin\SliderController@create')->name('sliders.create');
        Route::post('/banners/store', 'Admin\SliderController@store')->name('sliders.store');
        Route::get('/banners/edit/{id}', 'Admin\SliderController@edit')->name('sliders.edit');
        Route::post('/banners/update/{id}', 'Admin\SliderController@update')->name('sliders.update');
        Route::get('/banners/delete/{id}', 'Admin\SliderController@delete')->name('sliders.delete');

        /*PAGES ROUTES*/
        Route::get('/pages', 'Admin\PageController@index')->name('pages');
        Route::get('/page/create', 'Admin\PageController@create')->name('pages.create');
        Route::post('/page/store', 'Admin\PageController@store')->name('pages.store');
        Route::get('/page/edit/{id}', 'Admin\PageController@edit')->name('pages.edit');
        Route::post('/page/update/{id}', 'Admin\PageController@update')->name('pages.update');
        Route::get('/page/delete/{id}', 'Admin\PageController@delete')->name('pages.delete');

        /*FAQs ROUTES*/
        Route::get('/faqs', 'Admin\FaqsController@index')->name('faqs');
        Route::get('/faq/create', 'Admin\FaqsController@create')->name('faqs.create');
        Route::post('/faq/store', 'Admin\FaqsController@store')->name('faqs.store');
        Route::get('/faq/edit/{id}', 'Admin\FaqsController@edit')->name('faqs.edit');
        Route::post('/faq/update/{id}', 'Admin\FaqsController@update')->name('faqs.update');
        Route::get('/faq/delete/{id}', 'Admin\FaqsController@delete')->name('faqs.delete');
        Route::get('/faq/sort-order', 'Admin\FaqsController@getSortOrderForm')->name('faqs.sort_order');
        Route::get('/get-faqs-by-category/{id}', 'Admin\FaqsController@getFaqsByCategory')->name('faqs.getFaqsByCategory');
        Route::post('/faq-order/update-order', 'Admin\FaqsController@updateFaqOrder')->name('faqs.updateFaqOrder');

        /*SERVICE CATEGORIES ROUTES*/
        Route::get('/service-categories', 'Admin\ServiceCategoryController@index')->name('service_categories');
        Route::get('/service-categories/create', 'Admin\ServiceCategoryController@create')->name('service_categories.create');
        Route::post('/service-categories/store', 'Admin\ServiceCategoryController@store')->name('service_categories.store');
        Route::get('/service-categories/edit/{id}', 'Admin\ServiceCategoryController@edit')->name('service_categories.edit');
        Route::post('/service-categories/update/{id}', 'Admin\ServiceCategoryController@update')->name('service_categories.update');
        Route::get('/service-categories/delete/{id}', 'Admin\ServiceCategoryController@delete')->name('service_categories.delete');

        /*SERVICES ROUTES*/
        Route::get('/services', 'Admin\ServiceController@index')->name('services');
        Route::get('/services/create', 'Admin\ServiceController@create')->name('services.create');
        Route::post('/services/store', 'Admin\ServiceController@store')->name('services.store');
        Route::get('/services/edit/{id}', 'Admin\ServiceController@edit')->name('services.edit');
        Route::post('/services/update/{id}', 'Admin\ServiceController@update')->name('services.update');
        Route::get('/services/delete/{id}', 'Admin\ServiceController@delete')->name('services.delete');

        /*SERVICE AREAS ROUTES*/
        Route::get('/service-areas', 'Admin\ServiceAreasController@index')->name('service_areas');
        Route::get('/service-area/create', 'Admin\ServiceAreasController@create')->name('service_areas.create');
        Route::post('/service-area/store', 'Admin\ServiceAreasController@store')->name('service_areas.store');
        Route::get('/service-area/edit/{id}', 'Admin\ServiceAreasController@edit')->name('service_areas.edit');
        Route::post('/service-area/update/{id}', 'Admin\ServiceAreasController@update')->name('service_areas.update');
        Route::get('/service-area/delete/{id}', 'Admin\ServiceAreasController@delete')->name('service_areas.delete');

        /*SERVICE STATUS ROUTES*/
        Route::get('/service-statuses', 'Admin\ServiceStatusController@index')->name('service_status');
        Route::get('/service-status/create', 'Admin\ServiceStatusController@create')->name('service_status.create');
        Route::post('/service-status/store', 'Admin\ServiceStatusController@store')->name('service_status.store');
        Route::get('/service-status/edit/{id}', 'Admin\ServiceStatusController@edit')->name('service_status.edit');
        Route::post('/service-status/update/{id}', 'Admin\ServiceStatusController@update')->name('service_status.update');
        Route::get('/service-status/delete/{id}', 'Admin\ServiceStatusController@delete')->name('service_status.delete');

        /*CUSTOMERS ROUTES*/
        Route::get('/customers', 'Admin\CustomerController@index')->name('customers');
        Route::get('/customer/detail/{id}', 'Admin\CustomerController@getCustomerDetails')->name('customer.detail');
        Route::post('/customer/conversation/get-messages', 'Admin\CustomerController@getMessagesList')->name('customer.messages');
        Route::post('/customer/message/send', 'Admin\CustomerController@sendMessageToCustomer')->name('customer.send_message');

        /*MANAGERS ROUTES*/
        Route::get('/managers', 'Admin\ManagerController@index')->name('managers');
        Route::get('/manager/get-available-services/{service_area}', 'Admin\ManagerController@getAvailableServicesByArea');
        Route::get('/manager/create', 'Admin\ManagerController@create')->name('managers.create');
        Route::post('/manager/store', 'Admin\ManagerController@store')->name('managers.store');
        Route::get('/manager/edit/{id}', 'Admin\ManagerController@edit')->name('managers.edit');
        Route::post('/manager/update/{id}', 'Admin\ManagerController@update')->name('managers.update');
        Route::get('/manager/show/{id}', 'Admin\ManagerController@show')->name('managers.show');

        /*VENDORS ROUTES*/
        Route::get('/vendors', 'Admin\VendorController@index')->name('vendors');
        Route::get('/vendor/create', 'Admin\VendorController@create')->name('vendors.create');
        Route::get('/vendor/get-manager-services/{id}', 'Admin\VendorController@getManagerServiceList');
        Route::post('/vendor/store', 'Admin\VendorController@store')->name('vendors.store');
        Route::get('/vendor/edit/{id}', 'Admin\VendorController@edit')->name('vendors.edit');
        Route::post('/vendor/update/{id}', 'Admin\VendorController@update')->name('vendors.update');
        Route::get('/vendor/view/{id}', 'Admin\VendorController@show')->name('vendors.show');

        /*BOOKINGS ROUTES*/
        Route::get('/bookings', 'Admin\BookingsController@index')->name('bookings');
        Route::get('/bookings/ajax', 'Admin\BookingsController@getListAjaxData');
        Route::get('/booking/create', 'Admin\BookingsController@create')->name('booking.create');
        Route::get('/booking/get-customers-list', 'Admin\CustomerController@getCustomersListBySearch');
        Route::get('/booking/get-customer-addresses', 'Admin\CustomerController@getAddressList');
        Route::get('/booking/get-managers-by-service', 'Admin\ManagerController@getManagersListByService');
        Route::get('/booking/get-service-price-details/{id}', 'Admin\ServiceController@getServiceDetails');
        Route::post('/booking/store', 'Admin\BookingsController@store')->name('booking.store');
        Route::get('/booking/get-available-vendors/{id}', 'Admin\BookingsController@getAvailableVendorsList')->name('booking.getAvailableVendorsList');
        Route::post('/booking/assign-vendor/{id}', 'Admin\BookingsController@assignBookingToVendor')->name('booking.assignVendor');
        Route::get('/booking/detail/{id}', 'Admin\BookingsController@getBookingDetails')->name('booking.detail');

        /*PACKAGES ROUTES*/
        Route::get('/packages', 'Admin\PackageController@index')->name('package');
        Route::get('/package/create', 'Admin\PackageController@create')->name('package.create');
        Route::post('/package/store', 'Admin\PackageController@store')->name('package.store');
        Route::get('/package/{id}/show', 'Admin\PackageController@show')->name('package.show');
        Route::get('/package/edit/{id}', 'Admin\PackageController@edit')->name('package.edit');
        Route::post('/package/update/{id}', 'Admin\PackageController@update')->name('package.update');
        Route::get('/package/delete/{id}', 'Admin\PackageController@delete')->name('package.delete');

        //PACKAGE SUBSCRIPTIONS ROUTES
        Route::get('/package-subscriptions', 'Admin\PackageSubscriptionsController@index')->name('package-subscription');
        Route::get('/package-subscription/{id}/show', 'Admin\PackageSubscriptionsController@show')->name('package-subscription.show');

        //SITE CONFIGURATIONS
        Route::get('site-configurations', "ConfigurationsController@index")->name('site_configurations');
        Route::post('site-configurations/update', "ConfigurationsController@update")->name('site_configurations.update');

        //INSURANCES ROUTES
        Route::get('insurances', 'Admin\InsuranceController@index')->name('insurances');
        Route::get('insurance/create', 'Admin\InsuranceController@create')->name('insurance.create');
        Route::post('insurance/store', 'Admin\InsuranceController@store')->name('insurance.store');
        Route::get('insurance/edit/{id}', 'Admin\InsuranceController@edit')->name('insurance.edit');
        Route::post('insurance/update/{id}', 'Admin\InsuranceController@update')->name('insurance.update');
        Route::get('insurance/delete/{id}', 'Admin\InsuranceController@delete')->name('insurance.delete');

        //TESTIMONIALS ROUTES
        Route::get('testimonials', 'Admin\TestimonialController@index')->name('testimonials');
        Route::get('testimonial/create', 'Admin\TestimonialController@create')->name('testimonial.create');
        Route::post('testimonial/store', 'Admin\TestimonialController@store')->name('testimonial.store');
        Route::get('testimonial/edit/{id}', 'Admin\TestimonialController@edit')->name('testimonial.edit');
        Route::post('testimonial/update/{id}', 'Admin\TestimonialController@update')->name('testimonial.update');
        Route::get('testimonial/delete/{id}', 'Admin\TestimonialController@delete')->name('testimonial.delete');

        //CONTACT REQUEST ROUTES
        Route::get('contact_request', 'Admin\ContactRequestController@index')->name('contact_request');
        Route::get('contact_request/ajax', 'Admin\ContactRequestController@getListAjaxData');
        Route::get('contact_request/detail/{id}', 'Admin\ContactRequestController@getContactDetails')->name('contact_request.detail');

        //REWARD PROGRAM ROUTES
        Route::get('reward_programs', 'Admin\RewardProgramController@index')->name('reward_program');
        Route::get('reward_program/create', 'Admin\RewardProgramController@create')->name('reward_program.create');
        Route::post('reward_program/store', 'Admin\RewardProgramController@store')->name('reward_program.store');
        Route::get('reward_program/edit/{id}', 'Admin\RewardProgramController@edit')->name('reward_program.edit');
        Route::post('reward_program/update/{id}', 'Admin\RewardProgramController@update')->name('reward_program.update');
        Route::get('reward_program/delete/{id}', 'Admin\RewardProgramController@delete')->name('reward_program.delete');

        //REPORTS ROUTES
        Route::get('report/by-vendor','Admin\ReportController@getBookingListByVendor')->name('reportByVendor');
        Route::get('report/by-vendor/ajax', 'Admin\ReportController@getListVendorData');
        Route::get('report/by-service','Admin\ReportController@getBookingListByService')->name('reportByService');
        Route::get('report/by-service/ajax','Admin\ReportController@getListServiceData');
        Route::get('report/by-category','Admin\ReportController@getBookingByCategory')->name('reportByCategory');
        Route::get('report/by-category/ajax','Admin\ReportController@getListCategoryData');
        Route::get('payout-history','Admin\ReportController@getPayoutReports')->name('payoutHistory');
        Route::get('payment_history/ajax','Admin\ReportController@getPayoutReportsAjax');

        //CONTACT SUBMISSION
        Route::get('contact_submission', 'ContactUsController@index')->name('contact_submission');
        Route::get('contact_submission/ajax', 'ContactUsController@getListAjaxData');

        //Client
        Route::get('clients','Admin\ClientController@index')->name('clients');
        Route::get('clients/create', 'Admin\ClientController@create')->name('clients.create');
        Route::post('clients/store', 'Admin\ClientController@store')->name('clients.store');
        Route::get('clients/edit/{id}', 'Admin\ClientController@edit')->name('clients.edit');
        Route::post('clients/update/{id}', 'Admin\ClientController@update')->name('clients.update');
        Route::get('clients/delete/{id}', 'Admin\ClientController@delete')->name('clients.delete');

    });
});
