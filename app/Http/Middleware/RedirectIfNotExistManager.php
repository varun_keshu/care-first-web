<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotExistManager
{
    public function handle($request, Closure $next)
    {
        $requestData = $request->all();
        if ($requestData['user_type'] == "manager") {
            return $next($request);
        }
        return response()->json([
            'status' => 100,
            'message' => "Only manager can access this URL!",
            'data' => null,
        ]);
    }
}