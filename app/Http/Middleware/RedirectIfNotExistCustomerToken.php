<?php

namespace App\Http\Middleware;

use App\Models\Customers;
use Closure;

class RedirectIfNotExistCustomerToken
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if ($token) {
            $token = explode(' ', $token);
            if (@$token[1]) {
                $token = $token[1];
            }
        }
        $customer = Customers::where('access_token', $token)->first();
        if (!$customer || empty($token)) {
            return response()->json([
                'status' => 401,
                'message' => "Invalid customer access token!",
                'data' => null,
            ]);
        }
        $request['customer_id'] = $customer['id'];
        return $next($request);
    }
}