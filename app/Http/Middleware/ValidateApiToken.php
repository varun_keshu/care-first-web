<?php

namespace App\Http\Middleware;

use Closure;

class ValidateApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('apiSecretKey') != env('API_HEADER_SECRET_KEY')) {
            $response_data = [
                'status' => 100,
                'message' => 'Api header secret key not found.',
                'data' => null
            ];
            return response()->json($response_data);
        }
        return $next($request);

    }
}
