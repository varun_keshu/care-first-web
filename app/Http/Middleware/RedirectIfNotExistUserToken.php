<?php

namespace App\Http\Middleware;

use App\Models\Customers;
use App\Models\User;
use Closure;

class RedirectIfNotExistUserToken
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if ($token) {
            $token = explode(' ', $token);
            if (@$token[1]) {
                $token = $token[1];
            }
        }
        $user = User::where('access_token', $token)->first();
        if (!$user || empty($token)) {
            return response()->json([
                'status' => 401,
                'message' => "Invalid user access token!",
                'data' => null,
            ]);
        }
        $request['user_id'] = $user['id'];
        $request['user_type'] = $user['user_type'];
        return $next($request);
    }
}