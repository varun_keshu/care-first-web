<?php

namespace App\Http\Controllers;

use App\Interfaces\ContactUsRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ContactUsController extends Controller
{
    protected $contactUsRepository;

    public function __construct(ContactUsRepositoryInterface $contactUsRepository)
    {
        $this->contactUsRepository = $contactUsRepository;
    }

    public function storeContactUsData(Request $request)
    {
        $this->contactUsRepository->storeContactUsData($request);
        return response()->json([
            'status' => 200,
            'message' => "Your contact request sent successfully!",
            'data' => null,
        ]);
    }

    public function index()
    {
        return view('admin.contact_submission.index');
    }

    public function getListAjaxData(Request $request)
    {
        $contact = $this->contactUsRepository->getListAjaxData($request);
        return $contact;
    }

    public function getContactDetails($contact_request_id)
    {
        $contact_request = $this->contactUsRepository->getContactDetails($contact_request_id);
        if (isset($contact_request)) {
            $previous_url = URL::previous();

            return view('admin.contact_submission.details', compact('contact_request', 'previous_url'));

        }
        return redirect()->back()->with('error', "Invalid Request!");
    }



}
