<?php

namespace App\Http\Controllers;

use App\Interfaces\ConfigurationRepositoryInterface;
use Illuminate\Http\Request;

class ConfigurationsController extends Controller
{
    protected $configurationRepository;
    public function __construct(ConfigurationRepositoryInterface $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }

    public function index()
    {
        $site_configurations = $this->configurationRepository->getAllConfigurations();
        return view('admin.site_configurations.index', compact('site_configurations'));
    }

    public function update(Request $request)
    {
        $this->configurationRepository->updateConfigurations($request);
        return redirect()->route('site_configurations')->with('success', "Data updated successfully!");
    }
}
