<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\CustomersRepositoryInterface;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Interfaces\Api\UserRepositoryInterface;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    protected $notificationRepository;
    protected $userRepository;
    protected $customersRepository;

    public function __construct(NotificationRepositoryInterface $notificationRepository, UserRepositoryInterface $userRepository,
                                CustomersRepositoryInterface $customersRepository)
    {
        $this->notificationRepository = $notificationRepository;
        $this->userRepository = $userRepository;
        $this->customersRepository = $customersRepository;
    }

    public function getUserNotificationsList(Request $request)
    {
        try {
            $notifications = $this->notificationRepository->getUserNotificationsList($request);
            $user = $this->userRepository->getUserDetail($request);
            return response()->json([
                'status' => 200,
                'message' => "User Notifications Sent Successfully!",
                'data' => $notifications,
                'count' => $user['unread_notifications_count']
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getCustomerNotificationsList(Request $request)
    {
        try {
            $notifications = $this->notificationRepository->getCustomerNotificationsList($request);
            $customer = $this->customersRepository->getCustomerDetail($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Notifications Sent Successfully!",
                'data' => $notifications,
                'count' => $customer['unread_notifications_count']
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function deleteNotification(Request $request)
    {
        try {
            $user_type = $this->notificationRepository->deleteNotification($request);
            if ($user_type == "customer"){
                $notifications = $this->notificationRepository->getCustomerNotificationsList($request);
                $customer = $this->customersRepository->getCustomerDetail($request);
                $notification_count = $customer['unread_notifications_count'];
            }else{
                $notifications = $this->notificationRepository->getUserNotificationsList($request);
                $user = $this->userRepository->getUserDetail($request);
                $notification_count = $user['unread_notifications_count'];
            }
            return response()->json([
                'status' => 200,
                'message' => "Notification Deleted Successfully!",
                'data' => $notifications,
                'count' => $notification_count
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function readNotification(Request $request)
    {
        try {
            $user_type = $this->notificationRepository->readNotification($request);
            if ($user_type == "customer"){
                $notifications = $this->notificationRepository->getCustomerNotificationsList($request);
                $customer = $this->customersRepository->getCustomerDetail($request);
                $notification_count = $customer['unread_notifications_count'];
            }else{
                $notifications = $this->notificationRepository->getUserNotificationsList($request);
                $user = $this->userRepository->getUserDetail($request);
                $notification_count = $user['unread_notifications_count'];
            }
            return response()->json([
                'status' => 200,
                'message' => "Notification Read Successfully!",
                'data' => $notifications,
                'count' => $notification_count
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
