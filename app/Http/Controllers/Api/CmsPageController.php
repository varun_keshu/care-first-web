<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\CmsPageRepositoryInterface;
use App\Models\Pages;

class CmsPageController extends Controller
{
    protected $cmsPageRepository;

    public function __construct(CmsPageRepositoryInterface $cmsPageRepository)
    {
        $this->cmsPageRepository = $cmsPageRepository;
    }

    public function getPageDetailWebLink($page_type)
    {
        try {
            $page_detail = $this->cmsPageRepository->getPageDetails($page_type);
            return response()->json([
                'status' => 200,
                'message' => "Page Detail Web Successfully!",
                'data' => [
                    "type" => $page_type,
                    "title" => isset($page_detail) && isset($page_detail['title']) ? $page_detail['title'] : Pages::TYPES[$page_type],
                    "content" => isset($page_detail) ? $page_detail['content'] : null,
                ]
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getFaqCategories()
    {
        try {
            $data = $this->cmsPageRepository->getFaqCategories();;
            return response()->json([
                'status' => 200,
                'message' => "Faq Categories Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getFaqsByCategories($category)
    {
        try {
            $data = $this->cmsPageRepository->getFaqsByCategories($category);
            return response()->json([
                'status' => 200,
                'message' => "Faq Categories Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
