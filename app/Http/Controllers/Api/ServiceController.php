<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\Api\BannerRepositoryInterface;
use App\Interfaces\Api\BookingRepositoryInterface;
use App\Interfaces\Api\CategoryRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    protected $bannerRepository;
    protected $categoryRepository;
    protected $bookingRepository;

    public function __construct(BannerRepositoryInterface $bannerRepository, CategoryRepositoryInterface $categoryRepository,
            BookingRepositoryInterface $bookingRepository)
    {
        $this->bannerRepository = $bannerRepository;
        $this->categoryRepository = $categoryRepository;
        $this->bookingRepository = $bookingRepository;
    }

    public function getAllServiceCategories()
    {
        try {
            $data['banners'] = $this->bannerRepository->getBannersListByType('all_services_page');
            $data['categories'] = $this->categoryRepository->getAllServiceCategoriesList();
            return response()->json([
                'status' => 200,
                'message' => "All Service Categories Page Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getAllServicesList()
    {
        try {
            $data = $this->categoryRepository->getAllServicesList();
            return response()->json([
                'status' => 200,
                'message' => "All Services List Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getServiceCategoryDetails($id)
    {
        try {
            $data['banners'] = $this->bannerRepository->getBannersListByType('service_category_page', $id);
            $data = $this->categoryRepository->getServiceCategoryDetails($id, $data);
            $data['reviews'] = $this->bookingRepository->getReviewList($id);
            return response()->json([
                'status' => 200,
                'message' => "Service Category Details Page Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getServiceCategoriesListByUser(Request $request)
    {
        try {
            $data = $this->categoryRepository->getServiceCategoriesListByUser($request);
            return response()->json([
                'status' => 200,
                'message' => "Service Category List Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getServicesListByUser(Request $request)
    {
        try {
            $data = $this->categoryRepository->getServicesListByUser($request);
            return response()->json([
                'status' => 200,
                'message' => "Services List Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
