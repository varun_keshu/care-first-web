<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\ConversationRepositoryInterface;
use App\Interfaces\ContactRequestRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConversationController extends Controller
{
    protected $conversationRepository;
    protected $contactRequestRepository;

    public function __construct(ConversationRepositoryInterface $conversationRepository, ContactRequestRepositoryInterface $contactRequestRepository)
    {
        $this->conversationRepository = $conversationRepository;
        $this->contactRequestRepository = $contactRequestRepository;
    }

    public function getConversationList(Request $request)
    {
        try {
            $data = $this->conversationRepository->getConversationList($request);
            return response()->json([
                'status' => 200,
                'message' => "Conversation List Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function cancelConversation(Request $request)
    {
        DB::beginTransaction();
        try {
            $contact_request_id = $this->conversationRepository->cancelConversation($request);
            $this->contactRequestRepository->deleteContactRequest($contact_request_id);
            $data = $this->conversationRepository->getConversationList($request);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "Conversation Cancel Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function createBookingConversation(Request $request)
    {
        DB::beginTransaction();
        try {
            $conversation_url = $this->contactRequestRepository->createBookingConversation($request);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "Conversation Created Successfully!",
                'data' => ['conversation_url' => $conversation_url]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
