<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\NotificationRepositoryInterface;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $notificationRepository;

    public function __construct(NotificationRepositoryInterface $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function getNotificationsList(Request $request)
    {
        try {
            $notifications = [];
            return response()->json([
                'status' => 200,
                'message' => "Notifications List Sent Successfully!",
                'data' => $notifications
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
