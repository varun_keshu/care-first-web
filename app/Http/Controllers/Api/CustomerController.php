<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\CustomersRepositoryInterface;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    protected $customersRepository;

    public function __construct(CustomersRepositoryInterface $customersRepository)
    {
        $this->customersRepository = $customersRepository;
    }

    public function authenticate(Request $request)
    {
        try {
            $is_valid = $this->customersRepository->validateContactNumber($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                $errors = $is_valid['result']->errors();
                return response()->json([
                    'status' => 100,
                    'message' => implode(",", $errors->all()),
                    'data' => NULL,
                ]);
            }
            $response = $this->customersRepository->sendOtp($request);
            if ($response['success']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Otp Sent Successfully!",
                    'data' => $response['data']
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function resendOtp(Request $request)
    {
        try {
            $response = $this->customersRepository->resendOtp($request);
            if ($response['success']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Otp Sent Successfully!",
                    'data' => $response['data']
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function verifyOtp(Request $request)
    {
        try {
            $response = $this->customersRepository->verifyOtp($request);
            if ($response['success']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Otp Verify Successfully!",
                    'data' => $response['data']
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function sendUpdateNumberOTP(Request $request)
    {
        try {
            $is_valid = $this->customersRepository->validateContactNumber($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                $errors = $is_valid['result']->errors();
                return response()->json([
                    'status' => 100,
                    'message' => implode(",", $errors->all()),
                    'data' => NULL,
                ]);
            }
            $response = $this->customersRepository->sendUpdateNumberOTP($request);
            if ($response['success']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Otp Sent Successfully!",
                    'data' => null
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function updateContactNumberVerification(Request $request)
    {
        try {
            $response = $this->customersRepository->updateContactNumberVerification($request);
            if ($response['success']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Your number updated successfully!",
                    'data' => $response['data']
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getCustomerDetail(Request $request)
    {
        try {
            $customer = $this->customersRepository->getCustomerDetail($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Detail Sent Successfully!",
                'data' => $customer
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function updateCustomerProfile(Request $request)
    {
        try {
            $this->customersRepository->updateCustomerProfile($request);
            $customer = $this->customersRepository->getCustomerDetail($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Detail Updated Successfully!",
                'data' => $customer
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function searchCustomers(Request $request)
    {
        try {
            $customers = $this->customersRepository->searchCustomers($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Data Sent Successfully!",
                'data' => $customers
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function createCustomerByManager(Request $request)
    {
        try {
            $is_valid = $this->customersRepository->validateContactNumber($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                $errors = $is_valid['result']->errors();
                return response()->json([
                    'status' => 100,
                    'message' => implode(",", $errors->all()),
                    'data' => NULL,
                ]);
            }
            $response = $this->customersRepository->createCustomerByManager($request);
            if ($response['status']){
                $customer_detail = $this->customersRepository->getCustomerDetailById($response['customer_id']);
                return response()->json([
                    'status' => 200,
                    'message' => "Customer Data Sent Successfully!",
                    'data' => $customer_detail
                ]);
            }
            return response()->json([
                'status' => 100,
                'message' => $response['message'],
                'data' => null
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
