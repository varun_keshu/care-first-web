<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\Api\BannerRepositoryInterface;
use App\Interfaces\Api\BookingRepositoryInterface;
use App\Interfaces\Api\CategoryRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Interfaces\Api\CmsPageRepositoryInterface;
use App\Interfaces\Api\CustomerAddressRepositoryInterface;
use App\Interfaces\ConfigurationRepositoryInterface;
use App\Services\PushNotificationService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $bannerRepository;
    protected $categoryRepository;
    protected $customerAddressRepository;
    protected $bookingRepository;
    protected $configurationRepository;
    protected $pushNotificationService;
    protected $cmsPageRepository;

    public function __construct(BannerRepositoryInterface $bannerRepository, CategoryRepositoryInterface $categoryRepository,
                                CustomerAddressRepositoryInterface $customerAddressRepository, BookingRepositoryInterface $bookingRepository,
                                ConfigurationRepositoryInterface $configurationRepository, PushNotificationService $pushNotificationService,
                                CmsPageRepositoryInterface $cmsPageRepository)
    {
        $this->bannerRepository = $bannerRepository;
        $this->categoryRepository = $categoryRepository;
        $this->customerAddressRepository = $customerAddressRepository;
        $this->bookingRepository = $bookingRepository;
        $this->configurationRepository = $configurationRepository;
        $this->pushNotificationService = $pushNotificationService;
        $this->cmsPageRepository = $cmsPageRepository;
    }

    public function index()
    {
        try {
            $data['banners'] = $this->bannerRepository->getBannersListByType('home_page');
            $data['categories'] = $this->categoryRepository->getServiceCategoriesList();
            $data['faqs'] = $this->cmsPageRepository->getFaqsByCategories('general');
            $data['reviews'] = $this->bookingRepository->getReviewList();
            return response()->json([
                'status' => 200,
                'message' => "Home Page Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getConfigurationsData()
    {
        try {
            $data['site_configurations'] = $this->configurationRepository->getContactData();
            $data['areas'] = $this->customerAddressRepository->getServiceAreas();
            $data['status_list'] = $this->bookingRepository->getStatusList();
            $data['services'] = $this->categoryRepository->getAllCategoryServices();
            $data['estimation_services'] = $this->categoryRepository->getAllEstimationServices();
            $data['customer_insurances'] = $this->configurationRepository->getCustomerInsuranceList();
            return response()->json([
                'status' => 200,
                'message' => "Configurations Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getInsuranceDetails(Request $request)
    {
        try {
            $data = $this->configurationRepository->getInsuranceDetails($request);
            return response()->json([
                'status' => 200,
                'message' => "Insurance Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function testNotification(Request $request)
    {
        try {
            $requestData = $request->all();
            $response = $this->pushNotificationService->sendNotification($requestData['message'], $requestData['device_token'], $requestData['title'], [], $requestData['user_type']);
            return response()->json([
                'status' => 200,
                'message' => "Notification Sent Successfully!",
                'data' => $response
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
