<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUserDetail(Request $request)
    {
        try {
            $user = $this->userRepository->getUserDetail($request);
            return response()->json([
                'status' => 200,
                'message' => "User Detail Sent Successfully!",
                'data' => $user
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getVendorsListByManager(Request $request)
    {
        try {
            $user = $this->userRepository->getVendorsListByManager($request);
            return response()->json([
                'status' => 200,
                'message' => "Vendors List Sent Successfully!",
                'data' => $user
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function updateUserProfile(Request $request)
    {
        try {
            $is_valid = $this->userRepository->validateUser($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                $errors = $is_valid['result']->errors();
                return response()->json(['status' => 100,
                    'message' => implode(",", $errors->all()),
                    'data' => NULL,
                ]);
            }
            $this->userRepository->updateUserProfile($request);
            $user = $this->userRepository->getUserDetail($request);
            return response()->json([
                'status' => 200,
                'message' => "User Detail Updated Successfully!",
                'data' => $user
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function sendUpdateNumberOTP(Request $request)
    {
        try {
            $is_valid = $this->userRepository->validateContactNumber($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                $errors = $is_valid['result']->errors();
                return response()->json([
                    'status' => 100,
                    'message' => implode(",", $errors->all()),
                    'data' => NULL,
                ]);
            }
            $response = $this->userRepository->sendUpdateNumberOTP($request);
            if ($response['success']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Otp Sent Successfully!",
                    'data' => $response['data']
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function updateContactNumberVerification(Request $request)
    {
        try {
            $response = $this->userRepository->updateContactNumberVerification($request);
            if ($response['success']) {
                $user = $this->userRepository->getUserDetail($request);
                return response()->json([
                    'status' => 200,
                    'message' => "Number Updated Successfully!",
                    'data' => $user
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getVendorList(Request $request)
    {
        try {
            $data = $this->userRepository->getVendorList($request);
            return response()->json([
                'status' => 200,
                'message' => "Vendor List Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getVendorWorkSchedule(Request $request)
    {
        try {
            $data = $this->userRepository->getVendorWorkSchedule($request);
            return response()->json([
                'status' => 200,
                'message' => "Vendor Schedule Send Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function storeVendorWorkSchedule(Request $request)
    {
        try {
            $this->userRepository->storeVendorWorkSchedule($request);
            return response()->json([
                'status' => 200,
                'message' => "Vendor Schedule Stored Successfully!",
                'data' => null
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getVendorUnavailabilityList(Request $request)
    {
        try {
            $data = $this->userRepository->getVendorUnavailabilityList($request);
            return response()->json([
                'status' => 200,
                'message' => "Vendor Unavailability List Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function storeVendorUnavailability(Request $request)
    {
        try {
            $this->userRepository->storeVendorUnavailability($request);
            return response()->json([
                'status' => 200,
                'message' => "Vendor Unavailability Stored Successfully!",
                'data' => null
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function cancelVendorUnavailability(Request $request)
    {
        try {
            $this->userRepository->cancelVendorUnavailability($request);
            return response()->json([
                'status' => 200,
                'message' => "Vendor Unavailability Deleted Successfully!",
                'data' => null
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
