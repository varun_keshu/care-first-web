<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\PackagesRepositoryInterface;
use App\Models\PackageSubscription;
use App\Services\RazorpayService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Razorpay\Api\Api;

class PackagesController extends Controller
{
    protected $packagesRepository;
    protected $razorpayService;

    public function __construct(PackagesRepositoryInterface $packagesRepository, RazorpayService $razorpayService)
    {
        $this->packagesRepository = $packagesRepository;
        $this->razorpayService = $razorpayService;
    }

    public function getPackagesList(Request $request)
    {
        try {
            $packages = $this->packagesRepository->getPackagesList($request);
            return response()->json([
                'status' => 200,
                'message' => "Packages List Sent Successfully!",
                'data' => $packages
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getCustomerSubscriptionsList(Request $request)
    {
        try {
            $packages = $this->packagesRepository->getCustomerSubscriptionsList($request);
            return response()->json([
                'status' => 200,
                'message' => "My Subscription Packages List Sent Successfully!",
                'data' => $packages
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function purchasePackage(Request $request)
    {
        try {
            $requestData = $request->all();
            $response = $this->packagesRepository->purchasePackage($requestData);
            if ($response['status']) {
                $data = $this->razorpayService->createSubscriptionOrderId($response['package_subscription_id']);
                return response()->json([
                    'status' => 200,
                    'message' => "Package Subscription Stored Successfully!",
                    'data' => [
                        'package_subscription_id' => $response['package_subscription_id'],
                        'razorpay_order_id' => $data['razorpay_order_id'],
                        'order_id' => $data['order_id'],
                    ]
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function dopayment(Request $request)
    {
//        $requestData = $request->all();
//        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));

        /*try {
            $packageDetail = PackageSubscription::where('razorpay_order_id', $requestData['razorpay_order_id'])->first();



            $generated_signature = $requestData['razorpay_order_id']."|".$requestData['razorpay_payment_id'];
            $sig = hash_hmac('sha256', $generated_signature, env('RAZORPAY_SECRET'));

           // $order = $api->utility->verifyPaymentSignature(['razorpay_payment_id' => $requestData['razorpay_payment_id'], 'razorpay_signature' => $requestData['razorpay_signature'], 'order_id' => 'order_rcptid_' . $packageDetail->unique_key]);
            return response()->json([
                'status' => 200,
                'message' => "razorpay Detail",
                'data' => $requestData,
                'sig'=>$sig
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }*/
        DB::beginTransaction();
        try {
            //Input items of form
            //get API Configuration
            $requestData = $request->all();
            $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
            //Fetch payment information by razorpay_payment_id
            $payment = $api->payment->fetch($requestData['razorpay_payment_id']);
            if (count($requestData) && !empty($requestData['razorpay_payment_id'])) {
                try {
                    $response = $api->payment->fetch($requestData['razorpay_payment_id'])->capture(array('amount' => $payment['amount']))->toArray();
                } catch (\Exception $e) {
                    return response()->json([
                        'status' => 100,
                        'message' => $e->getMessage(),
                        'data' => null
                    ]);
                }
                $package_subscription_detail = PackageSubscription::where('razorpay_order_id', $response['order_id'])->first();

                // Do something here for store payment details in database...
                $data = ['razorpay_payment_id' => $response['id'], 'razorpay_order_id' => $response['order_id'],
                    'payment_method' => $response['method'],
                    'payment_status' => 1,
                    'payment_gateway_response' => json_encode($response)];

                $package_subscription_detail->update($data);
                $responseData = $this->packagesRepository->successPackageSubscription($package_subscription_detail->id, $requestData);
                if ($responseData['status']) {
                    DB::commit();
                    return response()->json([
                        'status' => 200,
                        'data' => ['order_id' => $package_subscription_detail['id']],
                        'message' => "Payment Successfully"
                    ]);
                } else {
                    DB::rollBack();
                    return response()->json([
                        'status' => 100,
                        'data' => null,
                        'message' => $responseData['message']
                    ]);
                }
            }
            DB::rollBack();
            return response()->json([
                'status' => 100,
                'data' => null,
                'message' => "Invalid Payment ID"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }
}
