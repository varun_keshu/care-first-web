<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\BookingRepositoryInterface;
use App\Interfaces\Api\ConversationRepositoryInterface;
use App\Interfaces\ContactRequestRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    protected $bookingRepository;
    protected $conversationRepository;
    protected $contactRequestRepository;

    public function __construct(BookingRepositoryInterface $bookingRepository, ConversationRepositoryInterface $conversationRepository,
                                ContactRequestRepositoryInterface $contactRequestRepository)
    {
        $this->bookingRepository = $bookingRepository;
        $this->conversationRepository = $conversationRepository;
        $this->contactRequestRepository = $contactRequestRepository;
    }

    public function bookServices(Request $request)
    {
        try {
            $response = $this->bookingRepository->createBooking($request);
            if ($response['status']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Booking Successfully!",
                    'data' => null
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getBookingsListForCustomer(Request $request)
    {
        try {
            $data = $this->bookingRepository->getBookingsListForCustomer($request);
            return response()->json([
                'status' => 200,
                'message' => "Booking List Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function generateServiceIssue(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->bookingRepository->generateServiceIssue($request);
            $booking = $this->bookingRepository->getBookingDetailsForUsers($request);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "We will resolve your issue.",
                'data' => $booking
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function updateBookingRatings(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->bookingRepository->updateBookingRatings($request);
            $data = $this->bookingRepository->getBookingsListForCustomer($request);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "Booking rating stored successfully!.",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function getVendorsListByService(Request $request)
    {
        try {
            $vendors = $this->bookingRepository->getVendorsListByService($request);
            return response()->json([
                'status' => 200,
                'message' => "Vendor list sent successfully!",
                'data' => $vendors
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function createBookingByManager(Request $request)
    {
        try {
            $response = $this->bookingRepository->createBookingByManager($request);
            if ($response['status']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Booking created successfully!",
                    'data' => $request->all()
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function getBookingsListForUsers(Request $request)
    {
        try {
            $bookings = $this->bookingRepository->getBookingsListForUsers($request);
            return response()->json([
                'status' => 200,
                'message' => "Booking List Sent Successfully!",
                'data' => $bookings
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function getBookingReports(Request $request)
    {
        try {
            $data = $this->bookingRepository->getBookingReports($request);
            return response()->json([
                'status' => 200,
                'message' => "Reports List Sent Successfully!",
                'data' => $data,
                'count' => count($data)
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function getBookingDetailsForUsers(Request $request)
    {
        try {
            $booking = $this->bookingRepository->getBookingDetailsForUsers($request);
            return response()->json([
                'status' => 200,
                'message' => "Booking Details Sent Successfully!",
                'data' => $booking
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function assignVendorForBookingService(Request $request)
    {
        try {
            $response = $this->bookingRepository->checkAnyVendorAcceptThisServiceBooking($request);
            if ($response) {
                return response()->json([
                    'status' => 100,
                    'message' => "Sorry! Other vendor already accepted this booking service.",
                    'data' => null
                ]);
            }
            $data = $this->bookingRepository->assignVendorForBookingService($request);
            if ($data['status']) {
                $booking = $this->bookingRepository->getBookingDetailsForUsers($request);
                return response()->json([
                    'status' => 200,
                    'message' => "Booking Assign Successfully!",
                    'data' => $booking
                ]);
            } else {
                return ['status' => 100, 'message' => $data['message'], 'data' => null];
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function serviceStartByVendor(Request $request)
    {
        try {
            $response = $this->bookingRepository->serviceStartByVendor($request);
            if ($response['status']) {
                $booking = $this->bookingRepository->getBookingDetailsForUsers($request);
                return response()->json([
                    'status' => 200,
                    'message' => "Service Start Successfully!",
                    'data' => $booking
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function sendServiceCompletedOTP(Request $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->bookingRepository->sendServiceCompleteOTP($request);
            if ($response['status']) {
                DB::commit();
                return response()->json([
                    'status' => 200,
                    'message' => "Service Completed Sent OTP Successfully!",
                    'data' => ['otp_code' => $response['otp_code']]
                ]);
            } else {
                DB::rollBack();
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function confirmServiceCompletedOTP(Request $request)
    {
        try {
            $response = $this->bookingRepository->confirmServiceCompletedOTP($request);
            if ($response['status']) {
                $booking = $this->bookingRepository->getBookingDetailsForUsers($request);
                return response()->json([
                    'status' => 200,
                    'message' => "Service Completed Successfully!",
                    'data' => $booking
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function cancelBookingService(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->bookingRepository->cancelBookingService($request);
            $booking = $this->bookingRepository->getBookingDetailsForUsers($request);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "Booking Cancelled Successfully!",
                'data' => $booking
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function getIssuedBookingsListByUser(Request $request)
    {
        try {
            $data = $this->bookingRepository->getIssuedBookingsListByUser($request);
            return response()->json([
                'status' => 200,
                'message' => "Issued Booking List Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function approveIssueRequest(Request $request)
    {
        try {
            $response = $this->bookingRepository->approveIssueRequest($request);
            if ($response['status']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Issued Request Approved Successfully!",
                    'data' => null
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function rejectIssueRequest(Request $request)
    {
        try {
            $response = $this->bookingRepository->rejectIssueRequest($request);
            if ($response['status']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Issued Request rejected Successfully!",
                    'data' => null
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function convertQuoteRequestToBooking(Request $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->bookingRepository->convertQuoteRequestToBooking($request);
            if ($response['status']) {
                $contact_request_id = $this->conversationRepository->cancelConversation($request);
                $this->contactRequestRepository->deleteContactRequest($contact_request_id);
                $data = $this->conversationRepository->getConversationList($request);
                DB::commit();
                return response()->json([
                    'status' => 200,
                    'message' => "Convert Contact Request To Booking Successfully!",
                    'data' => $data
                ]);
            } else {
                DB::rollBack();
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function getBookingPaymentHistoryForUsers(Request $request)
    {
        try {
            $data = $this->bookingRepository->getBookingPaymentHistoryForUsers($request);
            return response()->json([
                'status' => 200,
                'message' => "Payment List Sent Successfully!",
                'data' => $data
            ]);

        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function getBookingPayoutHistoryForUsers(Request $request)
    {
        try {
            $data = $this->bookingRepository->getBookingPayoutHistoryForUsers($request);
            return response()->json([
                'status' => 200,
                'message' => "Payout List Sent Successfully!",
                'data' => $data
            ]);

        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function bookingPayoutCompleted(Request $request)
    {
        try {
            $this->bookingRepository->bookingPayoutCompleted($request);
            return response()->json([
                'status' => 200,
                'message' => "Payout Completed Successfully!",
                'data' => null
            ]);

        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }
}
