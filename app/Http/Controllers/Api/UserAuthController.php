<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserAuthController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function sendOtp(Request $request)
    {
        try {
            $is_valid = $this->userRepository->validateContactNumber($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                $errors = $is_valid['result']->errors();
                return response()->json([
                    'status' => 100,
                    'message' => implode(",", $errors->all()),
                    'data' => NULL,
                ]);
            }
            $response = $this->userRepository->sendOtp($request);
            if ($response['success']) {
                return response()->json([
                    'status' => 200,
                    'message' => "Otp Sent Successfully!",
                    'data' => $response['data']
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function verifyOtp(Request $request)
    {
        try {
            $response = $this->userRepository->verifyOtp($request);
            if ($response['success']) {
                $response['data']['user_details'] = $this->userRepository->getUserDetailById($response['user_id']);
                return response()->json([
                    'status' => 200,
                    'message' => "Otp Verify Successfully!",
                    'data' => $response['data']
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => $response['message'],
                    'data' => null
                ]);
            }
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
