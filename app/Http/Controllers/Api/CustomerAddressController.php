<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\CustomerAddressRepositoryInterface;
use Illuminate\Http\Request;

class CustomerAddressController extends Controller
{
    protected $customerAddressRepository;

    public function __construct(CustomerAddressRepositoryInterface $customerAddressRepository)
    {
        $this->customerAddressRepository = $customerAddressRepository;
    }

    public function index(Request $request)
    {
        try {
            $data = $this->customerAddressRepository->getAddressList($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Address List Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function storeAddress(Request $request)
    {
        try {
            $is_valid = $this->customerAddressRepository->validateAddress($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                $errors = $is_valid['result']->errors();
                return response()->json(['status' => 100,
                    'message' => implode(",", $errors->all()),
                    'data' => null,
                ]);
            }
            $this->customerAddressRepository->storeAddress($request);
            $data = $this->customerAddressRepository->getAddressList($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Address Stored Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function getAddressDetail(Request $request)
    {
        try {
            $address_detail = $this->customerAddressRepository->getAddressDetail($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Address Detail Sent Successfully!",
                'data' => $address_detail
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function updateAddress(Request $request)
    {
        try {
            $is_valid = $this->customerAddressRepository->validateAddress($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                $errors = $is_valid['result']->errors();
                return response()->json(['status' => 100,
                    'message' => implode(",", $errors->all()),
                    'data' => null,
                ]);
            }
            $this->customerAddressRepository->updateAddress($request);
            $data = $this->customerAddressRepository->getAddressList($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Address Updated Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }

    public function deleteAddress(Request $request)
    {
        try {
            $this->customerAddressRepository->deleteAddress($request);
            $data = $this->customerAddressRepository->getAddressList($request);
            return response()->json([
                'status' => 200,
                'message' => "Customer Address Deleted Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
    }
}
