<?php

namespace App\Http\Controllers\AppViews;

use App\Models\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Razorpay\Api\Api;

class PaymentController extends Controller
{
    public function __construct()
    {
    }

    public function dopayment(Request $request)
    {
        //Input items of form
        $requestData = $request->all();
        //get API Configuration
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($requestData['razorpay_payment_id']);
        if (!empty($payment)) {
            $booking = Booking::where('razorpay_order_id', $payment['order_id'])->first();
            // Do something here for store payment details in database...
            if (isset($booking)){
                $data = ['razorpay_payment_id' => $requestData['razorpay_payment_id'], 'razorpay_order_id' => $payment['order_id'],
                    'payment_method' => $payment['method'],
                    'payment_status' => 1,
                    'payment_gateway_response' => json_encode($payment)];
                $booking->update($data);
            }
        }
        return redirect()->route('app.payment.success');
    }

    public function paymentSuccess(Request $request)
    {
        dd("Payment Success");
    }
}
