<?php

namespace App\Http\Controllers\AppViews;

use App\Interfaces\Api\ConversationRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{
    protected $conversationRepository;
    public function __construct(ConversationRepositoryInterface $conversationRepository)
    {
        $this->conversationRepository = $conversationRepository;
    }

    public function getMessagesByConversation(Request $request)
    {
        try {
            $requestData = $request->all();
            if (!isset($requestData['conversation_id'])) {
                return response()->json([
                    'status' => 100,
                    'header_message' => "Error!",
                    'message' => "Conversation Id not Found!",
                    'data' => null
                ]);
            }
            $user_type = $requestData['login_user_type'];
            $conversation = $this->conversationRepository->getConversationListById($requestData);
            return view('app.user_messages', compact('conversation', 'user_type'));
        } catch (\Exception $e) {
            return ['status' => 100, 'message' => $e->getMessage()];
        }
    }

    public function checkNewMessage(Request $request)
    {
        $response = $this->conversationRepository->checkNewMessageExistOrNot($request);
        if ($response['status']) {
            $newMessageByUser = "receiver";
            $messageData = $response['data'];
            $returnHtml = view('app.partials.new_message', compact('messageData', 'newMessageByUser'))->render();
        } else {
            $returnHtml = '';
        }
        return response()->json(['status' => true, 'html' => $returnHtml, 'last_message_id' => isset($messageData) ? $messageData['id'] : null]);
    }

    public function sendMessage(Request $request)
    {
        $messageData = $this->conversationRepository->sendMessage($request);
        $newMessageByUser = "sender";
        $returnHtml = view('app.partials.new_message', compact('messageData', 'newMessageByUser'))->render();
        return response()->json(['status' => true, 'html' => $returnHtml, 'message_id' => $messageData->id]);
    }
}
