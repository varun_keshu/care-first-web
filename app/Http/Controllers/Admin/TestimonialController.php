<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\InsuranceRepositoryInterface;
use App\Interfaces\Admin\TestimonialRepositoryInterface;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    protected $testimonialRepository;

    public function __construct(TestimonialRepositoryInterface $testimonialRepository)
    {
        $this->testimonialRepository = $testimonialRepository;
    }

    public function index()
    {
        $testimonials = $this->testimonialRepository->getAllTestimonials();
        return view('admin.testimonials.index', compact('testimonials'));
    }

    public function create()
    {
        return view('admin.testimonials.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => "required",
            'image' => "required",
        ]);
        $this->testimonialRepository->storeTestimonial($request);
        return redirect()->route('testimonials')->with('success', "Testimonial Created Successfully!");
    }

    public function edit($id)
    {
        $testimonial = $this->testimonialRepository->getTestimonialDetail($id);
        if (isset($testimonial)) {
            return view('admin.testimonials.edit', compact('testimonial'));
        }
        return redirect()->back()->with('error', "Invalid Request!");
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => "required",
        ]);
        $this->testimonialRepository->updateTestimonial($request, $id);
        return redirect()->route('testimonials')->with('success', "Testimonial Updated Successfully!");
    }

    public function delete($id)
    {
        $this->testimonialRepository->deleteTestimonial($id);
        return redirect()->route('testimonials')->with('success', "Testimonial Deleted Successfully!");
    }
}
