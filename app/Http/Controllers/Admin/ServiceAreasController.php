<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ServiceAreaRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceAreasController extends Controller
{
    protected $serviceAreaRepository;

    public function __construct(ServiceAreaRepositoryInterface $serviceAreaRepository)
    {
        $this->serviceAreaRepository = $serviceAreaRepository;
    }

    public function index()
    {
        $cities = $this->serviceAreaRepository->getAllCitiesWithArea();
        return view('admin.service_areas.index', compact('cities'));
    }

    public function create()
    {
        return view('admin.service_areas.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => "required",
            'areas.*.title' => 'required|string',
            'areas.*.zip_code' => 'digits:6'
        ], [
            'areas.*.zip_code.digits' => 'Area Zip Code must be 6 digits.'
        ]);
        DB::beginTransaction();
        try {
            $this->serviceAreaRepository->storeServiceAreasData($request);
            DB::commit();
            return redirect()->route('service_areas')->with('success', "Data Created Successfully!");
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function edit($id)
    {
        $city = $this->serviceAreaRepository->getCityDetail($id);
        return view('admin.service_areas.edit', compact('city'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => "required",
            'areas.*.title' => 'required|string',
            'areas.*.zip_code' => 'digits:6',
            'exist_areas.*.title' => 'required|string',
            'exist_areas.*.zip_code' => 'digits:6',
        ], [
            'areas.*.zip_code.digits' => 'Area Zip Code must be 6 digits.',
            'exist_areas.*.zip_code.digits' => 'Area Zip Code must be 6 digits.'
        ]);

        DB::beginTransaction();
        try {
            $this->serviceAreaRepository->updateServiceAreasData($request, $id);
            DB::commit();
            return redirect()->route('service_areas')->with('success', "Data Updated Successfully!");
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function delete($id)
    {
        $this->serviceAreaRepository->deleteCity($id);
        return redirect()->route('service_areas')->with('success', "Data Deleted Successfully!");
    }
}
