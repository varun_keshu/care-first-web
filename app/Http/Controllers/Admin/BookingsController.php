<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\BookingsRepositoryInterface;
use App\Interfaces\Admin\CustomerRepositoryInterface;
use App\Interfaces\Admin\ServiceRepositoryInterface;
use App\Interfaces\Admin\ServiceStatusRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class BookingsController extends Controller
{
    protected $bookingsRepository;
    protected $customerRepository;
    protected $serviceRepository;
    protected $serviceStatusRepository;

    public function __construct(BookingsRepositoryInterface $bookingsRepository, CustomerRepositoryInterface $customerRepository,
                                ServiceRepositoryInterface $serviceRepository, ServiceStatusRepositoryInterface $serviceStatusRepository)
    {
        $this->bookingsRepository = $bookingsRepository;
        $this->customerRepository = $customerRepository;
        $this->serviceRepository = $serviceRepository;
        $this->serviceStatusRepository = $serviceStatusRepository;
    }

    public function index(Request $request)
    {
        $selectedStatuses = [];
        if ($request->service_state == 'open') {
            $selectedStatuses = [1, 2, 3, 5];
        } else if ($request->service_state == 'completed') {
            $selectedStatuses = [6];
        } else if ($request->service_state == 'cancelled') {
            $selectedStatuses = [4];
        } else if ($request->service_state == 'issue') {
            $selectedStatuses = [7, 8, 9, 10];
        }
        $status_list = $this->serviceStatusRepository->getStatusList();
        return view('admin.bookings.index', compact('status_list', 'selectedStatuses'));
    }

    public function getListAjaxData(Request $request)
    {
        $bookings = $this->bookingsRepository->getAllBookings($request);
        return $bookings;
    }

    public function create()
    {
        $customers = $this->customerRepository->getCustomersList();
        $services = $this->serviceRepository->getAllPluckIdName();
        return view('admin.bookings.create', compact('customers', 'services'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_id' => "required",
            'manager_id' => "required",
            'service_id' => "required",
            'customer_address_id' => "required",
            'price' => "required",
            'service_date' => "required",
            'service_time' => "required",
        ], [
            'customer_id.required' => "Please select customer.",
            'manager_id.required' => "Please select manager.",
            'service_id.required' => "Please select service.",
            'customer_address_id.required' => "Please select customer address.",
        ]);
        $response = $this->bookingsRepository->createOrder($request);
        if ($response['status']) {
            return redirect()->route('bookings')->with('success', "Booking Created Successfully!");
        }
        return redirect()->back()->with('error', $response['message']);
    }

    public function getAvailableVendorsList($booking_id)
    {
        $response = $this->bookingsRepository->getAvailableVendorsList($booking_id);
        if ($response['status']) {
            $vendors = $response['data'];
            return view('admin.bookings.vendor_list', compact('vendors', 'booking_id'));
        } else {
            return redirect()->route('bookings')->with('error', $response['message']);
        }
    }

    public function assignBookingToVendor(Request $request, $booking_id)
    {
        $this->bookingsRepository->assignBookingToVendor($request, $booking_id);
        return redirect()->route('bookings')->with('success', "vendor assign successfully!");
    }

    public function getBookingDetails($booking_id)
    {
        $booking = $this->bookingsRepository->getBookingDetails($booking_id);
        if (isset($booking)) {
            $status_history = $this->bookingsRepository->getBookingStatusHistory($booking);
            $previous_url = URL::previous();
            return view('admin.bookings.detail', compact('booking', 'previous_url', 'status_history'));
        }
        return redirect()->back()->with('error', "Invalid Request!");
    }
}
