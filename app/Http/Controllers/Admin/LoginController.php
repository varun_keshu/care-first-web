<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        if (auth()->guard('admins')->check()) {
            return redirect()->route('dashboard');
        }
        return view('admin.auth.login');
    }

    public function authenticateAdmin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        if (auth()->guard('admins')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->back()->with('error', "Invalid Credentials!");
        }
    }

    public function logout()
    {
        Auth::guard('admins')->logout();
        return redirect()->route('login');
    }
}
