<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\PackageSubscriptionInterface;

class PackageSubscriptionsController extends Controller
{
    protected $packageSubscriptionRepository;

    public function __construct(PackageSubscriptionInterface $packageSubscriptionRepository)
    {
        $this->packageSubscriptionRepository = $packageSubscriptionRepository;
    }

    public function index()
    {
        $packageSubscriptions = $this->packageSubscriptionRepository->getAll();
        return view('admin.package_subscriptions.index', compact('packageSubscriptions'));
    }

    public function show($id)
    {
        $packageSubscription = $this->packageSubscriptionRepository->getPackageSubscriptionDetail($id);
        return view('admin.package_subscriptions.show', compact('packageSubscription'));
    }

    public function delete($id)
    {
        $this->packageSubscriptionRepository->delete($id);
        return redirect()->route('package')->with('success', "Data Deleted Successfully!");
    }
}
