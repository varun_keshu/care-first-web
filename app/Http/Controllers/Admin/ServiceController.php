<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ServiceCategoryRepositoryInterface;
use App\Interfaces\Admin\ServiceRepositoryInterface;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    protected $serviceCategoryRepository;
    protected $serviceRepository;
    public function __construct(ServiceCategoryRepositoryInterface $serviceCategoryRepository, ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceCategoryRepository = $serviceCategoryRepository;
        $this->serviceRepository = $serviceRepository;
    }

    public function index()
    {
        $services = $this->serviceRepository->getAllServices();
        return view('admin.services.index', compact('services'));
    }

    public function create()
    {
        $service_categories = $this->serviceCategoryRepository->getAllServiceCategoriesTitles();
        return view('admin.services.create', compact('service_categories'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
             'title' => "required",
             'image' => "required",
             'service_category_id' => "required",
        ], [
            'service_category_id.required' => "Please select service category!"
        ]);

        $this->serviceRepository->storeServiceData($request);
        return redirect()->route('services')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $service = $this->serviceRepository->getServiceDetail($id);
        $service_categories = $this->serviceCategoryRepository->getAllServiceCategoriesTitles();
        return view('admin.services.edit', compact('service', 'service_categories'));
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => "required",
            'service_category_id' => "required",
        ], [
            'service_category_id.required' => "Please select service category!"
        ]);
        $this->serviceRepository->updateServiceData($request, $id);

        return redirect()->route('services')->with('success', "Data Updated Successfully!");

    }

    public function delete($id)
    {
        $this->serviceRepository->deleteServiceData($id);
        return redirect()->route('services')->with('success', "Data Deleted Successfully!");
    }

    public function getServiceDetails($service_id)
    {
        $service = $this->serviceRepository->getServiceDetail($service_id);
        $returnHtml = view('admin.bookings.ajax.service_price_details', compact('service'))->render();
        return response()->json(['success' => true, "html" => $returnHtml]);
    }
}
