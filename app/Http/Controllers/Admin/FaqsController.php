<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\FaqRepositoryInterface;
use App\Interfaces\Admin\PageRepositoryInterface;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    protected $faqRepository;
    public function __construct(FaqRepositoryInterface $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    public function index()
    {
        $faqs = $this->faqRepository->getAllFaqs();
        return view('admin.faqs.index', compact('faqs'));
    }

    public function create()
    {
        return view('admin.faqs.create');
    }

    public function store(Request $request)
    {
        $this->faqRepository->storeFaqData($request);
        return redirect()->route('faqs')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $faq = $this->faqRepository->getFaqDetail($id);
        return view('admin.faqs.edit', compact('faq'));
    }

    public function update(Request $request, $id)
    {
        $this->faqRepository->updateFaqData($request, $id);
        return redirect()->route('faqs')->with('success', "Data Updated Successfully!");
    }

    public function delete($id)
    {
        $this->faqRepository->deleteFaqData($id);
        return redirect()->route('faqs')->with('success', "Data Deleted Successfully!");
    }

    public function getSortOrderForm()
    {
        return view('admin.faqs.sort_order');
    }

    public function getFaqsByCategory($category)
    {
        $faqs = $this->faqRepository->getFaqsByCategory($category);
        $returnHtml = view('admin.faqs.ajax.faqs', compact('faqs'))->render();
        return response()->json(['success' => true, 'html' => $returnHtml]);
    }

    public function updateFaqOrder(Request $request)
    {
        $this->faqRepository->updateFaqOrder($request);
        return response()->json(['success' => true, 'message' => "Order Updated Successfully"]);
    }

    public function getFrontFaqsList()
    {
        $faqs_list = $this->faqRepository->getAllFaqsByCategory();
        return view('front.faqs', compact('faqs_list'));
    }
}
