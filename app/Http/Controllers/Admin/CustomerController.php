<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\CustomerRepositoryInterface;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    protected $customerRepository;

    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function index()
    {
        $customers = $this->customerRepository->getAllCustomers();
        return view('admin.customers.index', compact('customers'));
    }

    public function getCustomerDetails($id)
    {
        $customer = $this->customerRepository->getCustomerDetail($id);
        return view('admin.customers.detail', compact('customer'));
    }

    public function getMessagesList(Request $request)
    {
        $conversation = $this->customerRepository->getCustomerConversationDetail($request);
        $returnHtml = view('admin.customers.ajax.messages', compact('conversation'))->render();
        return response()->json(['success' => true, 'html' => $returnHtml]);
    }

    public function sendMessageToCustomer(Request $request)
    {
        $message = $this->customerRepository->sendMessageToCustomer($request);
        $returnHtml = view('admin.customers.ajax.new_message', compact('message'))->render();
        return response()->json(['success' => true, 'html' => $returnHtml]);
    }

    public function getCustomersListBySearch(Request $request)
    {
        $addresses = $this->customerRepository->getCustomersListBySearch($request);
        echo json_encode($addresses);
    }

    public function getAddressList(Request $request)
    {
        $addresses = $this->customerRepository->getAddressListByCustomer($request);
        echo json_encode($addresses);
    }
}
