<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20-11-2020
 * Time: 09:44 AM
 */

namespace App\Http\Controllers\Admin;

use App\Interfaces\Admin\RewardProgramRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ServiceRepositoryInterface;
use Illuminate\Http\Request;

class RewardProgramController extends Controller
{
    protected $rewardProgramRepository;
    protected $serviceRepository;

    public function __construct(RewardProgramRepositoryInterface $rewardProgramRepository, ServiceRepositoryInterface $serviceRepository)
    {
        $this->rewardProgramRepository = $rewardProgramRepository;
        $this->serviceRepository = $serviceRepository;
    }

    public function index()
    {
        $reward_programs = $this->rewardProgramRepository->getAllRewardProgramsList();
        return view('admin.reward_programs.index', compact('reward_programs'));
    }

    public function create()
    {
        $services = $this->serviceRepository->getAllPluckIdName();
        return view('admin.reward_programs.create', compact('services'));
    }

    public function store(Request $request)
    {
        $this->rewardProgramRepository->storeRewardProgram($request);
        return redirect()->route('reward_program')->with('success', "Data created successfully!");
    }

    public function edit($id)
    {
        $services = $this->serviceRepository->getAllPluckIdName();
        $reward_program = $this->rewardProgramRepository->getRewardProgramDetail($id);
        return view('admin.reward_programs.edit', compact('services', 'reward_program'));
    }

    public function update($request, $id)
    {
        $this->rewardProgramRepository->updateRewardProgram($request, $id);
        return redirect()->route('reward_program')->with('success', "Data updated successfully!");
    }

    public function delete($id)
    {
        $this->rewardProgramRepository->deleteRewardProgram($id);
        return redirect()->route('reward_program')->with('success', "Data deleted successfully!");
    }
}
