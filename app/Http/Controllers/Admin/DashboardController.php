<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Interfaces\Admin\BookingsRepositoryInterface;
use App\Interfaces\Admin\VendorRepositoryInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class DashboardController extends Controller
{

    protected $vendorRepository;
    protected $bookingsRepository;

    public function __construct(VendorRepositoryInterface $vendorRepository, BookingsRepositoryInterface $bookingsRepository)
    {
        $this->vendorRepository = $vendorRepository;
        $this->bookingsRepository = $bookingsRepository;

    }

    public function index()
    {
        $booking = $this->bookingsRepository->getBookedData();
        $vendor_list = $this->bookingsRepository->getCustomerDetails();
        $data = $this->bookingsRepository->getCount();
        $earning_data=$this->bookingsRepository->getEarningDetails();
        return view('admin.dashboard.index', compact('booking', 'vendor_list', 'data' ,'earning_data'));
    }


}
