<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ServiceStatusRepositoryInterface;
use Illuminate\Http\Request;

class ServiceStatusController extends Controller
{
    protected $serviceStatusRepository;

    public function __construct(ServiceStatusRepositoryInterface $serviceStatusRepository)
    {
        $this->serviceStatusRepository = $serviceStatusRepository;
    }

    public function index()
    {
        $service_statuses = $this->serviceStatusRepository->getAllServiceStatusList();
        return view('admin.service_status.index', compact('service_statuses'));
    }

    public function create()
    {
        return view('admin.service_status.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => "required|unique:service_status,title",
        ]);
        $this->serviceStatusRepository->storeServiceStatus($request);
        return redirect()->route('service_status')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $service_status = $this->serviceStatusRepository->getServiceStatusDetail($id);
        return view('admin.service_status.edit', compact('service_status'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => "required"
        ]);
        $this->serviceStatusRepository->updateStatusCategory($request, $id);
        return redirect()->route('service_status')->with('success', "Data Updated Successfully!");
    }

    public function delete($id)
    {
        $this->serviceStatusRepository->deleteStatusCategory($id);
        return redirect()->route('service_status')->with('success', "Data Deleted Successfully!");
    }
}
