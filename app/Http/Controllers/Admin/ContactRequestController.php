<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20-11-2020
 * Time: 09:44 AM
 */

namespace App\Http\Controllers\Admin;
 use App\Interfaces\ContactRequestRepositoryInterface;
 use App\Models\ContactRequest;
 use Yajra\DataTables\Utilities\Request;
 use App\Http\Controllers\Controller;
 use Illuminate\Support\Facades\URL;
 use Illuminate\Http\File;

class ContactRequestController extends Controller
{

    protected $contactRequestRepository;
    public function __construct(ContactRequestRepositoryInterface $contactRequestRepository)
    {
        $this->contactRequestRepository = $contactRequestRepository;
    }
    public function index()
    {
        return view('admin.contact_request.index');
    }
    public function getListAjaxData(Request $request)
    {
        $contact=$this->contactRequestRepository->getListAjaxData($request);
        return $contact;
    }
    public function getContactDetails($contact_request_id)
    {
        $contact_request = $this->contactRequestRepository->getContactDetails($contact_request_id);
        if (isset($contact_request)){
            $previous_url = URL::previous();

            return view('admin.contact_request.detail', compact('contact_request', 'previous_url'));

        }
        return redirect()->back()->with('error', "Invalid Request!");
    }

}
