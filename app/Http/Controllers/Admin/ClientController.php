<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07-01-2021
 * Time: 11:37 AM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ClientRepositoryInterface;
use Illuminate\Http\Request;


class ClientController  extends Controller
{
    protected $clientRepository;

    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository =$clientRepository;
    }

    public function index()
    {
        $clients = $this->clientRepository->getAllClients();
        return view('admin.clients.index', compact('clients'));

    }

    public function create()
    {
        return view('admin.clients.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => "required",
            'image' => "required",
        ]);
        $this->clientRepository->storeClient($request);
        return redirect()->route('clients')->with('success', "Client Created Successfully!");
    }

    public function edit($id)
    {
        $client = $this->clientRepository->getClientDetail($id);
        if (isset($client)) {
            return view('admin.clients.edit', compact('client'));
        }
        return redirect()->back()->with('error', "Invalid Request!");
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => "required",
        ]);
        $this->clientRepository->updateClient($request, $id);
        return redirect()->route('clients')->with('success', "Client Updated Successfully!");
    }

    public function delete($id)
    {
        $this->clientRepository->deleteClient($id);
        return redirect()->route('clients')->with('success', "Client Deleted Successfully!");
    }


}
