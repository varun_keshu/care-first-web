<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ServiceCategoryRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceCategoryController extends Controller
{
    protected $serviceCategoryRepository;
    public function __construct(ServiceCategoryRepositoryInterface $serviceCategoryRepository)
    {
        $this->serviceCategoryRepository = $serviceCategoryRepository;
    }

    public function index()
    {
        $service_categories = $this->serviceCategoryRepository->getAllServiceCategories();
        return view('admin.service_categories.index', compact('service_categories'));
    }

    public function create()
    {
        return view('admin.service_categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => "required",
            'image' => "required",
        ]);
        DB::beginTransaction();
        try {
            $this->serviceCategoryRepository->storeServiceCategory($request);
            DB::commit();
            return redirect()->route('service_categories')->with('success', "Data Stored Successfully!");
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function edit($id)
    {
        $service_category = $this->serviceCategoryRepository->getServiceCategoryDetail($id);
        return view('admin.service_categories.edit', compact('service_category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => "required",
        ]);
        DB::beginTransaction();
        try {
            $this->serviceCategoryRepository->updateServiceCategory($request, $id);
            DB::commit();
            return redirect()->route('service_categories')->with('success', "Data Updated Successfully!");
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function delete($id)
    {
        $this->serviceCategoryRepository->deleteServiceCategory($id);
        return redirect()->route('service_categories')->with('success', "Data Deleted Successfully!");
    }
}
