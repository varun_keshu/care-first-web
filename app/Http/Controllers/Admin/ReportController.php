<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-12-2020
 * Time: 09:45 AM
 */

namespace App\Http\Controllers\Admin;

use App\Interfaces\Admin\BookingsRepositoryInterface;
use Illuminate\Http\Request;

class ReportController
{
    protected $bookingsRepository;

    public function __construct(BookingsRepositoryInterface $bookingsRepository)
    {
        $this->bookingsRepository = $bookingsRepository;
    }

    public function getBookingListByVendor()
    {
        $vendor_list = $this->bookingsRepository->getVendorList();
        return view('admin.reports.index', compact('vendor_list'));
    }

    public function getListVendorData(Request $request)
    {
        $service = $this->bookingsRepository->getVendorBookings($request);
        return $service;
    }

    public function getBookingListByService()
    {
        $service_list = $this->bookingsRepository->getServiceList();
        return view('admin.reports.service', compact('service_list'));
    }

    public function getListServiceData(Request $request)
    {
        $service = $this->bookingsRepository->getServiceBookings($request);
        return $service;
    }

    public function getBookingByCategory()
    {
        $category_list = $this->bookingsRepository->getCategoryList();
        return view('admin.reports.category', compact('category_list'));
    }

    public function getListCategoryData(Request $request)
    {
        $category = $this->bookingsRepository->getCategoryBookings($request);
        return $category;
    }

    public function getPayoutReports()
    {
        return view('admin.reports.payout_history');
    }

    public function getPayoutReportsAjax(Request $request)
    {
        $payment = $this->bookingsRepository->getPayoutReportsAjax($request);
        return $payment;
    }

}
