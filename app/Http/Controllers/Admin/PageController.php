<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\PageRepositoryInterface;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected $pageRepository;
    public function __construct(PageRepositoryInterface $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    public function index()
    {
        $pages = $this->pageRepository->getAllPages();
        return view('admin.pages.index', compact('pages'));
    }

    public function create()
    {
        $page_types = $this->pageRepository->getPageTypes();
        return view('admin.pages.create', compact('page_types'));
    }

    public function store(Request $request)
    {
        $this->pageRepository->storePageData($request);
        return redirect()->route('pages')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $page = $this->pageRepository->getPageDetail($id);
        $page_types = $this->pageRepository->getPageTypes($id);
        return view('admin.pages.edit', compact('page', 'page_types'));
    }

    public function update(Request $request, $id)
    {
        $this->pageRepository->updatePageData($request, $id);
        return redirect()->route('pages')->with('success', "Data Updated Successfully!");
    }

    public function delete($id)
    {
        $this->pageRepository->deletePageData($id);
        return redirect()->route('pages')->with('success', "Data Deleted Successfully!");
    }

    public function getPageDetailByType($page_type = null)
    {
        $page = str_replace("-", "_", $page_type);
        $page = $this->pageRepository->getPageDetailByType($page);
        if (isset($page)){
            return view('front.page_details', compact('page'));
        }else{
            return redirect()->back();
        }
    }
}
