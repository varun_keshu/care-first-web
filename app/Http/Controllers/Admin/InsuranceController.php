<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\InsuranceRepositoryInterface;
use Illuminate\Http\Request;

class InsuranceController extends Controller
{
    protected $insuranceRepository;

    public function __construct(InsuranceRepositoryInterface $insuranceRepository)
    {
        $this->insuranceRepository = $insuranceRepository;
    }

    public function index()
    {
        $insurances = $this->insuranceRepository->getAllInsurance();
        return view('admin.insurance.index', compact('insurances'));
    }

    public function create()
    {
        return view('admin.insurance.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_type' => "required",
            'insurance_amount' => "required",
            'minimum_bookings_value' => "required",
            'maximum_bookings_value' => "required",
            'insurance_premium_value' => "required",
        ]);
        $this->insuranceRepository->storeInsurance($request);
        return redirect()->route('insurances')->with('success', "Insurance Created Successfully!");
    }

    public function edit($id)
    {
        $insurance = $this->insuranceRepository->getInsuranceDetail($id);
        if (isset($insurance)) {
            return view('admin.insurance.edit', compact('insurance'));
        }
        return redirect()->back()->with('error', "Invalid Request!");
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_type' => "required",
            'insurance_amount' => "required",
            'minimum_bookings_value' => "required",
            'maximum_bookings_value' => "required",
            'insurance_premium_value' => "required",
        ]);
        $this->insuranceRepository->updateInsurance($request, $id);
        return redirect()->route('insurances')->with('success', "Insurance Updated Successfully!");
    }

    public function delete($id)
    {
        $this->insuranceRepository->deleteInsurance($id);
        return redirect()->route('insurances')->with('success', "Insurance Deleted Successfully!");
    }
}
