<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\EmailTemplatesRepositoryInterface;
use Illuminate\Http\Request;

class EmailTemplateController extends Controller
{
    protected $emailTemplatesRepository;
    public function __construct(EmailTemplatesRepositoryInterface $emailTemplatesRepository)
    {
        $this->emailTemplatesRepository = $emailTemplatesRepository;
    }

    public function index()
    {
        $email_templates = $this->emailTemplatesRepository->getAllEmailTemplates();
        return view('admin.email_templates.index', compact('email_templates'));
    }

    public function create()
    {
        $identifiers = $this->emailTemplatesRepository->getEmailIdentifierTitles();
        return view('admin.email_templates.create', compact('identifiers'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => "required",
            'subject' => "required",
            'identifier' => "required",
            'content' => "required",
        ], [
            'identifier.required' => "Please select email identifier!"
        ]);
        $this->emailTemplatesRepository->storeEmailTemplate($request);
        return redirect()->route('email_templates')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $email_template = $this->emailTemplatesRepository->getEmailTemplateDetail($id);
        $identifiers = $this->emailTemplatesRepository->getEmailIdentifierTitles($id);
        return view('admin.email_templates.edit', compact('email_template', 'identifiers'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => "required",
            'subject' => "required",
            'identifier' => "required",
            'content' => "required",
        ], [
            'identifier.required' => "Please select email identifier!"
        ]);
        $this->emailTemplatesRepository->updateEmailTemplate($request, $id);
        return redirect()->route('email_templates')->with('success', "Data Updated Successfully!");
    }

    public function delete($id)
    {
        $this->emailTemplatesRepository->deleteEmailTemplate($id);
        return redirect()->route('email_templates')->with('success', "Data Deleted Successfully!");
    }
}
