<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\PackageRepositoryInterface;
use App\Interfaces\Admin\ServiceRepositoryInterface;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    protected $serviceRepository, $packageRepository;

    public function __construct(ServiceRepositoryInterface $serviceRepository, PackageRepositoryInterface $packageRepository)
    {
        $this->serviceRepository = $serviceRepository;
        $this->packageRepository = $packageRepository;
    }

    public function index()
    {
        $packages = $this->packageRepository->getAllData();
        return view('admin.packages.index', compact('packages'));
    }

    public function create()
    {
        $services = $this->serviceRepository->getAllPluckIdName();
        return view('admin.packages.create', compact('services'));
    }

    public function show($id)
    {
        $services = $this->serviceRepository->getAllPluckIdName();
        $package = $this->packageRepository->getPackageDetail($id);
        return view('admin.packages.show', compact('package', 'services'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => "required|unique:packages,title",
            'description' => 'required|string',
            'price' => 'required|integer|min:0',
            'service_id' => 'required',
            'service_count' => 'required',
            'duration' => 'required',
        ], [
            'service_id.required' => "Please select a service.",
            'service_count.required' => "Please enter the service count.",
            'duration.required' => "Please enter the time duration!",
        ]);
        $this->packageRepository->storePackageData($request);
        return redirect()->route('package')->with('success', "Data Stored Successfully!");

    }

    public function edit($id)
    {
        $services = $this->serviceRepository->getAllPluckIdName();
        $package = $this->packageRepository->getPackageDetail($id);
        return view('admin.packages.edit', compact('package', 'services'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => "required|unique:packages,title" . $id . ',id,deleted_at,NULL',
            'description' => 'required|string',
            'price' => 'required|integer|min:0',
            'service_id' => 'required',
            'service_count' => 'required',
            'duration' => 'required',
        ], [
            'service_id.required' => "Please select a service.",
            'service_count.required' => "Please enter the service count.",
            'duration.required' => "Please enter the time duration!",
        ]);
        $this->packageRepository->updatePackageData($request, $id);
        return redirect()->route('package')->with('success', "Data Updated Successfully!");
    }

    public function delete($id)
    {
        $this->packageRepository->deletePackageData($id);
        return redirect()->route('package')->with('success', "Data Deleted Successfully!");
    }
}
