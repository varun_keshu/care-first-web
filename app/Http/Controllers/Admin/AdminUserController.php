<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RegisterRequest;
use App\Interfaces\Admin\AdminUserInterface;
use App\Models\Admin;
use App\Models\Admins;
use App\Models\PasswordResets;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AdminUserController extends Controller
{
    protected $adminUserRepository;

    public function __construct(AdminUserInterface $adminUserRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
    }

    public function index(Request $request)
    {
        $admins = $this->adminUserRepository->getAllAdmins($request);
        return view('admin.admin-users.index', compact('admins'));
    }

    public function createNewAdmin()
    {
        return view('admin.admin-users.create');
    }

    public function store(RegisterRequest $request)
    {
        $this->adminUserRepository->storeAdmin($request);
        return redirect()->route('admin.admin_users')->with('success', 'New Admin Added Successfully.');
    }

    public function setPasswordLink($token)
    {
        $user = $this->adminUserRepository->getByActivationToken($token);
        is_null($user) ? abort(403, 'Unauthorized action.') : null;
        return view('admin.auth.add-password', compact('user'));
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'activation_token' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        $requestData = $request->all();
        $user = $this->adminUserRepository->getByActivationToken($request->activation_token);
        if (isset($user)) {
            $requestData['status'] = 1;
            $requestData['activation_token'] = null;
            $requestData['activation_date'] = Carbon::today()->toDateString();
            $user->update($requestData);
            Auth::guard('admins')->loginUsingId($user->id);
            return redirect()->route('dashboard');
        }else{
            abort(403, 'Unauthorized action.');
        }
    }

    public function delete($id)
    {
        Admins::destroy($id);
        Auth::guard('admins')->logout();
        return redirect()->route('login')->with('success', 'Account Deleted Successfully');
    }

    public function getProfile()
    {
        $admin = getCurrentAdmin();
        return view('admin.profile.form', compact('admin'));
    }

    public function updateProfile(Request $request)
    {
        $this->adminUserRepository->updateProfile($request);
        return redirect()->back()->with('success', "Profile updated successfully!");
    }
}
