<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ManagerRepositoryInterface;
use App\Interfaces\Admin\ServiceAreaRepositoryInterface;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    protected $managerRepository;
    protected $serviceAreaRepository;

    public function __construct(ManagerRepositoryInterface $managerRepository,
                                ServiceAreaRepositoryInterface $serviceAreaRepository)
    {
        $this->managerRepository = $managerRepository;
        $this->serviceAreaRepository = $serviceAreaRepository;
    }

    public function index()
    {
        $managers = $this->managerRepository->getAllManagers();
        return view('admin.managers.index', compact('managers'));
    }

    public function create()
    {
        $service_areas = $this->serviceAreaRepository->getServiceAreaTitleWithCity();
        return view('admin.managers.create', compact('service_areas'));
    }

    public function getAvailableServicesByArea($service_area_id, Request $request)
    {
        $service_area = $this->serviceAreaRepository->getServiceAreaDetail($service_area_id);
        $service_categories = $this->managerRepository->getAvailableServicesByArea($service_area_id, $request);
        $returnHtml = view('admin.managers.partials.services', compact('service_categories', 'service_area_id', 'service_area'))->render();
        return response()->json(['success' => 200, "html" => $returnHtml]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => "required",
            'contact_number' => "required|regex:/[0-9]{10}/|digits:10|unique:users",
        ]);
        $this->managerRepository->storeManager($request);
        return redirect()->route('managers')->with('success', "Data Created Successfully!");
    }

    public function edit($id)
    {
        $manager = $this->managerRepository->getManagerDetail($id);
        if (isset($manager)) {
            $service_areas = $this->serviceAreaRepository->getServiceAreaTitleWithCity();
            $user_service_categories = $this->managerRepository->getManagerServiceCategoriesDetails($manager);
            return view('admin.managers.edit', compact('service_areas', 'manager', 'user_service_categories'));
        }
        return redirect()->back()->with('error', "Invalid Request!");
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => "required",
            'contact_number' => "required|regex:/[0-9]{10}/|digits:10|unique:users,contact_number,$id",
        ]);
        $this->managerRepository->updateManager($request, $id);
        return redirect()->route('managers')->with('success', "Data Updated Successfully!");
    }

    public function show($id)
    {
        $manager = $this->managerRepository->getManagerDetail($id);
        if (isset($manager)) {
            return view('admin.managers.show', compact('manager'));
        }
        return redirect()->back()->with('error', "Invalid Request!");
    }

    public function getManagersListByService(Request $request)
    {
        $managers = $this->managerRepository->getManagersListByService($request);
        echo json_encode($managers);
    }
}
