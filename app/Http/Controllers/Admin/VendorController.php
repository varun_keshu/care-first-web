<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ManagerRepositoryInterface;
use App\Interfaces\Admin\VendorRepositoryInterface;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    protected $vendorRepository;
    protected $managerRepository;

    public function __construct(VendorRepositoryInterface $vendorRepository, ManagerRepositoryInterface $managerRepository)
    {
        $this->vendorRepository = $vendorRepository;
        $this->managerRepository = $managerRepository;
    }

    public function index()
    {
        $vendors = $this->vendorRepository->getAllVendors();
        return view('admin.vendors.index', compact('vendors'));
    }

    public function create()
    {
        $managers = $this->managerRepository->getAllManagersNameList();
        return view('admin.vendors.create', compact('managers'));
    }

    public function getManagerServiceList($manager_id)
    {
        $manager = $this->managerRepository->getManagerDetail($manager_id);
        $user_service_categories = $this->managerRepository->getManagerServicesListByCategory($manager);
        $returnHtml = view('admin.vendors.partials.services', compact('user_service_categories'))->render();
        return response()->json(['success' => true, 'html' => $returnHtml]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'manager_id' => "required",
            'name' => "required",
            'contact_number' => "required|regex:/[0-9]{10}/|digits:10|unique:users",
                ], [
            'manager_id.required' => "Please select manager."
        ]);
        $response = $this->vendorRepository->storeVendor($request);
        if ($response['status']){
            return redirect()->route('vendors')->with('success', "Data Created Successfully!");
        }else{
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function edit($id)
    {
        $vendor = $this->vendorRepository->getVendorDetail($id);
        if (isset($vendor)) {
            $managers = $this->managerRepository->getAllManagersNameList();
            $user_service_categories = $this->managerRepository->getManagerServicesListByCategory($vendor->manager, $vendor);
            return view('admin.vendors.edit', compact('vendor', 'managers', 'user_service_categories'));
        }
        return redirect()->back()->with('error', "Invalid Request!");
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'manager_id' => "required",
            'name' => "required",
            'contact_number' => "required|regex:/[0-9]{10}/|digits:10|unique:users,contact_number,$id",
        ], [
            'manager_id.required' => "Please select manager."
        ]);
        $response = $this->vendorRepository->updateVendor($request, $id);
        if ($response['status']){
            return redirect()->route('vendors')->with('success', "Data Updated Successfully!");
        }else{
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function show($id)
    {
        $vendor = $this->vendorRepository->getVendorDetail($id);
        if (isset($vendor)) {
            return view('admin.vendors.show', compact('vendor'));
        }
        return redirect()->back()->with('error', "Invalid Request!");
    }

}
