<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ServiceCategoryRepositoryInterface;
use App\Interfaces\Admin\SliderRepositoryInterface;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    protected $sliderRepository;
    protected $serviceCategoryRepository;
    public function __construct(SliderRepositoryInterface $sliderRepository, ServiceCategoryRepositoryInterface $serviceCategoryRepository)
    {
        $this->sliderRepository = $sliderRepository;
        $this->serviceCategoryRepository = $serviceCategoryRepository;
    }

    public function index()
    {
        $sliders = $this->sliderRepository->getAllSliders();
        return view('admin.sliders.index', compact('sliders'));
    }

    public function create()
    {
        $service_categories = $this->serviceCategoryRepository->getAllServiceCategoriesTitles();
        return view('admin.sliders.create', compact('service_categories'));
    }

    public function store(Request $request)
    {
        $this->sliderRepository->storeSliderData($request);
        return redirect()->route('sliders')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $slider = $this->sliderRepository->getSliderDetail($id);
        $service_categories = $this->serviceCategoryRepository->getAllServiceCategoriesTitles();
        return view('admin.sliders.edit', compact('slider', 'service_categories'));
    }

    public function update(Request $request, $id)
    {
        $this->sliderRepository->updateSliderData($request, $id);
        return redirect()->route('sliders')->with('success', "Data Updated Successfully!");
    }

    public function delete($id)
    {
        $this->sliderRepository->deleteSliderData($id);
        return redirect()->route('sliders')->with('success', "Data Deleted Successfully!");
    }
}
