<?php

namespace App\Http\Controllers;

use App\Interfaces\ContactRequestRepositoryInterface;
use Illuminate\Http\Request;

class ContactRequestController extends Controller
{
    protected $contactRequestRepository;
    public function __construct(ContactRequestRepositoryInterface $contactRequestRepository)
    {
        $this->contactRequestRepository = $contactRequestRepository;
    }

    public function storeContactRequest(Request $request)
    {
        $is_valid = $this->contactRequestRepository->validateRequest($request);
        if (!empty($is_valid) && $is_valid['status'] == false) {
            $errors = $is_valid['result']->errors();
            return response()->json(['status' => 100,
                'message' => implode(",", $errors->all()),
                'data' => null,
            ]);
        }
        $response = $this->contactRequestRepository->storeContactRequest($request);
        if ($response['status']){
            return response()->json([
                'status' => 200,
                'message' => "Your estimation request received. We will contact to you through messages.",
                'data' => null,
            ]);
        }else{
            return response()->json([
                'status' => 100,
                'message' => $response['message'],
                'data' => null,
            ]);
        }
    }
}
