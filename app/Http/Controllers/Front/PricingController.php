<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21-12-2020
 * Time: 02:45 PM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\PackageRepositoryInterface;

class PricingController extends Controller
{
    protected $PackageRepository;

    public function __construct(PackageRepositoryInterface $PackageRepository)
    {
        $this->PackageRepository = $PackageRepository;
    }

    public function index()
    {
        $packages = $this->PackageRepository->getAll();
        return view('front.pricing', compact('packages'));
    }
}
