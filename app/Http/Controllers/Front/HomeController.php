<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17-12-2020
 * Time: 04:58 PM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ClientRepositoryInterface;
use App\Interfaces\Admin\PackageRepositoryInterface;
use App\Interfaces\Admin\ServiceCategoryRepositoryInterface;
use App\Interfaces\Admin\TestimonialRepositoryInterface;

class HomeController extends Controller
{
    protected $serviceCategoryRepository;
    protected $testimonialRepository;
    protected $PackageRepository;
    protected $clientRepository;

    public function __construct(ServiceCategoryRepositoryInterface $serviceCategoryRepository, TestimonialRepositoryInterface $testimonialRepository,
                                PackageRepositoryInterface $PackageRepository, ClientRepositoryInterface $clientRepository)
    {
        $this->serviceCategoryRepository = $serviceCategoryRepository;
        $this->testimonialRepository = $testimonialRepository;
        $this->PackageRepository = $PackageRepository;
        $this->clientRepository = $clientRepository;
    }

    public function index()
    {
        $service_categories = $this->serviceCategoryRepository->getAllServiceCategories();
        $testimonials = $this->testimonialRepository->getAllTestimonials();
        $packages = $this->PackageRepository->getAll();
        $clients = $this->clientRepository->getAllClients();
        return view('front.home', compact('service_categories', 'testimonials', 'packages', 'clients'));
    }

    public function getFeaturePageData()
    {
        $testimonials = $this->testimonialRepository->getAllTestimonials();
        return view('front.feature', compact('testimonials'));
    }


}
