<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21-12-2020
 * Time: 11:10 AM
 */

namespace App\Http\Controllers\Front;
use App\Interfaces\ConfigurationRepositoryInterface;
use App\Interfaces\ContactUsRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    protected $contactUsRepository;
    Protected $SiteConfigurations;

    public function __construct(ContactUsRepositoryInterface $contactUsRepository ,ConfigurationRepositoryInterface $SiteConfigurations)
    {
        $this->contactUsRepository = $contactUsRepository;
        $this->SiteConfigurations=$SiteConfigurations;
    }

    public function index()
    {
       $data=$this->SiteConfigurations->getContactData();
        return view('front.contact',compact('data'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => "required|email",
            'message' => "required",

        ], [
            'email.required' => "Please Enter valid Email!",
        ]);
        $this->contactUsRepository->storeContactUsData($request);
        return redirect()->route('contact')->with('success', "Data Send Successfully!");

    }
}
