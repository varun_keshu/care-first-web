<?php

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ServiceCategoryRepositoryInterface;
use App\Interfaces\Admin\SliderRepositoryInterface;

class ServiceController extends Controller
{
    protected $ServiceCategoryRepository;
    protected $sliderRepository;

    public function __construct(ServiceCategoryRepositoryInterface $ServiceCategoryRepository, SliderRepositoryInterface $sliderRepository)
    {
        $this->ServiceCategoryRepository = $ServiceCategoryRepository;
        $this->sliderRepository = $sliderRepository;
    }

    public function index()
    {
        $service_category = $this->ServiceCategoryRepository->getAllServiceCategories();
        $sliders = $this->sliderRepository->getOtherSliderList();
        return view('front.service_categories', compact('service_category', 'sliders'));
    }

    public function getServiceCategoryDetail($id)
    {
        $service_category = $this->ServiceCategoryRepository->getServiceCategoryDetail($id);
        if ($service_category) {
            $sliders = $this->sliderRepository->getSliderListByCategory($id);
            return view('front.services', compact('service_category', 'sliders'));
        } else {
            return back()->withErrors('error', 'Invalid ');
        }
    }
}
