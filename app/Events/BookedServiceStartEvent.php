<?php

namespace App\Events;

use App\Models\Booking;
use Illuminate\Queue\SerializesModels;

class BookedServiceStartEvent
{
    use SerializesModels;

    public $bookingDetails;
    public function __construct(Booking $bookingDetails)
    {
        $this->bookingDetails = $bookingDetails;
    }
}
