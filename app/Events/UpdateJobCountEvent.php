<?php

namespace App\Events;

use App\Models\Booking;
use Illuminate\Queue\SerializesModels;

class UpdateJobCountEvent
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }
}
