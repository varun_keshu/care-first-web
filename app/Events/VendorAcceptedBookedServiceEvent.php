<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class VendorAcceptedBookedServiceEvent
{
    use SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
}
