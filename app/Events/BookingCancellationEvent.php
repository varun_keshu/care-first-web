<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class BookingCancellationEvent
{
    use SerializesModels;

    public $bookingDetails;

    public function __construct($data)
    {
        $this->bookingDetails = $data;
    }
}
