<?php

namespace App\Events;

use App\Models\Message;
use Illuminate\Queue\SerializesModels;

class NewContactRequestEvent
{
    use SerializesModels;

    public $messageData;

    public function __construct(Message $message)
    {
        $this->messageData = $message;
    }
}
