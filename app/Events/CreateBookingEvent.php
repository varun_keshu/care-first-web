<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class CreateBookingEvent
{
    use SerializesModels;

    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }
}
