<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07-02-2019
 * Time: 10:42
 */

namespace App\Services;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class PushNotificationService
{
    public function sendNotification($message, $tokens, $title = null, $addData = [], $user_type = "manager")
    {
        $fcmConfigArray = Config::get('fcm');
        $fcmConfigArray['http']['server_key'] = ($user_type == "customer") ? env('FCM_CUSTOMER_SERVER_KEY') : env('FCM_SERVER_KEY');
        $fcmConfigArray['http']['sender_id'] = ($user_type == "customer") ? env('FCM_CUSTOMER_SENDER_ID') : env('FCM_SENDER_ID');
        $configFileData = var_export($fcmConfigArray, 1);
        if (File::put(config_path('fcm.php'), "<?php\n return $configFileData ;")) {
        }
        Artisan::call('config:clear');

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $title = is_null($title) ? $message : $title;
        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($message)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $addData['a_data'] = "my_data";
        $dataBuilder->addData($addData);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $downstreamResponse = \LaravelFCM\Facades\FCM::sendTo($tokens, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        // return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();

        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
        $response['numberTokensSuccess'] = $downstreamResponse->numberSuccess();
        $response['numberTokensFailure'] = $downstreamResponse->numberFailure();
        $response['tokensWithError'] = $downstreamResponse->tokensWithError();
        return $response;
    }
}