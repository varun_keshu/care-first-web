<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 30-07-2018
 * Time: 03:54 PM
 */

namespace App\Services;

class SmsService
{
    private $API_KEY, $SENDER_ID, $ROUTE_NO = 4, $RESPONSE_TYPE;

    public function __construct()
    {
        $this->API_KEY = env('MSG91_API_KEY');
        $this->SENDER_ID = env('MSG91_SENDER_ID');
        $this->ROUTE_NO = env('MSG91_ROUTE_NO');
        $this->RESPONSE_TYPE = env('MSG91_RESPONSE_TYPE');
    }

    public function send($message, $mobileNumber)
    {
        try {
            //Your message to send, Adding URL encoding.
            $message = urlencode($message);
            //Preparing post parameters
            $postData = [
                "sender" => "SOCKET",
                "route" => "4",
                "country" => "91",
                "sms" => [
                    [
                        "message" => $message,
                        "to" => [
                            $mobileNumber,
                        ]
                    ]
                ]
            ];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTPHEADER => array(
                    "authkey: $this->API_KEY",
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if (!$err) {
                return array('status' => true, 'message' => 'OTP Sent Successfully', 'data' => json_decode($response));
            }
            return array('status' => false, 'message' => $err, 'data' => json_decode($response));
        } catch (\Exception $e) {
            dd($e);
        }
    }
}