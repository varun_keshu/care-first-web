<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07-02-2019
 * Time: 10:42
 */

namespace App\Services;

use App\Models\Booking;
use App\Models\Services;
use App\Models\User;
use App\Models\VendorUnavailability;
use App\Models\VendorWorkSchedule;
use Carbon\Carbon;

class BookingService
{
    public function getAvailableVendorList($booking)
    {
        $manager_id = $booking->manager_id;

        $service = Services::where('id', $booking->service_id)->first();
        $service_at = $booking->service_date . ' ' . $booking->service_time;
        $service_start_date = Carbon::parse($service_at)->toDateString();
        $dayOfWeek = Carbon::parse($service_at)->dayOfWeek;

        $service_start_time = Carbon::parse($service_at)->toTimeString();
        $service_end_date = Carbon::parse($service_at)->addHours($service->time_period)->toDateString();
        $service_end_time = Carbon::parse($service_at)->addHours($service->time_period)->toTimeString();
        $assignVendors = Booking::whereNotNull('vendor_id')->where('is_cancelled_booking', 0)->whereNotIn('service_status', [6, 9, 10])
            ->where('manager_id', $manager_id)->whereBetween('service_date', [$service_start_date, $service_end_date])
            ->whereBetween('service_time', [$service_start_time, $service_end_time])->groupBy('vendor_id')->pluck('vendor_id')->toArray();

        $getUnavailableVendors = VendorUnavailability::whereHas("vendorDetails", function ($q) use ($manager_id) {
            $q->where('manager_id', $manager_id);
        })->where('date', $service_start_date)->groupBy('vendor_id')->pluck('vendor_id')->toArray();

        $getUnavailableTimeVendors = VendorWorkSchedule::whereHas("vendorDetails", function ($q) use ($manager_id) {
            $q->where('manager_id', $manager_id);
        })->where(function ($q) use ($service_start_time, $dayOfWeek) {
            $q->where('is_available', 0)->where('week_day', $dayOfWeek);
        })->orWhere(function ($q) use ($service_start_time, $dayOfWeek) {
            $q->where('start_time', ">", $service_start_time)->where('week_day', $dayOfWeek);
        })->orWhere(function ($q) use ($service_start_time, $dayOfWeek) {
            $q->where('end_time', "<", $service_start_time)->where('week_day', $dayOfWeek);
        })->groupBy('vendor_id')->pluck('vendor_id')->toArray();

        $notAvailableVendors = array_unique(array_merge($assignVendors, $getUnavailableTimeVendors, $getUnavailableVendors));

        $vendors = User::whereHas('userServices', function ($q) use ($service){
            $q->where('service_id', $service->id);
        })->where('user_type', 'vendor')->whereNotIn('id', $notAvailableVendors)->where('manager_id', $booking->manager_id)->select('id', 'name', 'contact_number', 'image')->get();
        return $vendors;
    }
}