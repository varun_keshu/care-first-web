<?php

function getCurrentAdmin()
{
    if (auth()->guard('admins')->check()) {
        $admin = auth()->guard('admins')->user();
        return $admin;
    }
}

if (!function_exists('checkCheckboxValue')) {
    function checkCheckboxValue($value)
    {
        return ($value == 1) ? "Yes" : "No";
    }
}

if (!function_exists('formatDate')) {
    function formatDate($date, $format = 'd M, Y h:i A')
    {
        if ($date != null) {
            return Carbon\Carbon::parse($date)->format($format);
        }
        return null;
    }
}

function formatPrice($price = 0)
{
    return "₹ " . number_format(($price ? floatval($price) : 0), 2, ".", ",");
}

if (!function_exists('showCustomerImage')) {
    function showCustomerImage($image_name)
    {
        return url(\App\Models\Customers::IMG_URL . $image_name);
    }
}

if (!function_exists('showUserImage')) {
    function showUserImage($image_name)
    {
        return url(\App\Models\User::IMG_URL . $image_name);
    }
}

if (!function_exists('showSliderImage')) {
    function showSliderImage($image_name)
    {
        return url(\App\Models\Sliders::IMG_URL . $image_name);
    }
}

if (!function_exists('showServiceCategoryImage')) {
    function showServiceCategoryImage($image_name)
    {
        return url(\App\Models\ServiceCategory::IMG_URL . $image_name);
    }
}

if (!function_exists('showServiceCategoryIconImage')) {
    function showServiceCategoryIconImage($image_name)
    {
        return url(\App\Models\ServiceCategory::ICON_IMG_URL . $image_name);
    }
}

if (!function_exists('showServiceCategoryBannerImage')) {
    function showServiceCategoryBannerImage($image_name)
    {
        return url(\App\Models\ServiceCategory::BANNER_IMG_URL . $image_name);
    }
}

if (!function_exists('showServiceCategoryProductImage')) {
    function showServiceCategoryProductImage($image_name)
    {
        return url(\App\Models\ServiceProductsByCategory::IMG_URL . $image_name);
    }
}

if (!function_exists('showServiceImage')) {
    function showServiceImage($image_name)
    {
        return url(\App\Models\Services::IMG_URL . $image_name);
    }
}
if (!function_exists('showServiceIssueImage')) {
    function showServiceIssueImage($image_name)
    {
        return url(\App\Models\BookedServiceIssues::IMG_URL . $image_name);
    }
}
if (!function_exists('showTestimonialImage')) {
    function showTestimonialImage($image_name)
    {
        return url(\App\Models\Testimonials::IMG_URL . $image_name);
    }
}
if (!function_exists('showClientImage')) {
    function showClientImage($image_name)
    {
        return url(\App\Models\Client::IMG_URL . $image_name);
    }
}

if (!function_exists('convertDateDurationInDays')) {
    function convertDateDurationInDays($duration, $duration_unit)
    {
        return $duration;
    }
}

if (!function_exists('getFixVisitingChargesCharges')) {
    function getFixVisitingChargesCharges($service_id)
    {
        $service = \App\Models\Services::where('id', $service_id)->select('visiting_charges')->first();
        if (!isset($service->visiting_charges)) {
            $site_configurations = \App\Models\SiteConfigurations::where('identifier', 'fixed_visiting_charges')->select('value')->first();
            return $site_configurations->value;
        } else {
            return $service->visiting_charges;
        }
    }
}

if (!function_exists('getServiceCommissionValue')) {
    function getServiceCommissionValue($commission, $service_price)
    {
        if (!isset($commission)) {
            $site_configurations = \App\Models\SiteConfigurations::where('identifier', 'commission_charges')->select('value')->first();
            $commission = $site_configurations->value;
        }
        return ($service_price * $commission) / 100;
    }
}

if (!function_exists('showContactRequestImage')) {
    function showContactRequestImage($image_name)
    {
        return url(\App\Models\ContactRequest::IMG_URL . $image_name);
    }
}

if (!function_exists('getAddressString')) {
    function getAddressString($address)
    {
        return $address['flat_or_house_number'] . ', ' . $address['address_line_1'] . ', ' . $address['service_area'] . ', ' . $address['city'] . ' ' . $address['zip_code'];
    }
}

if (!function_exists('getGoogleMapData')) {
    function getGoogleMapData($address_str)
    {
        $address = urlencode($address_str);
        $url = "https://maps.google.com/maps/api/geocode/json?address=$address&region=IN&key=" . env('GOOGLE_API_KEY');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        $status = $response_a->status;
        if ($status == 'ZERO_RESULTS' || $status == 'OVER_QUERY_LIMIT') {
            return ['latitude' => null, 'longitude' => null];
        } else {
            if (empty($response_a->results)) {
                return ['latitude' => null, 'longitude' => null];
            }
            return ['latitude' => $response_a->results[0]->geometry->location->lat, 'longitude' => $long = $response_a->results[0]->geometry->location->lng];
        }
    }
}

