<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CreateCustomDirectories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    const DIRECTORY_PATH = [
        'app/public/service_categories' => 'service_categories',
        'app/public/service_categories/icons' => 'service_categories/icons',
        'app/public/service_categories/banner' => 'service_categories/banner',
        'app/public/services' => 'services',
        'app/public/service_products' => 'service_products',
        'app/public/sliders' => 'sliders',
        'app/public/issue_images' => 'issue_images',
        'app/public/customers' => 'customers',
        'app/public/users' => 'users',
        'app/public/testimonials' => 'testimonials',
        'app/public/contact_requests' => 'contact_requests',
    ];

    protected $signature = 'create:uploadable_directories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will create the directories needed in the application and also create the symbolic link between storage and public/storage directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        # CREATE FOLDER PUBLIC
        foreach (self::DIRECTORY_PATH as $key => $path) {
            if (!File::isDirectory(storage_path($key))) {
                Storage::disk('public')->makeDirectory($path);
            }
        }
    }
}
