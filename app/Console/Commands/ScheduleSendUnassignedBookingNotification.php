<?php

namespace App\Console\Commands;

use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Booking;
use App\Models\BookingDetails;
use App\Services\PushNotificationService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class ScheduleSendUnassignedBookingNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:send_unassigned_booked_service_notification';

    /**
     * The console  command description.
     *
     * @var string
     */
    protected $description = 'Send unassigned booking notification to manager.';
    protected $pushNotificationService;
    protected $notificationRepository;

    /**
     * Create a new command instance.
     *
     * @param PushNotificationService $pushNotificationService
     * @param NotificationRepositoryInterface $notificationRepository
     */
    public function __construct(PushNotificationService $pushNotificationService, NotificationRepositoryInterface $notificationRepository)
    {
        parent::__construct();
        $this->pushNotificationService = $pushNotificationService;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bookings = Booking::whereNull('vendor_id')->where('is_cancelled_booking', 0)->get();
        foreach ($bookings as $booking) {
            $service_date = $booking->service_date . ' ' . $booking->service_time;
            $diff = Carbon::now()->diffInMinutes($service_date);
            if ($diff <= 30) {
                $notificationDetails = Config::get('notification_messages.vendor_unassigned_booking_notification_to_manager');
                $notificationData['title'] = $notificationDetails['title'];
                $notificationData['description'] = str_replace([":service_name"], [$booking->serviceDetails->title], $notificationDetails['description']);
                $notificationData['data']['notification_type'] = "vendor_unassigned_booking_notification_to_manager";
                $notificationData['data']['booking_id'] = $booking->id;
                $notificationData['user_id'] = $booking->manager_id;
                $notificationData['user_type'] = "manager";
                $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
                if (isset($booking->manager->device_token)) {
                    $this->pushNotificationService->sendNotification($notificationData['description'], $booking->manager->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
                }
            }
        }
        $this->info('Unassigned bookings notifications send successfully.');
    }
}
