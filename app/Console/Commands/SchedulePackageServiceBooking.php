<?php

namespace App\Console\Commands;

use App\Events\CreateBookingEvent;
use App\Models\Booking;
use App\Models\PackageSubscription;
use App\Models\UserServices;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SchedulePackageServiceBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:package-service-booking';

    /**
     * The console  command description.
     *
     * @var string
     */
    protected $description = 'Package booking schedule command.';

    /**
     * Create a new command instance.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $todaySubscriptionServices = PackageSubscription::where('payment_status', 1)->whereNotNull('next_service_start_date')
            ->where('is_expired', 0)->whereDate('next_service_start_date', Carbon::today())->get();
        foreach ($todaySubscriptionServices as $package_subscription){
            $userServiceData = UserServices::whereHas('relatedUser', function ($q) {
                $q->where('user_type', 'manager');
            })->where('service_id', $package_subscription->service_id)->where('service_area_id', $package_subscription->customer_address_details['service_area_id'])->first();

            $servicePrice = isset($package_subscription->serviceDetails->discounted_price_details[$package_subscription->priority]) ? $package_subscription->serviceDetails->discounted_price_details[$package_subscription->priority] : $package_subscription->serviceDetails->price_details[$package_subscription->priority];
            $data = [
                'customer_id' => $package_subscription->customer_id,
                'unique_booking_key' => $package_subscription->unique_key,
                'razorpay_payment_id' => $package_subscription->razorpay_payment_id,
                'razorpay_order_id' => $package_subscription->razorpay_order_id,
                'service_id' => $package_subscription->service_id,
                'service_category_id' => $package_subscription->service_category_id,
                'manager_id' => isset($userServiceData) ? $userServiceData->user_id : null,
                'package_subscription_id' => $package_subscription->id,
                'customer_address_id' => $package_subscription->customer_address_id,
                'service_area_id' => $package_subscription->customer_address_details['service_area_id'],
                'quantity' => $package_subscription->quantity,
                'priority' => $package_subscription->priority,
                'service_price' => $servicePrice,
                'price' => $servicePrice * $package_subscription->quantity,
                'service_status' => 1,
                'payment_status' => 1,
                'payment_method' => $package_subscription->payment_method,
                'payment_gateway_response' => $package_subscription->payment_gateway_response,
                'address_details' => $package_subscription->customer_address_details,
                'service_date' => $package_subscription->next_service_start_date,
                'service_time' => $package_subscription->service_time,
                'notes' => $package_subscription->notes,
            ];
            $data['commission_fee'] = getServiceCommissionValue($package_subscription->serviceDetails->commission_value, $data['price']) * $package_subscription->quantity;
            $data['vendor_earning_price'] = $data['price'] - $data['commission_fee'];
            $data['total_amount'] = $data['price'] + $data['insurance_premium_amount'];
            $booking = Booking::create($data);
            $eventData = [
                'booking_id' => $booking->id,
                'user_type' => "system",
                'type' => "subscription_package",
            ];
            event(new CreateBookingEvent($eventData));
        }
        $this->info('All Package Subscription Bookings processed.');
        return 0;
    }
}
