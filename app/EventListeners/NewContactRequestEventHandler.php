<?php

namespace App\EventListeners;

use App\Events\NewContactRequestEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\ContactRequest;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class NewContactRequestEventHandler
{
    protected $notificationRepository;
    protected $pushNotificationService;

    public function __construct(NotificationRepositoryInterface $notificationRepository, PushNotificationService $pushNotificationService)
    {
        $this->notificationRepository = $notificationRepository;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function handle(NewContactRequestEvent $event)
    {
        $data = $event->messageData;
        $conversationDetail = $data->conversationDetail;
        if (isset($conversationDetail->receiver_user_id)) {
            $contactRequestData = ContactRequest::find($conversationDetail->contact_request_id);

            $notificationDetails = Config::get('notification_messages.new_contact_request');
            $notificationData['title'] = $notificationDetails['title'];
            $notificationData['description'] = str_replace([':customer_name', ":service_name"], [$conversationDetail->customer->name, $contactRequestData->serviceDetails->title], $notificationDetails['description']);
            $notificationData['data']['notification_type'] = "new_contact_request";
            $notificationData['data']['conversation_id'] = $conversationDetail->id;
            $notificationData['user_id'] = $conversationDetail->receiver_user_id;
            $notificationData['user_type'] = "manager";
            $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
            if (isset($conversationDetail->userDetail->device_token)) {
                $this->pushNotificationService->sendNotification($notificationData['description'], $conversationDetail->userDetail->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
            }
        }
        return true;
    }
}