<?php

namespace App\EventListeners;

use App\Events\BookingCancellationEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Booking;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class SendBookingCancellationNotificationEventHandler
{
    protected $pushNotificationService;
    protected $notificationRepository;

    public function __construct(PushNotificationService $pushNotificationService, NotificationRepositoryInterface $notificationRepository)
    {
        $this->pushNotificationService = $pushNotificationService;
        $this->notificationRepository = $notificationRepository;
    }

    public function handle(BookingCancellationEvent $event)
    {
        $data = $event->bookingDetails;
        $booking = Booking::where('id', $data['booking_id'])->first();
        $cancelled_by_user_type = isset($data['user_type']) ? $data['user_type'] : "customer";

        if ($cancelled_by_user_type != "vendor") {
            //SEND NOTIFICATION TO CUSTOMER
            $customerNotificationDetails = Config::get('notification_messages.customer_booking_cancelled_notification_to_customer');
            $customerNotificationData['title'] = $customerNotificationDetails['title'];
            $customerNotificationData['description'] = str_replace([":service_name"], [$booking->serviceDetails->title], $customerNotificationDetails['description']);
            $customerNotificationData['data']['notification_type'] = "customer_booking_cancelled_notification_to_customer";
            $customerNotificationData['data']['booking_id'] = $booking->id;
            $customerNotificationData['user_id'] = $booking->customer_id;
            $customerNotificationData['user_type'] = "customer";
            $customerNotificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($customerNotificationData);
            if (isset($booking->customer->device_token) && ($cancelled_by_user_type != "customer")) {
                $this->pushNotificationService->sendNotification($customerNotificationData['description'], $booking->customer->device_token, $customerNotificationData['title'], $customerNotificationData['data'], $customerNotificationData['user_type']);
            }

            //SEND NOTIFICATION TO VENDOR
            if (isset($booking->vendor_id)) {
                $vendorNotificationDetails = Config::get('notification_messages.customer_booking_cancelled_notification_to_vendor');
                $vendorNotificationData['title'] = $vendorNotificationDetails['title'];
                $vendorNotificationData['description'] = str_replace([":customer_name", ":service_name"], [$booking->customer->name, $booking->serviceDetails->title], $vendorNotificationDetails['description']);
                $vendorNotificationData['data']['notification_type'] = "customer_booking_cancelled_notification_to_vendor";
                $vendorNotificationData['data']['booking_id'] = $booking->id;
                $vendorNotificationData['user_id'] = $booking->vendor_id;
                $vendorNotificationData['user_type'] = "vendor";
                $vendorNotificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($vendorNotificationData);
                if (isset($booking->vendor->device_token)) {
                    $this->pushNotificationService->sendNotification($vendorNotificationData['description'], $booking->vendor->device_token, $vendorNotificationData['title'], $vendorNotificationData['data'], $vendorNotificationData['user_type']);
                }
            }

            //SEND NOTIFICATION TO MANAGER
            $managerNotificationDetails = Config::get('notification_messages.customer_booking_cancelled_notification_to_manager');
            $managerNotificationData['title'] = $managerNotificationDetails['title'];
            $managerNotificationData['description'] = str_replace([":customer_name", ":service_name"], [$booking->customer->name, $booking->serviceDetails->title], $managerNotificationDetails['description']);
            $managerNotificationData['data']['notification_type'] = "customer_booking_cancelled_notification_to_manager";
            $managerNotificationData['data']['booking_id'] = $booking->id;
            $managerNotificationData['user_id'] = $booking->manager_id;
            $managerNotificationData['user_type'] = "manager";
            $managerNotificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($managerNotificationData);
            if (isset($booking->manager->device_token) && ($cancelled_by_user_type != "manager")) {
                $this->pushNotificationService->sendNotification($managerNotificationData['description'], $booking->manager->device_token, $managerNotificationData['title'], $managerNotificationData['data'], $managerNotificationData['user_type']);
            }
        } else {
            //SEND NOTIFICATION TO MANAGER
            $managerNotificationDetails = Config::get('notification_messages.vendor_booking_cancelled_notification_to_manager');
            $notificationData['title'] = $managerNotificationDetails['title'];
            $notificationData['description'] = str_replace([':vendor_name', ":service_name"], [$booking->vendor->name, $booking->serviceDetails->title], $managerNotificationDetails['description']);
            $notificationData['data']['notification_type'] = "vendor_booking_cancelled_notification_to_manager";
            $notificationData['data']['booking_id'] = $booking->id;
            $notificationData['user_id'] = $booking->manager_id;
            $notificationData['user_type'] = "manager";
            $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
            if (isset($booking->manager->device_token)) {
                $this->pushNotificationService->sendNotification($notificationData['description'], $booking->manager->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
            }
        }

        if ($cancelled_by_user_type != 'vendor') {
            $booking->is_cancelled_booking = 1;
        } else {
            $booking->vendor_id = null;
        }
        $booking->save();
    }
}