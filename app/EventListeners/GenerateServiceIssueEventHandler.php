<?php

namespace App\EventListeners;

use App\Events\GenerateServiceIssueEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\BookingStatusHistory;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class GenerateServiceIssueEventHandler
{
    protected $notificationRepository;
    protected $pushNotificationService;

    public function __construct(NotificationRepositoryInterface $notificationRepository, PushNotificationService $pushNotificationService)
    {
        $this->notificationRepository = $notificationRepository;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function handle(GenerateServiceIssueEvent $event)
    {
        $booking = $event->bookingDetails;
        $serviceStatus = $booking->serviceStatusDetail;

        //Store customer status history
        if ($serviceStatus->visible_to_customer == 1) {
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'customer',
                'state' => 'open_issue_request',
                'notes' => $serviceStatus->customer_title,
            ]);
        }

        //Store manager status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'manager',
            'state' => 'open_issue_request',
            'notes' => Config::get('service_status_messages.manager.open_issue_request'),
        ]);

        //Sent notification to manager
        $notificationDetails = Config::get('notification_messages.customer_issue_generate_notification_to_manager');
        $notificationData['title'] = $notificationDetails['title'];
        $notificationData['description'] = str_replace([':customer_name', ":service_name"], [$booking->customer->name, $booking->serviceDetails->title], $notificationDetails['description']);
        $notificationData['data']['notification_type'] = "customer_issue_generate_notification_to_manager";
        $notificationData['data']['booking_id'] = $booking->id;
        $notificationData['user_id'] = $booking->manager_id;
        $notificationData['user_type'] = "manager";
        $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
        if (isset($booking->manager->device_token)){
            $this->pushNotificationService->sendNotification($notificationData['description'], $booking->manager->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
        }
    }
}