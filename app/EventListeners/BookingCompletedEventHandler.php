<?php

namespace App\EventListeners;

use App\Events\BookingCompletedEvent;
use App\Models\BookingStatusHistory;
use App\Models\ServiceCategory;
use App\Models\Services;
use App\Models\UserServices;
use Illuminate\Support\Facades\Config;

class BookingCompletedEventHandler
{
    public function handle(BookingCompletedEvent $event)
    {
        $booking = $event->bookingDetails;
        if ($booking->serviceStatusDetail->visible_to_customer == 1){
            //Store customer status history
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'customer',
                'state' => 'completed',
                'notes' => $booking->serviceStatusDetail->customer_title,
            ]);
        }

        //Store vendor status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'vendor',
            'state' => 'completed',
            'notes' => Config::get('service_status_messages.vendor.completed'),
        ]);

        //Store manager status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'manager',
            'state' => 'completed',
            'notes' => Config::get('service_status_messages.manager.completed'),
        ]);

        if ($booking->serviceStatusDetail->update_completion_count == 1){
            ServiceCategory::where('id', $booking->service_category_id)->increment('completed_jobs_count');
            Services::where('id', $booking->service_id)->increment('completed_jobs_count');
            UserServices::where('user_id', $booking->vendor_id)->where('service_id', $booking->service_id)->where('service_area_id', $booking->service_area_id)->increment('completed_jobs_count');
            UserServices::where('user_id', $booking->manager_id)->where('service_id', $booking->service_id)->where('service_area_id', $booking->service_area_id)->increment('completed_jobs_count');
        }
    }
}