<?php

namespace App\EventListeners;

use App\Events\IssueRequestRejectedEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\BookingStatusHistory;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class IssueRequestRejectedEventHandler
{
    protected $notificationRepository;
    protected $pushNotificationService;

    public function __construct(NotificationRepositoryInterface $notificationRepository, PushNotificationService $pushNotificationService)
    {
        $this->notificationRepository = $notificationRepository;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function handle(IssueRequestRejectedEvent $event)
    {
        $booking = $event->bookingDetails;

        //Store Manager Status History
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'manager',
            'state' => 'rejected',
            'notes' => Config::get('service_status_messages.vendor.issue_request_rejected'),
            'reason' => $booking->issueDetails->reason,
        ]);

        //Store Customer Status History
        if ($booking->serviceStatusDetail->visible_to_customer == 1) {
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'customer',
                'state' => 'rejected',
                'notes' => $booking->serviceStatusDetail->customer_title,
                'reason' => $booking->issueDetails->reason,
            ]);
        }

        //Send Push Notification To Customer
        $customerNotificationDetails = Config::get('notification_messages.issue_request_rejected_notification_to_customer');
        $customerNotificationData['title'] = $customerNotificationDetails['title'];
        $customerNotificationData['description'] = str_replace([":service_name"], [$booking->serviceDetails->title], $customerNotificationDetails['description']);
        $customerNotificationData['data']['notification_type'] = "issue_request_rejected_notification_to_customer";
        $customerNotificationData['data']['booking_id'] = $booking->id;
        $customerNotificationData['user_id'] = $booking->customer_id;
        $customerNotificationData['user_type'] = "customer";
        $customerNotificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($customerNotificationData);
        if (isset($booking->customer->device_token)) {
            $this->pushNotificationService->sendNotification($customerNotificationData['description'], $booking->customer->device_token, $customerNotificationData['title'], $customerNotificationData['data'], $customerNotificationData['user_type']);
        }
    }
}