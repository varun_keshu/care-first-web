<?php

namespace App\EventListeners;

use App\Events\VendorAcceptedBookedServiceEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Booking;
use App\Models\BookingStatusHistory;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class VendorAcceptedBookedServiceEventHandler
{
    protected $notificationRepository;
    protected $pushNotificationService;

    public function __construct(NotificationRepositoryInterface $notificationRepository, PushNotificationService $pushNotificationService)
    {
        $this->notificationRepository = $notificationRepository;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function handle(VendorAcceptedBookedServiceEvent $event)
    {
        $eventData = $event->data;

        $booking = Booking::where('id', $eventData['booking_id'])->first();
        $serviceStatus = $booking->serviceStatusDetail;

        //Store customer status history
        if ($serviceStatus->visible_to_customer == 1) {
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'customer',
                'state' => 'confirmed',
                'notes' => $serviceStatus->customer_title,
            ]);
        }

        //Store vendor status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'vendor',
            'state' => 'confirmed',
            'notes' => ($eventData['user_type'] == "manager") ? Config::get('service_status_messages.vendor.manager_assign') : Config::get('service_status_messages.vendor.vendor_confirmed'),
        ]);

        //Store manager status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'manager',
            'state' => 'confirmed',
            'notes' => ($eventData['user_type'] == "manager") ? str_replace([':vendor_name'], [$booking->vendor->name], Config::get('service_status_messages.manager.manager_assign')) : str_replace([':vendor_name'], [$booking->vendor->name], Config::get('service_status_messages.manager.vendor_confirmed')),
        ]);

        if ($eventData['user_type'] == "vendor"){
            //Sent notification to manager
            $notificationDetails = Config::get('notification_messages.vendor_booking_confirmed_notification_to_manager');
            $notificationData['title'] = $notificationDetails['title'];
            $notificationData['description'] = str_replace([':vendor_name', ":service_name"], [$booking->vendor->name, $booking->serviceDetails->title], $notificationDetails['description']);
            $notificationData['data']['notification_type'] = "vendor_booking_confirmed_notification_to_manager";
            $notificationData['data']['booking_id'] = $booking->id;
            $notificationData['user_id'] = $booking->manager_id;
            $notificationData['user_type'] = "manager";
            $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
            if (isset($booking->manager->device_token)){
                $this->pushNotificationService->sendNotification($notificationData['description'], $booking->manager->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
            }
        }
        if ($eventData['user_type'] == "manager"){
            //Sent notification to manager
            $notificationDetails = Config::get('notification_messages.booking_assign_notification_to_vendor');
            $notificationData['title'] = $notificationDetails['title'];
            $notificationData['description'] = str_replace([':manager_name', ":service_name"], [$booking->manager->name, $booking->serviceDetails->title], $notificationDetails['description']);
            $notificationData['data']['notification_type'] = "booking_assign_notification_to_vendor";
            $notificationData['data']['booking_id'] = $booking->id;
            $notificationData['user_id'] = $booking->vendor_id;
            $notificationData['user_type'] = "vendor";
            $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
            if (isset($booking->vendor->device_token)){
                $this->pushNotificationService->sendNotification($notificationData['description'], $booking->vendor->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
            }
        }
    }
}