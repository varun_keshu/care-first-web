<?php

namespace App\EventListeners;

use App\Events\BookingCancellationEvent;
use App\Interfaces\ConfigurationRepositoryInterface;
use App\Models\Booking;
use App\Models\BookingCancellationLogs;
use App\Models\BookingStatusHistory;
use App\Models\Customers;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class BookingCancellationEventHandler
{
    protected $configurationRepository;

    public function __construct(ConfigurationRepositoryInterface $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }

    public function handle(BookingCancellationEvent $event)
    {
        $data = $event->bookingDetails;
        $booking = Booking::where('id', $data['booking_id'])->first();
        $cancelled_by_user_type = isset($data['user_type']) ? $data['user_type'] : "customer";
        $booking->service_status = ($cancelled_by_user_type == "vendor") ? 3 : 4;
        $booking->save();

        if ($cancelled_by_user_type == "vendor"){
            BookingStatusHistory::where('booking_id', $booking->id)->whereIn('user_type', ['vendor', 'customer'])->where('state', 'confirmed')->forceDelete();
            BookingStatusHistory::where('booking_id', $booking->id)->where('user_type', 'manager')->where('state', 'confirmed')->update(['state' => 'booked']);

            //Store manager status history
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'manager',
                'state' => 'booked',
                'notes' => str_replace([':vendor_name'], [$booking->vendor->name], Config::get('service_status_messages.manager.cancelled_by_vendor')),
                'reason' => isset($data['reason']) ? $data['reason'] : null,
            ]);
        }else{
            //Store customer status history
            if ($booking->serviceStatusDetail->visible_to_customer == 1){
                BookingStatusHistory::create([
                    'booking_id' => $booking->id,
                    'service_status_id' => $booking->service_status,
                    'user_type' => 'customer',
                    'state' => 'cancelled',
                    'notes' => $booking->serviceStatusDetail->customer_title,
                    'reason' => isset($data['reason']) ? $data['reason'] : null,
                ]);
            }

            if (isset($booking->vendor_id)){
                //Store customer status history
                BookingStatusHistory::create([
                    'booking_id' => $booking->id,
                    'service_status_id' => $booking->service_status,
                    'user_type' => 'vendor',
                    'state' => 'cancelled',
                    'notes' => ($cancelled_by_user_type == "customer") ? Config::get('service_status_messages.vendor.cancelled_by_owner'): Config::get('service_status_messages.vendor.cancelled_by_manager'),
                    'reason' => isset($data['reason']) ? $data['reason'] : null,
                ]);
            }

            //Store manager status history
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'manager',
                'state' => 'cancelled',
                'notes' => ($cancelled_by_user_type == "customer") ? Config::get('service_status_messages.manager.cancelled_by_owner') : Config::get('service_status_messages.manager.cancelled_by_manager'),
                'reason' => isset($data['reason']) ? $data['reason'] : null,
            ]);
        }

        $minimum_cancellation_time = $this->configurationRepository->getConfigurationValue('specific_time_before_taken_cancellation_charges');
        $service_at = $booking->service_date.' '.$booking->service_time;
        $minute_difference = Carbon::parse($service_at)->diffInMinutes(Carbon::now());
        $minimum_charges = 0;
        if ($minute_difference <= $minimum_cancellation_time){
            $minimum_charges = $this->configurationRepository->getConfigurationValue('fixed_visiting_charges');
            if($cancelled_by_user_type == "vendor"){
                $userData = User::where('id', $booking->vendor_id)->first();
            }else{
                $userData = Customers::where('id', $booking->customer_id)->first();
            }
            $userData->update(['pending_charges' => $userData->pending_charges + $minimum_charges]);
        }
        BookingCancellationLogs::create([
            'booking_id' => $booking->id,
            'cancelled_by_user_id' => ($cancelled_by_user_type == "vendor") ? $booking->vendor_id : $booking->customer_id,
            'cancelled_by_user_type' => $cancelled_by_user_type,
            'charges' => $minimum_charges,
            'is_paid' => ($minimum_charges == 0) ? 1 : 0
        ]);
    }
}