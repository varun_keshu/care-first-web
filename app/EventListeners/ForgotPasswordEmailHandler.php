<?php

namespace App\EventListeners;

use App\Events\ForgotPasswordEmail;
use App\Interfaces\Admin\EmailServiceInterface;

class ForgotPasswordEmailHandler
{
    protected $email_service;

    public function __construct(EmailServiceInterface $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(ForgotPasswordEmail $event)
    {
        $user = $event->data;
        $link = $user['reset_link'];
        $content_var_values = ['NAME' => $user['name'], 'RESET_LINK' => $link];
        $mail_params_array = ['to' => $user['email']];
        $email_template = "reset_password";
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
    }
}