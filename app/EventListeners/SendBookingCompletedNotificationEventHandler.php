<?php

namespace App\EventListeners;

use App\Events\BookingCompletedEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Booking;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class SendBookingCompletedNotificationEventHandler
{
    protected $pushNotificationService;
    protected $notificationRepository;

    public function __construct(PushNotificationService $pushNotificationService, NotificationRepositoryInterface $notificationRepository)
    {
        $this->pushNotificationService = $pushNotificationService;
        $this->notificationRepository = $notificationRepository;
    }

    public function handle(BookingCompletedEvent $event)
    {
        $booking = $event->bookingDetails;

        //SEND NOTIFICATION TO CUSTOMER
        $customerNotificationDetails = Config::get('notification_messages.customer_booking_completed_notification_to_customer');
        $customerNotificationData['title'] = $customerNotificationDetails['title'];
        $customerNotificationData['description'] = str_replace([":service_name"], [$booking->serviceDetails->title], $customerNotificationDetails['description']);
        $customerNotificationData['data']['notification_type'] = "customer_booking_completed_notification_to_customer";
        $customerNotificationData['data']['booking_id'] = $booking->id;
        $customerNotificationData['user_id'] = $booking->customer_id;
        $customerNotificationData['user_type'] = "customer";
        $customerNotificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($customerNotificationData);
        if (isset($booking->customer->device_token)) {
            $this->pushNotificationService->sendNotification($customerNotificationData['description'], $booking->customer->device_token, $customerNotificationData['title'], $customerNotificationData['data'], $customerNotificationData['user_type']);
        }

        //SEND NOTIFICATION TO MANAGER
        $managerNotificationDetails = Config::get('notification_messages.customer_booking_completed_notification_to_manager');
        $managerNotificationData['title'] = $managerNotificationDetails['title'];
        $managerNotificationData['description'] = str_replace([":customer_name", ":service_name", ":vendor_name"], [$booking->customer->name, $booking->serviceDetails->title, $booking->vendor->name], $managerNotificationDetails['description']);
        $managerNotificationData['data']['notification_type'] = "customer_booking_completed_notification_to_manager";
        $managerNotificationData['data']['booking_id'] = $booking->id;
        $managerNotificationData['user_id'] = $booking->manager_id;
        $managerNotificationData['user_type'] = "manager";
        $managerNotificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($managerNotificationData);
        if (isset($booking->manager->device_token)) {
            $this->pushNotificationService->sendNotification($managerNotificationData['description'], $booking->manager->device_token, $managerNotificationData['title'], $managerNotificationData['data'], $managerNotificationData['user_type']);
        }
    }
}