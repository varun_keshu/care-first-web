<?php

namespace App\EventListeners;

use App\Events\UpdateJobCountEvent;
use App\Models\Booking;

class UpdateJobCountEventHandler
{
    public function handle(UpdateJobCountEvent $event)
    {
        $data = $event->booking;
        $booking = Booking::where('id', $data->id)->first();
        if ($booking->serviceStatusDetail->update_completion_count == 1) {
            $booking->serviceDetails->increment('completed_jobs_count');
            $booking->serviceCategoryDetails->increment('completed_jobs_count');
        } elseif ($booking->serviceStatusDetail->update_issue_confirmation_count == 1) {
            $booking->serviceDetails->increment('jobs_count_with_issues');
            $booking->serviceCategoryDetails->increment('jobs_count_with_issues');
        }
        return true;
    }
}