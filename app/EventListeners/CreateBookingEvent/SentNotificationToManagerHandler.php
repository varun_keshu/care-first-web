<?php

namespace App\EventListeners\CreateBookingEvent;

use App\Events\CreateBookingEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Booking;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class SentNotificationToManagerHandler
{
    protected $notificationRepository;
    protected $pushNotificationService;

    public function __construct(NotificationRepositoryInterface $notificationRepository, PushNotificationService $pushNotificationService)
    {
        $this->notificationRepository = $notificationRepository;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function handle(CreateBookingEvent $event)
    {
        $eventData = $event->data;
        $booking = Booking::where('id', $eventData['booking_id'])->first();
        if (isset($booking->manager_id) && (in_array($eventData['user_type'], ['customer', 'system']))) {
            $notificationDetails = Config::get('notification_messages.new_booking_notification_to_manager');
            $notificationData['title'] = $notificationDetails['title'];
            $notificationData['description'] = str_replace([':customer_name', ":service_name"], [$booking->customer->name, $booking->serviceDetails->title], $notificationDetails['description']);
            $notificationData['data']['notification_type'] = "new_booking_notification_to_manager";
            $notificationData['data']['booking_id'] = $booking->id;
            $notificationData['user_id'] = $booking->manager_id;
            $notificationData['user_type'] = "manager";
            $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
            if (isset($booking->manager->device_token)){
                $this->pushNotificationService->sendNotification($notificationData['description'], $booking->manager->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
            }
        }
        return true;
    }
}