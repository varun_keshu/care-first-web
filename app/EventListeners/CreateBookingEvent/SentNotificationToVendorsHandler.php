<?php

namespace App\EventListeners\CreateBookingEvent;

use App\Events\CreateBookingEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Booking;
use App\Services\BookingService;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class SentNotificationToVendorsHandler
{
    protected $notificationRepository;
    protected $pushNotificationService;
    protected $bookingService;
    public function __construct(NotificationRepositoryInterface $notificationRepository, PushNotificationService $pushNotificationService,
            BookingService $bookingService)
    {
        $this->notificationRepository = $notificationRepository;
        $this->pushNotificationService = $pushNotificationService;
        $this->bookingService = $bookingService;
    }

    public function handle(CreateBookingEvent $event)
    {
        $eventData = $event->data;
        $booking = Booking::where('id', $eventData['booking_id'])->first();
        $vendors = $this->bookingService->getAvailableVendorList($booking);
        if (count($vendors) > 0){
            $notificationDetails = Config::get('notification_messages.new_booking_notification_to_vendor');
            $notificationData['title'] = $notificationDetails['title'];
            $notificationData['description'] = str_replace([':customer_name', ":service_name"], [$booking->customer->name, $booking->serviceDetails->title], $notificationDetails['description']);
            $notificationData['data']['notification_type'] = "new_booking_notification_to_vendor";
            $notificationData['data']['booking_id'] = $booking->id;
            foreach ($vendors as $vendor){
                $notificationData['user_id'] = $vendor->id;
                $notificationData['user_type'] = "vendor";

                $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
                if (isset($vendor->device_token)){
                    $this->pushNotificationService->sendNotification($notificationData['description'], $vendor->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
                }
            }
        }
        return true;
    }
}