<?php

namespace App\EventListeners\CreateBookingEvent;

use App\Events\CreateBookingEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Booking;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class SentNotificationToCustomerHandler
{
    protected $notificationRepository;
    protected $pushNotificationService;

    public function __construct(NotificationRepositoryInterface $notificationRepository, PushNotificationService $pushNotificationService)
    {
        $this->notificationRepository = $notificationRepository;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function handle(CreateBookingEvent $event)
    {
        $eventData = $event->data;
        $booking = Booking::where('id', $eventData['booking_id'])->first();
        if ((in_array($eventData['user_type'], ['manager', 'system']))) {
            $notificationDetails = Config::get('notification_messages.new_booking_notification_to_customer');
            $notificationData['title'] = $notificationDetails['title'];
            $notificationData['description'] = str_replace([":service_name"], [$booking->serviceDetails->title], $notificationDetails['description']);
            $notificationData['data']['notification_type'] = "new_booking_notification_to_customer";
            $notificationData['data']['booking_id'] = $booking->id;
            $notificationData['user_id'] = $booking->customer_id;
            $notificationData['user_type'] = "customer";
            $notificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($notificationData);
            if (isset($booking->customer->device_token)){
                $this->pushNotificationService->sendNotification($notificationData['description'], $booking->customer->device_token, $notificationData['title'], $notificationData['data'], $notificationData['user_type']);
            }
        }
        return true;
    }
}