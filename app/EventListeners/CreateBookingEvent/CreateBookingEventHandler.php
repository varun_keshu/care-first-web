<?php

namespace App\EventListeners\CreateBookingEvent;

use App\Events\CreateBookingEvent;
use App\Models\Booking;
use App\Models\BookingStatusHistory;
use App\Models\PackageSubscription;
use App\Models\ServiceCategory;
use App\Models\Services;
use App\Models\UserServices;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class CreateBookingEventHandler
{
    public function handle(CreateBookingEvent $event)
    {
        $eventData = $event->data;
        $booking = Booking::where('id', $eventData['booking_id'])->first();
        $serviceStatus = $booking->serviceStatusDetail;

        //Store customer status history
        if ($serviceStatus->visible_to_customer == 1){
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'customer',
                'state' => 'booked',
                'notes' => isset($booking->package_subscription_id) ? Config::get('service_status_messages.customer.subscription_booked') : $serviceStatus->customer_title,
            ]);
        }

        //Store vendor status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'vendor',
            'state' => 'booked',
            'notes' => isset($booking->package_subscription_id) ? Config::get('service_status_messages.vendor.subscription_booked') : Config::get('service_status_messages.vendor.booked'),
        ]);

        //Store manager status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'manager',
            'state' => 'booked',
            'notes' => isset($booking->package_subscription_id) ? Config::get('service_status_messages.manager.subscription_booked') : Config::get('service_status_messages.manager.booked'),
        ]);

        if (isset($booking->package_subscription_id)){
            $packageSubscriptionService = PackageSubscription::where('id', $booking->package_subscription_id)->first();
            $packageSubscriptionService->used_count = $packageSubscriptionService->used_count + 1;
            $packageSubscriptionService->is_expired = ($packageSubscriptionService->used_count == $packageSubscriptionService->service_count) ? 1 : 0;
            if ($packageSubscriptionService->is_expired == 0){
                if ($packageSubscriptionService->duration_unit == "days"){
                    $packageSubscriptionService->next_service_start_date = Carbon::now()->addDays($packageSubscriptionService->duration)->toDateString();
                }elseif ($packageSubscriptionService->duration_unit == "months"){
                    $packageSubscriptionService->next_service_start_date = Carbon::now()->addMonths($packageSubscriptionService->duration)->toDateString();
                }elseif ($packageSubscriptionService->duration_unit == "years"){
                    $packageSubscriptionService->next_service_start_date = Carbon::now()->addYears($packageSubscriptionService->duration)->toDateString();
                }
            }else{
                $packageSubscriptionService->next_service_start_date = null;
            }
            $packageSubscriptionService->save();
        }

        UserServices::where('user_id', $booking->manager_id)->where('service_id', $booking->service_id)->where('service_area_id', $booking->service_area_id)->increment('total_jobs_count');
        UserServices::where('user_id', $booking->vendor_id)->where('service_id', $booking->service_id)->where('service_area_id', $booking->service_area_id)->increment('total_jobs_count');
        ServiceCategory::where('id', $booking->service_category_id)->increment('jobs_count');
        Services::where('id', $booking->service_id)->increment('jobs_count');
        return true;
    }
}