<?php

namespace App\EventListeners;

use App\Events\IssueRequestApprovedEvent;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\BookingStatusHistory;
use App\Models\ServiceCategory;
use App\Models\Services;
use App\Models\UserServices;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\Config;

class IssueRequestApprovedEventHandler
{
    protected $notificationRepository;
    protected $pushNotificationService;

    public function __construct(NotificationRepositoryInterface $notificationRepository, PushNotificationService $pushNotificationService)
    {
        $this->notificationRepository = $notificationRepository;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function handle(IssueRequestApprovedEvent $event)
    {
        $booking = $event->bookingDetails;

        BookingStatusHistory::where('booking_id', $booking->parent_id)->whereIn('user_type', ['manager', 'customer'])
            ->where('state', 'open_issue_request')->update(['booking_id' => $booking->id]);

        //Store Manager Status History
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'manager',
            'state' => 'open_issue_request',
            'notes' => Config::get('service_status_messages.vendor.issue_request_approved'),
        ]);

        //Store Customer Status History
        if ($booking->serviceStatusDetail->visible_to_customer == 1) {
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'customer',
                'state' => 'open_issue_request',
                'notes' => $booking->serviceStatusDetail->customer_title,
            ]);
        }

        //Send Push Notification To Customer
        $customerNotificationDetails = Config::get('notification_messages.issue_request_approved_notification_to_customer');
        $customerNotificationData['title'] = $customerNotificationDetails['title'];
        $customerNotificationData['description'] = str_replace([":service_name"], [$booking->serviceDetails->title], $customerNotificationDetails['description']);
        $customerNotificationData['data']['notification_type'] = "issue_request_approved_notification_to_customer";
        $customerNotificationData['data']['booking_id'] = $booking->id;
        $customerNotificationData['user_id'] = $booking->customer_id;
        $customerNotificationData['user_type'] = "customer";
        $customerNotificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($customerNotificationData);
        if (isset($booking->customer->device_token)) {
            $this->pushNotificationService->sendNotification($customerNotificationData['description'], $booking->customer->device_token, $customerNotificationData['title'], $customerNotificationData['data'], $customerNotificationData['user_type']);
        }

        //Store Vendor Status History
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'vendor',
            'state' => 'open_issue_request',
            'notes' => Config::get('service_status_messages.vendor.issue_request_approved'),
        ]);

        //Send Push Notification To Customer
        $vendorNotificationDetails = Config::get('notification_messages.issue_request_approved_notification_to_vendor');
        $vendorNotificationData['title'] = $vendorNotificationDetails['title'];
        $vendorNotificationData['description'] = str_replace([":customer_name", ":service_name"], [$booking->customer->title, $booking->serviceDetails->title], $vendorNotificationDetails['description']);
        $vendorNotificationData['data']['notification_type'] = "issue_request_approved_notification_to_vendor";
        $vendorNotificationData['data']['booking_id'] = $booking->id;
        $vendorNotificationData['user_id'] = $booking->vendor_id;
        $vendorNotificationData['user_type'] = "vendor";
        $vendorNotificationData['data']['notification_id'] = $this->notificationRepository->storeNotification($vendorNotificationData);
        if (isset($booking->vendor->device_token)) {
            $this->pushNotificationService->sendNotification($vendorNotificationData['description'], $booking->vendor->device_token, $vendorNotificationData['title'], $vendorNotificationData['data'], $vendorNotificationData['user_type']);
        }

        if ($booking->serviceStatusDetail->update_issue_confirmation_count == 1) {
            ServiceCategory::where('id', $booking->service_category_id)->increment('jobs_count_with_issues');
            Services::where('id', $booking->service_id)->increment('jobs_count_with_issues');
            UserServices::where('user_id', $booking->vendor_id)->where('service_id', $booking->service_id)->where('service_area_id', $booking->service_area_id)->increment('jobs_count_with_issues');
            UserServices::where('user_id', $booking->manager_id)->where('service_id', $booking->service_id)->where('service_area_id', $booking->service_area_id)->increment('jobs_count_with_issues');
        }
    }
}