<?php

namespace App\EventListeners;

use App\Events\NewAdminActivationEvent;
use App\Interfaces\Admin\EmailServiceInterface;

class NewAdminActivationEventHandler
{
    protected $email_service;

    public function __construct(EmailServiceInterface $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(NewAdminActivationEvent $event)
    {
        $details = $event->data;
        $content_var_values = ['NAME' => $details['name'], 'URL' => $details['link']];
        $mail_params_array = ['to' => $details['email']];
        $email_template = "admin_activation_set_password";
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
    }
}