<?php

namespace App\EventListeners;

use App\Events\BookedServiceStartEvent;
use App\Models\BookingStatusHistory;
use Illuminate\Support\Facades\Config;

class BookedServiceStartEventHandler
{
    public function handle(BookedServiceStartEvent $event)
    {
        $booking = $event->bookingDetails;
        $serviceStatus = $booking->serviceStatusDetail;

        //Store customer status history
        if ($serviceStatus->visible_to_customer == 1) {
            BookingStatusHistory::create([
                'booking_id' => $booking->id,
                'service_status_id' => $booking->service_status,
                'user_type' => 'customer',
                'state' => 'in_process',
                'notes' => $serviceStatus->customer_title,
            ]);
        }

        //Store vendor status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'vendor',
            'state' => 'in_process',
            'notes' => Config::get('service_status_messages.vendor.in_process'),
        ]);

        //Store manager status history
        BookingStatusHistory::create([
            'booking_id' => $booking->id,
            'service_status_id' => $booking->service_status,
            'user_type' => 'manager',
            'state' => 'in_process',
            'notes' => Config::get('service_status_messages.manager.in_process'),
        ]);
    }
}