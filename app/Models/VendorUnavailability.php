<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorUnavailability extends Model
{
    use SoftDeletes;

    protected $table = 'vendor_unavailability';

    protected $primaryKey = 'id';

    protected $fillable = ['vendor_id', 'date', 'reason', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function vendorDetails()
    {
        return $this->hasOne(User::class, "id", "vendor_id");
    }
}
