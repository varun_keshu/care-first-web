<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorWorkSchedule extends Model
{
    use SoftDeletes;
    const WEEK_DAYS = [0 => "Sunday", 1 => "Monday", 2 => "Tuesday", 3 => "Wednesday", 4 => "Thursday", 5 => "Friday", 6 => "Saturday"];

    protected $table = 'vendor_work_schedule';

    protected $primaryKey = 'id';

    protected $fillable = ['vendor_id', 'week_day', 'start_time', 'end_time', 'is_available', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function vendorDetails()
    {
        return $this->hasOne(User::class, "id", "vendor_id");
    }
}
