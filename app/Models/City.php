<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $table = 'cities';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function relatedServiceAreas()
    {
        return $this->hasMany(ServiceAreas::class, "city_id", 'id');
    }
}
