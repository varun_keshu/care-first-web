<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    use SoftDeletes;

    const IMG_URL = "storage/services/";
    const IMG_PATH = "app/public/services/";

    protected $table = 'services';

    protected $primaryKey = 'id';

    protected $fillable = ['service_category_id', 'title', 'description', 'image', 'price_details', 'commission_value', 'visiting_charges',
        'time_period', 'discounted_price_details', 'is_warranty_apply', 'warranty_expiration_days', 'show_quantity_option',
        'is_insurance_applicable', 'estimation_request_available', 'jobs_count', 'completed_jobs_count', 'jobs_count_with_issues',
        'created_at', 'updated_at', 'deleted_at'];

    public function setPriceDetailsAttribute($value)
    {
        $this->attributes['price_details'] = json_encode($value);
    }

    public function getPriceDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setDiscountedPriceDetailsAttribute($value)
    {
        $this->attributes['discounted_price_details'] = json_encode($value);
    }

    public function getDiscountedPriceDetailsAttribute($value)
    {
        return json_decode($value, true);
    }


    //Relationships
    public function serviceCategoryDetails()
    {
        return $this->hasOne(ServiceCategory::class, "id", 'service_category_id');
    }

    public function userServices()
    {
        return $this->hasMany(UserServices::class, "service_id", 'id');
    }
}
