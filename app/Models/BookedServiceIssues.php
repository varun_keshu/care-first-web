<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookedServiceIssues extends Model
{
    use SoftDeletes;

    const IMG_URL = "storage/issue_images/";
    const IMG_PATH = "app/public/issue_images/";
    const STATUSES = [0 => "Pending", 1 => "Approved", 2 => "Rejected", 3 => "Completed"];

    protected $table = 'booked_service_issues';

    protected $primaryKey = 'id';

    protected $fillable = ['booking_id', 'service_id', 'service_category_id', 'issue_title_id', 'description', 'reason',
        'images', 'status', 'created_at', 'updated_at', 'deleted_at'];


    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }

    public function getImagesAttribute($value)
    {
        return json_decode($value, true);
    }

    //Relationships
    public function bookingDetail()
    {
        return $this->hasOne(Booking::class, "id", 'booking_id');
    }

    public function serviceDetail()
    {
        return $this->hasOne(Services::class, "id", 'service_id');
    }

    public function serviceCategoryDetail()
    {
        return $this->hasOne(ServiceCategory::class, "id", 'service_category_id');
    }

    public function issueTitleDetail()
    {
        return $this->hasOne(IssueTitlesByCategory::class, "id", 'issue_title_id');
    }

}
