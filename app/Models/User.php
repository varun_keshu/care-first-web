<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use AuthenticableTrait;
    use SoftDeletes;

    const IMG_URL = "storage/users/";
    const IMG_PATH = "app/public/users/";

    protected $table = 'users';

    protected $fillable = ['manager_id', 'name', 'contact_number', 'user_type', 'status', 'pending_charges', 'has_complete_profile', 'image',
        'unread_notifications_count', 'otp_code', 'login_attempt_count', 'access_token', 'device_token'];

    //Relationships
    public function manager()
    {
        return $this->hasOne(User::class, "id", 'manager_id');
    }

    public function vendors()
    {
        return $this->hasMany(User::class, "manager_id", 'id');
    }

    public function userServices()
    {
        return $this->hasMany(UserServices::class, "user_id", 'id');
    }

    public function userServiceAreas()
    {
        return $this->hasMany(UserServiceAreas::class, "user_id", 'id');
    }

    public function vendorBookings()
    {
        return $this->hasMany(Booking::class, "vendor_id", 'id');
    }

    public function vendorWorkSchedule()
    {
        return $this->hasMany(VendorWorkSchedule::class, "vendor_id", 'id');
    }

    public function vendorUnavailability()
    {
        return $this->hasMany(VendorUnavailability::class, "vendor_id", 'id');
    }
}
