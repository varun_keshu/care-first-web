<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IssueTitlesByCategory extends Model
{
    use SoftDeletes;

    protected $table = 'issue_titles_by_category';

    protected $primaryKey = 'id';

    protected $fillable = ['service_category_id', 'title', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
}
