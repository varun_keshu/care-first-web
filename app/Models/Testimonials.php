<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    const IMG_URL = "storage/testimonials/";
    const IMG_PATH = "app/public/testimonials/";

    protected $table = 'testimonials';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'description', 'image', 'status', 'created_at', 'updated_at'];
}
