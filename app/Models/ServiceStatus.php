<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceStatus extends Model
{
    use SoftDeletes;

    const BACKEND_STATES = ['open' => "Open", 'completed' => "Completed", 'cancelled' => "Cancelled", 'issued' => "Issued"];
    const STATES = ['booked' => "Booked", 'confirmed' => "Confirmed", 'in_process' => "In Process", 'completed' => "Completed", 'cancelled' => "Cancelled", 'open_issue_request' => "Claim Submitted", 'rejected' => "Claim Rejected", "issue_resolved" => "Claim Resolved"];
    const OPEN_STATES = ['booked' => "Booked", 'confirmed' => "Confirmed", 'in_process' => "In Process", 'completed' => "Completed"];
    const STATES_WITH_ISSUE = ['booked' => "Booked", 'confirmed' => "Confirmed", 'in_process' => "In Process", 'completed' => "Completed", 'open_issue_request' => "Warranty Claim Submitted", "issue_resolved" => "Claim Resolved"];
    const STATES_WITH_ISSUE_REJECTED = ['booked' => "Booked", 'confirmed' => "Confirmed", 'in_process' => "In Process", 'completed' => "Completed", 'open_issue_request' => "Warranty Claim Submitted", "rejected" => "Claim Rejected"];

    const SHORT_TIME_LINE_STATES = ['booked' => "Booked", 'completed' => "Completed"];
    const SHORT_TIME_LINE_CANCELLED_STATES = ['booked' => "Booked", 'cancelled' => "Cancelled"];
    const SHORT_TIME_LINE_ISSUE_STATES = ['booked' => "Booked", 'completed' => "Completed", 'open_issue_request' => "Claim Submitted", "issue_resolved" => "Claim Resolved"];
    const SHORT_TIME_LINE_ISSUE_REJECTED_STATES = ['booked' => "Booked", 'completed' => "Completed", 'open_issue_request' => "Claim Submitted", "rejected" => "Claim Rejected"];

    protected $table = 'service_status';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'customer_title', 'state', 'backend_state', 'state_color', 'update_completion_count', 'update_issue_confirmation_count', 'visible_to_customer', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships

}
