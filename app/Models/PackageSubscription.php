<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageSubscription extends Model
{
    protected $table = 'package_subscriptions';

    protected $fillable = ['id', 'customer_id', 'service_id', 'service_category_id', 'customer_address_id', 'package_id', 'unique_key',
        'razorpay_order_id', 'razorpay_payment_id', 'title', 'description', 'quantity', 'price', 'total_amount', 'service_count', 'used_count',
        'duration', 'duration_unit', 'priority', 'service_start_date', 'next_service_start_date', 'service_time', 'notes', 'payment_status',
        'payment_method', 'payment_gateway_response', 'customer_address_details', 'is_expired', 'created_at', 'deleted_at', 'updated_at'];

    public function setCustomerAddressDetailsAttribute($value)
    {
        $this->attributes['customer_address_details'] = json_encode($value);
    }

    public function getCustomerAddressDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

    // Relationships
    public function serviceDetails()
    {
        return $this->hasOne(Services::class, 'id', 'service_id');
    }

    public function serviceCategoryDetails()
    {
        return $this->hasOne(ServiceCategory::class, 'id', 'service_id');
    }

    public function customer()
    {
        return $this->hasOne(Customers::class, 'id', 'customer_id');
    }

    public function customerAddress()
    {
        return $this->hasOne(CustomerAddresses::class, 'id', 'customer_address_id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'id', 'package_id');
    }
}
