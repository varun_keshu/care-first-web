<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RewardProgram extends Model
{
    use SoftDeletes;

    const CATEGORIES = [
        'total_number_of_customer_initiate_completed_bookings' => "Total number of customer initiate completed bookings",
        'referral_count' => "Referral Counts",
    ];

    protected $table = 'reward_programs';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'description', 'category', 'criteria', 'coupon_code', 'created_at', 'updated_at', 'deleted_at'];

    public function setCriteriaAttribute($value)
    {
        $this->attributes['criteria'] = json_encode($value);
    }

    public function getCriteriaAttribute($value)
    {
        return json_decode($value, true);
    }
}
