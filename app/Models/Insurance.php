<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Insurance extends Model
{
    use SoftDeletes;

    protected $table = 'insurances';

    protected $primaryKey = 'id';

    protected $fillable = ['user_type', 'insurance_amount', 'minimum_bookings_value', 'maximum_bookings_value', 'insurance_premium_value',
        'created_at', 'updated_at', 'deleted_at'];

}
