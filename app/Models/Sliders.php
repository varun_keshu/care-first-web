<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    const IMG_URL = "storage/sliders/";
    const IMG_PATH = "app/public/sliders/";
    const TYPES = ['home_page' => "Home Page", "all_services_page" => "All Services Page", "service_category_page" => "Service Category Detail Page"];

    protected $table = 'sliders';

    protected $primaryKey = 'id';

    protected $fillable = ['service_category_id', 'title', 'type', 'image', 'created_at', 'updated_at'];

    //Relationships
    public function serviceCategoryDetails()
    {
        return $this->hasOne(ServiceCategory::class, "id", 'service_category_id');
    }
}
