<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $table = 'conversations';

    protected $primaryKey = 'id';

    protected $fillable = ['customer_id', 'receiver_user_id', 'contact_request_id', 'booking_id', 'title', 'created_at', 'updated_at'];

    //Relationships
    public function relatedMessages()
    {
        return $this->hasMany(Message::class, "conversation_id", 'id');
    }
    public function customer()
    {
        return $this->hasOne(Customers::class, "id", 'customer_id');
    }
    public function contactRequestDetail()
    {
        return $this->hasOne(ContactRequest::class, "id", 'contact_request_id');
    }
    public function userDetail()
    {
        return $this->hasOne(User::class, "id", 'receiver_user_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($data) {
            $data->relatedMessages()->delete();
        });
        static::deleted(function ($data) {
            $data->relatedMessages()->delete();
        });
    }
}
