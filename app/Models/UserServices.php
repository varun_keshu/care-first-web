<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserServices extends Model
{
    use SoftDeletes;

    protected $table = 'user_services';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'service_id', 'service_category_id', 'service_area_id', 'total_jobs_count', 'completed_jobs_count', 'jobs_count_with_issues', 'total_ratings_count',
        'average_ratings_value', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function relatedUser()
    {
        return $this->hasOne(User::class, "id", 'user_id');
    }
    public function serviceDetail()
    {
        return $this->hasOne(Services::class, "id", 'service_id');
    }
    public function serviceCategoryDetail()
    {
        return $this->hasOne(ServiceCategory::class, "id", 'service_category_id');
    }
    public function serviceAreaDetail()
    {
        return $this->hasOne(ServiceAreas::class, "id", 'service_area_id');
    }
}
