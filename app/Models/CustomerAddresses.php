<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerAddresses extends Model
{
    use SoftDeletes;

    protected $table = 'customer_addresses';

    protected $primaryKey = 'id';

    protected $fillable = ['customer_id', 'title', 'flat_or_house_number', 'address_line_1', 'address_line_2', 'landmark', 'service_area_id', 'service_area', 'zip_code', 'city',
        'is_default', 'address_type', 'google_map_address', 'latitude', 'longitude', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function serviceArea()
    {
        return $this->hasOne(ServiceAreas::class, "id", 'service_area_id');
    }
}
