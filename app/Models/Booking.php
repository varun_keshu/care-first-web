<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    protected $table = 'bookings';

    protected $primaryKey = 'id';

    protected $fillable = ['parent_id', 'customer_id', 'unique_booking_key', 'razorpay_payment_id', 'razorpay_order_id', 'service_id',
        'service_category_id', 'manager_id', 'vendor_id', 'package_subscription_id', 'customer_address_id', 'service_area_id',
        'priority', 'quantity', 'price', 'service_price', 'vendor_earning_price', 'commission_fee', 'insurance_amount', 'insurance_premium_amount',
        'previous_pending_charges', 'total_amount', 'service_status', 'payment_status', 'payment_method', 'payment_gateway_response',
        'is_vendor_paid', 'has_insurance', 'notes', 'address_details', 'rating_value', 'review', 'service_date', 'service_time', 'otp_code',
        'is_cancelled_booking', 'generate_issue_status', 'warranty_expiration_date', 'created_at', 'updated_at', 'deleted_at'];

    public function setAddressDetailsAttribute($value)
    {
        $this->attributes['address_details'] = json_encode($value);
    }

    public function getAddressDetailsAttribute($value)
    {
        return json_decode($value, true);
    }



    //Relationships
    public function customer()
    {
        return $this->hasOne(Customers::class, "id", 'customer_id');
    }

    public function vendor()
    {
        return $this->hasOne(User::class, "id", 'vendor_id');
    }

    public function manager()
    {
        return $this->hasOne(User::class, "id", 'manager_id');
    }

    public function serviceCategoryDetails()
    {
        return $this->hasOne(ServiceCategory::class, "id", 'service_category_id');
    }

    public function bookingCancellationLogDetails()
    {
        return $this->hasMany(BookingCancellationLogs::class, "booking_id", 'id');
    }

    public function serviceDetails()
    {
        return $this->hasOne(Services::class, "id", 'service_id');
    }

    public function issueDetails()
    {
        return $this->hasOne(BookedServiceIssues::class, "booking_id", 'id');
    }

    public function packageSubscriptionServiceDetails()
    {
        return $this->hasOne(PackageSubscription::class, "id", 'package_subscription_id');
    }

    public function customerAddress()
    {
        return $this->hasOne(CustomerAddresses::class, "id", 'customer_address_id');
    }

    public function serviceStatusDetail()
    {
        return $this->hasOne(ServiceStatus::class, "id", 'service_status');
    }

    public function bookingStatusHistory()
    {
        return $this->hasMany(BookingStatusHistory::class, "booking_id", 'id');
    }
}
