<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceCategory extends Model
{
    use SoftDeletes;

    const IMG_URL = "storage/service_categories/";
    const IMG_PATH = "app/public/service_categories/";
    const ICON_IMG_URL = "storage/service_categories/icons/";
    const ICON_IMG_PATH = "app/public/service_categories/icons/";
    const BANNER_IMG_URL = "storage/service_categories/banner/";
    const BANNER_IMG_PATH = "app/public/service_categories/banner/";

    protected $table = 'service_categories';

    protected $primaryKey = 'id';

    protected $fillable = ['parent_id', 'title', 'description', 'image', 'icon_image', 'banner_image', 'jobs_count', 'completed_jobs_count', 'jobs_count_with_issues', 'rating_value', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function relatedServices()
    {
        return $this->hasMany(Services::class, "service_category_id", 'id');
    }
    public function serviceInstructions()
    {
        return $this->hasMany(ServiceInstructionByCategory::class, "service_category_id", 'id');
    }
    public function serviceProducts()
    {
        return $this->hasMany(ServiceProductsByCategory::class, "service_category_id", 'id');
    }
    public function issueTitles()
    {
        return $this->hasMany(IssueTitlesByCategory::class, "service_category_id", 'id');
    }
    public function contactRequests()
    {
        return $this->hasMany(ContactRequest::class, "customer_request_id", 'id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($data) {
            $data->relatedServices()->delete();
            $data->serviceInstructions()->delete();
            $data->serviceProducts()->delete();
            $data->issueTitles()->delete();
        });
        static::deleted(function ($data) {
            $data->relatedServices()->delete();
            $data->serviceInstructions()->delete();
            $data->serviceProducts()->delete();
            $data->issueTitles()->delete();
        });
    }
}
