<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customers extends Authenticatable
{
    use AuthenticableTrait;
    use SoftDeletes;

    const IMG_URL = "storage/customers/";
    const IMG_PATH = "app/public/customers/";

    protected $table = 'customers';

    protected $fillable = ['name', 'email', 'contact_number', 'status', 'pending_charges', 'has_complete_profile', 'image',
        'unread_notifications_count', 'otp_code', 'login_attempt_count', 'access_token', 'device_token'];

    //Relationships
    public function relatedAddresses()
    {
        return $this->hasMany(CustomerAddresses::class, "customer_id", 'id');
    }

    public function relatedConversations()
    {
        return $this->hasMany(Conversation::class, "customer_id", 'id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class, "customer_id", 'id');
    }

    public function contactRequests()
    {
        return $this->hasMany(ContactRequest::class, "customer_request_id", 'id');
    }
}
