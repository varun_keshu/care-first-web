<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingCancellationLogs extends Model
{
    protected $table = 'booking_cancellation_logs';

    protected $primaryKey = 'id';

    protected $fillable = ['booking_id', 'cancelled_by_user_id', 'cancelled_by_user_type', 'charges', 'is_paid', 'created_at', 'updated_at'];
}
