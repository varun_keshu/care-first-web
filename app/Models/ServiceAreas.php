<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceAreas extends Model
{
    use SoftDeletes;

    protected $table = 'service_areas';

    protected $primaryKey = 'id';

    protected $fillable = ['city_id', 'zip_code', 'title', 'created_at', 'updated_at', 'deleted_at'];
}
