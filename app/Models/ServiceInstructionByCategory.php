<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceInstructionByCategory extends Model
{
    const ICONS = ['circle', 'package', 'user-check', 'repeat'];

    protected $table = 'service_instructions_by_category';

    protected $primaryKey = 'id';

    protected $fillable = ['service_category_id', 'title', 'icon', 'created_at', 'updated_at'];

    //Relationships
}
