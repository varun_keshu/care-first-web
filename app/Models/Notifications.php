<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = 'notifications';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'user_type', 'title', 'description', 'data', 'is_read', 'created_at', 'updated_at'];

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = json_encode($value);
    }

    public function getDataAttribute($value)
    {
        return json_decode($value, true);
    }
}
