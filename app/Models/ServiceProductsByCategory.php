<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceProductsByCategory extends Model
{
    const IMG_URL = "storage/service_products/";
    const IMG_PATH = "app/public/service_products/";

    protected $table = 'service_products_by_category';

    protected $primaryKey = 'id';

    protected $fillable = ['service_category_id', 'title', 'image', 'created_at', 'updated_at'];

    //Relationships
}
