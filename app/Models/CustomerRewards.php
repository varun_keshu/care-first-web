<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerRewards extends Model
{
    protected $table = 'customer_rewards';

    protected $primaryKey = 'id';

    protected $fillable = ['customer_id', 'reward_program_id', 'created_at', 'updated_at', 'deleted_at'];

}
