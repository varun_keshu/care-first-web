<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $primaryKey = 'id';

    protected $fillable = ['conversation_id', 'sender_type', 'receiver_type', 'message', 'is_read', 'read_time', 'created_at', 'updated_at'];

    //Relationships
    public function conversationDetail()
    {
        return $this->hasOne(Conversation::class, "id", 'conversation_id');
    }
}
