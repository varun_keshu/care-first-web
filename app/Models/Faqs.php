<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faqs extends Model
{
    use SoftDeletes;

    const CATEGORIES = [
        'general' => "General",
        'booking_service' => "Booking Services",
        'paying_for_services' => "Paying for services",
        'warranty_claim' => "Warranty Claim",
        'buy_package' => "Buy Package",
    ];

    protected $table = 'faqs';

    protected $primaryKey = 'id';

    protected $fillable = ['sort_order', 'category', 'question', 'answer', 'created_at', 'updated_at', 'deleted_at'];
}
