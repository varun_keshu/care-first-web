<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model
{

    const IMG_URL = "storage/contact_requests/";
    const IMG_PATH = "app/public/contact_requests";
    protected $table = 'contact_requests';

    protected $primaryKey = 'id';

    protected $fillable = ['customer_id', 'booking_id', 'service_id', 'customer_address_id', 'service_area_id',
        'title', 'description', 'images', 'created_at', 'updated_at'];

    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }

    public function getImagesAttribute($value)
    {
        return json_decode($value, true);
    }

    //RELATIONSHIPS
    public function serviceDetails()
    {
        return $this->hasOne(Services::class, "id", "service_id");
    }

    public function addressDetails()
    {
        return $this->hasOne(CustomerAddresses::class, "id", "customer_address_id");
    }

    public function customer()
    {
        return $this->hasOne(Customers::class, "id", 'customer_id');
    }

    public function booking()
    {
        return $this->hasOne(Booking::class, "id", 'booking_id');
    }
}
