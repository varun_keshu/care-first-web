<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07-01-2021
 * Time: 12:42 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    const IMG_URL = "storage/clients/";
    const IMG_PATH = "app/public/clients/";

    protected $table = 'clients';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'image','created_at', 'updated_at'];

}
