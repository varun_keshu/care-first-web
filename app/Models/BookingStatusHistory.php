<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingStatusHistory extends Model
{
    use SoftDeletes;

    protected $table = 'booking_status_history';

    protected $primaryKey = 'id';

    protected $fillable = ['booking_id', 'service_status_id', 'state', 'user_type', 'notes', 'reason', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function serviceStatusDetail()
    {
        return $this->hasOne(ServiceStatus::class, "id", 'service_status_id');
    }

}
