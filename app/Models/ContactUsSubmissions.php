<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUsSubmissions extends Model
{
    protected $table = 'contact_us_submissions';

    protected $primaryKey = 'id';

    protected $fillable = ['customer_id', 'email', 'message', 'created_at', 'updated_at'];

    //RELATIONSHIPS
}
