<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplates extends Model
{
    const EMAIL_ACTION = [
        'reset_password' => 'reset_password',
        'admin_activation_set_password' => 'admin_activation_set_password',
    ];
    protected $table = 'email_templates';

    protected $primaryKey = 'id';

    protected $fillable = ['identifier', 'title', 'subject', 'content', 'created_at',  'updated_at'];

    //Relationships

}
