<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    const DURATION_UNITS = ['days' => 'Day', 'months' => 'Month', 'years' => 'Year'];

    protected $table = 'packages';
    protected $fillable = ['id', 'service_id', 'service_category_id', 'title', 'price', 'service_count', 'duration', 'duration_unit',
        'description', 'created_at', 'updated_at', 'deleted_at'];

    // RELATIONS
    public function serviceDetails()
    {
        return $this->hasOne(Services::class, 'id', 'service_id');
    }
    public function serviceCategoryDetails()
    {
        return $this->hasOne(ServiceCategory::class, 'id', 'service_category_id');
    }
}
