<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserServiceAreas extends Model
{
    use SoftDeletes;

    protected $table = 'user_service_areas';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'city_id', 'service_area_id', 'created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function userServices()
    {
        return $this->hasMany(UserServices::class, "service_area_id", 'service_area_id');
    }
    public function serviceArea()
    {
        return $this->hasOne(ServiceAreas::class, "id", 'service_area_id');
    }
    public function city()
    {
        return $this->hasOne(City::class, "id", 'city_id');
    }
}
