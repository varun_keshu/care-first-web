<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    const TYPES = ['privacy_policy' => "Privacy Policy", 'about_us' => "About Us", 'terms_and_conditions' => "Terms & Conditions",
        'insurance_policy' => "Insurance Policy", 'company_profile' => "Company Profile"];

    protected $table = 'pages';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'page_type', 'content', 'page_title', 'meta_keywords', 'meta_description', 'created_at', 'updated_at'];
}
