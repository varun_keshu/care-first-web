<?php

namespace App\Interfaces\Api;

interface BookingRepositoryInterface
{
    public function createBooking($request);

    public function getBookingsListForCustomer($request);

    public function getStatusList();

    public function generateServiceIssue($request);

    public function updateBookingRatings($request);

    public function getVendorsListByService($request);

    public function createBookingByManager($request);

    public function getBookingsListForUsers($request);

    public function getIssuedBookingsListByUser($request);

    public function getBookingReports($request);

    public function getBookingDetailsForUsers($request);

    public function checkAnyVendorAcceptThisServiceBooking($request);

    public function assignVendorForBookingService($request);

    public function serviceStartByVendor($request);

    public function sendServiceCompleteOTP($request);

    public function confirmServiceCompletedOTP($request);

    public function cancelBookingService($request);

    public function approveIssueRequest($request);

    public function rejectIssueRequest($request);

    public function convertQuoteRequestToBooking($request);

    public function getBookingPaymentHistoryForUsers($request);

    public function getBookingPayoutHistoryForUsers($request);

    public function bookingPayoutCompleted($request);

    public function getReviewList($category_id = null);
}
