<?php

namespace App\Interfaces\Api;

interface CmsPageRepositoryInterface
{
    public function getPageDetails($page_type);

    public function getFaqCategories();

    public function getFaqsByCategories($category);

}