<?php

namespace App\Interfaces\Api;

interface BannerRepositoryInterface
{
    public function getBannersListByType($type, $category_id = null);
}