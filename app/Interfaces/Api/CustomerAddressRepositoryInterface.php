<?php

namespace App\Interfaces\Api;

interface CustomerAddressRepositoryInterface
{
    public function getAddressList($request);

    public function validateAddress($request);

    public function storeAddress($request);

    public function getAddressDetail($request);

    public function updateAddress($request);

    public function deleteAddress($request);

    public function getServiceAreas();
}