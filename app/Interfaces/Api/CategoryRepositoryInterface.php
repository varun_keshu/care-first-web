<?php

namespace App\Interfaces\Api;

interface CategoryRepositoryInterface
{
    public function getAllCategoryServices();

    public function getAllEstimationServices();

    public function getAllServicesList();

    public function getServiceCategoriesList();

    public function getAllServiceCategoriesList();

    public function getServiceCategoryDetails($id, $data);

    public function getServiceCategoriesListByUser($request);

    public function getServicesListByUser($request);
}