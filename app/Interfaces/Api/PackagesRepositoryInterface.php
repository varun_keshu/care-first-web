<?php

namespace App\Interfaces\Api;

interface PackagesRepositoryInterface
{
    public function getPackagesList($request);

    public function getCustomerSubscriptionsList($request);

    public function purchasePackage($requestData);

    public function successPackageSubscription($package_subscription_id, $requestData);
}