<?php

namespace App\Interfaces\Api;

interface UserRepositoryInterface
{
    public function validateContactNumber($request);

    public function sendOtp($request);

    public function verifyOtp($request);

    public function sendUpdateNumberOTP($request);

    public function updateContactNumberVerification($request);

    public function getUserDetail($request);

    public function getUserDetailById($request);

    public function getVendorsListByManager($request);

    public function validateUser($request);

    public function updateUserProfile($request);

    public function getVendorList($request);

    public function getVendorWorkSchedule($request);

    public function storeVendorWorkSchedule($request);

    public function getVendorUnavailabilityList($request);

    public function storeVendorUnavailability($request);

    public function cancelVendorUnavailability($request);
}