<?php

namespace App\Interfaces\Api;

interface NotificationRepositoryInterface
{
    public function storeNotification($data);

    public function getUserNotificationsList($request);

    public function getCustomerNotificationsList($request);

    public function deleteNotification($request);

    public function readNotification($request);
}