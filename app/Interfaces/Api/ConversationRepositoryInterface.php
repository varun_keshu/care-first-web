<?php

namespace App\Interfaces\Api;

interface ConversationRepositoryInterface
{
    public function getConversationList($request);

    public function cancelConversation($request);

    public function getConversationListById($requestData);

    public function checkNewMessageExistOrNot($request);

    public function sendMessage($request);
}