<?php

namespace App\Interfaces\Api;

interface CustomersRepositoryInterface
{
    public function validateContactNumber($request);

    public function sendOtp($request);

    public function resendOtp($request);

    public function verifyOtp($request);

    public function sendUpdateNumberOTP($request);

    public function updateContactNumberVerification($request);

    public function getCustomerDetail($request);

    public function getCustomerDetailById($request);

    public function updateCustomerProfile($request);

    public function searchCustomers($request);

    public function createCustomerByManager($request);
}