<?php

namespace App\Interfaces;

interface ContactRequestRepositoryInterface
{
    public function validateRequest($request);

    public function storeContactRequest($request);

    public function storeImage($requestData);

    public function getListAjaxData($request);

    public function getContactDetails($request);

    public function deleteContactRequest($contact_request_id);

    public function createBookingConversation($request);
}
