<?php

namespace App\Interfaces\Admin;

interface EmailTemplatesRepositoryInterface
{
    public function getAllEmailTemplates();

    public function getEmailIdentifierTitles($id = null);

    public function storeEmailTemplate($request);

    public function getEmailTemplateDetail($id);

    public function updateEmailTemplate($request, $id);

    public function deleteEmailTemplate($id);
}
