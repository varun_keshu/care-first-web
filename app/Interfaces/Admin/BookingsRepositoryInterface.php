<?php

namespace App\Interfaces\Admin;

interface BookingsRepositoryInterface
{
    public function getAllBookings($request);

    public function createOrder($request);

    public function getBookingDetails($booking_id);

    public function getBookingStatusHistory($booking);

    public function getAvailableVendorsList($booking_id);

    public function assignBookingToVendor($request, $booking_id);

    public function getBookedData();

    public function getCustomerDetails();

    public function getCount();

    public function getVendorList();

    public function getVendorBookings($request);

    public function getServiceList();

    public function getServiceBookings($request);

    public function getCategoryList();

    public function getCategoryBookings($request);

    public function getPayoutReportsAjax($request);

    public function getEarningDetails();



}
