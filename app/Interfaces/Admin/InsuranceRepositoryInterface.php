<?php

namespace App\Interfaces\Admin;

interface InsuranceRepositoryInterface
{
    public function getAllInsurance();

    public function storeInsurance($request);

    public function getInsuranceDetail($id);

    public function updateInsurance($request, $id);

    public function deleteInsurance($id);
}
