<?php

namespace App\Interfaces\Admin;

interface ServiceCategoryRepositoryInterface
{
    public function getAllServiceCategoriesTitles();

    public function getAllServiceCategories();

    public function getAllServiceCategoriesForHome();

    public function storeServiceCategory($request);

    public function getServiceCategoryDetail($id);

    public function updateServiceCategory($request, $id);

    public function deleteServiceCategory($id);

    public function getAllPluckIdNameWithServices();

}
