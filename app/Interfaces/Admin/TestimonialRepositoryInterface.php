<?php

namespace App\Interfaces\Admin;

interface TestimonialRepositoryInterface
{
    public function getAllTestimonials();

    public function storeTestimonial($request);

    public function getTestimonialDetail($id);

    public function updateTestimonial($request, $id);

    public function deleteTestimonial($id);
}
