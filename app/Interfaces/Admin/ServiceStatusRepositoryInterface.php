<?php

namespace App\Interfaces\Admin;

interface ServiceStatusRepositoryInterface
{
    public function getAllServiceStatusList();

    public function getStatusList();

    public function storeServiceStatus($request);

    public function getServiceStatusDetail($id);

    public function updateStatusCategory($request, $id);

    public function deleteStatusCategory($id);
}
