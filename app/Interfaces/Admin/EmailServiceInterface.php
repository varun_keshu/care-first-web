<?php

namespace App\Interfaces\Admin;

interface EmailServiceInterface
{
    public function sendEmail($mail_params_array, $template, $dynamic_data);
}
