<?php

namespace App\Interfaces\Admin;

use Illuminate\Http\Request;

interface AdminUserInterface
{
    public function getAllAdmins();

    public function storeAdmin(Request $request);

    public function getByActivationToken($token);

    public function updateProfile($request);
}
