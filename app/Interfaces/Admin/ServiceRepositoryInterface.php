<?php

namespace App\Interfaces\Admin;

interface ServiceRepositoryInterface
{
    public function getAllServices();

    public function storeServiceData($request);

    public function getServiceDetail($id);

    public function updateServiceData($request, $id);

    public function deleteServiceData($id);

    public function getAllPluckIdName();
}
