<?php

namespace App\Interfaces\Admin;

interface SliderRepositoryInterface
{
    public function getAllSliders();

    public function storeSliderData($request);

    public function getSliderDetail($id);

    public function updateSliderData($request, $id);

    public function deleteSliderData($id);

    public function getSliderListByCategory($id);

    public function getOtherSliderList();
}
