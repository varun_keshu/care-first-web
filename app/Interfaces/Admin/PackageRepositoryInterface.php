<?php

namespace App\Interfaces\Admin;

interface PackageRepositoryInterface
{
    public function getAll();

    public function storePackageData($request);

    public function getPackageDetail($id);

    public function updatePackageData($request, $id);

    public function deletePackageData($id);

    public function getAllData();
}
