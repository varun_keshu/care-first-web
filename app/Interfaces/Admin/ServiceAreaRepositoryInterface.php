<?php

namespace App\Interfaces\Admin;

interface ServiceAreaRepositoryInterface
{
    public function getAllCitiesWithArea();

    public function storeServiceAreasData($request);

    public function getCityDetail($id);

    public function getServiceAreaDetail($id);

    public function updateServiceAreasData($request, $id);

    public function deleteCity($id);

    public function getServiceAreaTitleWithCity();
}
