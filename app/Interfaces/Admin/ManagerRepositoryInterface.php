<?php

namespace App\Interfaces\Admin;

interface ManagerRepositoryInterface
{
    public function getAllManagers();

    public function getAllManagersNameList();

    public function getAvailableServicesByArea($service_area_id, $request);

    public function storeManager($request);

    public function getManagerDetail($id);

    public function getManagerServiceCategoriesDetails($manager);

    public function getManagerServicesListByCategory($manager, $vendor = null);

    public function updateManager($request, $id);

    public function getManagersListByService($request);
}
