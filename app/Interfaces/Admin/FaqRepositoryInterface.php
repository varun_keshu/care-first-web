<?php

namespace App\Interfaces\Admin;

interface FaqRepositoryInterface
{
    public function getAllFaqs();

    public function getAllFaqsByCategory();

    public function storeFaqData($request);

    public function getFaqDetail($id);

    public function updateFaqData($request, $id);

    public function deleteFaqData($id);

    public function getFaqsByCategory($faq_category);

    public function updateFaqOrder($request);

}
