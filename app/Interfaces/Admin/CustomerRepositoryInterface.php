<?php

namespace App\Interfaces\Admin;

interface CustomerRepositoryInterface
{
    public function getAllCustomers();

    public function getCustomersList();

    public function getCustomerDetail($id);

    public function getCustomerConversationDetail($request);

    public function sendMessageToCustomer($request);

    public function getCustomersListBySearch($request);

    public function getAddressListByCustomer($request);
}
