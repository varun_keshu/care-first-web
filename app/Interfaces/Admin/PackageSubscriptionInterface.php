<?php


namespace App\Interfaces\Admin;


interface PackageSubscriptionInterface
{
    public function getAll();

    public function getPackageSubscriptionDetail($id);

    public function delete($id);
}
