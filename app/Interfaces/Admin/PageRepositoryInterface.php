<?php

namespace App\Interfaces\Admin;

interface PageRepositoryInterface
{
    public function getAllPages();

    public function getPageTypes($id = null);

    public function storePageData($request);

    public function getPageDetail($id);

    public function getPageDetailByType($page_type);

    public function updatePageData($request, $id);

    public function deletePageData($id);
}
