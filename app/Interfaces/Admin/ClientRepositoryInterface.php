<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07-01-2021
 * Time: 12:27 PM
 */

namespace App\Interfaces\Admin;


interface ClientRepositoryInterface
{
    public function getAllClients();

    public function storeClient($request);

    public function getClientDetail($id);

    public function updateClient($request, $id);

    public function deleteClient($id);
}
