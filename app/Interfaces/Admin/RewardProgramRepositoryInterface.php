<?php

namespace App\Interfaces\Admin;

interface RewardProgramRepositoryInterface
{
    public function getAllRewardProgramsList();

    public function storeRewardProgram($request);

    public function getRewardProgramDetail($id);

    public function updateRewardProgram($request, $id);

    public function deleteRewardProgram($id);
}
