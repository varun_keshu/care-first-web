<?php

namespace App\Interfaces\Admin;

interface VendorRepositoryInterface
{
    public function getAllVendors();

    public function storeVendor($request);

    public function getVendorDetail($id);

    public function updateVendor($request, $id);

    public function vendorList();
}
