<?php

namespace App\Interfaces;

interface ConfigurationRepositoryInterface
{
    public function getAllConfigurations();

    public function getConfigurationValue($identifier);

    public function updateConfigurations($request);

    public function getCustomerInsuranceList();

    public function getInsuranceDetails($request);

    public function getContactData();
}
