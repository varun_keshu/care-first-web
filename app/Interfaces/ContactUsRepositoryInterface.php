<?php

namespace App\Interfaces;

interface ContactUsRepositoryInterface
{
    public function storeContactUsData($request);

    public function getListAjaxData($request);


}
