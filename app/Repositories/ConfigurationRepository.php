<?php
namespace App\Repositories;

use App\Interfaces\ConfigurationRepositoryInterface;
use App\Models\Insurance;
use App\Models\SiteConfigurations;

class ConfigurationRepository implements ConfigurationRepositoryInterface
{
    public function getAllConfigurations()
    {
        return SiteConfigurations::get();
    }

    public function getConfigurationValue($identifier)
    {
        $data = SiteConfigurations::where('identifier', $identifier)->first();
        return $data->value;
    }

    public function updateConfigurations($request)
    {
        $requestData = $request->except("_token");
        foreach ($requestData['configuration_value'] as $identifier => $value) {
            SiteConfigurations::where('identifier', $identifier)->update(['value' => $value]);
        }
        return true;
    }

    public function getCustomerInsuranceList()
    {
        return Insurance::orderBy("minimum_bookings_value", "ASC")->where('user_type', "customer")->select('user_type', 'insurance_amount', 'minimum_bookings_value', 'maximum_bookings_value', 'insurance_premium_value')->get()->toArray();
    }

    public function getInsuranceDetails($request)
    {
        $requestData = $request->except("_token");
        $insurances = Insurance::where('user_type', "vendor")->where("minimum_bookings_value", "<", $requestData['total_amount'])->where("maximum_bookings_value", ">", $requestData['total_amount'])
            ->select('insurance_amount', 'insurance_premium_value')->get()->toArray();
        if (empty($insurances)) {
            $insurances[] = Insurance::where('user_type', "vendor")->orderBy('maximum_bookings_value', "DESC")->select('insurance_amount', 'insurance_premium_value')->first()->toArray();
        }
        return $insurances;
    }

    public function getContactData()
    {
        $data = SiteConfigurations::whereIn('identifier', ['company_address', 'contact_number', 'email'])->pluck('value', 'identifier')->toArray();
        return $data;
    }
}
