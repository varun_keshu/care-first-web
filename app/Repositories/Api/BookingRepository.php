<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Events\BookedServiceStartEvent;
use App\Events\BookingCancellationEvent;
use App\Events\BookingCompletedEvent;
use App\Events\CreateBookingEvent;
use App\Events\GenerateServiceIssueEvent;
use App\Events\IssueRequestApprovedEvent;
use App\Events\IssueRequestRejectedEvent;
use App\Events\VendorAcceptedBookedServiceEvent;
use App\Interfaces\Api\BookingRepositoryInterface;
use App\Models\BookedServiceIssues;
use App\Models\Booking;
use App\Models\BookingStatusHistory;
use App\Models\ContactRequest;
use App\Models\Customers;
use App\Models\ServiceCategory;
use App\Models\Services;
use App\Models\ServiceStatus;
use App\Models\UserServices;
use App\Models\VendorUnavailability;
use App\Models\VendorWorkSchedule;
use App\Services\BookingService;
use App\Services\RazorpayService;
use App\Services\SmsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Str;

class BookingRepository implements BookingRepositoryInterface
{
    protected $smsService;
    protected $razorpayService;
    protected $bookingService;

    public function __construct(SmsService $smsService, RazorpayService $razorpayService, BookingService $bookingService)
    {
        $this->smsService = $smsService;
        $this->razorpayService = $razorpayService;
        $this->bookingService = $bookingService;
    }

    public function createBooking($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            foreach ($requestData['service_categories'] as $serviceCategoryArray) {
                foreach ($serviceCategoryArray['itemList'] as $serviceDetails) {
                    $random_key = Str::random(10);

                    $addressDetail = $serviceCategoryArray['selectedAddress'];
                    $userService = UserServices::whereHas('relatedUser', function ($q) {
                        $q->where('user_type', 'manager');
                    })->where('service_id', $serviceDetails['id'])->where('service_area_id', $addressDetail['service_area_id'])->first();
                    $service = Services::find($serviceDetails['id']);
                    $customer = Customers::find($requestData['customer_id']);
                    $quantity = isset($serviceDetails['quantity']) ? $serviceDetails['quantity'] : 1;
                    $data = [
                        'customer_id' => $requestData['customer_id'],
                        'unique_booking_key' => $random_key,
                        'service_id' => $serviceDetails['id'],
                        'service_category_id' => $service['service_category_id'],
                        'manager_id' => isset($userService) ? $userService->user_id : null,
                        'customer_address_id' => $addressDetail['id'],
                        'service_area_id' => $addressDetail['service_area_id'],
                        'priority' => $serviceCategoryArray['priorityType'],
                        'quantity' => $quantity,
                        'service_price' => $serviceDetails['price'],
                        'price' => $serviceDetails['price'] * $quantity,
                        'insurance_amount' => isset($requestData['insurance_details']) ? $requestData['insurance_details']['insurance_amount'] : 0,
                        'insurance_premium_amount' => isset($requestData['insurance_details']) ? $requestData['insurance_details']['insurance_premium_value'] : 0,
                        'previous_pending_charges' => $customer->pending_charges,
                        'service_status' => 1,
                        'has_insurance' => isset($requestData['insurance_details']) && ($service->is_insurance_applicable == 1) ? 1 : 0,
                        'notes' => isset($serviceCategoryArray['note']) ? $serviceCategoryArray['note'] : null,
                        'address_details' => $addressDetail,
                        'service_date' => Carbon::parse($serviceCategoryArray['selectedDate'])->toDateString(),
                        'service_time' => Carbon::parse($serviceCategoryArray['selectedTime'])->toTimeString(),
                    ];
                    $data['commission_fee'] = getServiceCommissionValue($service->commission_value, $data['price']) * $quantity;
                    $data['vendor_earning_price'] = $data['price'] - $data['commission_fee'];
                    $data['total_amount'] = $data['price'] + $data['insurance_premium_amount'] + $data['previous_pending_charges'];
                    $bookedServiceDetail = Booking::create($data);

                    /*Event*/
                    $eventData = [
                        'booking_id' => $bookedServiceDetail->id,
                        'user_type' => "customer",
                    ];
                    event(new CreateBookingEvent($eventData));
                }
            }
            DB::commit();
            return ['status' => true, 'message' => 'Booking done.'];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getBookingsListForCustomer($request)
    {
        $returnData['current'] = $returnData['history'] = [];
        $requestData = $request->except("_token");
        $bookings = Booking::with(['bookingStatusHistory' => function ($q) {
            $q->latest();
        }])->where('customer_id', $requestData['customer_id'])->latest()->get();
        foreach ($bookings as $booking) {
            $data['id'] = $booking->id;
            $data['manager_name'] = isset($booking->manager) ? $booking->manager->name : null;
            $data['manager_image'] = (isset($booking->manager) && isset($booking->manager->image)) ? showUserImage($booking->manager->image) : "";
            $data['vendor_name'] = isset($booking->vendor) ? $booking->vendor->name : null;
            $data['vendor_image'] = (isset($booking->vendor) && isset($booking->vendor->image)) ? showUserImage($booking->vendor->image) : "";
            $data['service'] = $booking->serviceDetails->title;
            $data['service_image'] = showServiceImage($booking->serviceDetails->image);
            $data['service_category'] = $booking->serviceCategoryDetails->title;
            $data['priority'] = ucfirst($booking->priority);
            $data['price'] = $booking->total_amount;
            $data['service_date'] = formatDate(Carbon::parse($booking->service_date), "d M, Y");
            $data['service_time'] = formatDate(Carbon::parse($booking->service_time), "h:i A");
            $data['rating_value'] = $booking->rating_value;
            $data['state_color'] = $booking->serviceStatusDetail->state_color;
            $data['state'] = ServiceStatus::STATES[$booking->serviceStatusDetail->state];
            if (in_array($booking->serviceStatusDetail->state, ["completed", "issue_resolved"]) || ($booking->is_cancelled_booking == 1)) {
                $returnData['history'][] = $data;
            } else {
                $returnData['current'][] = $data;
            }
        }
        return $returnData;
    }

    public function getStatusList()
    {
        $returnData = [];
        $status_list = ServiceStatus::STATES;
        foreach ($status_list as $key => $title) {
            $returnData[] = [
                'id' => $key,
                'title' => $title
            ];
        }
        return $returnData;
    }

    public function generateServiceIssue($request)
    {
        $requestData = $request->except("_token");
        $bookingDetail = Booking::where('id', $requestData['booking_id'])->first();
        $data['booking_id'] = $bookingDetail['id'];
        $data['service_id'] = $bookingDetail['service_id'];
        $data['service_category_id'] = $bookingDetail['service_category_id'];
        $data['issue_title_id'] = $requestData['issue_title_id'];
        $data['description'] = isset($requestData['description']) ? $requestData['description'] : null;
        $data['images'] = [];
        if (!empty($requestData['images'])) {
            foreach ($requestData['images'] as $issueImg) {
                $path = storage_path(BookedServiceIssues::IMG_PATH);
                $extension = 'png';
                $filename_to_store = rand(1, 100000) . "." . $extension;
                file_put_contents($path . $filename_to_store, base64_decode($issueImg));
                $data['images'][] = $filename_to_store;
            }
        }
        BookedServiceIssues::create($data);

        $bookingDetail->service_status = 7;
        $bookingDetail->generate_issue_status = 1;
        $bookingDetail->save();

        event(new GenerateServiceIssueEvent($bookingDetail));
        return true;
    }

    public function updateBookingRatings($request)
    {
        $requestData = $request->except("_token");
        $bookingDetail = Booking::where('id', $requestData['booking_id'])->first();
        $bookingDetail->rating_value = $requestData['rating_value'];
        $bookingDetail->review = isset($requestData['review']) ? $requestData['review'] : null;
        $bookingDetail->save();

        $averageCategoryRatings = Booking::where('service_category_id', $bookingDetail->service_category_id)->whereNotNull('rating_value')->avg('rating_value');
        ServiceCategory::where('id', $bookingDetail->service_category_id)->update(['rating_value' => $averageCategoryRatings]);

        $averageServiceVendorRatings = Booking::where('vendor_id', $bookingDetail->vendor_id)->where('service_id', $bookingDetail->service_id)->where('service_area_id', $bookingDetail->service_area_id)->whereNotNull('rating_value')->avg('rating_value');
        UserServices::where('user_id', $bookingDetail->vendor_id)->where('service_id', $bookingDetail->service_id)->where('service_area_id', $bookingDetail->service_area_id)->update(['average_ratings_value' => $averageServiceVendorRatings]);
        UserServices::where('user_id', $bookingDetail->vendor_id)->where('service_id', $bookingDetail->service_id)->where('service_area_id', $bookingDetail->service_area_id)->increment('total_ratings_count');

        $averageServiceManagerRatings = Booking::where('manager_id', $bookingDetail->manager_id)->where('service_id', $bookingDetail->service_id)->where('service_area_id', $bookingDetail->service_area_id)->whereNotNull('rating_value')->avg('rating_value');
        UserServices::where('user_id', $bookingDetail->manager_id)->where('service_id', $bookingDetail->service_id)->where('service_area_id', $bookingDetail->service_area_id)->update(['average_ratings_value' => $averageServiceManagerRatings]);
        UserServices::where('user_id', $bookingDetail->manager_id)->where('service_id', $bookingDetail->service_id)->where('service_area_id', $bookingDetail->service_area_id)->increment('total_ratings_count');
        return true;
    }

    public function getVendorsListByService($request)
    {
        $requestData = $request->except("_token");
        $booking = Booking::where('id', $requestData['booking_id'])->first();
        $vendors = $this->bookingService->getAvailableVendorList($booking);
        $vendors = $vendors->filter(function ($item) {
            $item->image = isset($item->image) ? showUserImage($item->image) : "";
            return $item;
        });
        return $vendors;
    }

    public function createBookingByManager($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            foreach ($requestData['services'] as $serviceDetails) {
                $random_key = Str::random(10);
                $service = Services::find($serviceDetails['id']);
                $quantity = isset($serviceDetails['quantity']) ? $serviceDetails['quantity'] : 1;
                $customer = Customers::find($requestData['customer_id']);
                $data = [
                    'customer_id' => $requestData['customer_id'],
                    'unique_booking_key' => $random_key,
                    'service_id' => $serviceDetails['id'],
                    'service_category_id' => $service['service_category_id'],
                    'manager_id' => $requestData['user_id'],
                    'customer_address_id' => $requestData['address']['id'],
                    'service_area_id' => $requestData['address']['service_area_id'],
                    'priority' => $requestData['priority'],
                    'quantity' => $quantity,
                    'service_price' => $serviceDetails['price'],
                    'price' => $serviceDetails['price'] * $quantity,
                    'previous_pending_charges' => $customer->pending_charges,
                    'insurance_amount' => isset($requestData['insurance_details']) ? $requestData['insurance_details']['insurance_amount'] : 0,
                    'insurance_premium_amount' => isset($requestData['insurance_details']) ? $requestData['insurance_details']['insurance_premium_value'] : 0,
                    'service_status' => 1,
                    'has_insurance' => isset($requestData['insurance_details']) && ($service->has_insurance == 1) ? 1 : 0,
                    'notes' => isset($requestData['message']) ? $requestData['message'] : null,
                    'address_details' => $requestData['address'],
                    'service_date' => Carbon::parse($requestData['service_date'])->toDateString(),
                    'service_time' => Carbon::parse($requestData['service_time'])->toTimeString(),
                ];
                $data['commission_fee'] = getServiceCommissionValue($service->commission_value, $data['price']) * $quantity;
                $data['vendor_earning_price'] = $data['price'] - $data['commission_fee'];
                $data['total_amount'] = $data['price'] + $data['insurance_premium_amount'] + $data['previous_pending_charges'];
                $bookingDetail = Booking::create($data);

                /*Event*/
                $eventData = [
                    'booking_id' => $bookingDetail->id,
                    'user_type' => "manager",
                ];
                event(new CreateBookingEvent($eventData));
            }
            DB::commit();
            return ['status' => true, 'message' => 'Booking done.'];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getBookingsListForUsers($request)
    {
        $requestData = $request->except("_token");
        $returnData['unassigned'] = $returnData['today'] = $returnData['future'] = $returnData['history'] = [];
        $bookings = Booking::whereNotNull('id');
        if ($requestData['user_type'] == "manager") {
            $bookings = $bookings->where('manager_id', $requestData['user_id']);
        } else {
            $bookings = $bookings->where('vendor_id', $requestData['user_id']);
        }
        $bookings = $bookings->latest()->get();
        foreach ($bookings as $booking) {
            $data['id'] = $booking->id;
            $data['customer_name'] = isset($booking->customer) ? $booking->customer->name : null;
            $data['manager_name'] = isset($booking->manager) ? $booking->manager->name : null;
            $data['vendor_name'] = isset($booking->vendor) ? $booking->vendor->name : null;
            $data['service'] = $booking->serviceDetails->title;
            $data['service_image'] = showServiceImage($booking->serviceDetails->image);
            $data['service_category'] = $booking->serviceCategoryDetails->title;
            $data['priority'] = ucfirst($booking->priority);
            $data['price'] = $booking->total_amount;
            $data['service_date'] = formatDate(Carbon::parse($booking->service_date), "d M, Y");
            $data['service_time'] = formatDate(Carbon::parse($booking->service_time), "h:i A");
            $state = $booking->serviceStatusDetail->state;
            $data['state_color'] = $booking->serviceStatusDetail->state_color;
            $data['state'] = ServiceStatus::STATES[$state];
            if (($requestData['user_type'] == "manager") && is_null($booking->vendor_id) && ($booking->is_cancelled_booking == 0) && !in_array($state, ["completed", "cancelled"]) && !Carbon::parse($booking->service_date . ' ' . $booking->service_time)->isPast()) {
                $returnData['unassigned'][] = $data;
            }
            if (Carbon::parse($booking->service_date)->isToday() && ($booking->is_cancelled_booking == 0) && !in_array($state, ["completed", "cancelled"])) {
                $returnData['today'][] = $data;
            }
            if (Carbon::parse($booking->service_date)->isFuture() && ($booking->is_cancelled_booking == 0) && !in_array($state, ["completed", "cancelled"])) {
                $returnData['future'][] = $data;
            }
            if (in_array($state, ["completed", "cancelled"])) {
                $returnData['history'][] = $data;
            }
        }
        if ($requestData['user_type'] == "vendor") {
            $unassignedBookings = Booking::whereNull('vendor_id')->where('is_cancelled_booking', 0)->whereNotIn('service_status', [6, 9, 10])->get();
            foreach ($unassignedBookings as $unassignedBooking){
                $service_at = $unassignedBooking->service_date . ' ' . $unassignedBooking->service_time;
                $service_start_date = Carbon::parse($service_at)->toDateString();
                $dayOfWeek = Carbon::parse($service_at)->dayOfWeek;

                $service_start_time = Carbon::parse($service_at)->toTimeString();
                $service_end_date = Carbon::parse($service_at)->addHours($unassignedBooking->serviceDetails->time_period)->toDateString();
                $service_end_time = Carbon::parse($service_at)->addHours($unassignedBooking->serviceDetails->time_period)->toTimeString();
                $existVendorBooking = Booking::where('is_cancelled_booking', 0)->whereNotIn('service_status', [6, 9, 10])
                    ->where('vendor_id', $requestData['user_id'])->whereBetween('service_date', [$service_start_date, $service_end_date])
                    ->whereBetween('service_time', [$service_start_time, $service_end_time])->first();
                if (!isset($existVendorBooking)) {
                    $unavailableVendor = VendorUnavailability::where('vendor_id', $requestData['user_id'])->where('date', $service_start_date)->first();
                    if (!isset($unavailableVendor)) {
                        $existUnavailableTimeVendors = VendorWorkSchedule::where('vendor_id', $requestData['user_id'])
                            ->where(function ($q) use ($service_start_time, $dayOfWeek) {
                                $q->where('is_available', 0)->where('week_day', $dayOfWeek);
                            })->orWhere(function ($q) use ($service_start_time, $dayOfWeek) {
                                $q->where('start_time', ">", $service_start_time)->where('week_day', $dayOfWeek);
                            })->orWhere(function ($q) use ($service_start_time, $dayOfWeek) {
                                $q->where('end_time', "<", $service_start_time)->where('week_day', $dayOfWeek);
                            })->first();
                        if (!isset($existUnavailableTimeVendors)){
                            $bookingState = $unassignedBooking->serviceStatusDetail->state;
                            $returnData['unassigned'][] = [
                                'id' => $unassignedBooking->id,
                                'customer_name' => isset($unassignedBooking->customer) ? $unassignedBooking->customer->name : null,
                                'manager_name' => isset($unassignedBooking->manager) ? $unassignedBooking->manager->name : null,
                                'vendor_name' => isset($unassignedBooking->vendor) ? $unassignedBooking->vendor->name : null,
                                'service' => $unassignedBooking->serviceDetails->title,
                                'service_image' => showServiceImage($unassignedBooking->serviceDetails->image),
                                'service_category' => $unassignedBooking->serviceCategoryDetails->title,
                                'priority' => ucfirst($unassignedBooking->priority),
                                'price' => $unassignedBooking->total_amount,
                                'service_date' => formatDate(Carbon::parse($unassignedBooking->service_date), "d M, Y"),
                                'service_time' => formatDate(Carbon::parse($unassignedBooking->service_time), "h:i A"),
                                'state_color' => $unassignedBooking->serviceStatusDetail->state_color,
                                'state' => ServiceStatus::STATES[$bookingState],
                            ];
                        }
                    }
                }
            }
        }
        return $returnData;
    }

    public function getIssuedBookingsListByUser($request)
    {
        $returnData['pending'] = $returnData['in_process'] = $returnData['completed'] = [];
        $requestData = $request->except("_token");
        $data = BookedServiceIssues::whereHas('bookingDetail', function ($q) use ($requestData) {
            if ($requestData['user_type'] == "manager") {
                $q->where('manager_id', $requestData['user_id']);
            } else {
                $q->where('vendor_id', $requestData['user_id']);
            }
        })->latest()->get();
        foreach ($data as $datum) {
            $arrData = [
                'id' => $datum->id,
                'booking_id' => $datum->booking_id,
                'issue_title' => $datum->issueTitleDetail->title,
                'description' => $datum->description,
                'service' => $datum->serviceDetail->title,
                'service_category' => $datum->serviceCategoryDetail->title,
                'service_date' => formatDate(Carbon::parse($datum->bookingDetail->service_date), "d M, Y"),
                'service_time' => formatDate(Carbon::parse($datum->bookingDetail->service_time), "h:i A"),
                'has_insurance' => ($datum->bookingDetail->has_insurance == 1) ? true : false,
                'customer_details' => [],
                'vendor_details' => [],
                'images' => [],
                'status' => BookedServiceIssues::STATUSES[$datum->status],
                'created_date' => formatDate($datum->created_at),
            ];
            if (isset($datum->bookingDetail->customer_id)) {
                $arrData['customer_details'] = [
                    'id' => $datum->bookingDetail->customer_id,
                    'name' => $datum->bookingDetail->customer->name,
                    'contact_number' => $datum->bookingDetail->customer->contact_number,
                    'image' => isset($datum->bookingDetail->customer->image) ? showCustomerImage($datum->bookingDetail->customer->image) : "",
                    'address' => $datum->bookingDetail['address_details']
                ];
            }
            if (isset($datum->bookingDetail->vendor_id)) {
                $arrData['vendor_details'] = [
                    'id' => $datum->bookingDetail->vendor_id,
                    'name' => $datum->bookingDetail->vendor->name,
                    'contact_number' => $datum->bookingDetail->vendor->contact_number,
                    'image' => isset($datum->bookingDetail->vendor->image) ? showUserImage($datum->bookingDetail->vendor->image) : ""
                ];
            }
            if (!empty($datum->images)) {
                foreach ($datum->images as $image) {
                    $arrData['images'][] = showServiceIssueImage($image);
                }
            }
            if ($requestData['user_type'] == "manager") {
                if ($datum->status == 0) {
                    $returnData['pending'][] = $arrData;
                } elseif ($datum->status == 1) {
                    $returnData['in_process'][] = $arrData;
                } else {
                    $returnData['completed'][] = $arrData;
                }
            } else {
                if ($datum->status == 1) {
                    $returnData['pending'][] = $arrData;
                } elseif ($datum->status == 3) {
                    $returnData['completed'][] = $arrData;
                }
            }
        }
        return $returnData;
    }

    public function getBookingReports($request)
    {
        $returnData = [];
        $requestData = $request->except("_token");
        $bookings = Booking::whereNotNull('id');
        if ($requestData['user_type'] == "manager") {
            $bookings = $bookings->where('manager_id', $requestData['user_id']);
            if (isset($requestData['by_vendors']) && !empty($requestData['by_vendors'])) {
                $bookings = $bookings->whereIn('vendor_id', $requestData['by_vendors']);
            }
        } else {
            $bookings = $bookings->where('vendor_id', $requestData['user_id']);
        }
        if (isset($requestData['by_services']) && !empty($requestData['by_services'])) {
            $bookings = $bookings->whereIn('service_id', $requestData['by_services']);
        }
        if (isset($requestData['by_statuses']) && !empty($requestData['by_statuses'])) {
            $bookings = $bookings->whereHas('serviceStatusDetail', function ($q) use ($requestData) {
                $q->whereIn('state', $requestData['by_statuses']);
            });
        }
        if (isset($requestData['date_range']) && !empty($requestData['date_range'])) {
            $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $requestData['date_range']['from'])));
            $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $requestData['date_range']['to'])));
            $bookings = $bookings->whereBetween('service_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        $bookings = $bookings->latest()->get();
        foreach ($bookings as $booking) {
            $data['id'] = $booking->id;
            $data['customer_name'] = isset($booking->customer) ? $booking->customer->name : null;
            $data['manager_name'] = isset($booking->manager) ? $booking->manager->name : null;
            $data['vendor_name'] = isset($booking->vendor) ? $booking->vendor->name : null;
            $data['service'] = $booking->serviceDetails->title;
            $data['service_image'] = showServiceImage($booking->serviceDetails->image);
            $data['service_category'] = $booking->serviceCategoryDetails->title;
            $data['priority'] = ucfirst($booking->priority);
            $data['price'] = $booking->price;
            $data['service_date'] = formatDate(Carbon::parse($booking->service_date), "d M, Y");
            $data['service_time'] = formatDate(Carbon::parse($booking->service_time), "h:i A");
            $state = $booking->serviceStatusDetail->state;
            $data['state_color'] = $booking->serviceStatusDetail->state_color;
            $data['state'] = ServiceStatus::STATES[$state];
            $returnData[] = $data;
        }
        return $returnData;
    }

    public function getBookingDetailsForUsers($request)
    {
        $requestData = $request->except("_token");
        $user_type = isset($requestData['user_type']) ? $requestData['user_type'] : 'customer';
        $booking = Booking::with(['bookingCancellationLogDetails' => function ($q) use ($user_type, $requestData) {
            if ($user_type == "customer") {
                $q->where('cancelled_by_user_id', $requestData['customer_id'])->where("cancelled_by_user_type", "customer");
            } else {
                $q->where('cancelled_by_user_id', $requestData['user_id'])->where("cancelled_by_user_type", $requestData['user_type']);
            }
        }])->where('id', $requestData['booking_id'])->first();
        $data['id'] = $booking->id;
        $data['customer'] = [
            'id' => $booking->customer->id,
            'name' => $booking->customer->name,
            'contact_number' => $booking->customer->contact_number,
            'image' => isset($booking->customer->image) ? showCustomerImage($booking->customer->image) : "",
            'address' => $booking['address_details']
        ];
        $data['customer']['address']['address'] = getAddressString($booking['address_details']);
        if (isset($booking->package_subscription_id)) {
            $data['subscription_package'] = [
                'id' => $booking->package_subscription_id,
                'package_name' => $booking->packageSubscriptionServiceDetails->title ?? null,
                'total_services' => $booking->packageSubscriptionServiceDetails->service_count,
                'used_services' => $booking->packageSubscriptionServiceDetails->used_count,
                'is_expired' => ($booking->packageSubscriptionServiceDetails->is_expired == 1) ? true : false,
            ];
        }
        if (isset($booking->vendor)) {
            $data['vendor'] = [
                'id' => $booking->vendor->id,
                'name' => $booking->vendor->name,
                'contact_number' => $booking->vendor->contact_number,
                'image' => isset($booking->vendor->image) ? showUserImage($booking->vendor->image) : ""
            ];
            $diffTime = Carbon::parse($booking->service_date . ' ' . $booking->service_time)->diffInMinutes(Carbon::now());
            $data['is_enable_complete'] = ($booking->service_status == 5) ? true : false;
            $data['is_enable_start'] = (!$data['is_enable_complete'] && ($booking->service_status == 2) && ($diffTime <= 30)) ? true : false;
        }
        $data['is_disable_cancel'] = ($booking->service_status >= 5) ? true : false;
        if (isset($requestData['customer_id'])) {
            $data['issue_titles'] = [];
            foreach ($booking->serviceCategoryDetails->issueTitles as $issueTitleDetails) {
                $issueData = [
                    'id' => $issueTitleDetails->id,
                    'title' => $issueTitleDetails->title,
                ];
                $data['issue_titles'][] = $issueData;
            }
        } else {
            $data['is_access_data'] = (($requestData['user_type'] == "manager") || (isset($booking->vendor_id) && ($booking->vendor_id == $requestData['user_id']))) ? true : false;
            $data['has_rejected_request'] = (($user_type == "vendor") && (count($booking->bookingCancellationLogDetails) > 0)) ? true : false;
        }
        $data['notes'] = $booking->notes;
        $data['service'] = $booking->serviceDetails->title;
        $data['service_id'] = $booking->service_id;
        $data['service_image'] = showServiceImage($booking->serviceDetails->image);
        $data['show_quantity_option'] = ($booking->serviceDetails->show_quantity_option == 1) ? true : false;
        $data['quantity'] = $booking->quantity;
        $data['service_category'] = $booking->serviceCategoryDetails->title;
        $data['priority'] = ucfirst($booking->priority);
        $data['has_insurance'] = ($booking->has_insurance == 1) ? true : false;
        $data['has_service_insurance'] = ($booking->serviceDetails->is_insurance_applicable == 1) ? true : false;
        $data['price'] = $booking->service_price;
        $data['insurance_premium_amount'] = $booking->insurance_premium_amount;
        $data['previous_pending_charges'] = $booking->previous_pending_charges;
        $data['total_amount'] = $booking->total_amount;
        $data['service_date'] = formatDate(Carbon::parse($booking->service_date), "d M, Y");
        $data['service_time'] = formatDate(Carbon::parse($booking->service_time), "h:i A");
        $data['is_warranty_applicable'] = isset($booking->warranty_expiration_date) && (Carbon::now() <= $booking->warranty_expiration_date . " 23:59:59") && ($booking->generate_issue_status == 0) ? true : false;
        if ($data['is_warranty_applicable']) {
            $data['warranty_expiration_date'] = formatDate($booking->warranty_expiration_date);
        }
        $data['rating_value'] = $booking->rating_value;
        $data['review'] = $booking->review;
        $data['is_ratings_enabled'] = ($booking->service_status >= 6) ? true : false;
        $data['is_cancelled_booking'] = ($booking->is_cancelled_booking == 1) ? true : false;
        $data['payment_status'] = ($booking->payment_status == 1) ? true : false;
        $data['payment_method'] = ($booking->payment_method == "cash_on_delivery") ? "Cash on delivery" : ucfirst($booking->payment_method);
        $data['is_vendor_paid'] = ($booking->is_vendor_paid == 1) ? true : false;
        $data['state'] = ServiceStatus::STATES[$booking->serviceStatusDetail->state];
        if ($booking->is_cancelled_booking == 1) {
            $short_status_list = ServiceStatus::SHORT_TIME_LINE_CANCELLED_STATES;
            $long_status_list = [];
        } elseif (($user_type != "vendor") && ($booking->generate_issue_status == 3)) {
            $short_status_list = ServiceStatus::SHORT_TIME_LINE_ISSUE_REJECTED_STATES;
            $long_status_list = ServiceStatus::STATES_WITH_ISSUE_REJECTED;
        } elseif ((($user_type == "vendor") && in_array($booking->generate_issue_status, [2, 4])) || (($booking->generate_issue_status >= 1) && ($user_type != "vendor"))) {
            $short_status_list = ServiceStatus::SHORT_TIME_LINE_ISSUE_STATES;
            $long_status_list = ServiceStatus::STATES_WITH_ISSUE;
        } else {
            $short_status_list = ServiceStatus::SHORT_TIME_LINE_STATES;
            $long_status_list = ServiceStatus::OPEN_STATES;
        }
        $data['short_time_line'] = $data['long_time_line'] = $completed_states = [];
        $bookingStatusHistoryList = BookingStatusHistory::where('user_type', $user_type);
        if (isset($booking->parent_id)) {
            $bookingStatusHistoryList = $bookingStatusHistoryList->whereIn('booking_id', [$booking->id, $booking->parent_id]);
        } else {
            $bookingStatusHistoryList = $bookingStatusHistoryList->where('booking_id', $booking->id);
        }
        $bookingStatusHistoryList = $bookingStatusHistoryList->get()->groupBy('state');
        foreach ($bookingStatusHistoryList as $state => $bookingStatusHistory) {
            $messagesData = [];
            foreach ($bookingStatusHistory as $datum) {
                $messagesData[] = [
                    'date' => formatDate($datum->created_at, "d M, Y h:i A"),
                    'description' => isset($datum->reason) ? $datum->reason : $datum->notes,
                ];
            }
            if ($booking->is_cancelled_booking == 1) {
                if (in_array($state, array_keys($short_status_list))) {
                    $data['short_time_line'][] = [
                        'title' => ServiceStatus::STATES[$state],
                        'date' => formatDate($bookingStatusHistory[0]->created_at),
                        'description' => isset($bookingStatusHistory[0]->reason) ? $bookingStatusHistory[0]->reason : $bookingStatusHistory[0]->notes,
                        "is_completed" => true,
                        'action' => in_array($state, ['open_issue_request', 'issue_resolved']) ? 'issue' : (($state == "cancelled") ? "cancelled" : "")
                    ];
                }
                $data['long_time_line'][] = [
                    'title' => ServiceStatus::STATES[$state],
                    'data' => $messagesData,
                    "is_completed" => true,
                    'action' => in_array($state, ['open_issue_request', 'issue_resolved']) ? 'issue' : (($state == "cancelled") ? "cancelled" : "")
                ];
            } else {
                if (in_array($state, array_keys($short_status_list))) {
                    $data['short_time_line'][] = [
                        'title' => $short_status_list[$state],
                        'date' => formatDate($bookingStatusHistory[0]->created_at),
                        'description' => isset($bookingStatusHistory[0]->reason) ? $bookingStatusHistory[0]->reason : $bookingStatusHistory[0]->notes,
                        "is_completed" => true,
                        'action' => in_array($state, ['open_issue_request', 'issue_resolved']) ? 'issue' : (($state == "cancelled") ? "cancelled" : "")
                    ];
                }
                if (in_array($state, array_keys($long_status_list))) {
                    $data['long_time_line'][] = [
                        'title' => $long_status_list[$state],
                        'data' => $messagesData,
                        "is_completed" => true,
                        'action' => in_array($state, ['open_issue_request', 'issue_resolved']) ? 'issue' : (($state == "cancelled") ? "cancelled" : "")
                    ];
                }
            }
            $completed_states[] = $state;
        }
        if ($booking->is_cancelled_booking != 1) {
            foreach ($short_status_list as $state => $title) {
                if (!in_array($state, $completed_states)) {
                    $data['short_time_line'][] = [
                        'title' => $short_status_list[$state],
                        'description' => null,
                        'date' => ($state == "completed") ? $data['service_date'] . ' ' . $data['service_time'] : null,
                        "is_completed" => false,
                        'action' => in_array($state, ['open_issue_request', 'issue_resolved']) ? 'issue' : (($state == "cancelled") ? "cancelled" : "")
                    ];
                }
            }
            foreach ($long_status_list as $state => $title) {
                if (!in_array($state, $completed_states)) {
                    $arrayData = [
                        'title' => $long_status_list[$state],
                        'data' => [],
                        "is_completed" => false,
                        'action' => in_array($state, ['open_issue_request', 'issue_resolved']) ? 'issue' : (($state == "cancelled") ? "cancelled" : "")
                    ];
                    if ($state == "completed") {
                        $arrayData['data'][] = ['description' => null, 'date' => $data['service_date'] . ' ' . $data['service_time']];
                    }
                    $data['long_time_line'][] = $arrayData;
                }
            }
        }
        return $data;
    }

    public function checkAnyVendorAcceptThisServiceBooking($request)
    {
        $requestData = $request->except("_token");
        $booking = Booking::where('id', $requestData['booking_id'])->first();
        if (isset($booking->vendor_id)) {
            return true;
        } else {
            return false;
        }
    }

    public function assignVendorForBookingService($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            $vendor_id = ($requestData['user_type'] == "manager") ? $requestData['vendor_id'] : $requestData['user_id'];
            $booking = Booking::where('id', $requestData['booking_id'])->first();
            $booking->vendor_id = $vendor_id;
            $booking->service_status = 2;
            $booking->save();

            $eventData = [
                'user_type' => $requestData['user_type'],
                "booking_id" => $booking->id
            ];
            event(new VendorAcceptedBookedServiceEvent($eventData));
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function serviceStartByVendor($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            $booking = Booking::where('id', $requestData['booking_id'])->first();
            $booking->service_status = 5;
            $booking->save();

            event(new BookedServiceStartEvent($booking));
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, "message" => $e->getMessage()];
        }
    }

    public function sendServiceCompleteOTP($request)
    {
        $requestData = $request->except("_token");
        $otp = random_int(pow(10, 4 - 1), pow(10, 4) - 1);
        $booking = Booking::where('id', $requestData['booking_id'])->first();
        $booking->otp_code = $otp;
        $booking->save();

        $message = 'Share this OTP ' . $otp . ' with vendor if completed your service. ' . date('d M Y');
        $smsResponse = $this->smsService->send($message, $booking->customer->contact_number);
        if (!$smsResponse['status']) {
            return ['status' => false, 'message' => $smsResponse['message']];
        }
        return ['status' => true, 'message' => 'Booking done.', 'otp_code' => $otp];
    }

    public function confirmServiceCompletedOTP($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            $booking = Booking::where('id', $requestData['booking_id'])->first();
            if ($requestData['otp_code'] != $booking->otp_code) {
                return ['status' => false, "message" => "Invalid OTP!"];
            }
            if ($booking->serviceDetails->is_warranty_apply == 1) {
                $booking->warranty_expiration_date = Carbon::now()->addDays($booking->serviceDetails->warranty_expiration_days)->toDateString();
            }
            $booking->service_status = 6;
            $booking->otp_code = null;
            $booking->save();

            event(new BookingCompletedEvent($booking));
            if ($booking->pending_charges > 0) {
                $bookings = Booking::where('id', "!=", $booking->id)->where('service_status', "<=", 5)->where('previous_pending_charges', ">=", 0)->where('customer_id', $booking->customer_id)->get();
                foreach ($bookings as $bookingData) {
                    $bookingData->update([
                        'previous_pending_charges' => 0,
                        'total_amount' => $bookingData->total_amount - $bookingData->previous_pending_charges
                    ]);
                }
            }
            if (isset($requestData['payment_method']) && ($requestData['payment_method'] != "subscription")) {
                if ($requestData['payment_method'] == "cash_on_delivery") {
                    $booking->update(['payment_status' => 1, "payment_method" => "cash_on_delivery"]);
                } else {
                    $response = $this->razorpayService->sendPaymentLink($booking);
                    if (isset($response['status'])) {
                        $booking->update(['razorpay_order_id' => $response['data']['order_id'], "payment_method" => "online"]);
                    } else {
                        return $response;
                    }
                }
            }
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function cancelBookingService($request)
    {
        $requestData = $request->except("_token");
        event(new BookingCancellationEvent($requestData));
        return true;
    }

    public function approveIssueRequest($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            $issueRequestData = BookedServiceIssues::where('id', $requestData['issue_request_id'])->first();
            $issueRequestData->status = 1;
            $issueRequestData->save();

            /*Create New Booking*/
            $serviceBooking = Booking::where('id', $issueRequestData->booking_id)->first();
            $newRow = $serviceBooking->replicate();
            $newRow->parent_id = $serviceBooking->id;
            $newRow->service_status = 8;
            $newRow->generate_issue_status = 2;
            $newRow->save();

            $serviceBooking->service_status = 6;
            $serviceBooking->generate_issue_status = 0;
            $serviceBooking->save();

            event(new IssueRequestApprovedEvent($newRow));
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function rejectIssueRequest($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            $issueRequestData = BookedServiceIssues::where('id', $requestData['issue_request_id'])->first();
            $issueRequestData->status = 2;
            $issueRequestData->reason = isset($requestData['reason']) ? $requestData['reason'] : null;
            $issueRequestData->save();

            /*Create New Booking*/
            $serviceBooking = Booking::where('id', $issueRequestData->booking_id)->first();
            $serviceBooking->service_status = 9;
            $serviceBooking->generate_issue_status = 3;
            $serviceBooking->save();

            event(new IssueRequestRejectedEvent($serviceBooking));
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function convertQuoteRequestToBooking($request)
    {
        try {
            $requestData = $request->except("_token");
            $contactRequest = ContactRequest::where('id', $requestData['contact_request_id'])->first();
            $quantity = isset($requestData['quantity']) ? $requestData['quantity'] : 1;
            $random_key = Str::random(10);
            $data = [
                'customer_id' => $contactRequest['customer_id'],
                'unique_booking_key' => $random_key,
                'service_id' => $contactRequest['service_id'],
                'service_category_id' => $contactRequest->serviceDetails->service_category_id,
                'manager_id' => $requestData['user_id'],
                'customer_address_id' => $contactRequest['customer_address_id'],
                'service_area_id' => $contactRequest['service_area_id'],
                'priority' => $requestData['priority'],
                'quantity' => $quantity,
                'service_price' => $requestData['price'],
                'price' => $requestData['price'] * $quantity,
                'insurance_amount' => 0,
                'insurance_premium_amount' => 0,
                'service_status' => 1,
                'has_insurance' => 0,
                'notes' => isset($requestData['notes']) ? $requestData['notes'] : null,
                'address_details' => [
                    'id' => $contactRequest->addressDetails->id,
                    'title' => $contactRequest->addressDetails->title,
                    'flat_or_house_number' => $contactRequest->addressDetails->flat_or_house_number,
                    'landmark' => $contactRequest->addressDetails->landmark,
                    'address_line_1' => $contactRequest->addressDetails->address_line_1,
                    'service_area_id' => $contactRequest->addressDetails->service_area_id,
                    'service_area' => $contactRequest->addressDetails->service_area,
                    'google_map_address' => $contactRequest->addressDetails->google_map_address,
                    'city' => $contactRequest->addressDetails->city,
                    'zip_code' => $contactRequest->addressDetails->zip_code,
                    'latitude' => $contactRequest->addressDetails->latitude,
                    'longitude' => $contactRequest->addressDetails->longitude,
                ],
                'service_date' => Carbon::parse($requestData['service_date'])->toDateString(),
                'service_time' => Carbon::parse($requestData['service_time'])->toTimeString(),
            ];
            $data['commission_fee'] = getServiceCommissionValue($contactRequest->serviceDetails->commission_value, $data['price']) * $quantity;
            $data['vendor_earning_price'] = $data['price'] - $data['commission_fee'];
            $data['total_amount'] = $data['price'] + $data['insurance_premium_amount'];
            $bookingDetail = Booking::create($data);

            /*Event*/
            $eventData = [
                'booking_id' => $bookingDetail->id,
                'user_type' => "manager",
            ];
            event(new CreateBookingEvent($eventData));
            return ['status' => true];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getBookingPaymentHistoryForUsers($request)
    {
        $requestData = $request->except("_token");
        $returnData['pending'] = $returnData['completed'] = [];
        $bookings = Booking::whereNull('parent_id')->where("service_status", ">", 5);
        if ($requestData['user_type'] == "manager") {
            $bookings = $bookings->where("manager_id", $requestData['user_id']);
            if (isset($requestData['by_vendors']) && !empty($requestData['by_vendors'])) {
                $bookings = $bookings->whereIn('vendor_id', $requestData['by_vendors']);
            }
        } else {
            $bookings = $bookings->where("vendor_id", $requestData['user_id']);
        }
        $bookings = $bookings->orderBy('id', "DESC")->get();
        foreach ($bookings as $booking) {
            $data['id'] = $booking->id;
            $data['service'] = $booking->serviceDetails->title;
            $data['price'] = $booking->total_amount;
            $data['vendor_name'] = $booking->vendor->name;
            $data['customer_name'] = $booking->customer->name;
            $data['service_at'] = formatDate($booking->service_date . ' ' . $booking->service_time);
            $data['payment_method'] = ($booking->payment_method == "cash_on_delivery") ? "Cash On Delivery" : ucfirst($booking->payment_method);
            if ($booking->payment_status == 1) {
                $returnData['completed'][] = $data;
            } else {
                $returnData['pending'][] = $data;
            }
        }
        return $returnData;
    }

    public function getBookingPayoutHistoryForUsers($request)
    {
        $requestData = $request->except("_token");
        $returnData['pending'] = $returnData['completed'] = [];
        $bookings = Booking::whereNull('parent_id')->where("service_status", ">", 5);
        if ($requestData['user_type'] == "manager") {
            $bookings = $bookings->where("manager_id", $requestData['user_id']);
            if (isset($requestData['by_vendors']) && !empty($requestData['by_vendors'])) {
                $bookings = $bookings->whereIn('vendor_id', $requestData['by_vendors']);
            }
        } else {
            $bookings = $bookings->where("vendor_id", $requestData['user_id']);
        }
        if (isset($requestData['date_range']) && !empty($requestData['date_range'])) {
            $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $requestData['date_range']['from'])));
            $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $requestData['date_range']['to'])));
            $bookings = $bookings->whereBetween('service_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        $bookings = $bookings->orderBy('id', "DESC")->get();
        foreach ($bookings as $booking) {
            $data['id'] = $booking->id;
            $data['service'] = $booking->serviceDetails->title;
            $data['price'] = $booking->total_amount;
            $data['vendor_name'] = $booking->vendor->name;
            $data['customer_name'] = $booking->customer->name;
            $data['service_at'] = formatDate($booking->service_date . ' ' . $booking->service_time);
            $data['payment_method'] = ($booking->payment_method == "cash_on_delivery") ? "Cash On Delivery" : ucfirst($booking->payment_method);
            if ($booking->is_vendor_paid == 1) {
                $returnData['completed'][] = $data;
            } else {
                $returnData['pending'][] = $data;
            }
        }
        return $returnData;
    }

    public function bookingPayoutCompleted($request)
    {
        $requestData = $request->except("_token");
        Booking::where('id', $requestData['booking_id'])->update(['is_vendor_paid' => 1]);
        return true;
    }

    public function getReviewList($category_id = null)
    {
        $bookings = Booking::whereNotNull('rating_value')->select('customer_id', 'service_id', 'rating_value', 'review');
        if (isset($category_id)) {
            $bookings = $bookings->where('service_category_id', $category_id);
        }
        $bookings = $bookings->latest()->get()->take(10);
        $bookings = $bookings->filter(function ($item) {
            $item->name = $item->customer->name;
            $item->image = isset($item->customer->image) ? showCustomerImage($item->customer->image) : "";
            $item->service_name = $item->serviceDetails->title;
            unset($item->customer, $item->serviceDetails, $item->customer_id, $item->service_id);
            return $item;
        });
        return $bookings->toArray();
    }
}
