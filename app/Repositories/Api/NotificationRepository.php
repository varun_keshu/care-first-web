<?php
namespace App\Repositories\Api;

use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Customers;
use App\Models\Notifications;
use App\Models\User;
use Carbon\Carbon;

class NotificationRepository implements NotificationRepositoryInterface
{
    public function storeNotification($data)
    {
        $notification = Notifications::create($data);
        if ($data['user_type'] == "customer") {
            Customers::where('id', $data['user_id'])->increment('unread_notifications_count');
        } else {
            User::where('id', $data['user_id'])->increment('unread_notifications_count');
        }
        return $notification->id;
    }

    public function getUserNotificationsList($request)
    {
        $requestData = $request->all();
        $returnData = $todayData = $earlierData = [];
        $notifications = Notifications::where('user_id', $requestData['user_id'])->where('user_type', $requestData['user_type'])->orderBy('id', "DESC")->get();
        $notifications = $notifications->filter(function ($item) {
            $item->notification_type = $item->data['notification_type'];
            $item->booking_id = isset($item->data['booking_id']) ? $item->data['booking_id'] : null;
            return $item;
        })->toArray();
        foreach ($notifications as $notification) {
            $notification_date_string = Carbon::parse($notification['created_at'])->toDateString();
            if (Carbon::today()->toDateString() == $notification_date_string) {
                $todayData[] = $notification;
            } else {
                $earlierData[formatDate($notification['created_at'], "d M, Y")][] = $notification;
            }
        }
        if (!empty($todayData)) {
            $returnData[] = ['title' => 'Today', 'data' => $todayData];
        }
        if (!empty($earlierData)) {
            foreach ($earlierData as $date => $details) {
                $returnData[] = ['title' => $date, 'data' => $details];
            }
        }
        return $returnData;
    }

    public function getCustomerNotificationsList($request)
    {
        $requestData = $request->all();
        $returnData = $todayData = $earlierData = [];
        $notifications = Notifications::where('user_id', $requestData['customer_id'])->where('user_type', 'customer')->orderBy('id', "DESC")->get();
        $notifications = $notifications->filter(function ($item) {
            $item->notification_type = $item->data['notification_type'];
            return $item;
        })->toArray();
        foreach ($notifications as $notification) {
            $notification_date_string = Carbon::parse($notification['created_at'])->toDateString();
            if (Carbon::today()->toDateString() == $notification_date_string) {
                $todayData[] = $notification;
            } else {
                $earlierData[formatDate($notification['created_at'], "d M, Y")][] = $notification;
            }
        }
        if (!empty($todayData)) {
            $returnData[] = ['title' => 'Today', 'data' => $todayData];
        }
        if (!empty($earlierData)) {
            foreach ($earlierData as $date => $details) {
                $returnData[] = ['title' => $date, 'data' => $details];
            }
        }
        return $returnData;
    }

    public function deleteNotification($request)
    {
        $requestData = $request->all();
        $user_type = isset($requestData['user_id']) ? $requestData['user_type'] : "customer";
        $notification = Notifications::where('id', $requestData['notification_id'])->first();
        if (isset($notification)) {
            if (($user_type == "customer") && ($notification->is_read == 0)) {
                Customers::where('id', $requestData['customer_id'])->decrement('unread_notifications_count');
            } elseif (in_array($user_type, ["vendor", "manager"]) && ($notification->is_read == 0)) {
                User::where('id', $requestData['user_id'])->decrement('unread_notifications_count');
            }
            $notification->delete();
        }
        return $user_type;
    }

    public function readNotification($request)
    {
        $requestData = $request->all();
        $user_type = isset($requestData['user_id']) ? $requestData['user_type'] : "customer";
        $notification = Notifications::where('id', $requestData['notification_id'])->first();
        if (isset($notification) && ($notification->is_read == 0)) {
            if (($user_type == "customer")) {
                Customers::where('id', $requestData['customer_id'])->decrement('unread_notifications_count');
            } elseif (in_array($user_type, ["vendor", "manager"])) {
                User::where('id', $requestData['user_id'])->decrement('unread_notifications_count');
            }
            $notification->update(['is_read' => 1]);
        }
        return $user_type;
    }
}