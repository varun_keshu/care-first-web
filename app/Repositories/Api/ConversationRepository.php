<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\ConversationRepositoryInterface;
use App\Models\Conversation;
use App\Models\Message;
use Carbon\Carbon;

class ConversationRepository implements ConversationRepositoryInterface
{
    public function getConversationList($request)
    {
        $requestData = $request->all();
        $conversations = Conversation::with(['relatedMessages' => function ($q) {
            $q->latest();
        }])->whereNull('booking_id');
        if (isset($requestData['customer_id'])) {
            $conversations = $conversations->where('customer_id', $requestData['customer_id']);
        } else {
            $conversations = $conversations->where('receiver_user_id', $requestData['user_id']);
        }
        $conversations = $conversations->orderBy('id', "DESC")->get();
        $user_type = isset($requestData['user_type']) ? $requestData['user_type'] : "customer";
        $returnData = [];
        foreach ($conversations as $conversation) {
            $unreadCount = 0;
            foreach ($conversation->relatedMessages as $message) {
                if (($message['is_read'] == 0) && ($message['receiver_type'] == $user_type)) {
                    $unreadCount++;
                }
            }
            $returnData[] = [
                'id' => $conversation->id,
                'contact_request_id' => $conversation->contact_request_id,
                'service_details' => [
                    'title' => isset($conversation->contactRequestDetail) ? $conversation->contactRequestDetail->serviceDetails->title : null,
                    'category' => isset($conversation->contactRequestDetail) ? $conversation->contactRequestDetail->serviceDetails->serviceCategoryDetails->title : null,
                    'image' => isset($conversation->contactRequestDetail) && isset($conversation->contactRequestDetail->serviceDetails->image) ? showServiceImage($conversation->contactRequestDetail->serviceDetails->image) : "",
                ],
                'title' => $conversation->title,
                'last_message' => isset($conversation->relatedMessages[0]) ? $conversation->relatedMessages[0]->message : null,
                'last_message_time' => isset($conversation->relatedMessages[0]) ? Carbon::createFromTimeStamp(strtotime($conversation->relatedMessages[0]->created_at))->diffForHumans() : null,
                'unread_count' => $unreadCount,
            ];
        }
        return $returnData;
    }

    public function cancelConversation($request)
    {
        $requestData = $request->all();
        $conversation = Conversation::where('id', $requestData['conversation_id'])->first();
        $contact_request_id = $conversation->contact_request_id;
        $conversation->delete();
        return $contact_request_id;
    }

    public function getConversationListById($requestData)
    {
        Message::where('conversation_id', $requestData['conversation_id'])->where('is_read', 0)
            ->update(['is_read' => 1, 'read_time' => Carbon::now()]);
        return $conversation = Conversation::where('id', $requestData['conversation_id'])->first();
    }

    public function checkNewMessageExistOrNot($request)
    {
        $requestData = $request->all();
        $last_message = Message::where('receiver_type', $requestData['login_user_type'])
            ->where('conversation_id', $requestData['conversation_id'])->orderBy('id', 'desc')->first();
        if (isset($last_message) && isset($requestData['last_message_id']) && $last_message->id > $requestData['last_message_id']) {
            Message::where('receiver_type', $requestData['login_user_type'])->where('is_read', 0)
                ->where('conversation_id', $requestData['conversation_id'])->update(['is_read' => 1, 'read_time' => Carbon::now()]);
            return ['status' => true, 'data' => $last_message];
        } else {
            return ['status' => false, 'data' => null];
        }
    }

    public function sendMessage($request)
    {
        $requestData = $request->all();
        $data = [
            'conversation_id' => $requestData['conversation_id'],
            'sender_type' => $requestData['login_user_type'],
            'receiver_type' => $requestData['receiver_type'],
            'message' => $requestData['message'],
        ];
        $messageData = Message::create($data);
        Conversation::where('id', $requestData['conversation_id'])->update(['updated_at' => Carbon::now()]);
        return $messageData;
    }
}