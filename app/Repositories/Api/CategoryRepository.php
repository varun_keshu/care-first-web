<?php

namespace App\Repositories\Api;

use App\Interfaces\Api\CategoryRepositoryInterface;
use App\Models\ServiceCategory;
use App\Models\Services;
use App\Models\UserServices;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function getAllCategoryServices()
    {
        $services = Services::select('id', 'title')->get();
        return $services->toArray();
    }

    public function getAllEstimationServices()
    {
        return Services::select('id', 'title')->where('estimation_request_available', 1)->get()->toArray();
    }

    public function getAllServicesList()
    {
        $services = Services::select('id', 'service_category_id', 'title', 'image', 'price_details', 'is_warranty_apply', 'discounted_price_details', 'show_quantity_option', 'is_insurance_applicable', 'estimation_request_available', 'jobs_count')->get();
        $services = $services->filter(function ($item) {
            $item->image = isset($item->image) ? showServiceImage($item->image) : "";
            $item->show_quantity_option = ($item->show_quantity_option == 1) ? true : false;
            $item->is_insurance_applicable = ($item->is_insurance_applicable == 1) ? true : false;
            $item->is_warranty_apply = ($item->is_warranty_apply == 1) ? true : false;
            $item->estimation_request_available = ($item->estimation_request_available == 1) ? true : false;
            $item->serviceCategoryDetails->image = isset($item->serviceCategoryDetails->image) ? showServiceCategoryImage($item->serviceCategoryDetails->image) : "";
            $item->serviceCategoryDetails->icon_image = isset($item->serviceCategoryDetails->icon_image) ? showServiceCategoryIconImage($item->serviceCategoryDetails->icon_image) : "";
            return $item;
        });
        return $services->toArray();
    }

    public function getServiceCategoriesList()
    {
        $service_categories = ServiceCategory::whereHas('relatedServices')->orderBy('jobs_count', 'desc')->select('id', 'title', 'icon_image')->take(8)->get();
        $service_categories = $service_categories->filter(function ($item) {
            $item->icon_image = showServiceCategoryIconImage($item->icon_image);
            return $item;
        });
        return $service_categories->toArray();
    }

    public function getAllServiceCategoriesList()
    {
        $service_categories = ServiceCategory::whereHas('relatedServices')->orderBy('jobs_count', 'desc')->select('id', 'title', 'image', 'jobs_count')->get();
        $service_categories = $service_categories->filter(function ($item) {
            $item->image = showServiceCategoryImage($item->image);
            return $item;
        });
        return $service_categories->toArray();
    }

    public function getServiceCategoryDetails($id, $data)
    {
        $service_category = ServiceCategory::where('id', $id)->select('id', 'title', 'image', 'icon_image', 'rating_value')->first();
        $service_category->image = showServiceCategoryImage($service_category->image);
        $service_category->icon_image = showServiceCategoryIconImage($service_category->icon_image);
        $data['category_detail'] = $service_category;
        $data['services'] = [];
        foreach ($service_category->relatedServices as $relatedService) {
            $data['services'][] = [
                'id' => $relatedService['id'],
                'title' => $relatedService['title'],
                'image' => showServiceImage($relatedService['image']),
                'price_details' => $relatedService['price_details'],
                'discounted_price_details' => $relatedService['discounted_price_details'],
                'show_quantity_option' => ($relatedService['show_quantity_option'] == 1) ? true : false,
                'is_insurance_applicable' => ($relatedService['is_insurance_applicable'] == 1) ? true : false,
                'estimation_request_available' => ($relatedService['estimation_request_available'] == 1) ? true : false,
                'is_warranty_apply' => ($relatedService['is_warranty_apply'] == 1) ? true : false,
                'warranty_expiration_days' => $relatedService['warranty_expiration_days']
            ];
        }
        $data['instructions'] = [];
        foreach ($service_category->serviceInstructions as $serviceInstruction) {
            $data['instructions'][] = [
                'title' => $serviceInstruction['title'],
                'icon' => $serviceInstruction['icon']
            ];
        }
        $data['products'] = [];
        foreach ($service_category->serviceProducts as $serviceProduct) {
            $data['products'][] = [
                'title' => $serviceProduct['title'],
                'image' => showServiceCategoryProductImage($serviceProduct['image'])
            ];
        }
        unset($data['category_detail']->relatedServices, $data['category_detail']->serviceInstructions, $data['category_detail']->serviceProducts);
        return $data;
    }

    public function getServiceCategoriesListByUser($request)
    {
        $requestData = $request->all();
        $services = UserServices::where('user_id', $requestData['user_id'])->where('service_area_id', $requestData['service_area_id'])->groupBy("service_id")->pluck('service_id')->toArray();
        if (!empty($services)) {
            $categories = ServiceCategory::with('relatedServices')->whereHas('relatedServices', function ($q) use ($services) {
                $q->whereIn('id', $services);
            })->select('id', 'title')->get();
            $categories = $categories->filter(function ($item) {
                $item->services = $item->relatedServices->filter(function ($subItem) {
                    $subItem->image = isset($subItem->image) ? showServiceImage($subItem->image) : "";
                    $subItem->is_selected = false;
                    $subItem->is_insurance_applicable = ($subItem->is_insurance_applicable == 1) ? true : false;
                    $subItem->show_quantity_option = ($subItem->show_quantity_option == 1) ? true : false;
                    $subItem->estimation_request_available = ($subItem->estimation_request_available == 1) ? true : false;
                    $subItem->is_warranty_apply = ($subItem->is_warranty_apply == 1) ? true : false;
                     unset($subItem->service_category_id, $subItem->completed_jobs_count, $subItem->jobs_count_with_issues, $subItem->created_at, $subItem->updated_at, $subItem->deleted_at);
                    return $subItem;
                })->toArray();
                unset($item->relatedServices);
                return $item;
            });
            return $categories->toArray();
        }
        return [];
    }

    public function getServicesListByUser($request)
    {
        $requestData = $request->all();
        $data = UserServices::where('user_id', $requestData['user_id'])->select('service_id', "service_category_id")->distinct('service_id')->get();
        $data = $data->filter(function ($item) {
            $item->id = $item->service_id;
            $item->title = $item->serviceDetail->title;
            $item->category = $item->serviceCategoryDetail->title;
            unset($item->service_id, $item->service_category_id, $item->serviceDetail, $item->serviceCategoryDetail);
            return $item;
        });
        return $data;
    }
}