<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\BannerRepositoryInterface;
use App\Models\Sliders;

class BannerRepository implements BannerRepositoryInterface
{
    public function getBannersListByType($type, $category_id = null)
    {
        $sliders = Sliders::where('type', $type)->select('title', 'image');
        if (isset($category_id)){
            $sliders = $sliders->where('service_category_id', $category_id);
        }
        $sliders = $sliders->get();
        $sliders = $sliders->filter(function ($item){
            $item->image =  showSliderImage($item->image);
            return $item;
        });
        return $sliders->toArray();
    }
}