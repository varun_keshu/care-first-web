<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\CmsPageRepositoryInterface;
use App\Models\Faqs;
use App\Models\Pages;

class CmsPageRepository implements CmsPageRepositoryInterface
{
    public function getPageDetails($page_type)
    {
        return Pages::where('page_type', $page_type)->select('title', 'content')->first();
    }

    public function getFaqCategories()
    {
        $returnData = [];
        $faq_categories = Faqs::select('category')->groupBy('category')->get();
        foreach ($faq_categories as $faq_detail) {
            $returnData[] = ['id' => $faq_detail->category, 'title' => Faqs::CATEGORIES[$faq_detail->category]];
        }
        return $returnData;
    }

    public function getFaqsByCategories($category)
    {
        $returnData = [];
        $faqs = Faqs::where('category', $category)->select('id', 'question', 'answer')->orderBy('sort_order', 'asc')->get();
        foreach ($faqs as $faq) {
            $returnData[] = [
                'question' => $faq->question,
                'answer' => $faq->answer,
            ];
        }
        return $returnData;
    }
}