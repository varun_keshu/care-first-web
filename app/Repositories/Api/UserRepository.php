<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\UserRepositoryInterface;
use App\Models\User;
use App\Models\UserServices;
use App\Models\VendorUnavailability;
use App\Models\VendorWorkSchedule;
use App\Services\SmsService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserRepository implements UserRepositoryInterface
{
    protected $smsService;

    public function __construct(SmsService $smsService)
    {
        $this->smsService = $smsService;
    }

    public function validateContactNumber($request)
    {
        if (isset($request->all()['user_id'])) {
            $validator = Validator::make($request->all(), [
                'contact_number' => 'required|regex:/[0-9]{10}/|digits:10|unique:users,contact_number,' . $request->all()['user_id'] . ',id,deleted_at,NULL',
            ], [
                'contact_number.unique' => "This number already in use try with another number"
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'contact_number' => 'required|regex:/[0-9]{10}/|digits:10',
            ]);
        }
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function sendOtp($request)
    {
        $requestData = $request->except("_token");
        $otp = random_int(pow(10, 4 - 1), pow(10, 4) - 1);
        $requestData['otp_code'] = $otp;
        $existUser = User::where('contact_number', $requestData['contact_number'])->first();
        if (isset($existUser)) {
            $existUser->update(['otp_code' => $otp]);
            $message = 'Use ' . $otp . ' as your verification code on Care First ' . date('d M Y');
            $smsResponse = $this->smsService->send($message, $requestData['contact_number']);
            if (!$smsResponse['status']) {
                return ['success' => false, 'message' => $smsResponse['message']];
            }
        } else {
            return ['success' => false, 'message' => "Sorry! Your number was not registered!"];
        }
        return ['success' => true, 'data' => $existUser];
    }

    public function verifyOtp($request)
    {
        $requestData = $request->except("_token");
        $token = str_random(15);
        $user = User::where('id', $requestData['user_id'])->first();
        if (!isset($user)) {
            return ['success' => false, "message" => "User not exist!"];
        } elseif (($user['otp_code'] == $requestData['otp_code']) || ("1234" == $requestData['otp_code'])) {
            if (isset($requestData['device_token'])) {
                User::where('device_token', $requestData['device_token'])->update(['device_token' => null]);
            }

            $user->access_token = $token;
            $user->otp_code = null;
            $user->device_token = isset($requestData['device_token']) ? $requestData['device_token'] : null;
            $user->login_attempt_count = $user->login_attempt_count + 1;
            $user->save();
            return ['success' => true, "data" => ['access_token' => $token, 'user_type' => $user->user_type], 'user_id' => $user->id];
        } else {
            return ['success' => false, "message" => "You entered wrong OTP. Please enter a correct OTP."];
        }
    }

    public function sendUpdateNumberOTP($request)
    {
        $requestData = $request->all();
        $otp = random_int(pow(10, 4 - 1), pow(10, 4) - 1);
        $requestData['otp_code'] = $otp;
        $user = User::where('id', $requestData['user_id'])->first();
        if ($requestData['contact_number'] == $user->contact_number){
            return ['success' => false, 'message' => "You already logged in by this number, try with another number."];
        }
        $user->update(['otp_code' => $otp]);
        $message = 'Use ' . $otp . ' as your verification code for update contact number on Care First ' . date('d M Y');
        $smsResponse = $this->smsService->send($message, $requestData['contact_number']);
        if (!$smsResponse['status']) {
            return ['success' => false, 'message' => $smsResponse['message']];
        }
        return ['success' => true, 'data' => null];
    }

    public function updateContactNumberVerification($request)
    {
        $requestData = $request->except("_token");
        $user = User::where('id', $requestData['user_id'])->first();
        if (!isset($user)) {
            return ['success' => false, "message" => "User not exist!"];
        } elseif (($user['otp_code'] == $requestData['otp_code']) || ("1234" == $requestData['otp_code'])) {
            $user->contact_number = $requestData['contact_number'];
            $user->otp_code = null;
            $user->save();
            return ['success' => true];
        } else {
            return ['success' => false, "message" => "You entered wrong OTP. Please enter a correct OTP."];
        }
    }

    public function getUserDetail($request)
    {
        $requestData = $request->except("_token");
        $user = User::with(['userServiceAreas.userServices' => function ($q) use ($requestData) {
            $q->where('user_id', $requestData['user_id']);
        }])->where('id', $requestData['user_id'])->first();
        $user['image'] = isset($user['image']) ? showUserImage($user['image']) : "";
        $serviceAreas = [];
        foreach ($user->userServiceAreas as $userServiceArea) {
            $services = [];
            foreach ($userServiceArea->userServices as $userService){
                $services[] = $userService->serviceDetail->title;
            }
            $serviceAreas[] = [
                'title' => $userServiceArea->serviceArea->title,
                'services' => $services
            ];
        }
        $user['service_areas'] = $serviceAreas;
        unset($user->userServiceAreas, $user->updated_at, $user->deleted_at);
        return $user;
    }

    public function getUserDetailById($user_id)
    {
        $user = User::with(['userServiceAreas.userServices' => function ($q) use ($user_id) {
            $q->where('user_id', $user_id);
        }])->where('id', $user_id)->first();
        $user['image'] = isset($user['image']) ? showUserImage($user['image']) : "";
        $serviceAreas = [];
        foreach ($user->userServiceAreas as $userServiceArea) {
            $services = [];
            foreach ($userServiceArea->userServices as $userService){
                $services[] = $userService->serviceDetail->title;
            }
            $serviceAreas[] = [
                'title' => $userServiceArea->serviceArea->title,
                'services' => $services
            ];
        }
        $user['service_areas'] = $serviceAreas;
        unset($user->userServiceAreas, $user->updated_at, $user->deleted_at);
        return $user;
    }

    public function getVendorsListByManager($request)
    {
        $requestData = $request->except("_token");
        $users = User::where('manager_id', $requestData['user_id'])->select('id', 'name', 'contact_number', 'image')->get();
        $users = $users->filter(function ($item) {
            $item->image = isset($item->image) ? showUserImage($item->image) : "";
            return $item;
        });
        return $users;
    }

    public function validateUser($request)
    {
        $method = $request->route()->getActionMethod();
        if ($method == "updateUserProfile") {
            $validator = Validator::make($request->all(), [
                'contact_number' => 'required|regex:/[0-9]{10}/|digits:10,unique:users,contact_number,NULL,id,deleted_at,NULL',
            ], [
                'contact_number.unique' => "This number already used, try with different contact number.",
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'contact_number' => 'string|max:255|regex:/[0-9]{10}/|digits:10,unique:users,contact_number,' . $request->all()['user_id'] . ',id,deleted_at,NULL',
            ], [
                'contact_number.unique' => "This number already used, try with different contact number.",
            ]);
        }
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function updateUserProfile($request)
    {
        $requestData = $request->except("_token");
        $user = User::where('id', $requestData['user_id'])->first();
        $requestData['name'] = isset($requestData['name']) ? $requestData['name'] : null;
        if (isset($requestData['image'])) {
            $path = storage_path(User::IMG_PATH);
            if (isset($user['image'])) {
                $imagePath = $path . $user['image'];
                @unlink($imagePath);
            }
            $extension = 'png';
            $filename_to_store = rand(1, 100000) . "." . $extension;
            file_put_contents($path . $filename_to_store, base64_decode($requestData['image']));
            $requestData['image'] = $filename_to_store;
        }
        $user->update($requestData);
        return true;
    }

    public function getVendorList($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        $vendors = User::where('manager_id', $requestData['user_id'])->where('user_type', 'vendor')->latest()->get();
        foreach ($vendors as $vendor){
            $areas = [];
            foreach ($vendor->userServiceAreas as $userServiceArea){
                $areas[] = $userServiceArea->serviceArea->title;
            }
            $vendorData = [
                'id' => $vendor->id,
                'name' => $vendor->name,
                'contact_number' => $vendor->contact_number,
                'user_type' => $vendor->user_type,
                'pending_charges' => $vendor->pending_charges,
                'has_complete_profile' => $vendor->has_complete_profile,
                'image' => isset($vendor->image) ? showUserImage($vendor->image) : "",
                'created_at' => formatDate($vendor->created_at, 'd M, Y h:i A'),
                'service_area' => implode(', ', $areas),
                'average_ratings' => round(UserServices::where('user_id', $vendor->id)->where('average_ratings_value', '>', 0)->avg('average_ratings_value')),
                'vendor_work_schedule' => [],
                'user_services' => [],
            ];
            foreach ($vendor->vendorWorkSchedule as $vendorWorkSchedule){
                $vendorData['vendor_work_schedule'][] = [
                    'id' => $vendorWorkSchedule->id,
                    'week_day' => $vendorWorkSchedule->week_day,
                    'day_title' => VendorWorkSchedule::WEEK_DAYS[$vendorWorkSchedule->week_day],
                    'start_time' => formatDate($vendorWorkSchedule->start_time, "h:i A"),
                    'end_time' => formatDate($vendorWorkSchedule->end_time, "h:i A"),
                    'is_available' => ($vendorWorkSchedule->is_available == 1) ? true : false,
                ];
            }
            $userServices = UserServices::where('user_id', $vendor->id)
                ->join('service_areas', 'user_services.service_area_id', '=', 'service_areas.id')
                ->select('service_id', DB::raw('sum(total_jobs_count) as total_jobs_count'), DB::raw('sum(completed_jobs_count) as completed_jobs_count'),
                    DB::raw('sum(jobs_count_with_issues) as jobs_count_with_issues'), DB::raw('sum(total_ratings_count) as total_ratings_count'),
                    DB::raw('avg(average_ratings_value) as average_ratings_value'), DB::raw('group_concat(service_areas.title) as service_area'))
                ->groupBy('service_id')->get();
            $userServices = $userServices->filter(function ($item){
                $item->service = $item->serviceDetail->title;
                $item->average_ratings_value = round($item->average_ratings_value);
                $item->image = isset($item->serviceDetail->image) ? showServiceImage($item->serviceDetail->image) : "";
                unset($item->serviceDetail);
                return $item;
            });
            $vendorData['user_services'] = $userServices->toArray();
            $returnData[] = $vendorData;
        }
        return $returnData;
    }

    public function getVendorWorkSchedule($request)
    {
        $requestData = $request->except("_token");
        $data = VendorWorkSchedule::where('vendor_id', $requestData['user_id'])->select('week_day', 'start_time', 'end_time', 'is_available')->get();
        $data = $data->filter(function ($item) {
            $item->day_title = VendorWorkSchedule::WEEK_DAYS[$item->week_day];
            $item->start_time = isset($item->start_time) ? formatDate(Carbon::parse($item->start_time), "h:i A") : null;
            $item->end_time = isset($item->end_time) ? formatDate(Carbon::parse($item->end_time), "h:i A") : null;
            $item->is_available = ($item->is_available == 1) ? true : false;
            return $item;
        });
        return $data;
    }

    public function getVendorUnavailabilityList($request)
    {
        $requestData = $request->except("_token");
        $data = VendorUnavailability::whereNotNull('id')->whereDate('date', ">", Carbon::today()->toDateString())->select('id', 'vendor_id', 'date', 'reason');
        if (isset($requestData['date_range']) && !empty($requestData['date_range'])) {
            $startDate = date('Y-m-d', strtotime($requestData['date_range']['from']));
            $endDate = date('Y-m-d', strtotime($requestData['date_range']['to']));
            $data = $data->whereBetween('date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if ($requestData['user_type'] == "vendor") {
            $data = $data->where('vendor_id', $requestData['user_id']);
        } else {
            $data = $data->whereHas('vendorDetails', function ($q) use ($requestData) {
                $q->where('manager_id', $requestData['user_id']);
            });
            if (isset($requestData['by_vendors']) && !empty($requestData['by_vendors'])) {
                $data = $data->whereIn('vendor_id', $requestData['by_vendors']);
            }
        }
        $data = $data->orderBy('date', "ASC")->get();
        if ($requestData['user_type'] == "vendor") {
            $data = $data->filter(function ($item) {
                $item->date = formatDate($item->date, "d M, Y");
                $item->vendor_name = $item->vendorDetails->name;
                unset($item->vendor_id, $item->vendorDetails);
                return $item;
            });
            return $data->toArray();
        } else {
            $data = $data->groupBy('date');
            $returnData = [];
            foreach ($data as $date => $unavailability_list) {
                $leaveData = [];
                foreach ($unavailability_list as $unavailability_data) {
                    $leaveData[] = [
                        'id' => $unavailability_data->id,
                        'vendor' => $unavailability_data->vendorDetails->name,
                        'reason' => $unavailability_data->reason,
                    ];
                }
                $returnData[] = ['title' => formatDate($date, "d M, Y"), 'data' => $leaveData];
            }
            return $returnData;
        }
    }

    public function storeVendorWorkSchedule($request)
    {
        $requestData = $request->except("_token");
        VendorWorkSchedule::where('vendor_id', $requestData['user_id'])->delete();
        foreach ($requestData['work_schedules'] as $work_schedule) {
            $data = [
                'vendor_id' => $requestData['user_id'],
                'week_day' => $work_schedule['week_day'],
                'start_time' => isset($work_schedule['start_time']) ? Carbon::parse($work_schedule['start_time'])->toTimeString() : null,
                'end_time' => isset($work_schedule['end_time']) ? Carbon::parse($work_schedule['end_time'])->toTimeString() : null,
                'is_available' => ($work_schedule['is_available']) ? 1 : 0,
            ];
            VendorWorkSchedule::create($data);
        }
        return true;
    }

    public function storeVendorUnavailability($request)
    {
        $requestData = $request->except("_token");
        $dates = CarbonPeriod::create($requestData['start_date'], $requestData['end_date']);
        foreach ($dates as $date) {
            $existData = VendorUnavailability::where('vendor_id', $requestData['user_id'])->where('date', formatDate($date, "Y-m-d"))->first();
            if (isset($existData)) {
                $existData->update(['reason' => $requestData['reason']]);
            } else {
                VendorUnavailability::create([
                    'vendor_id' => $requestData['user_id'],
                    'date' => formatDate($date, "Y-m-d"),
                    'reason' => $requestData['reason'],
                ]);
            }
        }
        return true;
    }

    public function cancelVendorUnavailability($request)
    {
        $requestData = $request->except("_token");
        VendorUnavailability::where('id', $requestData['id'])->forceDelete();
        return true;
    }
}
