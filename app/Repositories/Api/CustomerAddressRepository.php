<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\CustomerAddressRepositoryInterface;
use App\Models\City;
use App\Models\CustomerAddresses;
use App\Models\ServiceAreas;
use Illuminate\Support\Facades\Validator;

class CustomerAddressRepository implements CustomerAddressRepositoryInterface
{
    public function getAddressList($request)
    {
        $requestData = $request->except("__token");
        $addresses = CustomerAddresses::where('customer_id', $requestData['customer_id'])->get();
        $addresses = $addresses->filter(function ($item) {
            $item->address = getAddressString($item);
            $item->is_default = ($item->is_default == 1) ? true : false;
            unset($item->customer_id, $item->address_line_2, $item->created_at, $item->updated_at, $item->deleted_at);
            return $item;
        });
        return $addresses->toArray();
    }

    public function validateAddress($request)
    {
        $validator = Validator::make($request->all(), [
            'flat_or_house_number' => 'required',
            'address_type' => 'required',
            'address_line_1' => 'required',
            'landmark' => 'required',
            'zip_code' => 'required|numeric',
            'city' => 'required',
            'service_area_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function storeAddress($request)
    {
        $requestData = $request->all();
        $existCustomerAddress = CustomerAddresses::where('customer_id', $requestData['customer_id'])->first();

        $service_area = ServiceAreas::where('id', $requestData['service_area_id'])->first();
        if (!isset($requestData['google_map_address'])){
            $address = $requestData['flat_or_house_number'].','.$requestData['address_line_1'].','.$requestData['landmark'].','.$requestData['city'].'-'.$requestData['zip_code'];
            $googleMapData = getGoogleMapData($address);
            $requestData['latitude'] = $googleMapData['latitude'];
            $requestData['longitude'] = $googleMapData['longitude'];
        }
        $requestData['service_area'] = $service_area['title'];
        $requestData['title'] = ($requestData['address_type'] == "home") ? "Home" : (($requestData['address_type'] == "office") ? "Office" : $requestData['title']);
        $is_default_address = isset($existCustomerAddress) ? 0 : 1;
        if (($is_default_address == 0) && ($requestData['is_default'])){
            $is_default_address = 1;
        }
        $requestData['is_default'] = $is_default_address;
        if ($requestData['is_default'] == 1){
            CustomerAddresses::where('customer_id', $requestData['customer_id'])->update(['is_default' => 0]);
        }
        CustomerAddresses::create($requestData);
        return true;
    }

    public function getAddressDetail($request)
    {
        $requestData = $request->all();
        $customer_address = CustomerAddresses::where('id', $requestData['address_id'])->first()->toArray();
        unset($customer_address['created_at'], $customer_address['updated_at'], $customer_address['deleted_at']);
        return $customer_address;
    }

    public function updateAddress($request)
    {
        $requestData = $request->all();
        $customer_address = CustomerAddresses::where('id', $requestData['address_id'])->first();
        if ($requestData['is_default']){
            $requestData['is_default'] = 1;
            CustomerAddresses::where('id', "!=", $requestData['address_id'])->where('customer_id', $requestData['customer_id'])->update(['is_default' => 0]);
        }else{
            $requestData['is_default'] = 0;
        }
        $service_area = ServiceAreas::where('id', $requestData['service_area_id'])->first();
        if (!isset($requestData['google_map_address'])){
            $address = $requestData['flat_or_house_number'].','.$requestData['address_line_1'].','.$requestData['landmark'].','.$requestData['city'].'-'.$requestData['zip_code'];
            $googleMapData = getGoogleMapData($address);
            $requestData['latitude'] = $googleMapData['latitude'];
            $requestData['longitude'] = $googleMapData['longitude'];
        }
        $requestData['service_area'] = $service_area['title'];
        $requestData['title'] = ($requestData['address_type'] == "home") ? "Home" : (($requestData['address_type'] == "office") ? "Office" : $requestData['title']);
        $customer_address->update($requestData);
        return true;
    }

    public function deleteAddress($request)
    {
        $requestData = $request->all();
        CustomerAddresses::destroy($requestData['address_id']);
        return true;
    }

    public function getServiceAreas()
    {
        $returnData = [];
        $cities = City::get();
        foreach ($cities as $city){
            $returnData[$city->title] = [];
            foreach ($city->relatedServiceAreas as $relatedServiceArea){
                $returnData[$city->title][] = [
                    'id' => $relatedServiceArea->id,
                    'title' => $relatedServiceArea->title,
                    'zip_code' => $relatedServiceArea->zip_code,
                ];
            }
        }
        return $returnData;
    }
}