<?php

namespace App\Repositories\Api;

use App\Events\CreateBookingEvent;
use App\Interfaces\Api\PackagesRepositoryInterface;
use App\Models\Booking;
use App\Models\CustomerAddresses;
use App\Models\Package;
use App\Models\PackageSubscription;
use App\Models\UserServices;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PackagesRepository implements PackagesRepositoryInterface
{
    public function getPackagesList($request)
    {
        $packages = Package::all();
        $packages = $packages->filter(function ($item) {
            $item->service_title = $item->serviceDetails->title;
            $item->show_quantity_option = isset($item->serviceDetails->show_quantity_option) && ($item->serviceDetails->show_quantity_option == 1) ? true : false;
            unset($item->created_at, $item->updated_at, $item->deleted_at, $item->serviceDetails);
            return $item;
        });
        return $packages->toArray();
    }

    public function getCustomerSubscriptionsList($request)
    {
        $requestData = $request->all();
        $returnData = [];
        $subscriptionPackages = PackageSubscription::where('customer_id', $requestData['customer_id'])->where('payment_status', 1)->get();
        foreach ($subscriptionPackages as $subscriptionPackage) {
            $data = [
                'id' => $subscriptionPackage->id,
                'title' => $subscriptionPackage->title,
                'description' => $subscriptionPackage->description,
                'razorpay_payment_id' => $subscriptionPackage->razorpay_payment_id,
                'price' => $subscriptionPackage->price,
                'total_amount' => $subscriptionPackage->total_amount,
                'payment_method' => $subscriptionPackage->payment_method,
                'customer_address_details' => $subscriptionPackage->customer_address_details,
                'service' => $subscriptionPackage->serviceDetails->title,
                'show_quantity_option' => ($subscriptionPackage->serviceDetails->show_quantity_option == 1) ? true : false,
                'quantity' => $subscriptionPackage->quantity,
                'duration' => $subscriptionPackage->duration,
                'duration_unit' => $subscriptionPackage->duration_unit,
                'service_count' => $subscriptionPackage->service_count,
                'used_count' => $subscriptionPackage->used_count,
                'service_start_date' => formatDate($subscriptionPackage->service_start_date, "d M, Y"),
                'next_service_start_date' => isset($subscriptionPackage->next_service_start_date) ? formatDate($subscriptionPackage->next_service_start_date, "d M, Y") : null,
                'service_time' => $subscriptionPackage->service_time,
                'is_expired' => ($subscriptionPackage->is_expired == 1) ? true : false,
            ];
            $returnData[] = $data;
        }
        return $returnData;
    }

    public function purchasePackage($requestData)
    {
        DB::beginTransaction();
        try {
            $existPurchasePackage = PackageSubscription::where('package_id', $requestData['package_id'])->where('payment_status', 1)->where('is_expired', 0)->where('customer_address_id', $requestData['customer_address_id'])->first();
            if (isset($existPurchasePackage)){
                return ['status' => false, 'message' => "Please use different address, because already used package by this address."];
            }
            $random_key = str_random(10);
            $package = Package::where('id', $requestData['package_id'])->first();
            $address = CustomerAddresses::where('id', $requestData['customer_address_id'])->first();
            $addressArray = [
                'id' => $address->id,
                'title' => $address->title,
                'flat_or_house_number' => $address->flat_or_house_number,
                'landmark' => $address->landmark,
                'address_line_1' => $address->address_line_1,
                'service_area_id' => $address->service_area_id,
                'service_area' => $address->service_area,
                'google_map_address' => $address->google_map_address,
                'city' => $address->city,
                'zip_code' => $address->zip_code,
                'latitude' => $address->latitude,
                'longitude' => $address->longitude,
            ];
            $quantity = $requestData['quantity'] ?? 1;
            $packageData = [
                'customer_id' => $requestData['customer_id'],
                'customer_address_id' => $requestData['customer_address_id'],
                'package_id' => $requestData['package_id'],
                'unique_key' => $random_key,
                'title' => $package['title'],
                'description' => $package['description'],
                'price' => $package['price'],
                'quantity' => $quantity,
                'total_amount' => $package['price'] * $quantity,
                'customer_address_details' => $addressArray,
                'service_id' => $package->service_id,
                'service_category_id' => $package->service_category_id,
                'duration' => $package->duration,
                'duration_unit' => $package->duration_unit,
                'service_count' => $package->service_count,
                'used_count' => 0,
                'priority' => "low",
                'service_start_date' => Carbon::parse($requestData['service_date'])->toDateString(),
                'next_service_start_date' => Carbon::parse($requestData['service_date'])->toDateString(),
                'service_time' => Carbon::parse($requestData['service_time'])->toTimeString(),
                'notes' => isset($requestData['notes']) ? $requestData['notes'] : null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            $packageSubscription = PackageSubscription::create($packageData);
            DB::commit();
            return ['status' => true, 'package_subscription_id' => $packageSubscription->id];
        } catch (\Exception $e) {
            DB::rollback();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function successPackageSubscription($package_subscription_id, $requestData)
    {
        DB::beginTransaction();
        try {
            $package_subscription = PackageSubscription::where('id', $package_subscription_id)->first();
            $userServiceData = UserServices::whereHas('relatedUser', function ($q) {
                $q->where('user_type', 'manager');
            })->where('service_id', $package_subscription->service_id)->where('service_area_id', $package_subscription->customer_address_details['service_area_id'])->first();

            $servicePrice = isset($package_subscription->serviceDetails->discounted_price_details[$package_subscription->priority]) ? $package_subscription->serviceDetails->discounted_price_details[$package_subscription->priority] : $package_subscription->serviceDetails->price_details[$package_subscription->priority];
            $data = [
                'customer_id' => $requestData['customer_id'],
                'unique_booking_key' => $package_subscription->unique_key,
                'razorpay_payment_id' => $package_subscription->razorpay_payment_id,
                'razorpay_order_id' => $package_subscription->razorpay_order_id,
                'service_id' => $package_subscription->service_id,
                'service_category_id' => $package_subscription->service_category_id,
                'manager_id' => isset($userServiceData) ? $userServiceData->user_id : null,
                'package_subscription_id' => $package_subscription->id,
                'customer_address_id' => $package_subscription->customer_address_id,
                'service_area_id' => $package_subscription->customer_address_details['service_area_id'],
                'quantity' => $package_subscription->quantity,
                'priority' => $package_subscription->priority,
                'service_price' => $servicePrice,
                'price' => $servicePrice * $package_subscription->quantity,
                'service_status' => 1,
                'payment_status' => 1,
                'payment_method' => $package_subscription->payment_method,
                'payment_gateway_response' => $package_subscription->payment_gateway_response,
                'address_details' => $package_subscription->customer_address_details,
                'service_date' => $package_subscription->next_service_start_date,
                'service_time' => $package_subscription->service_time,
                'notes' => $package_subscription->notes,
            ];
            $data['commission_fee'] = getServiceCommissionValue($package_subscription->serviceDetails->commission_value, $data['price']) * $package_subscription->quantity;
            $data['vendor_earning_price'] = $data['price'] - $data['commission_fee'];
            $data['total_amount'] = $data['price'];
            $booking = Booking::create($data);
            $eventData = [
                'booking_id' => $booking->id,
                'user_type' => "system",
                'type' => "subscription_package",
            ];
            event(new CreateBookingEvent($eventData));
            DB::commit();

            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }
}