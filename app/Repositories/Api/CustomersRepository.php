<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\CustomersRepositoryInterface;
use App\Models\Customers;
use App\Services\SmsService;
use Illuminate\Support\Facades\Validator;

class CustomersRepository implements CustomersRepositoryInterface
{
    protected $smsService;

    public function __construct(SmsService $smsService)
    {
        $this->smsService = $smsService;
    }

    public function validateContactNumber($request)
    {
        if (isset($request->all()['customer_id'])) {
            $validator = Validator::make($request->all(), [
                'contact_number' => 'required|regex:/[0-9]{10}/|digits:10|unique:customers,contact_number,' . $request->all()['customer_id'] . ',id,deleted_at,NULL',
            ], [
                'contact_number.unique' => "This number already in use try with another number"
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'contact_number' => 'required|regex:/[0-9]{10}/|digits:10',
            ]);
        }
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function sendOtp($request)
    {
        $requestData = $request->all();
        $otp = random_int(pow(10, 4 - 1), pow(10, 4) - 1);
        $requestData['otp_code'] = $otp;
        $existCustomer = Customers::where('contact_number', $requestData['contact_number'])->first();
        if (!isset($existCustomer)) {
            $customer = Customers::create($requestData);
        } else {
            $existCustomer->update(['otp_code' => $otp]);
            $customer = $existCustomer;
        }
        $message = 'Use ' . $otp . ' as your verification code on Care First ' . date('d M Y');
        $smsResponse = $this->smsService->send($message, $requestData['contact_number']);
        if (!$smsResponse['status']) {
            return ['success' => false, 'message' => $smsResponse['message']];
        }
        return ['success' => true, 'data' => $customer];
    }

    public function resendOtp($request)
    {
        $requestData = $request->all();
        $otp = random_int(pow(10, 4 - 1), pow(10, 4) - 1);
        $requestData['otp_code'] = $otp;
        $existCustomer = Customers::where('contact_number', $requestData['contact_number'])->first();
        $existCustomer->update(['otp_code' => $otp]);
        $message = 'Use ' . $otp . ' as your verification code on Care First ' . date('d M Y');
        $smsResponse = $this->smsService->send($message, $requestData['contact_number']);
        if (!$smsResponse['status']) {
            return ['success' => false, 'message' => $smsResponse['message']];
        }
        return ['success' => true, 'data' => $existCustomer];
    }

    public function verifyOtp($request)
    {
        $requestData = $request->all();
        $token = str_random(15);
        $customer = Customers::where('id', $requestData['customer_id'])->first();
        if (!isset($customer)) {
            return ['success' => false, "message" => "Customer not exist!"];
        } elseif ($customer['otp_code'] == $requestData['otp_code']) {
            if (isset($requestData['device_token'])) {
                Customers::where('device_token', $requestData['device_token'])->update(['device_token' => null]);
            }

            $customer->access_token = $token;
            $customer->otp_code = null;
            $customer->status = 1;
            $customer->device_token = isset($requestData['device_token']) ? $requestData['device_token'] : null;
            $customer->login_attempt_count = $customer->login_attempt_count + 1;
            $customer->save();

            $customer->image = isset($customer->image) ? showCustomerImage($customer->image) : "";
            return ['success' => true, "data" => ['access_token' => $token, 'customer_details' => $customer]];
        } else {
            return ['success' => false, "message" => "You entered wrong OTP. Please enter a correct OTP."];
        }
    }

    public function sendUpdateNumberOTP($request)
    {
        $requestData = $request->all();
        $otp = random_int(pow(10, 4 - 1), pow(10, 4) - 1);
        $requestData['otp_code'] = $otp;
        $customer = Customers::where('id', $requestData['customer_id'])->first();
        if ($requestData['contact_number'] == $customer->contact_number){
            return ['success' => false, 'message' => "You already logged in by this number, try with another number."];
        }
        $customer->update(['otp_code' => $otp]);
        $message = 'Use ' . $otp . ' as your verification code for update contact number on Care First ' . date('d M Y');
        $smsResponse = $this->smsService->send($message, $requestData['contact_number']);
        if (!$smsResponse['status']) {
            return ['success' => false, 'message' => $smsResponse['message']];
        }
        return ['success' => true];
    }

    public function updateContactNumberVerification($request)
    {
        $requestData = $request->all();
        $customer = Customers::where('id', $requestData['customer_id'])->first();
        if (!isset($customer)) {
            return ['success' => false, "message" => "Customer not exist!"];
        } elseif ($customer['otp_code'] == $requestData['otp_code']) {
            $customer->otp_code = null;
            $customer->contact_number = $requestData['contact_number'];
            $customer->save();
            $customer->image = isset($customer->image) ? showCustomerImage($customer->image) : "";
            return ['success' => true, "data" => $customer];
        } else {
            return ['success' => false, "message" => "You entered wrong OTP. Please enter a correct OTP."];
        }
    }

    public function getCustomerDetail($request)
    {
        $requestData = $request->all();
        $customer = Customers::where('id', $requestData['customer_id'])->first();
        $customer['image'] = isset($customer['image']) ? showCustomerImage($customer['image']) : "";
        $customer->address_list = $customer->relatedAddresses->filter(function ($item) {
            $item->address = getAddressString($item);
            unset($item->customer_id, $item->address_line_2, $item->created_at, $item->updated_at, $item->deleted_at);
            return $item;
        });
        unset($customer['updated_at'], $customer['deleted_at'], $customer['relatedAddresses']);
        return $customer->toArray();
    }

    public function getCustomerDetailById($id)
    {
        $customer = Customers::where('id', $id)->first();
        $customer['image'] = isset($customer['image']) ? showCustomerImage($customer['image']) : "";
        $customer->address_list = $customer->relatedAddresses->filter(function ($item) {
            unset($item->customer_id, $item->title, $item->flat_or_house_number, $item->address_line_1, $item->address_line_2, $item->landmark,
                $item->zip_code, $item->city, $item->created_at, $item->updated_at, $item->deleted_at);
            return $item;
        });
        unset($customer['updated_at'], $customer['deleted_at'], $customer['relatedAddresses']);
        return $customer->toArray();
    }

    public function updateCustomerProfile($request)
    {
        $requestData = $request->all();
        $customer = Customers::where('id', $requestData['customer_id'])->first();
        if ($customer['has_complete_profile'] == 0) {
            $requestData['has_complete_profile'] = 1;
        }
        $requestData['name'] = isset($requestData['name']) ? $requestData['name'] : null;
        $requestData['email'] = isset($requestData['email']) ? $requestData['email'] : null;
        if (isset($requestData['image'])) {
            $path = storage_path(Customers::IMG_PATH);
            if (isset($customer['image'])) {
                $imagePath = $path . $customer['image'];
                @unlink($imagePath);
            }
            $extension = 'png';
            $filename_to_store = rand(1, 100000) . "." . $extension;
            file_put_contents($path . $filename_to_store, base64_decode($requestData['image']));
            $requestData['image'] = $filename_to_store;
        }
        $customer->update($requestData);
        return true;
    }

    public function searchCustomers($request)
    {
        $requestData = $request->all();
        $customers = Customers::where('status', 1)->where('contact_number', 'like', '%' . $requestData['contact_number'] . '%')->select('id', 'name', 'contact_number', 'image', 'pending_charges')->get();
        $customers = $customers->filter(function ($item) {
            $item->address_list = $item->relatedAddresses->filter(function ($subItem) {
                $subItem->address = getAddressString($subItem);
                unset($subItem->customer_id, $subItem->address_line_2, $subItem->created_at, $subItem->updated_at, $subItem->deleted_at);
                return $subItem;
            });
            $item->image = isset($item->image) ? showCustomerImage($item->image) : "";
            unset($item->relatedAddresses);
            return $item;
        });
        return $customers->toArray();
    }

    public function createCustomerByManager($request)
    {
        $requestData = $request->all();
        $existCustomer = Customers::where('contact_number', $requestData['contact_number'])->first();
        if (!isset($existCustomer)) {
            $requestData['status'] = 1;
            $customer = Customers::create($requestData);
            return ['status' => true, 'customer_id' => $customer->id];
        }
        return ['status' => false, 'message' => "Customer already exist! Please try with different contact number."];
    }
}