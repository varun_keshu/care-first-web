<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07-01-2021
 * Time: 12:25 PM
 */

namespace App\Repositories\Admin;


use App\Interfaces\Admin\ClientRepositoryInterface;
use App\Models\Client;

class ClientRepository implements ClientRepositoryInterface
{
    public function getAllClients()
    {
        return Client::get();
    }

  public function storeClient($request)
    {
        $requestData = $request->all();
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData);
        }
        Client::create($requestData);
        return true;
    }

    public function uploadImage($requestData, $testimonial = null)
    {
        $file = $requestData['image'];
        $uploadPath = storage_path(Client::IMG_PATH);
        if (isset($testimonial)) {
            $imagePath = $uploadPath . $testimonial->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function getClientDetail($id)
    {
        return Client::where('id', $id)->first();
    }

    public function updateClient($request, $id)
    {
        $requestData = $request->all();
        $testimonial = self::getClientDetail($id);
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData,$testimonial);
        }
        $testimonial->update($requestData);
        return true;
    }

    public function deleteClient($id)
    {
        Client::destroy($id);
        return true;
    }
}
