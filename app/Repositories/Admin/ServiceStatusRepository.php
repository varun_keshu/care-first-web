<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\ServiceStatusRepositoryInterface;
use App\Models\ServiceStatus;

class ServiceStatusRepository implements ServiceStatusRepositoryInterface
{
    public function getAllServiceStatusList()
    {
        return ServiceStatus::get();
    }

    public function getStatusList()
    {
       return ServiceStatus::pluck('title', 'id')->toArray();

    }

    public function storeServiceStatus($request)
    {
        $requestData = $request->all();
        $requestData['update_completion_count'] = isset($requestData['update_completion_count']) ? 1 : 0;
        $requestData['update_issue_confirmation_count'] = isset($requestData['update_issue_confirmation_count']) ? 1 : 0;
        $requestData['visible_to_customer'] = isset($requestData['visible_to_customer']) ? 1 : 0;
        ServiceStatus::create($requestData);
        return true;
    }

    public function getServiceStatusDetail($id)
    {
        return ServiceStatus::where('id', $id)->first();
    }

    public function updateStatusCategory($request, $id)
    {
        $requestData = $request->all();
        $service_status = self::getServiceStatusDetail($id);
        if (isset($service_status)) {
            $requestData['update_completion_count'] = isset($requestData['update_completion_count']) ? 1 : 0;
            $requestData['update_issue_confirmation_count'] = isset($requestData['update_issue_confirmation_count']) ? 1 : 0;
            $requestData['visible_to_customer'] = isset($requestData['visible_to_customer']) ? 1 : 0;
            $service_status->update($requestData);
        }
        return true;
    }

    public function deleteStatusCategory($id)
    {
        ServiceStatus::destroy($id);
        return true;
    }
}
