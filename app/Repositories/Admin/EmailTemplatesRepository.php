<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\EmailTemplatesRepositoryInterface;
use App\Models\EmailTemplates;

class EmailTemplatesRepository implements EmailTemplatesRepositoryInterface
{
    public function getAllEmailTemplates()
    {
        return EmailTemplates::get();
    }

    public function getEmailIdentifierTitles($id = null)
    {
        $titles = [];
        $exist_record_actions = EmailTemplates::whereNotNull('id');
        if (isset($id)){
            $exist_record_actions = $exist_record_actions->where('id', '!=', $id);
        }
        $exist_record_actions = $exist_record_actions->pluck('identifier')->toArray();
        $actions = EmailTemplates::EMAIL_ACTION;
        foreach ($actions as $key => $value) {
            if (!in_array($key, $exist_record_actions)){
                $titles[$key] = $value;
            }
        }
        return $titles;
    }

    public function storeEmailTemplate($request)
    {
        $requestData = $request->all();
        EmailTemplates::create($requestData);
        return true;
    }

    public function getEmailTemplateDetail($id)
    {
        return EmailTemplates::where('id', $id)->first();
    }

    public function updateEmailTemplate($request, $id)
    {
        $requestData = $request->all();
        $email_template = self::getEmailTemplateDetail($id);
        if (isset($email_template)){
            $email_template->update($requestData);
        }
        return true;
    }

    public function deleteEmailTemplate($id)
    {
        EmailTemplates::destroy($id);
        return true;
    }
}
