<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\InsuranceRepositoryInterface;
use App\Models\Insurance;

class InsuranceRepository implements InsuranceRepositoryInterface
{
    public function getAllInsurance()
    {
        return Insurance::get();
    }

    public function storeInsurance($request)
    {
        $requestData = $request->all();
        Insurance::create($requestData);
        return true;
    }

    public function getInsuranceDetail($id)
    {
        return Insurance::where('id', $id)->first();
    }

    public function updateInsurance($request, $id)
    {
        $requestData = $request->all();
        $insurance = self::getInsuranceDetail($id);
        $insurance->update($requestData);
        return true;
    }

    public function deleteInsurance($id)
    {
        Insurance::destroy($id);
        return true;
    }
}
