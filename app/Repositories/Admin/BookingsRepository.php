<?php

namespace App\Repositories\Admin;

use App\Events\CreateBookingEvent;
use App\Events\VendorAcceptedBookedServiceEvent;
use App\Interfaces\Admin\BookingsRepositoryInterface;
use App\Models\Booking;
use App\Models\BookingStatusHistory;
use App\Models\CustomerAddresses;
use App\Models\ServiceCategory;
use App\Models\Services;
use App\Models\ServiceStatus;
use App\Models\User;
use App\Services\BookingService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class BookingsRepository implements BookingsRepositoryInterface
{
    protected $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    public function getAllBookings($request)
    {
        $modifyDate = Carbon::now()->subMinutes(30);
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Booking::where(function ($q) use ($modifyDate, $request){
            $q->where('created_at', '<', $modifyDate)->whereNull('vendor_id');
            if ($request->has('search') && $request->get('search') != '') {
                $search_key = $request->get('search');
                $q->whereHas('manager', function ($q) use ($search_key) {
                    $q->where('name', 'LIKE', "%{$search_key}%");
                })->orWhereHas('customer', function ($q) use ($search_key) {
                    $q->where('name', 'LIKE', "%{$search_key}%");
                    $q->orWhere('contact_number', 'LIKE', "%{$search_key}%");
                })->orWhereHas('vendor', function ($q) use ($search_key) {
                    $q->where('name', 'LIKE', "%{$search_key}%");
                });
            }
            if ($request->has('service_status') && $request->get('service_status') != '') {
                $q->where('service_status', $request->get('service_status'));
            }

            if ($request->has('payment_status') && $request->get('payment_status') == 'total') {
                $q->where('payment_status',1);}

            if ($request->has('payment_status') && $request->get('payment_status') == 'pending') {
                $q->where('is_vendor_paid',0)->where('payment_status',1);
               }

            if ($request->has('payment_status') && $request->get('payment_status') == 'completed') {
                $q->where('is_vendor_paid',1)->where('payment_status',1);
               }

            if ($request->has('booking_date') && $request->get('booking_date') != '') {
                $data_list = (explode(' - ', $request->get('booking_date')));
                $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[0])));
                $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[1])));
                $q->whereBetween('service_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
        })->orWhere(function ($q) use ($request) {
            $q->whereNotNull('vendor_id');
            if ($request->has('search') && $request->get('search') != '') {
                $search_key = $request->get('search');
                $q->whereHas('manager', function ($q) use ($search_key) {
                    $q->where('name', 'LIKE', "%{$search_key}%");
                })->orWhereHas('customer', function ($q) use ($search_key) {
                    $q->where('name', 'LIKE', "%{$search_key}%");
                    $q->orWhere('contact_number', 'LIKE', "%{$search_key}%");
                })->orWhereHas('vendor', function ($q) use ($search_key) {
                    $q->where('name', 'LIKE', "%{$search_key}%");
                });
            }
            if ($request->has('service_status') && $request->get('service_status') != '') {
                $q->where('service_status', $request->get('service_status'));}

            if ($request->has('payment_status') && $request->get('payment_status') == 'total') {
                $q->where('payment_status',1);}

            if ($request->has('payment_status') && $request->get('payment_status') == 'pending') {
                $q->where('is_vendor_paid',0)->where('payment_status',1);
            }

            if ($request->has('payment_status') && $request->get('payment_status') == 'completed') {
                $q->where('is_vendor_paid',1)->where('payment_status',1);
                }

            if ($request->has('booking_date') && $request->get('booking_date') != '') {
                $data_list = (explode(' - ', $request->get('booking_date')));
                $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[0])));
                $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[1])));
                $q->whereBetween('service_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
        });
        $count_total = $count_filter = $dataInput->count();

        $dataInput = $dataInput->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('customer', function ($booking_detail) {
            return $booking_detail->customer->contact_number;
        });
        $data_tables->EditColumn('manager', function ($booking_detail) {
            return isset($booking_detail->manager) ? $booking_detail->manager->name : "- - -";
        });
        $data_tables->EditColumn('vendor', function ($booking_detail) {
            return isset($booking_detail->vendor) ? $booking_detail->vendor->name : "- - -";
        });
        $data_tables->EditColumn('service_details', function ($booking_detail) {
            $str = $booking_detail->serviceDetails->title;
            $str .= "<br><b>Category : </b>" . $booking_detail->serviceCategoryDetails->title;
            return $str;
        });
        $data_tables->EditColumn('service_at', function ($booking_detail) {
            return formatDate($booking_detail->service_date . ' ' . $booking_detail->service_time);
        });
        $data_tables->EditColumn('booking_date', function ($booking_detail) {
            return formatDate($booking_detail->created_at);
        });
        $data_tables->EditColumn('status', function ($booking_detail) {
            return $booking_detail->serviceStatusDetail->title;
        });
        $data_tables->EditColumn('amount', function ($booking_detail) {
            return formatPrice($booking_detail->total_amount);
        });
        $data_tables->EditColumn('commission_fee', function ($booking_detail) {
            return formatPrice($booking_detail->commission_fee);
        });

        $data_tables->EditColumn('action', function ($booking_detail) {
            return view('admin.bookings.partials.actions', compact('booking_detail'))->render();
        });
        /*$data_tables->setRowClass(function ($transaction) {
            return "bg-success";
        });*/
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action', 'service_details']);
        return $data_tables->make(true);
    }

    public function createOrder($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            $random_key = Str::random(10);
            $service = Services::find($requestData['service_id']);
            $address = CustomerAddresses::where('id', $requestData['customer_address_id'])->select('id', 'title', 'flat_or_house_number', 'address_line_1', 'service_area_id',
                'landmark', 'service_area', 'google_map_address', 'city', 'zip_code', 'latitude', 'longitude')->first()->toArray();
            $data = [
                'customer_id' => $requestData['customer_id'],
                'unique_booking_key' => $random_key,
                'service_id' => $requestData['service_id'],
                'service_category_id' => $service['service_category_id'],
                'manager_id' => $requestData['manager_id'],
                'customer_address_id' => $requestData['customer_address_id'],
                'service_area_id' => $address['service_area_id'],
                'priority' => $requestData['priority'],
                'quantity' => 1,
                'service_price' => $requestData['price'],
                'price' => $requestData['price'],
                'commission_fee' => getServiceCommissionValue($service->commission_value, $requestData['price']),
                'total_amount' => $requestData['price'],
                'service_status' => 1,
                'notes' => isset($requestData['notes']) ? $requestData['notes'] : null,
                'address_details' => $address,
                'service_date' => Carbon::parse($requestData['service_date'])->toDateString(),
                'service_time' => Carbon::parse($requestData['service_time'])->toTimeString()
            ];
            $data['vendor_earning_price'] = $data['price'] - $data['commission_fee'];
            $bookedServiceDetail = Booking::create($data);
            $eventData = [
                'booking_id' => $bookedServiceDetail->id,
                'user_type' => "system"
            ];
            event(new CreateBookingEvent($eventData));
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getBookingDetails($booking_id)
    {
        return Booking::where('id', $booking_id)->first();
    }

    public function getBookingStatusHistory($booking)
    {
        $data = BookingStatusHistory::where('user_type', 'manager');
        if (isset($booking->parent_id)){
            $data = $data->whereIn('booking_id', [$booking->id, $booking->parent_id]);
        }else{
            $data = $data->where('booking_id', $booking->id);
        }
        return $data->get();
    }

    public function getAvailableVendorsList($booking_id)
    {
        $booking = self::getBookingDetails($booking_id);
        if (isset($booking) && is_null($booking->vendor_id)) {
            $vendors = $this->bookingService->getAvailableVendorList($booking);
            return ['status' => true, 'data' => $vendors];
        }
        return ['status' => false, "message" => "Invalid Request!"];
    }

    public function assignBookingToVendor($request, $booking_id)
    {
        $requestData = $request->except("_token");
        $booking = self::getBookingDetails($booking_id);
        if (isset($booking)) {
            if (is_null($booking->manager_id)) {
                $vendor = User::where('id', $requestData['vendor_id'])->select('id', 'manager_id')->first();
                $booking->manager_id = $vendor->manager_id;
            }
            $booking->vendor_id = $requestData['vendor_id'];
            $booking->service_status = 2;
            $booking->save();

            $eventData = [
                'user_type' => 'manager',
                'booking_id' => $booking->id
            ];
            event(new VendorAcceptedBookedServiceEvent($eventData));
        }
        return true;
    }

    public function getBookedData()
    {
        $data['open'] = $data['complete'] = $data['issue'] = [];
        $get = Booking::whereDate('service_date', Carbon::today())->get();
        foreach ($get as $getData) {
            $arrayData = [
                'id' => $getData['id'],
                'customer_contact' => $getData->customer->contact_number,
                'manager' => $getData->manager->name ?? "",
                'vendor_name' => $getData->vendor->name ?? "",
                'title' => $getData->serviceDetails->title,
                'category' => $getData->serviceCategoryDetails->title,
                'status' => $getData->serviceStatusDetail->state,
                'created_at' => $getData['created_at'],
            ];
            if (($getData['service_status'] < 6) && ($getData['is_cancelled_booking'] == 0)) {
                $data['open'][] = $arrayData;
            } else if ($getData['service_status'] >= 6) {
                $data['complete'][] = $arrayData;
            } else if ($getData['service_status'] > 6) {
                $data['issue'][] = $arrayData;
            }
        }
        return $data;
    }

    public function getCustomerDetails()
    {
        $data = [];
        $result = Booking::whereNotNull('vendor_id')->where('payment_status', '1')->get()
            ->groupby('vendor_id');
        foreach ($result as $res) {
            $data[] = [
                'vendor_name' => $res[0]->vendor->name,
                'contact_number' => $res[0]->vendor->contact_number,
                'payout' => $res->where('is_vendor_paid', 1)->sum('price'),
                'vendor_earning_price' => $res->sum('vendor_earning_price'),
            ];
        }
        return $data;
    }

    public function getCount()
    {
        $data = [
            'total' => Booking::count(),
            'open' => Booking::where('service_status', '<', '6')->where('is_cancelled_booking', 0)->count(),
            'completed' => Booking::where('service_status', '>=', '6')->count(),
            'cancelled' => Booking::where('is_cancelled_booking', 1)->count(),
            'issue' => Booking::where('service_status', '>', '6')->count()
        ];
        return $data;
    }

    public function getVendorList()
    {
        $vendors = User::where('user_type', 'vendor')
            ->pluck('name', 'id')->toArray();
        return $vendors;
    }

    public function getVendorBookings($request)
    {
        $dataInput = Booking::whereNotNull('vendor_id');
        if ($request->has('vendor_id') && ($request->get('vendor_id') != '')) {
            $dataInput = $dataInput->where('vendor_id', $request->get('vendor_id'));
        }
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('customer', function ($booking_detail) {
            return $booking_detail->customer->contact_number ?? "--";
        });
        $data_tables->EditColumn('manager', function ($booking_detail) {
            return isset($booking_detail->manager) ? $booking_detail->manager->name : "- - -";
        });
        $data_tables->EditColumn('vendor', function ($booking_detail) {
            return isset($booking_detail->vendor) ? $booking_detail->vendor->name : "- - -";
        });
        $data_tables->EditColumn('service_details', function ($booking_detail) {
            $str = $booking_detail->serviceDetails->title;
            $str .= "<br><b>Category : </b>" . $booking_detail->serviceCategoryDetails->title;
            return $str;
        });
        $data_tables->EditColumn('service_at', function ($booking_detail) {
            return formatDate($booking_detail->service_date . ' ' . $booking_detail->service_time);
        });
        $data_tables->EditColumn('booking_date', function ($booking_detail) {
            return formatDate($booking_detail->created_at);
        });
        $data_tables->EditColumn('status', function ($booking_detail) {
            return ServiceStatus::STATES[$booking_detail->serviceStatusDetail->state];
        });
        $data_tables->EditColumn('action', function ($booking_detail) {
            return view('admin.bookings.partials.actions', compact('booking_detail'))->render();
        });

        $data_tables->filter(function ($query) use ($request) {
            if ($request->has('vendor_id') && $request->get('vendor_id') != '') {
                $query->where('vendor_id', $request->get('vendor_id'));
            }
            if ($request->has('booking_date') && $request->get('booking_date') != '') {
                $data_list = (explode(' - ', $request->get('booking_date')));
                $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[0])));
                $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[1])));
                $query->whereBetween('service_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
        });
        $data_tables->rawColumns(['action', 'service_details']);
        return $data_tables->make(true);
    }

    public function getServiceList()
    {
        $service = Services::pluck('title', 'id')->toArray();
        return $service;
    }

    public function getServiceBookings($request)
    {
        $dataInput = Booking::whereNotNull('service_id');
        if ($request->has('service_id') && ($request->get('service_id') != '')) {
            $dataInput = $dataInput->where('service_id', $request->get('service_id'));
        }
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('customer', function ($booking_detail) {
            return $booking_detail->customer->contact_number ?? "--";
        });
        $data_tables->EditColumn('manager', function ($booking_detail) {
            return isset($booking_detail->manager) ? $booking_detail->manager->name : "- - -";
        });
        $data_tables->EditColumn('vendor', function ($booking_detail) {
            return isset($booking_detail->vendor) ? $booking_detail->vendor->name : "- - -";
        });
        $data_tables->EditColumn('service_details', function ($booking_detail) {
            $str = $booking_detail->serviceDetails->title;
            $str .= "<br><b>Category : </b>" . $booking_detail->serviceCategoryDetails->title;
            return $str;
        });
        $data_tables->EditColumn('service_at', function ($booking_detail) {
            return formatDate($booking_detail->service_date . ' ' . $booking_detail->service_time);
        });
        $data_tables->EditColumn('booking_date', function ($booking_detail) {
            return formatDate($booking_detail->created_at);
        });
        $data_tables->EditColumn('status', function ($booking_detail) {
            return ServiceStatus::STATES[$booking_detail->serviceStatusDetail->state];
        });
        $data_tables->EditColumn('action', function ($booking_detail) {
            return view('admin.bookings.partials.actions', compact('booking_detail'))->render();
        });

        $data_tables->filter(function ($query) use ($request) {
            if ($request->has('service_id') && $request->get('service_id') != '') {
                $query->where('service_id', $request->get('service_id'));
            }
            if ($request->has('booking_date') && $request->get('booking_date') != '') {
                $data_list = (explode(' - ', $request->get('booking_date')));
                $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[0])));
                $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[1])));
                $query->whereBetween('service_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
        });
        $data_tables->rawColumns(['action', 'service_details']);
        return $data_tables->make(true);
    }

    public function getCategoryList()
    {
        $category = ServiceCategory::pluck('title', 'id')->toArray();
        return $category;
    }

    public function getCategoryBookings($request)
    {

        $dataInput = Booking::whereNotNull('service_category_id');
        if ($request->has('service_category_id') && ($request->get('service_category_id') != '')) {
            $dataInput = $dataInput->where('service_category_id', $request->get('service_category_id'));
        }
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('customer', function ($booking_detail) {
            return $booking_detail->customer->contact_number ?? "--";
        });
        $data_tables->EditColumn('manager', function ($booking_detail) {
            return isset($booking_detail->manager) ? $booking_detail->manager->name : "- - -";
        });
        $data_tables->EditColumn('vendor', function ($booking_detail) {
            return isset($booking_detail->vendor) ? $booking_detail->vendor->name : "- - -";
        });
        $data_tables->EditColumn('service_details', function ($booking_detail) {
            $str = $booking_detail->serviceDetails->title;
            $str .= "<br><b>Category : </b>" . $booking_detail->serviceCategoryDetails->title;
            return $str;
        });
        $data_tables->EditColumn('service_at', function ($booking_detail) {
            return formatDate($booking_detail->service_date . ' ' . $booking_detail->service_time);
        });
        $data_tables->EditColumn('booking_date', function ($booking_detail) {
            return formatDate($booking_detail->created_at);
        });
        $data_tables->EditColumn('status', function ($booking_detail) {
            return ServiceStatus::STATES[$booking_detail->serviceStatusDetail->state];
        });
        $data_tables->EditColumn('action', function ($booking_detail) {
            return view('admin.bookings.partials.actions', compact('booking_detail'))->render();
        });

        $data_tables->filter(function ($query) use ($request) {
            if ($request->has('service_category_id') && $request->get('service_category_id') != '') {
                $query->where('service_category_id', $request->get('service_category_id'));
            }
            if ($request->has('booking_date') && $request->get('booking_date') != '') {
                $data_list = (explode(' - ', $request->get('booking_date')));
                $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[0])));
                $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[1])));
                $query->whereBetween('service_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
        });
        $data_tables->rawColumns(['action', 'service_details']);
        return $data_tables->make(true);
    }

    public function getPayoutReportsAjax($request)
    {
        $dataInput = Booking::where('payment_status', 1);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('customer', function ($booking_detail) {
            return $booking_detail->customer->contact_number ?? "--";
        });
        $data_tables->EditColumn('vendor', function ($booking_detail) {
            return isset($booking_detail->vendor) ? $booking_detail->vendor->name : "- - -";
        });
        $data_tables->EditColumn('service_details', function ($booking_detail) {
            $str = $booking_detail->serviceDetails->title;
            $str .= "<br><b>Category : </b>" . $booking_detail->serviceCategoryDetails->title;
            return $str;
        });

        $data_tables->EditColumn('service_at', function ($booking_detail) {
            return formatDate($booking_detail->service_date . ' ' . $booking_detail->service_time);
        });
        $data_tables->EditColumn('vendor_earning_price', function ($booking_detail) {
            return formatPrice($booking_detail->vendor_earning_price);
        });
        $data_tables->EditColumn('status', function ($booking_detail) {
            if ($booking_detail->is_vendor_paid == 1) {
                return "Completed";
            } else {
                return "Pending";
            }
        });
        $data_tables->EditColumn('action', function ($booking_detail) {
            return view('admin.bookings.partials.actions', compact('booking_detail'))->render();
        });
        $data_tables->filter(function ($query) use ($request) {
            if ($request->has('is_vendor_paid') && $request->get('is_vendor_paid') == "1") {
                $query->where('is_vendor_paid', $request->get('is_vendor_paid'));
            }
            if ($request->has('booking_date') && $request->get('booking_date') != '') {
                $data_list = (explode(' - ', $request->get('booking_date')));
                $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[0])));
                $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $data_list[1])));
                $query->whereBetween('service_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
        });
        $data_tables->rawColumns(['action', 'service_details']);
        return $data_tables->make(true);

    }

    public function getEarningDetails()
    {

        $result = Booking::whereNotNull('id')->where('payment_status', '1')->get();
        $data = [
            'vendor_earning_price' => $result->sum('vendor_earning_price'),
            'complete' => $result->where('is_vendor_paid', 1)->sum('vendor_earning_price'),
            'pending' => $result->where('is_vendor_paid', 0)->sum('vendor_earning_price')
        ];
        return $data;
    }

}
