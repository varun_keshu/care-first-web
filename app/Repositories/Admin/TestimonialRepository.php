<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\TestimonialRepositoryInterface;
use App\Models\Testimonials;

class TestimonialRepository implements TestimonialRepositoryInterface
{
    public function getAllTestimonials()
    {
        return Testimonials::get();
    }

    public function storeTestimonial($request)
    {
        $requestData = $request->all();
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData);
        }
        Testimonials::create($requestData);
        return true;
    }

    public function uploadImage($requestData, $testimonial = null)
    {
        $file = $requestData['image'];
        $uploadPath = storage_path(Testimonials::IMG_PATH);
        if (isset($testimonial)) {
            $imagePath = $uploadPath . $testimonial->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function getTestimonialDetail($id)
    {
        return Testimonials::where('id', $id)->first();
    }

    public function updateTestimonial($request, $id)
    {
        $requestData = $request->all();
        $testimonial = self::getTestimonialDetail($id);
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData,$testimonial);
        }
        $testimonial->update($requestData);
        return true;
    }

    public function deleteTestimonial($id)
    {
        Testimonials::destroy($id);
        return true;
    }
}
