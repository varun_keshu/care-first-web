<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\PackageRepositoryInterface;
use App\Interfaces\Admin\ServiceRepositoryInterface;
use App\Models\Package;
use App\Models\Services;
use Illuminate\Support\Facades\DB;

class PackageRepository implements PackageRepositoryInterface
{
    protected $serviceRepository;

    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    public function getAllData()
    {
        return Package::all();
    }

    public function getAll()
    {
        $requestData = Package::all();
        $data = [];
        foreach ($requestData as $allData) {
            $data[] = [
                'title' => $allData->title,
                'services' => $allData->serviceDetails->title,
                'service_category' => $allData->serviceCategoryDetails->title,
                'price' => $allData->price,
                'service_count' => $allData->service_count,
                'duration' => $allData->duration,
                'duration_unit' => $allData->duration_unit,
                'description' => $allData->description
            ];
        }
        return $data;
    }


    public function storePackageData($request)
    {
        $requestData = $request->all();
        $service = Services::where('id', $requestData['service_id'])->first();
        $requestData['service_category_id'] = $service->service_category_id;
        Package::create($requestData);
        return true;
    }

    public function getPackageDetail($id)
    {
        return Package::where('id', $id)->first();
    }

    public function updatePackageData($request, $id)
    {
        $requestData = $request->all();
        $annualPackage = self::getPackageDetail($id);
        if (isset($annualPackage)) {
            $service = Services::where('id', $requestData['service_id'])->first();
            $requestData['service_category_id'] = $service->service_category_id;
            $annualPackage->update($requestData);
        }
        return true;
    }

    public function deletePackageData($id)
    {
        Package::destroy($id);
        return true;
    }
}
