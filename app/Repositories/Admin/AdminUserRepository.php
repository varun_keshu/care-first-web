<?php
namespace App\Repositories\Admin;

use App\Events\NewAdminActivationEvent;
use App\Interfaces\Admin\AdminUserInterface;
use App\Models\Admins;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminUserRepository implements AdminUserInterface
{
    public function getAllAdmins()
    {
        return Admins::get();
    }

    public function storeAdmin(Request $request)
    {
        $requestData = $request->except('_token');
        $requestData['activation_token'] = Str::random(10);
        $requestData['status'] = 0;
        self::sendRegistrationEmail($requestData);
        return Admins::create($requestData);
    }

    private function sendRegistrationEmail($data)
    {
        $link = route('admin.set_password', $data['activation_token']);
        $emailData = ['name' => $data['name'], 'email' => $data['email'], 'link' => $link];
        event(new NewAdminActivationEvent($emailData));
    }

    public function getByActivationToken($token)
    {
        return Admins::where('activation_token', $token)->first();
    }

    public function updateProfile($request)
    {
        $requestData = $request->all();
        $currentAdmin = getCurrentAdmin();
        if (!isset($requestData['change_password'])){
            unset($requestData['password'], $requestData['password_confirmation']);
        }
        $currentAdmin->update($requestData);
        return true;
    }
}
