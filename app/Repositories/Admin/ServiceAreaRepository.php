<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\ServiceAreaRepositoryInterface;
use App\Models\City;
use App\Models\ServiceAreas;

class ServiceAreaRepository implements ServiceAreaRepositoryInterface
{
    public function getAllCitiesWithArea()
    {
        return City::get();
    }

    public function storeServiceAreasData($request)
    {
        $requestData = $request->all();
        $city = City::create($requestData);
        if (isset($requestData['areas'])) {
            self::storeServiceAreas($city->id, $requestData);
        }
        return true;
    }

    public function storeServiceAreas($city_id, $requestData)
    {
        foreach ($requestData['areas'] as $area_detail) {
            ServiceAreas::create([
                'city_id' => $city_id,
                'title' => $area_detail['title'],
                'zip_code' => $area_detail['zip_code']
            ]);
        }
        return true;
    }

    public function getCityDetail($id)
    {
        return City::where('id', $id)->first();
    }

    public function getServiceAreaDetail($id)
    {
        return ServiceAreas::where('id', $id)->first();
    }

    public function updateServiceAreasData($request, $id)
    {
        $requestData = $request->all();
        $city = self::getCityDetail($id);
        if (isset($city)) {
            $city->update($requestData);
            self::updateExistServiceAreas($requestData, $id);
            if (isset($requestData['areas'])) {
                self::storeServiceAreas($id, $requestData);
            }
        }
        return true;
    }

    public function updateExistServiceAreas($requestData, $id)
    {
        $updatedRecords = [];
        if (isset($requestData['exist_areas'])) {
            foreach ($requestData['exist_areas'] as $exist_area) {
                $updatedRecords[] = $exist_area['id'];
                ServiceAreas::where('id', $exist_area['id'])->update($exist_area);
            }
        }
        ServiceAreas::where('city_id', $id)->whereNotIn('id', $updatedRecords)->delete();
        return true;
    }

    public function deleteCity($id)
    {
        City::destroy($id);
        return true;
    }

    public function getServiceAreaTitleWithCity()
    {
        $returnData = [];
        $cities = City::get();
        foreach ($cities as $city){
            $returnData[$city->title] = [];
            foreach ($city->relatedServiceAreas as $serviceArea){
                $returnData[$city->title][$serviceArea->id] = $serviceArea->title;
            }
        }
        return $returnData;
    }
}
