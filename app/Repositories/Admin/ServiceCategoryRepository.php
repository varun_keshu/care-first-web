<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\ServiceCategoryRepositoryInterface;
use App\Models\IssueTitlesByCategory;
use App\Models\ServiceCategory;
use App\Models\ServiceInstructionByCategory;
use App\Models\ServiceProductsByCategory;
use App\Models\Services;

class ServiceCategoryRepository implements ServiceCategoryRepositoryInterface
{
    public function getAllServiceCategoriesTitles()
    {
        return ServiceCategory::pluck('title', 'id')->toArray();
    }

    public function getAllServiceCategories()
    {
        return ServiceCategory::get();

    }

    public function getAllServiceCategoriesForHome()
    {
        return ServiceCategory::whereHas('relatedServices')->get();
    }

    public function storeServiceCategory($request)
    {
        $requestData = $request->all();
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData);
        }
        if (isset($requestData['icon_image'])) {
            $requestData['icon_image'] = self::uploadIconImage($requestData);
        }
        if (isset($requestData['banner_image'])) {
            $requestData['banner_image'] = self::uploadBannerImage($requestData);
        }
        $serviceCategory = ServiceCategory::create($requestData);
        if (isset($requestData['service_instructions'])) {
            self::storeServiceInstructions($serviceCategory->id, $requestData);
        }
        if (isset($requestData['service_products'])) {
            self::storeServiceProducts($serviceCategory->id, $requestData);
        }
        if (isset($requestData['issue_titles'])) {
            self::storeServiceIssueTitles($serviceCategory->id, $requestData);
        }
        return true;
    }

    public function storeServiceInstructions($service_category_id, $requestData)
    {
        ServiceInstructionByCategory::where('service_category_id', $service_category_id)->delete();
        foreach ($requestData['service_instructions'] as $service_instruction) {
            ServiceInstructionByCategory::create([
                'service_category_id' => $service_category_id,
                'title' => $service_instruction['title'],
                'icon' => $service_instruction['icon']
            ]);
        }
        return true;
    }

    public function storeServiceProducts($service_category_id, $requestData)
    {
        foreach ($requestData['service_products'] as $service_product) {
            $image_name = self::uploadServiceProductImage($service_product['image']);
            ServiceProductsByCategory::create([
                'service_category_id' => $service_category_id,
                'title' => $service_product['title'],
                'image' => $image_name
            ]);
        }
        return true;
    }

    public function storeServiceIssueTitles($service_category_id, $requestData)
    {
        foreach ($requestData['issue_titles'] as $service_product) {
            IssueTitlesByCategory::create([
                'service_category_id' => $service_category_id,
                'title' => $service_product,
            ]);
        }
        return true;
    }

    public function uploadServiceProductImage($file, $exist_record = null)
    {
        $uploadPath = storage_path(ServiceProductsByCategory::IMG_PATH);
        if (isset($exist_record)) {
            $imagePath = $uploadPath . $exist_record->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function getServiceCategoryDetail($id)
    {
        return ServiceCategory::where('id', $id)->first();
    }

    public function updateServiceCategory($request, $id)
    {
        $requestData = $request->all();
        $service_category = self::getServiceCategoryDetail($id);
        if (isset($service_category)) {
            if (isset($requestData['image'])) {
                $requestData['image'] = self::uploadImage($requestData, $service_category);
            }
            if (isset($requestData['icon_image'])) {
                $requestData['icon_image'] = self::uploadIconImage($requestData, $service_category);
            }
            if (isset($requestData['banner_image'])) {
                $requestData['banner_image'] = self::uploadBannerImage($requestData, $service_category);
            }
            $service_category->update($requestData);
            if (isset($requestData['service_instructions'])) {
                self::storeServiceInstructions($id, $requestData);
            }
            self::updateExistServiceProducts($requestData, $id);
            if (isset($requestData['service_products'])) {
                self::storeServiceProducts($id, $requestData);
            }
            self::updateExistIssueTitles($requestData, $id);
            if (isset($requestData['issue_titles'])) {
                self::storeServiceIssueTitles($id, $requestData);
            }
        }
        return true;
    }

    public function updateExistServiceProducts($requestData, $id)
    {
        $updatedRecords = [];
        if (isset($requestData['exist_service_products'])) {
            foreach ($requestData['exist_service_products'] as $exist_service_product) {
                $updatedRecords[] = $exist_service_product['id'];
                $service_product_data = ServiceProductsByCategory::where('id', $exist_service_product['id'])->first();
                if (isset($exist_service_product['image'])) {
                    $exist_service_product['image'] = self::uploadServiceProductImage($exist_service_product['image'], $service_product_data);
                }
                $service_product_data->update($exist_service_product);
            }
        }
        $other_exist_records = ServiceProductsByCategory::where('service_category_id', $id)->whereNotIn('id', $updatedRecords)->get();
        foreach ($other_exist_records as $other_exist_record) {
            $imagePath = storage_path(ServiceProductsByCategory::IMG_PATH) . $other_exist_record->image;
            @unlink($imagePath);
            $other_exist_record->delete();
        }
        return true;
    }

    public function updateExistIssueTitles($requestData, $id)
    {
        $updatedRecords = [];
        if (isset($requestData['exist_issue_titles'])) {
            foreach ($requestData['exist_issue_titles'] as $exist_issue_title) {
                $updatedRecords[] = $exist_issue_title['id'];
                IssueTitlesByCategory::where('id', $exist_issue_title['id'])->update($exist_issue_title);
            }
        }
        IssueTitlesByCategory::where('service_category_id', $id)->whereNotIn('id', $updatedRecords)->delete();
        return true;
    }

    public function uploadImage($requestData, $service_category = null)
    {
        $file = $requestData['image'];
        $uploadPath = storage_path(ServiceCategory::IMG_PATH);
        if (isset($service_category)) {
            $imagePath = $uploadPath . $service_category->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadIconImage($requestData, $service_category = null)
    {
        $file = $requestData['icon_image'];
        $uploadPath = storage_path(ServiceCategory::ICON_IMG_PATH);
        if (isset($service_category)) {
            $imagePath = $uploadPath . $service_category->icon_image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadBannerImage($requestData, $service_category = null)
    {
        $file = $requestData['banner_image'];
        $uploadPath = storage_path(ServiceCategory::BANNER_IMG_PATH);
        if (isset($service_category)) {
            $imagePath = $uploadPath . $service_category->banner_image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function deleteServiceCategory($id)
    {
        $service_category = self::getServiceCategoryDetail($id);
        $imagePath = storage_path(ServiceCategory::IMG_PATH) . $service_category->image;
        @unlink($imagePath);
        $service_category->delete();
        return true;
    }

    public function getAllPluckIdNameWithServices()
    {
        return ServiceCategory::with(['relatedServices' => function ($q) {
            return $q->get()->pluck('title', 'id');
        }])->select('relatedServices', 'title', 'id');
    }


}
