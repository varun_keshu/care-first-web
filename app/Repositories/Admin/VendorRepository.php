<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\VendorRepositoryInterface;
use App\Models\Booking;
use App\Models\ServiceAreas;
use App\Models\Services;
use App\Models\User;
use App\Models\UserServiceAreas;
use App\Models\UserServices;
use App\Models\VendorWorkSchedule;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class VendorRepository implements VendorRepositoryInterface
{
    public function getAllVendors()
    {
        return User::where('user_type', 'vendor')->get();
    }

    public function storeVendor($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            $requestData['user_type'] = "vendor";
            $requestData['status'] = 1;
            $vendor = User::create($requestData);
            if (isset($requestData['services'])) {
                foreach ($requestData['services'] as $service_area_id => $data) {
                    $service_area = ServiceAreas::where('id', $service_area_id)->first();
                    $updatedData = [
                        'user_id' => $vendor->id,
                        'city_id' => $service_area->city_id,
                        'service_area_id' => $service_area_id
                    ];
                    UserServiceAreas::create($updatedData);

                    foreach ($data as $service_id => $val) {
                        $service = Services::where('id', $service_id)->first();
                        $data = [
                            'user_id' => $vendor->id,
                            'service_id' => $service_id,
                            'service_category_id' => $service->service_category_id,
                            'service_area_id' => $service_area_id
                        ];
                        UserServices::create($data);
                    }
                }
            }
            self::storeVendorWorkScheduleData($vendor->id);
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function storeVendorWorkScheduleData($vendor_id)
    {
        foreach (VendorWorkSchedule::WEEK_DAYS as $key => $day) {
            VendorWorkSchedule::create([
                'vendor_id' => $vendor_id,
                'week_day' => $key,
                'start_time' => "09:00:00",
                'end_time' => "20:00:00",
                'is_available' => 1
            ]);
        }
        return true;
    }

    public function getVendorDetail($id)
    {
        return User::with(['userServiceAreas.userServices' => function ($q) use ($id) {
            $q->where('user_id', $id);
        }, "vendorUnavailability" => function ($q){
            $q->whereDate('date', ">=", Carbon::today());
        }])->where('user_type', "vendor")->where('id', $id)->first();
    }

    public function updateVendor($request, $id)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            $vendor = self::getVendorDetail($id);
            $vendor->update($requestData);
            $exist_user_services_records = $exist_user_service_areas_records = [];
            if (isset($requestData['services'])) {
                foreach ($requestData['services'] as $service_area_id => $data) {
                    $service_area = ServiceAreas::where('id', $service_area_id)->first();
                    $existUserServiceArea = UserServiceAreas::where('user_id', $vendor->id)->where('city_id', $service_area->city_id)->where('service_area_id', $service_area_id)->first();
                    if (isset($existUserServiceArea)) {
                        $exist_user_service_areas_records[] = $existUserServiceArea->id;
                    } else {
                        $updatedData = [
                            'user_id' => $vendor->id,
                            'city_id' => $service_area->city_id,
                            'service_area_id' => $service_area_id
                        ];
                        $newUserAreaRecord = UserServiceAreas::create($updatedData);
                        $exist_user_service_areas_records[] = $newUserAreaRecord->id;
                    }
                    foreach ($data as $service_id => $val) {
                        $service = Services::where('id', $service_id)->first();
                        if (isset($service)) {
                            $existUserService = UserServices::where('user_id', $vendor->id)->where('service_id', $service_id)->where('service_area_id', $service_area_id)->first();
                            if (isset($existUserService)) {
                                $exist_user_services_records[] = $existUserService->id;
                            } else {
                                $data = [
                                    'user_id' => $vendor->id,
                                    'service_id' => $service_id,
                                    'service_category_id' => $service->service_category_id,
                                    'service_area_id' => $service_area_id
                                ];
                                $newUserService = UserServices::create($data);
                                $exist_user_services_records[] = $newUserService->id;
                            }
                        }
                    }
                }
            }
            UserServices::where('user_id', $vendor->id)->whereNotIn('id', $exist_user_services_records)->forceDelete();
            UserServiceAreas::where('user_id', $vendor->id)->whereNotIn('id', $exist_user_service_areas_records)->forceDelete();
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function vendorList()
    {
        $data = [];
        $vendor = User::where('user_type', 'vendor')->get();
        foreach ($vendor as $getData) {
            $data[] = [
                'name' => $getData->name ?? "--",
                'contact_number' => $getData->contact_number,
                'pending_charges' => $getData->pending_charges ?? "--",
                'payout' => $getData->payout ?? "--"
            ];
        }
        return $data;
    }
}
