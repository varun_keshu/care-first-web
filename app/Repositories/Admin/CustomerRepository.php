<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\CustomerRepositoryInterface;
use App\Models\Conversation;
use App\Models\CustomerAddresses;
use App\Models\Customers;
use App\Models\Message;

class CustomerRepository implements CustomerRepositoryInterface
{
    public function getAllCustomers()
    {
        return Customers::where('status', 1)->get();
    }

    public function getCustomersList()
    {
        return Customers::pluck('contact_number', 'id')->toArray();
    }

    public function getCustomerDetail($id)
    {
        return Customers::with(['relatedAddresses', 'relatedConversations' => function ($q) {
            $q->orderBy('updated_at', "DESC");
        }, 'bookings' => function ($q) {
            $q->orderBy('id', "DESC");
        }])->where('id', $id)->first();
    }

    public function getCustomerConversationDetail($request)
    {
        $requestData = $request->except("_token");
        return Conversation::where('id', $requestData['conversation_id'])->first();
    }

    public function sendMessageToCustomer($request)
    {
        $requestData = $request->except("_token");
        $message = Message::create([
            'conversation_id' => $requestData['conversation_id'],
            'sender_type' => 'admin',
            'message' => $requestData['message'],
        ]);
        return $message;
    }

    public function getCustomersListBySearch($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        $customers = Customers::where('status', 1);
        if (isset($requestData['searchTerm'])) {
            $customers = $customers->where('name', "Like", "%" . $requestData['searchTerm'] . "%")
                ->orWhere('contact_number', "Like", "%" . $requestData['searchTerm'] . "%");
        }
        $customers = $customers->get();
        foreach ($customers as $customer) {
            $returnData[] = ['id' => $customer->id, 'text' => $customer->name . ' (' . $customer->contact_number . ')'];
        }
        return $returnData;
    }

    public function getAddressListByCustomer($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        if (isset($requestData['customerId'])) {
            $customer_addresses = CustomerAddresses::where('customer_id', $requestData['customerId']);
            if (isset($requestData['searchTerm'])) {
                $customer_addresses = $customer_addresses->where('flat_or_house_number', "Like", "%" . $requestData['searchTerm'] . "%")
                    ->orWhere('address_line_1', "Like", "%" . $requestData['searchTerm'] . "%")
                    ->orWhere('service_area', "Like", "%" . $requestData['searchTerm'] . "%")
                    ->orWhere('city', "Like", "%" . $requestData['searchTerm'] . "%")
                    ->orWhere('landmark', "Like", "%" . $requestData['searchTerm'] . "%");
            }
            $customer_addresses = $customer_addresses->get();
            foreach ($customer_addresses as $customer_address) {
                $returnData[] = ['id' => $customer_address->id, 'text' => getAddressString($customer_address)];
            }
        }
        return $returnData;
    }
}
