<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\PageRepositoryInterface;
use App\Models\Pages;

class PageRepository implements PageRepositoryInterface
{
    public function getAllPages()
    {
        return Pages::get();
    }

    public function getPageTypes($id = null)
    {
        $titles = [];
        $exist_record_actions = Pages::whereNotNull('id');
        if (isset($id)){
            $exist_record_actions = $exist_record_actions->where('id', '!=', $id);
        }
        $exist_record_actions = $exist_record_actions->pluck('page_type')->toArray();
        foreach (Pages::TYPES as $key => $value) {
            if (!in_array($key, $exist_record_actions)){
                $titles[$key] = $value;
            }
        }
        return $titles;
    }

    public function storePageData($request)
    {
        $requestData = $request->all();
        Pages::create($requestData);
        return true;
    }

    public function getPageDetail($id)
    {
        return Pages::where('id', $id)->first();
    }

    public function getPageDetailByType($page_type)
    {
        return Pages::where('page_type', $page_type)->first();
    }

    public function updatePageData($request, $id)
    {
        $requestData = $request->all();
        $page = self::getPageDetail($id);
        if (isset($page)){
            $page->update($requestData);
        }
        return true;
    }

    public function deletePageData($id)
    {
        Pages::destroy($id);
        return true;
    }
}
