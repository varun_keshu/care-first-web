<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\SliderRepositoryInterface;
use App\Models\ServiceCategory;
use App\Models\Services;
use App\Models\Sliders;

class SliderRepository implements SliderRepositoryInterface
{
    public function getAllSliders()
    {
        return Sliders::get();
    }

    public function storeSliderData($request)
    {
        $requestData = $request->all();
        $requestData['service_category_id'] = ($requestData['type'] == "service_category_page") ? $requestData['service_category_id'] : null;
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData);
        }
        Sliders::create($requestData);
        return true;
    }

    public function getSliderDetail($id)
    {
        return Sliders::where('id', $id)->first();
    }

    public function updateSliderData($request, $id)
    {
        $requestData = $request->all();
        $slider = self::getSliderDetail($id);
        if (isset($slider)) {
            $requestData['service_category_id'] = ($requestData['type'] == "service_category_page") ? $requestData['service_category_id'] : null;
            if (isset($requestData['image'])) {
                $requestData['image'] = self::uploadImage($requestData, $slider);
            }
            $slider->update($requestData);
        }
        return true;
    }

    public function uploadImage($requestData, $slider = null)
    {
        $file = $requestData['image'];
        $uploadPath = storage_path(Sliders::IMG_PATH);
        if (isset($slider)) {
            $imagePath = $uploadPath . $slider->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function deleteSliderData($id)
    {
        $slider = self::getSliderDetail($id);
        $imagePath = storage_path(Sliders::IMG_PATH) . $slider->image;
        @unlink($imagePath);
        $slider->delete();
        return true;
    }

    public function getSliderListByCategory($id)
    {
         $data= Sliders::where('service_category_id',$id)->get();
         return $data;
    }

    public function getOtherSliderList()
    {
         $data= Sliders::where('service_category_id',null)->get();
         return $data;
    }
}
