<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\ManagerRepositoryInterface;
use App\Models\CustomerAddresses;
use App\Models\ServiceAreas;
use App\Models\Services;
use App\Models\User;
use App\Models\UserServiceAreas;
use App\Models\UserServices;
use App\Services\SmsService;

class ManagerRepository implements ManagerRepositoryInterface
{
    protected $smsService;

    public function __construct(SmsService $smsService)
    {
        $this->smsService = $smsService;
    }

    public function getAllManagers()
    {
        return User::where('user_type', 'manager')->get();
    }

    public function getAllManagersNameList()
    {
        return User::where('user_type', 'manager')->pluck('name', 'id')->toArray();
    }

    public function getAvailableServicesByArea($service_area_id, $request)
    {
        $requestData = $request->except("_token");
        $existingServicesForArea = UserServices::where('service_area_id', $service_area_id);
        if (isset($requestData['user_id'])) {
            $existingServicesForArea = $existingServicesForArea->where('user_id', "!=", $requestData['user_id']);
        }
        $existingServicesForArea = $existingServicesForArea->pluck('service_id')->toArray();
        $data = Services::whereNotIn('id', $existingServicesForArea)->get()->groupBy('service_category_id');
        return $data;

    }

    public function storeManager($request)
    {
        $requestData = $request->except("_token");
        $requestData['status'] = 1;
        $requestData['user_type'] = 'manager';
        $user = User::create($requestData);
        if (isset($requestData['service_areas'])) {
            foreach ($requestData['service_areas'] as $service_area_id => $value) {
                $service_area = ServiceAreas::where('id', $service_area_id)->first();
                if (isset($requestData['services']) && isset($requestData['services'][$service_area_id])) {
                    $data = [
                        'user_id' => $user->id,
                        'city_id' => $service_area->city_id,
                        'service_area_id' => $service_area_id
                    ];
                    UserServiceAreas::create($data);

                    foreach ($requestData['services'][$service_area_id] as $service_id => $val) {
                        $service = Services::where('id', $service_id)->first();
                        $data = [
                            'user_id' => $user->id,
                            'service_id' => $service_id,
                            'service_category_id' => $service->service_category_id,
                            'service_area_id' => $service_area_id
                        ];
                        UserServices::create($data);
                    }
                }
            }
        }
        $message = 'Your number was registered as care first manager, ' . date('d M Y');
        $this->smsService->send($message, $requestData['contact_number']);
        return true;
    }

    public function getManagerDetail($id)
    {
        return User::with(['userServiceAreas.userServices' => function ($q) use ($id) {
            $q->where('user_id', $id);
        }])->where('user_type', 'manager')->where('id', $id)->first();
    }

    public function getManagerServiceCategoriesDetails($manager)
    {
        $serviceAreaData = [];
        foreach ($manager->userServiceAreas as $userServiceAreas) {
            $managerServiceAreaData['service_area_id'] = $userServiceAreas->service_area_id;
            $managerServiceAreaData['service_area_title'] = $userServiceAreas->serviceArea->title;
            $managerServiceAreaData['services'] = [];
            $existingServicesForArea = UserServices::whereHas('relatedUser', function ($q){
                $q->where('user_type', 'manager');
            })->where('user_id', "!=", $manager->id)->where('service_area_id', $userServiceAreas->service_area_id)->pluck('service_id')->toArray();
            $data = Services::whereNotIn('id', $existingServicesForArea)->get();
            $data = $data->filter(function ($item) use ($userServiceAreas) {
                $item->is_checked = false;
                foreach ($userServiceAreas->userServices as $userService) {
                    if ($userService->service_id == $item->id) {
                        $item->is_checked = true;
                    }
                }
                return $item;
            });
            $managerServiceAreaData['services'] = $data->groupBy('service_category_id');
            $serviceAreaData[] = $managerServiceAreaData;
        }
        return $serviceAreaData;
    }

    public function getManagerServicesListByCategory($manager, $vendor = null)
    {
        $serviceAreaData = [];
        foreach ($manager->userServiceAreas as $userServiceAreas) {
            $managerServiceAreaData['service_area_id'] = $userServiceAreas->service_area_id;
            $managerServiceAreaData['service_area_title'] = $userServiceAreas->serviceArea->title;
            $managerServiceAreaData['services'] = [];
            $existingServicesForArea = UserServices::where('user_id', $manager->id)->where('service_area_id', $userServiceAreas->service_area_id)->pluck('service_id')->toArray();
            /*if ($userServiceAreas->service_area_id == 2){
                dd($existingServicesForArea);
            }*/
            $data = Services::whereIn('id', $existingServicesForArea)->get();
            if (isset($vendor)){
                $data = $data->filter(function ($item) use ($vendor) {
                    $item->is_checked = false;
                    foreach ($vendor->userServices as $userService) {
                        if ($userService->service_id == $item->id) {
                            $item->is_checked = true;
                            break;
                        }
                    }
                    return $item;
                });
            }
            $managerServiceAreaData['services'] = $data->groupBy('service_category_id');
            $serviceAreaData[] = $managerServiceAreaData;
        }
        return $serviceAreaData;
    }

    public function updateManager($request, $id)
    {
        $requestData = $request->except("_token");
        $user = User::find($id);
        if (isset($requestData['service_areas'])) {
            $exist_user_service_areas_records = [];
            $exist_user_services_records = [];
            foreach ($requestData['service_areas'] as $service_area_id => $value) {
                $service_area = ServiceAreas::where('id', $service_area_id)->first();
                if (isset($requestData['services']) && isset($requestData['services'][$service_area_id])) {
                    $existUserServiceArea = UserServiceAreas::where('user_id', $user->id)->where('city_id', $service_area->city_id)->where('service_area_id', $service_area_id)->first();
                    if (isset($existUserServiceArea)) {
                        $exist_user_service_areas_records[] = $existUserServiceArea->id;
                    } else {
                        $data = [
                            'user_id' => $user->id,
                            'city_id' => $service_area->city_id,
                            'service_area_id' => $service_area_id
                        ];
                        $newUserAreaRecord = UserServiceAreas::create($data);
                        $exist_user_service_areas_records[] = $newUserAreaRecord->id;
                    }
                    foreach ($requestData['services'][$service_area_id] as $service_id => $val) {
                        $service = Services::where('id', $service_id)->first();

                        $existUserService = UserServices::where('user_id', $user->id)->where('service_id', $service_id)->where('service_area_id', $service_area_id)->first();
                        if (isset($existUserService)) {
                            $exist_user_services_records[] = $existUserService->id;
                        } else {
                            $data = [
                                'user_id' => $user->id,
                                'service_id' => $service_id,
                                'service_category_id' => $service->service_category_id,
                                'service_area_id' => $service_area_id
                            ];
                            $newUserService = UserServices::create($data);
                            $exist_user_services_records[] = $newUserService->id;
                        }
                    }
                }
            }
            UserServiceAreas::where('user_id', $user->id)->whereNotIn('id', $exist_user_service_areas_records)->forceDelete();
            UserServices::where('user_id', $user->id)->whereNotIn('id', $exist_user_services_records)->forceDelete();
        }
        return true;
    }

    public function getManagersListByService($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        if (isset($requestData['serviceId'])){
            $service_id = $requestData['serviceId'];
            $service_area_id = null;
            if (isset($requestData['addressId'])){
                $address = CustomerAddresses::where('id', $requestData['addressId'])->select('service_area_id')->first();
                $service_area_id = $address['service_area_id'];
            }
            $managers = User::whereHas('userServices', function ($q) use ($service_id, $service_area_id){
                $q->where('service_id', $service_id);
                if (isset($service_area_id)){
                    $q->where('service_area_id', $service_area_id);
                }
            })->where('user_type', 'manager');
            if (isset($requestData['searchTerm'])){
                $managers = $managers->where('name', "Like", "%". $requestData['searchTerm'] . "%");
            }
            $managers = $managers->get();
            foreach ($managers as $manager){
                $returnData[] = ['id' => $manager->id, 'text' => $manager->name];
            }
        }
        return $returnData;
    }
}
