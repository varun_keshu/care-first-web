<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\FaqRepositoryInterface;
use App\Models\Faqs;

class FaqRepository implements FaqRepositoryInterface
{
    public function getAllFaqs()
    {
        return Faqs::get();
    }

    public function getAllFaqsByCategory()
    {
        return Faqs::orderBy('sort_order')->get()->groupBy('category');
    }

    public function storeFaqData($request)
    {
        $requestData = $request->all();
        Faqs::create($requestData);
        return true;
    }

    public function getFaqDetail($id)
    {
        return Faqs::where('id', $id)->first();
    }

    public function updateFaqData($request, $id)
    {
        $requestData = $request->all();
        $faq = self::getFaqDetail($id);
        if (isset($faq)) {
            $faq->update($requestData);
        }
        return true;
    }

    public function deleteFaqData($id)
    {
        Faqs::destroy($id);
        return true;
    }

    public function getFaqsByCategory($faq_category)
    {
        $returnData = Faqs::where('category', $faq_category)->orderBy('sort_order', 'asc')->get();
        return $returnData;
    }

    public function updateFaqOrder($request)
    {
        $returnData = $request->all();
        $sortIds = explode(',', $returnData['ids']);

        foreach ($sortIds as $sortOrder => $id) {
            $faq = Faqs::find($id);
            $faq->update(['sort_order' => $sortOrder]);
        }
        return true;
    }
}
