<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\PackageSubscriptionInterface;
use App\Models\PackageSubscription;

class PackageSubscriptionRepository implements PackageSubscriptionInterface
{
    public function __construct()
    {
    }

    public function getAll()
    {
        return PackageSubscription::where('payment_status', 1)->get()->all();
    }

    public function getPackageSubscriptionDetail($id)
    {
        return PackageSubscription::findOrFail($id);
    }

    public function delete($id)
    {
        PackageSubscription::destroy($id);
        return true;
    }
}
