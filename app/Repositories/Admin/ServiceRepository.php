<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\ServiceRepositoryInterface;
use App\Models\Services;

class ServiceRepository implements ServiceRepositoryInterface
{
    public function getAllServices()
    {
        return Services::get();
    }

    public function storeServiceData($request)
    {
        $requestData = $request->all();
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData);
        }
        $requestData['is_warranty_apply'] = isset($requestData['is_warranty_apply']) ? 1 : 0;
        $requestData['warranty_expiration_days'] = ($requestData['is_warranty_apply'] == 1) ? $requestData['warranty_expiration_days'] : null;
        $requestData['show_quantity_option'] = isset($requestData['show_quantity_option']) ? 1 : 0;
        $requestData['is_insurance_applicable'] = isset($requestData['is_insurance_applicable']) ? 1 : 0;
        $requestData['estimation_request_available'] = isset($requestData['estimation_request_available']) ? 1 : 0;
        Services::create($requestData);
        return true;
    }

    public function getServiceDetail($id)
    {
        return Services::where('id', $id)->first();
    }

    public function updateServiceData($request, $id)
    {
        $requestData = $request->all();

        $service = self::getServiceDetail($id);

        if (isset($service)) {
            $requestData['is_warranty_apply'] = isset($requestData['is_warranty_apply']) ? 1 : 0;
            $requestData['warranty_expiration_days'] = ($requestData['is_warranty_apply'] == 1) ? $requestData['warranty_expiration_days'] : null;
            $requestData['show_quantity_option'] = isset($requestData['show_quantity_option']) ? 1 : 0;
            $requestData['is_insurance_applicable'] = isset($requestData['is_insurance_applicable']) ? 1 : 0;
            $requestData['estimation_request_available'] = isset($requestData['estimation_request_available']) ? 1 : 0;
            if (isset($requestData['image'])) {
                $requestData['image'] = self::uploadImage($requestData, $service);
            }

            $service->update($requestData);
        }

        return true;
    }

    public function uploadImage($requestData, $service = NULL)
    {
        $file = $requestData['image'];
        $uploadPath = storage_path(Services::IMG_PATH);
        if (isset($service)) {
            $imagePath = $uploadPath . $service->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function deleteServiceData($id)
    {
        $service_category = self::getServiceDetail($id);
        $imagePath = storage_path(Services::IMG_PATH) . $service_category->image;
        @unlink($imagePath);
        $service_category->delete();
        return true;
    }

    public function getAllPluckIdName()
    {
        $serviceCategories = Services::all()->groupBy('serviceCategoryDetails.title');
        $arr = [];
        foreach ($serviceCategories as $key => $services) {
            foreach ($services as $service) {
                $arr[$key][$service->id] = $service->title;
            }
        }

        return $arr;
    }
}
