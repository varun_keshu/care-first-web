<?php

namespace App\Repositories\Admin;

use App\Interfaces\Admin\RewardProgramRepositoryInterface;
use App\Models\EmailTemplates;
use App\Models\RewardProgram;

class RewardProgramRepository implements RewardProgramRepositoryInterface
{
    public function getAllRewardProgramsList()
    {
        return RewardProgram::get();
    }

    public function storeRewardProgram($request)
    {
        $requestData = $request->all();
        RewardProgram::create($requestData);
        return true;
    }

    public function getRewardProgramDetail($id)
    {
        return RewardProgram::where('id', $id)->first();
    }

    public function updateRewardProgram($request, $id)
    {
        $requestData = $request->all();
        $reward_program = self::getRewardProgramDetail($id);
        if (isset($reward_program)){
            $reward_program->update($requestData);
        }
        return true;
    }

    public function deleteRewardProgram($id)
    {
        RewardProgram::destroy($id);
        return true;
    }
}
