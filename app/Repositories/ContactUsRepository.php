<?php
namespace App\Repositories;

use App\Interfaces\ContactUsRepositoryInterface;
use App\Models\ContactUsSubmissions;
use Yajra\DataTables\Facades\DataTables;

class ContactUsRepository implements ContactUsRepositoryInterface
{
    public function storeContactUsData($request)
    {
        $requestData = $request->all();
        $existData = ContactUsSubmissions::where('email', $requestData['email'])->first();
        if (isset($existData)){
            $existData->update($requestData);
        }else{
            ContactUsSubmissions::create($requestData);
        }
        return true;
    }

    public function getListAjaxData($request)
    {
        $dataInput = ContactUsSubmissions::whereNotNull('id');
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('email', function ($contact_request) {
            return $contact_request->email ?? "- - -";
        });
        $data_tables->EditColumn('message', function ($contact_request) {
            return $contact_request->message ?? "- - -";
        });
        return $data_tables->make(true);
    }



}
