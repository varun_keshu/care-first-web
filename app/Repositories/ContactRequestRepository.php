<?php
namespace App\Repositories;

use App\Events\NewContactRequestEvent;
use App\Interfaces\ContactRequestRepositoryInterface;
use App\Models\Booking;
use App\Models\ContactRequest;
use App\Models\Conversation;
use App\Models\CustomerAddresses;
use App\Models\Message;
use App\Models\UserServices;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ContactRequestRepository implements ContactRequestRepositoryInterface
{
    public function validateRequest($request)
    {
        $requestData = $request->except("_token");
        $validator = Validator::make($requestData, [
            'service_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'customer_address_id' => 'required',
        ], [
            'service_id.required' => "Please select at least one service.",
            'customer_address_id.required' => "Please select customer address.",

        ]);
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function storeContactRequest($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except("_token");
            if (!empty($requestData['images'])) {
                $requestData['images'] = self::storeImage($requestData);
            }
            if (isset($requestData['customer_address_id'])) {
                $customerAddress = CustomerAddresses::find($requestData['customer_address_id']);
                $requestData['service_area_id'] = $customerAddress->service_area_id;
            }
            $data = ContactRequest::create($requestData);
            self::createConversation($data->id, $requestData);
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, "message" => $e->getMessage()];
        }
    }

    public function storeImage($requestData)
    {
        $returnData = [];
        foreach ($requestData['images'] as $image) {
            $path = storage_path('app/public/contact_requests/');
            $extension = 'png';
            $filename_to_store = rand(1, 100000) . "." . $extension;
            file_put_contents($path . $filename_to_store, base64_decode($image));
            $returnData[] = $filename_to_store;
        }
        return $returnData;
    }

    public function createConversation($contactRequestId, $requestData)
    {
        $managerService = UserServices::whereHas('relatedUser', function ($q) {
            $q->where('user_type', 'manager');
        })->where('service_id', $requestData['service_id'])->where('service_area_id', $requestData['service_area_id'])->first();
        if (isset($managerService)) {
            $conversationData = [
                'customer_id' => $requestData['customer_id'],
                'contact_request_id' => $contactRequestId,
                'title' => $requestData['title'],
                'receiver_user_id' => $managerService['user_id']
            ];
            $conversation = Conversation::create($conversationData);

            $messageData = Message::create([
                'conversation_id' => $conversation->id,
                'sender_type' => "customer",
                'receiver_type' => "manager",
                'message' => $requestData['description']
            ]);

            event(new NewContactRequestEvent($messageData));
        }
        return true;
    }

    public function getListAjaxData($request)
    {
        $dataInput = ContactRequest::whereNotNull('id');
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('customer_details', function ($contact_request) {
            return $contact_request->customer->name ?? "- - -";
        });
        $data_tables->EditColumn('booking', function ($contact_request) {
            return $contact_request->booking->id ?? "- - -";
        });
        $data_tables->EditColumn('service_categories', function ($contact_request) {
            return $contact_request->serviceDetails->title ?? "- - -";
        });
        $data_tables->EditColumn('action', function ($contact_request) {
            return view('admin.contact_request.action', compact('contact_request'))->render();
        });
        return $data_tables->make(true);
    }

    public function getContactDetails($contact_request_id)
    {
        return ContactRequest::where('id', $contact_request_id)->first();
    }

    public function deleteContactRequest($contact_request_id)
    {
        return ContactRequest::destroy($contact_request_id);
    }

    public function createBookingConversation($request)
    {
        $requestData = $request->all();
        $booking = Booking::where('id', $requestData['booking_id'])->first();
        $existConversation = Conversation::where('booking_id', $requestData['booking_id'])->first();
        if (isset($existConversation)) {
            $conversation_id = $existConversation->id;
        } else {
            $conversation = Conversation::create([
                'booking_id' => $booking->id,
                'customer_id' => $booking->customer_id,
                'receiver_user_id' => $booking->manager_id,
                'title' => $booking->serviceDetails->title,
            ]);
            $conversation_id = $conversation->id;
        }
        return route('app_view.getMessagesByConversation', ['conversation_id' => $conversation_id, 'login_user_type' => isset($requestData['customer_id']) ? 'customer' : 'manager']);
    }
}
