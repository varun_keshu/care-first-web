<?php

namespace App\Providers;

use App\Interfaces\Admin\AdminUserInterface;
use App\Interfaces\Admin\ClientRepositoryInterface;
use App\Interfaces\Admin\InsuranceRepositoryInterface;
use App\Interfaces\Admin\BookingsRepositoryInterface;
use App\Interfaces\Admin\CustomerRepositoryInterface;
use App\Interfaces\Admin\EmailServiceInterface;
use App\Interfaces\Admin\EmailTemplatesRepositoryInterface;
use App\Interfaces\Admin\FaqRepositoryInterface;
use App\Interfaces\Admin\ManagerRepositoryInterface;
use App\Interfaces\Admin\PackageRepositoryInterface;
use App\Interfaces\Admin\PackageSubscriptionInterface;
use App\Interfaces\Admin\RewardProgramRepositoryInterface;
use App\Interfaces\Admin\ServiceAreaRepositoryInterface;
use App\Interfaces\Admin\ServiceStatusRepositoryInterface;
use App\Interfaces\Admin\TestimonialRepositoryInterface;
use App\Interfaces\Admin\VendorRepositoryInterface;
use App\Interfaces\Api\BannerRepositoryInterface;
use App\Interfaces\Api\BookingRepositoryInterface;
use App\Interfaces\Api\CategoryRepositoryInterface;
use App\Interfaces\Api\CmsPageRepositoryInterface;
use App\Interfaces\Api\ConversationRepositoryInterface;
use App\Interfaces\Api\CustomerAddressRepositoryInterface;
use App\Interfaces\Api\CustomersRepositoryInterface;
use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Interfaces\Api\PackagesRepositoryInterface;
use App\Interfaces\Api\UserRepositoryInterface;
use App\Interfaces\ConfigurationRepositoryInterface;
use App\Interfaces\ContactRequestRepositoryInterface;
use App\Interfaces\ContactUsRepositoryInterface;
use App\Repositories\Admin\AdminUserRepository;
use App\Interfaces\Admin\PageRepositoryInterface;
use App\Interfaces\Admin\ServiceCategoryRepositoryInterface;
use App\Interfaces\Admin\ServiceRepositoryInterface;
use App\Interfaces\Admin\SliderRepositoryInterface;
use App\Repositories\Admin\ClientRepository;
use App\Repositories\Admin\InsuranceRepository;
use App\Repositories\Admin\PackageRepository;
use App\Repositories\Admin\BookingsRepository;
use App\Repositories\Admin\CustomerRepository;
use App\Repositories\Admin\EmailTemplatesRepository;
use App\Repositories\Admin\FaqRepository;
use App\Repositories\Admin\ManagerRepository;
use App\Repositories\Admin\PackageSubscriptionRepository;
use App\Repositories\Admin\PageRepository;
use App\Repositories\Admin\RewardProgramRepository;
use App\Repositories\Admin\ServiceAreaRepository;
use App\Repositories\Admin\ServiceCategoryRepository;
use App\Repositories\Admin\ServiceRepository;
use App\Repositories\Admin\ServiceStatusRepository;
use App\Repositories\Admin\SliderRepository;
use App\Repositories\Admin\TestimonialRepository;
use App\Repositories\Admin\VendorRepository;
use App\Repositories\Api\BannerRepository;
use App\Repositories\Api\BookingRepository;
use App\Repositories\Api\CategoryRepository;
use App\Repositories\Api\CmsPageRepository;
use App\Repositories\Api\ConversationRepository;
use App\Repositories\Api\CustomerAddressRepository;
use App\Repositories\Api\CustomersRepository;
use App\Repositories\Api\NotificationRepository;
use App\Repositories\Api\PackagesRepository;
use App\Repositories\Api\UserRepository;
use App\Repositories\ConfigurationRepository;
use App\Repositories\ContactRequestRepository;
use App\Repositories\ContactUsRepository;
use App\Services\EmailService;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EmailServiceInterface::class, function ($app) {
            return $app->make(EmailService::class);
        });

        /*GLOBAL*/
        $this->app->bind(ConfigurationRepositoryInterface::class, function ($app) {
            return $app->make(ConfigurationRepository::class);
        });
        $this->app->bind(ContactRequestRepositoryInterface::class, function ($app) {
            return $app->make(ContactRequestRepository::class);
        });
        $this->app->bind(ContactUsRepositoryInterface::class, function ($app) {
            return $app->make(ContactUsRepository::class);
        });

        /*ADMIN*/
        $this->app->bind(EmailTemplatesRepositoryInterface::class, function ($app) {
            return $app->make(EmailTemplatesRepository::class);
        });
        $this->app->bind(SliderRepositoryInterface::class, function ($app) {
            return $app->make(SliderRepository::class);
        });
        $this->app->bind(ServiceCategoryRepositoryInterface::class, function ($app) {
            return $app->make(ServiceCategoryRepository::class);
        });
        $this->app->bind(ServiceRepositoryInterface::class, function ($app) {
            return $app->make(ServiceRepository::class);
        });
        $this->app->bind(PageRepositoryInterface::class, function ($app) {
            return $app->make(PageRepository::class);
        });
        $this->app->bind(FaqRepositoryInterface::class, function ($app) {
            return $app->make(FaqRepository::class);
        });
        $this->app->bind(AdminUserInterface::class, function ($app) {
            return $app->make(AdminUserRepository::class);
        });
        $this->app->bind(ServiceAreaRepositoryInterface::class, function ($app) {
            return $app->make(ServiceAreaRepository::class);
        });
        $this->app->bind(ServiceStatusRepositoryInterface::class, function ($app) {
            return $app->make(ServiceStatusRepository::class);
        });
        $this->app->bind(CustomerRepositoryInterface::class, function ($app) {
            return $app->make(CustomerRepository::class);
        });
        $this->app->bind(ManagerRepositoryInterface::class, function ($app) {
            return $app->make(ManagerRepository::class);
        });
        $this->app->bind(VendorRepositoryInterface::class, function ($app) {
            return $app->make(VendorRepository::class);
        });
        $this->app->bind(BookingsRepositoryInterface::class, function ($app) {
            return $app->make(BookingsRepository::class);
        });
        $this->app->bind(InsuranceRepositoryInterface::class, function ($app) {
            return $app->make(InsuranceRepository::class);
        });
        $this->app->bind(TestimonialRepositoryInterface::class, function ($app) {
            return $app->make(TestimonialRepository::class);
        });
        $this->app->bind(RewardProgramRepositoryInterface::class, function ($app) {
            return $app->make(RewardProgramRepository::class);
        });

        /*API*/
        $this->app->bind(BannerRepositoryInterface::class, function ($app) {
            return $app->make(BannerRepository::class);
        });
        $this->app->bind(CategoryRepositoryInterface::class, function ($app) {
            return $app->make(CategoryRepository::class);
        });
        $this->app->bind(CmsPageRepositoryInterface::class, function ($app) {
            return $app->make(CmsPageRepository::class);
        });
        $this->app->bind(CustomersRepositoryInterface::class, function ($app) {
            return $app->make(CustomersRepository::class);
        });
        $this->app->bind(CustomerAddressRepositoryInterface::class, function ($app) {
            return $app->make(CustomerAddressRepository::class);
        });
        $this->app->bind(UserRepositoryInterface::class, function ($app) {
            return $app->make(UserRepository::class);
        });
        $this->app->bind(BookingRepositoryInterface::class, function ($app) {
            return $app->make(BookingRepository::class);
        });
        $this->app->bind(PackageRepositoryInterface::class, function ($app) {
            return $app->make(PackageRepository::class);
        });
        $this->app->bind(PackageSubscriptionInterface::class, function ($app) {
            return $app->make(PackageSubscriptionRepository::class);
        });
        $this->app->bind(NotificationRepositoryInterface::class, function ($app) {
            return $app->make(NotificationRepository::class);
        });
        $this->app->bind(ConversationRepositoryInterface::class, function ($app) {
            return $app->make(ConversationRepository::class);
        });
        $this->app->bind(PackagesRepositoryInterface::class, function ($app) {
            return $app->make(PackagesRepository::class);
        });
        $this->app->bind(ClientRepositoryInterface::class, function ($app) {
            return $app->make(ClientRepository::class);
        });

    }
}
