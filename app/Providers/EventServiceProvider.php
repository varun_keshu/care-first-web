<?php

namespace App\Providers;

use App\EventListeners\BookedServiceStartEventHandler;
use App\EventListeners\BookingCancellationEventHandler;
use App\EventListeners\BookingCompletedEventHandler;
use App\EventListeners\CreateBookingEvent\CreateBookingEventHandler;
use App\EventListeners\CreateBookingEvent\SentNotificationToCustomerHandler;
use App\EventListeners\CreateBookingEvent\SentNotificationToManagerHandler;
use App\EventListeners\CreateBookingEvent\SentNotificationToVendorsHandler;
use App\EventListeners\ForgotPasswordEmailHandler;
use App\EventListeners\GenerateServiceIssueEventHandler;
use App\EventListeners\IssueRequestApprovedEventHandler;
use App\EventListeners\IssueRequestRejectedEventHandler;
use App\EventListeners\NewAdminActivationEventHandler;
use App\EventListeners\NewContactRequestEventHandler;
use App\EventListeners\SendBookingCancellationNotificationEventHandler;
use App\EventListeners\SendBookingCompletedNotificationEventHandler;
use App\EventListeners\UpdateJobCountEventHandler;
use App\EventListeners\VendorAcceptedBookedServiceEventHandler;
use App\Events\BookedServiceStartEvent;
use App\Events\BookingCancellationEvent;
use App\Events\BookingCompletedEvent;
use App\Events\CreateBookingEvent;
use App\Events\ForgotPasswordEmail;
use App\Events\GenerateServiceIssueEvent;
use App\Events\IssueRequestApprovedEvent;
use App\Events\IssueRequestRejectedEvent;
use App\Events\NewAdminActivationEvent;
use App\Events\NewContactRequestEvent;
use App\Events\UpdateJobCountEvent;
use App\Events\VendorAcceptedBookedServiceEvent;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ForgotPasswordEmail::class => [
            ForgotPasswordEmailHandler::class,
        ],
        NewAdminActivationEvent::class => [
            NewAdminActivationEventHandler::class,
        ],
        UpdateJobCountEvent::class => [
            UpdateJobCountEventHandler::class,
        ],
        CreateBookingEvent::class => [
            CreateBookingEventHandler::class,
            SentNotificationToCustomerHandler::class,
            SentNotificationToVendorsHandler::class,
            SentNotificationToManagerHandler::class,
        ],
        VendorAcceptedBookedServiceEvent::class => [
            VendorAcceptedBookedServiceEventHandler::class,
        ],
        BookingCancellationEvent::class => [
            BookingCancellationEventHandler::class,
            SendBookingCancellationNotificationEventHandler::class,
        ],
        BookedServiceStartEvent::class => [
            BookedServiceStartEventHandler::class,
        ],
        BookingCompletedEvent::class => [
            BookingCompletedEventHandler::class,
            SendBookingCompletedNotificationEventHandler::class,
        ],
        GenerateServiceIssueEvent::class => [
            GenerateServiceIssueEventHandler::class,
        ],
        IssueRequestApprovedEvent::class => [
            IssueRequestApprovedEventHandler::class,
        ],
        IssueRequestRejectedEvent::class => [
            IssueRequestRejectedEventHandler::class,
        ],
        NewContactRequestEvent::class => [
            NewContactRequestEventHandler::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
