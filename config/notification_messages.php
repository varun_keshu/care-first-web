<?php

return [
    'new_booking_notification_to_customer' => [
        'title' => 'Created Your Booking!',
        'description' => 'There is new booking created by your account for service :service_name.',
    ],
    'new_booking_notification_to_vendor' => [
        'title' => 'New Booking!',
        'description' => 'There is new booking arrive in your area by customer :customer_name for service :service_name.',
    ],
    'new_booking_notification_to_manager' => [
        'title' => 'New Booking!',
        'description' => 'There is new booking arrive in your area by customer :customer_name for service :service_name.',
    ],
    'vendor_unassigned_booking_notification_to_manager' => [
        'title' => 'Unassigned Booking!',
        'description' => 'There is unassigned booking in your area please assign to vendor before its service time.',
    ],
    'booking_assign_notification_to_vendor' => [
        'title' => 'New Booking Assigned!',
        'description' => ':manager_name assigned you for booking service :service_name.',
    ],
    'vendor_booking_confirmed_notification_to_manager' => [
        'title' => 'Booking Confirmed!',
        'description' => ':vendor_name has confirmed booking service :service_name.',
    ],
    'customer_booking_completed_notification_to_customer' => [
        'title' => 'Booking Completed!',
        'description' => 'Your booked service :service_name was completed.',
    ],
    'customer_booking_completed_notification_to_manager' => [
        'title' => 'Booking Completed!',
        'description' => ':vendor_name was completed booked service :service_name for customer :customer_name.',
    ],
    'customer_booking_cancelled_notification_to_customer' => [
        'title' => 'Booking Cancelled!',
        'description' => 'You cancelled your booked service :service_name.',
    ],
    'customer_booking_cancelled_notification_to_vendor' => [
        'title' => 'Booking Cancelled!',
        'description' => 'Your assigned service :service_name cancelled by customer :customer_name.',
    ],
    'customer_booking_cancelled_notification_to_manager' => [
        'title' => 'Booking Cancelled!',
        'description' => 'Booked service :service_name cancelled by customer :customer_name.',
    ],
    'vendor_booking_cancelled_notification_to_manager' => [
        'title' => 'Booking Cancelled!',
        'description' => ':vendor_name unassigned service :service_name.',
    ],
    'customer_issue_generate_notification_to_manager' => [
        'title' => 'Warranty Claim Submitted!',
        'description' => ':customer_name has submitted warranty claim for booking service :service_name.',
    ],
    'issue_request_approved_notification_to_vendor' => [
        'title' => 'Warranty Claim Submitted!',
        'description' => ':customer_name has submitted warranty claim for booking service :service_name.',
    ],
    'issue_request_approved_notification_to_customer' => [
        'title' => 'Warranty Claim Approved!',
        'description' => 'Your warranty claim approved for service :service_name.',
    ],
    'issue_request_rejected_notification_to_customer' => [
        'title' => 'Warranty Claim Rejected!',
        'description' => 'Your warranty claim rejected for service :service_name.',
    ],
    'new_contact_request' => [
        'title' => 'New Contact Request!',
        'description' => 'New contact request submitted by customer :customer_name for service :service_name.',
    ],
];

