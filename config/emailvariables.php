<?php

return [
    'reset_password' => [
        'NAME' => 'Name of the User',
        'RESET_LINK' => 'Reset Link of the User',
    ],
    'admin_activation_set_password' => [
        'NAME' => 'Name of the User',
        'URL' => 'Set Password link sor new admin user',
    ],
];

