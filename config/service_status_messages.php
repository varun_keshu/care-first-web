<?php

return [
    'customer' => [
        'subscription_booked' => 'Booking created using subscription package.',
    ],
    'vendor' => [
        'booked' => 'New Booking Arrived!',
        'subscription_booked' => 'New booking arrived using subscription package.',
        'manager_assign' => 'Manager has assigned you new service.',
        'vendor_confirmed' => 'You have confirmed this service.',
        'cancelled_by_vendor' => 'This booking service was cancelled by owner.',
        'cancelled_by_owner' => 'This booking service was cancelled by customer.',
        'cancelled_by_manager' => 'This booking service was cancelled by manager.',
        'in_process' => "Your service was started.",
        'completed' => "Your service was completed.",
        'open_issue_request' => "Warranty claim submitted for which has been completed by you.",
        'issue_request_approved' => "Open issue request which service has completed by you.",
        'issue_resolved' => "Issue resolved which service has completed by you.",
    ],
    'manager' => [
        'booked' => 'New Booking Arrived!',
        'subscription_booked' => 'New booking arrived using subscription package.',
        'manager_assign' => 'You have assigned this service to vendor :vendor_name.',
        'vendor_confirmed' => 'Vendor :vendor_name has confirmed this service.',
        'cancelled_by_vendor' => 'This booking service was cancelled by :vendor_name.',
        'cancelled_by_owner' => 'This booking service was cancelled by customer.',
        'cancelled_by_manager' => 'This booking service was cancelled by manager.',
        'in_process' => "This service was started.",
        'completed' => "This service was completed.",
        'open_issue_request' => "Warranty claim submitted which service has completed.",
        'issue_request_approved' => "You reassign for your completed service because the customer has submitted warranty claim.",
        'issue_request_rejected' => "You rejected warranty claim.",
        'issue_resolved' => "Warranty claim resolved.",
    ]
];

