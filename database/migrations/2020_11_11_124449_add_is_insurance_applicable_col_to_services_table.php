<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsInsuranceApplicableColToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->boolean('is_insurance_applicable')->default(0)->after('show_quantity_option');
        });
        Schema::table('booking_details', function (Blueprint $table) {
            $table->boolean('has_insurance')->default(0)->after('is_vendor_paid');
        });
        Schema::table('booking_status_history', function (Blueprint $table) {
            $table->text('reason')->nullable()->after('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('is_insurance_applicable');
        });
        Schema::table('booking_details', function (Blueprint $table) {
            $table->dropColumn('has_insurance');
        });
        Schema::table('booking_status_history', function (Blueprint $table) {
            $table->dropColumn('reason');
        });
    }
}
