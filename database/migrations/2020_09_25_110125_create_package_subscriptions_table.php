<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->integer('package_id');
            $table->string('title');
            $table->text('description');
            $table->integer('duration');
            $table->string('duration_unit');
            $table->double('price', 8, 2)->default(0.0);
            $table->dateTime('start_date')->default(null);
            $table->dateTime('end_date')->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('package_subscription_services', function (Blueprint $table) {
            $table->id();
            $table->integer('package_id');
            $table->integer('service_id');
            $table->integer('package_service_id');
            $table->integer('package_subscription_id');
            $table->integer('service_category_id');
            $table->integer('service_count');
            $table->integer('used_count');
            $table->integer('frequency')->default(1);
            $table->string('frequency_unit')->default('months');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('package_subscription_service_id')->nullable()->after('service_category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_subscriptions');
        Schema::dropIfExists('package_subscription_services');
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('package_subscription_service_id');
        });
    }
}
