<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePackageServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('package_services');
        Schema::table('packages', function (Blueprint $table) {
            $table->integer('service_id')->after('id')->nullable();
            $table->integer('service_category_id')->after('service_id')->nullable();
            $table->integer('service_count')->after('price')->default(1);
            $table->integer('duration')->after('service_count')->nullable();
            $table->string('duration_unit')->after('duration')->nullable();
        });

        Schema::dropIfExists('package_subscription_services');
        Schema::table('package_subscriptions', function (Blueprint $table) {
            $table->integer('service_id')->after('customer_id')->nullable();
            $table->integer('service_category_id')->after('service_id')->nullable();
            $table->integer('quantity')->after('description')->default(1);
            $table->double('total_amount', 8, 2)->after('price')->default(0);
            $table->integer('service_count')->after('total_amount')->nullable();
            $table->integer('used_count')->after('service_count')->nullable();
            $table->integer('duration')->after('used_count')->nullable();
            $table->string('duration_unit')->after('duration')->nullable();
            $table->string('priority')->after('duration_unit')->nullable();
            $table->date('service_start_date')->after('priority')->nullable();
            $table->date('next_service_start_date')->after('service_start_date')->nullable();
            $table->time('service_time')->after('next_service_start_date')->nullable();
            $table->text('notes')->after('service_time')->nullable();
        });

        Schema::table('bookings', function (Blueprint $table){
            $table->integer('package_subscription_id')->nullable()->after('vendor_id');
            $table->dropColumn('package_subscription_service_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn('service_id');
            $table->dropColumn('service_category_id');
            $table->dropColumn('service_count');
            $table->dropColumn('duration');
            $table->dropColumn('duration_unit');
        });

        Schema::table('package_subscriptions', function (Blueprint $table) {
            $table->dropColumn('service_id');
            $table->dropColumn('service_category_id');
            $table->dropColumn('quantity');
            $table->dropColumn('total_amount');
            $table->dropColumn('service_count');
            $table->dropColumn('used_count');
            $table->dropColumn('duration');
            $table->dropColumn('duration_unit');
            $table->dropColumn('priority');
            $table->dropColumn('service_start_date');
            $table->dropColumn('next_service_start_date');
            $table->dropColumn('service_time');
            $table->dropColumn('notes');
        });

        Schema::table('bookings', function (Blueprint $table){
            $table->integer('package_subscription_service_id')->nullable()->after('vendor_id');
            $table->dropColumn('package_subscription_id');
        });
    }
}
