<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnualPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->double('price', 8, 2)->default(0);
            $table->string('description');
            $table->integer('duration');
            $table->string('duration_unit'); // day,month,year
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('package_services', function (Blueprint $table) {
            $table->id();
            $table->integer('package_id');
            $table->integer('service_id');
            $table->integer('service_category_id');
            $table->integer('service_count');
            $table->integer('frequency')->default(1);
            $table->string('frequency_unit')->default('months');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
        Schema::dropIfExists('service_packages');
    }
}
