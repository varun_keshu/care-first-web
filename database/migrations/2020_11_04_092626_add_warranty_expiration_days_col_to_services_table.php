<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWarrantyExpirationDaysColToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->integer('warranty_expiration_days')->after('time_period')->nullable();
        });
        Schema::table('booking_details', function (Blueprint $table) {
            $table->string('priority')->after('customer_address_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('warranty_expiration_days');
        });
        Schema::table('booking_details', function (Blueprint $table) {
            $table->dropColumn('priority');
        });
    }
}
