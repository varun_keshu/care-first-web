<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInsurancePremiumAmountColToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->double('insurance_premium_amount', 8, 2)->after('insurance_amount')->nullable();
            $table->double('previous_pending_charges', 8, 2)->after('insurance_premium_amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('insurance_premium_amount');
            $table->dropColumn('previous_pending_charges');
        });
    }
}
