<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorWorkScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_work_schedule', function (Blueprint $table) {
            $table->id();
            $table->integer('vendor_id');
            $table->integer('week_day');
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->boolean('is_available')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('vendor_unavailability', function (Blueprint $table) {
            $table->id();
            $table->integer('vendor_id');
            $table->date('date')->nullable();
            $table->text('reason')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_work_schedule');
        Schema::dropIfExists('vendor_unavailability');
    }
}
