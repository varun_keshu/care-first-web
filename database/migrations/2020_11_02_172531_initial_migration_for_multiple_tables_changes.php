<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitialMigrationForMultipleTablesChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->renameColumn("customer_price", "price");
            $table->renameColumn("vendor_price", "commission_value");
        });
        Schema::table('customers', function (Blueprint $table) {
            $table->boolean("has_complete_profile")->default(0)->after('status');
            $table->double("pending_charges", 8, 2)->default(0)->after('status');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->boolean("has_complete_profile")->default(0)->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->renameColumn("price", "customer_price");
            $table->renameColumn("commission_value", "vendor_price");
        });
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn("pending_charges");
            $table->dropColumn("has_complete_profile");
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("has_complete_profile");
        });
    }
}
