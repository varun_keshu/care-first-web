<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BookingsTableRelatedChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('booking_details');
        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('parent_id')->after('id')->nullable();
            $table->integer('service_id')->after('razorpay_order_id')->nullable();
            $table->integer('service_category_id')->after('service_id')->nullable();
            $table->integer('manager_id')->after('service_category_id')->nullable();
            $table->integer('vendor_id')->after('manager_id')->nullable();
            $table->integer('package_subscription_service_id')->after('vendor_id')->nullable();
            $table->integer('customer_address_id')->after('package_subscription_service_id')->nullable();
            $table->integer('service_area_id')->after('customer_address_id')->nullable();
            $table->string('priority')->after('service_area_id')->nullable();
            $table->integer('quantity')->after('priority')->nullable();
            $table->double('actual_received_amount', 8, 2)->after('price')->nullable();
            $table->double('commission_fee', 8, 2)->after('actual_received_amount')->nullable();
            $table->integer('service_status')->after('total_amount')->nullable();
            $table->boolean('is_vendor_paid')->after('payment_gateway_response')->default(0);
            $table->boolean('has_insurance')->after('is_vendor_paid')->default(0);
            $table->text('notes')->after('has_insurance')->nullable();
            $table->text('address_details')->after('notes')->nullable();
            $table->integer('rating_value')->after('address_details')->nullable();
            $table->text('review')->after('rating_value')->nullable();
            $table->date('service_date')->after('review')->nullable();
            $table->time('service_time')->after('service_date')->nullable();
            $table->string('otp_code')->after('service_time')->nullable();
            $table->boolean('is_cancelled_booking')->after('otp_code')->default(0);
            $table->integer('generate_issue_status')->after('is_cancelled_booking')->default(0);
            $table->date('warranty_expiration_date')->after('generate_issue_status')->nullable();
        });

        Schema::table('booking_status_history', function (Blueprint $table) {
            $table->dropColumn('booked_service_id');
        });

        Schema::table('booked_service_issues', function (Blueprint $table) {
            $table->dropColumn('booked_service_id');
        });

        Schema::table('booking_cancellation_logs', function (Blueprint $table) {
            $table->dropColumn('booking_service_id');
        });

        Schema::table('contact_requests', function (Blueprint $table) {
            $table->dropColumn('booking_service_id');
            $table->integer('booked_id')->after('customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('service_id');
            $table->dropColumn('service_category_id');
            $table->dropColumn('manager_id');
            $table->dropColumn('vendor_id');
            $table->dropColumn('package_subscription_service_id');
            $table->dropColumn('customer_address_id');
            $table->dropColumn('service_area_id');
            $table->dropColumn('priority');
            $table->dropColumn('quantity');
            $table->dropColumn('actual_received_amount');
            $table->dropColumn('commission_fee');
            $table->dropColumn('service_status');
            $table->dropColumn('is_vendor_paid');
            $table->dropColumn('has_insurance');
            $table->dropColumn('notes');
            $table->dropColumn('address_details');
            $table->dropColumn('rating_value');
            $table->dropColumn('review');
            $table->dropColumn('service_date');
            $table->dropColumn('service_time');
            $table->dropColumn('otp_code');
            $table->dropColumn('is_cancelled_booking');
            $table->dropColumn('generate_issue_status');
            $table->dropColumn('warranty_expiration_date');
        });

        Schema::table('booking_status_history', function (Blueprint $table) {
            $table->integer('booked_service_id')->after('booking_id');
        });

        Schema::table('booked_service_issues', function (Blueprint $table) {
            $table->integer('booked_service_id')->after('booking_id');
        });

        Schema::table('booking_cancellation_logs', function (Blueprint $table) {
            $table->integer('booking_service_id')->after('booking_id');
        });

        Schema::table('contact_requests', function (Blueprint $table) {
            $table->dropColumn('booked_id');
            $table->integer('booking_service_id')->after('customer_id');
        });
    }
}
