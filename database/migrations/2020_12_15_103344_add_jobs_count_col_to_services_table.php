<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJobsCountColToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->boolean('estimation_request_available')->default(0)->after('is_insurance_applicable');
            $table->integer('jobs_count')->default(0)->after('estimation_request_available');
        });
        Schema::table('service_status', function (Blueprint $table) {
            $table->string('backend_state')->nullable()->after('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('estimation_request_available');
            $table->dropColumn('jobs_count');
        });
        Schema::table('service_status', function (Blueprint $table) {
            $table->dropColumn('backend_state');
        });
    }
}
