<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentsRelatedColsToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn("user_address_id");
            $table->string("unique_booking_key")->nullable()->after('customer_id');
            $table->integer("customer_address_id")->nullable()->after('service_category_id');
            $table->string("razorpay_payment_id")->nullable()->after('customer_address_id');
            $table->string("razorpay_order_id")->nullable()->after('razorpay_payment_id');
            $table->integer("quantity")->nullable()->after('razorpay_order_id');
            $table->boolean("payment_status")->default(0)->after('service_status');
            $table->string("payment_method")->nullable()->after('payment_status');
            $table->text("payment_gateway_response")->nullable()->after('payment_method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn("customer_address_id");
            $table->integer("user_address_id")->nullable()->after('service_category_id');
            $table->dropColumn("unique_booking_key");
            $table->dropColumn("razorpay_payment_id");
            $table->dropColumn("razorpay_order_id");
            $table->dropColumn("quantity");
            $table->dropColumn("payment_status");
            $table->dropColumn("payment_method");
            $table->dropColumn("payment_gateway_response");
        });
    }
}
