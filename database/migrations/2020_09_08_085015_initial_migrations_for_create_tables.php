<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitialMigrationsForCreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('service_categories')) {
            Schema::create('service_categories', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('parent_id')->default(0);
                $table->string('title')->nullable();
                $table->string('image')->nullable();
                $table->string('icon_image')->nullable();
                $table->integer('jobs_count')->default(0);
                $table->integer('completed_jobs_count')->default(0);
                $table->integer('jobs_count_with_issues')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('services')) {
            Schema::create('services', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('service_category_id');
                $table->string('title')->nullable();
                $table->text('price_details')->nullable();
                $table->integer('time_period')->default(1);
                $table->string('image')->nullable();
                $table->boolean('show_quantity_option')->default(0);
                $table->integer('completed_jobs_count')->default(0);
                $table->integer('jobs_count_with_issues')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('service_instructions_by_category')) {
            Schema::create('service_instructions_by_category', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('service_category_id');
                $table->string('title')->nullable();
                $table->string('icon')->nullable();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('service_products_by_category')) {
            Schema::create('service_products_by_category', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('service_category_id');
                $table->string('title')->nullable();
                $table->string('image')->nullable();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('issue_titles_by_category')) {
            Schema::create('issue_titles_by_category', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('service_category_id');
                $table->string('title')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('sliders')) {
            Schema::create('sliders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('service_category_id')->nullable();
                $table->string('title')->nullable();
                $table->string('type')->nullable();
                $table->string('image')->nullable();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('email_templates')) {
            Schema::create('email_templates', function (Blueprint $table) {
                $table->increments('id');
                $table->string('identifier')->nullable();
                $table->string('title')->nullable();
                $table->text('subject')->nullable();
                $table->text('content')->nullable();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('pages')) {
            Schema::create('pages', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable();
                $table->string('page_type')->nullable();
                $table->text('content')->nullable();
                $table->string('page_title')->nullable();
                $table->string('meta_keywords')->nullable();
                $table->text('meta_description')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('faqs')) {
            Schema::create('faqs', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('sort_order')->default(0);
                $table->string('category')->nullable();
                $table->string('question')->nullable();
                $table->text('answer')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('customers')) {
            Schema::create('customers', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->string('email')->nullable();
                $table->string('contact_number')->nullable();
                $table->boolean('status')->default(0);
                $table->string('image')->nullable();
                $table->string('otp_code')->nullable();
                $table->integer('login_attempt_count')->default(0);
                $table->string('access_token')->nullable();
                $table->string('device_token')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('customer_addresses')) {
            Schema::create('customer_addresses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('customer_id');
                $table->string('title')->nullable();
                $table->string('flat_or_house_number')->nullable();
                $table->string('address_line_1')->nullable();
                $table->string('address_line_2')->nullable();
                $table->string('landmark')->nullable();
                $table->string('zip_code')->nullable();
                $table->string('city')->nullable();
                $table->boolean('is_default')->default(0);
                $table->string('address_type')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('cities')) {
            Schema::create('cities', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('service_areas')) {
            Schema::create('service_areas', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('city_id');
                $table->string('title')->nullable();
                $table->string('zip_code')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('contact_number')->nullable();
                $table->string('user_type')->nullable();
                $table->boolean('status')->default(0);
                $table->string('image')->nullable();
                $table->string('otp_code')->nullable();
                $table->integer('login_attempt_count')->default(0);
                $table->string('access_token')->nullable();
                $table->string('device_token')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('user_services')) {
            Schema::create('user_services', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('service_id');
                $table->integer('total_jobs_count')->default(0);
                $table->integer('completed_jobs_count')->default(0);
                $table->integer('jobs_count_with_issues')->default(0);
                $table->integer('total_ratings_count')->default(0);
                $table->double('average_ratings_value')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('user_service_areas')) {
            Schema::create('user_service_areas', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('city_id');
                $table->integer('service_area_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('service_status')) {
            Schema::create('service_status', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->boolean('update_completion_count')->default(0);
                $table->boolean('update_issue_confirmation_count')->default(0);
                $table->boolean('visible_to_customer')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('customer_reviews')) {
            Schema::create('customer_reviews', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('booking_id');
                $table->integer('customer_id');
                $table->integer('service_id');
                $table->integer('rating_value');
                $table->text('review')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('bookings')) {
            Schema::create('bookings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('customer_id');
                $table->integer('vendor_id')->nullable();
                $table->integer('manager_id')->nullable();
                $table->integer('service_id');
                $table->integer('service_category_id');
                $table->integer('user_address_id');
                $table->double('customer_price', 8, 2)->default(0);
                $table->double('vendor_price', 8, 2)->default(0);
                $table->integer('service_status')->nullable();
                $table->text('notes')->nullable();
                $table->integer('rating_value')->nullable();
                $table->text('review')->nullable();
                $table->date('service_date')->nullable();
                $table->time('service_time')->nullable();
                $table->date('warranty_expiration_date')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if (!Schema::hasTable('booking_status_history')) {
            Schema::create('booking_status_history', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('booking_id');
                $table->integer('service_status_id');
                $table->text('notes')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_categories');
        Schema::dropIfExists('services');
        Schema::dropIfExists('service_instructions_by_category');
        Schema::dropIfExists('service_products_by_category');
        Schema::dropIfExists('issue_titles_by_category');
        Schema::dropIfExists('sliders');
        Schema::dropIfExists('email_templates');
        Schema::dropIfExists('faqs');
        Schema::dropIfExists('customers');
        Schema::dropIfExists('customer_addresses');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('service_areas');
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_services');
        Schema::dropIfExists('user_service_areas');
        Schema::dropIfExists('service_status');
        Schema::dropIfExists('customer_reviews');
        Schema::dropIfExists('bookings');
        Schema::dropIfExists('booking_status_history');
    }
}
