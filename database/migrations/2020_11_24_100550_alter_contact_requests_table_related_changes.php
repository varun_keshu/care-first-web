<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterContactRequestsTableRelatedChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_requests', function (Blueprint $table) {
            $table->renameColumn('request_category', 'service_id');
        });
        Schema::table('contact_requests', function (Blueprint $table) {
            $table->integer('customer_address_id')->after('service_id')->nullable();
            $table->integer('service_area_id')->after('customer_address_id')->nullable();
        });
        Schema::table('conversations', function (Blueprint $table) {
            $table->integer('receiver_user_id')->after('customer_id')->nullable();
            $table->integer('contact_request_id')->after('receiver_user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_requests', function (Blueprint $table) {
            $table->renameColumn('service_id', 'request_category');
            $table->dropColumn('customer_address_id');
            $table->dropColumn('service_area_id');
        });
        Schema::table('conversations', function (Blueprint $table){
            $table->dropColumn('receiver_user_id');
            $table->dropColumn('contact_request_id');
        });
    }
}
