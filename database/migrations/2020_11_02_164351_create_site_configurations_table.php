<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_configurations', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('identifier')->nullable();
            $table->string('value')->nullable();
            $table->timestamps();
        });

        Schema::table('services', function (Blueprint $table) {
           $table->double('commission_value', 8, 2)->nullable()->after('price_details');
           $table->double('visiting_charges', 8, 2)->nullable()->after('commission_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_configurations');

        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('commission_value');
            $table->dropColumn('visiting_charges');
        });
    }
}
