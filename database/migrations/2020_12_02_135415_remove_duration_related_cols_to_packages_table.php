<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveDurationRelatedColsToPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn('duration');
            $table->dropColumn('duration_unit');
        });
        Schema::table('package_services', function (Blueprint $table) {
            $table->dropColumn('frequency');
            $table->dropColumn('frequency_unit');
            $table->integer('duration')->nullable()->after('service_count');
            $table->string('duration_unit')->nullable()->after('duration');
        });
        Schema::table('package_subscriptions', function (Blueprint $table) {
            $table->dropColumn('duration');
            $table->dropColumn('duration_unit');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->string('customer_address_id')->nullable()->after('customer_id');
            $table->string('unique_key')->nullable()->after('package_id');
            $table->string('razorpay_order_id')->nullable()->after('unique_key');
            $table->string('razorpay_payment_id')->nullable()->after('razorpay_order_id');
            $table->boolean('payment_status')->default(0)->after('price');
            $table->string('payment_method')->nullable()->after('payment_status');
            $table->text('payment_gateway_response')->nullable()->after('payment_method');
            $table->text('customer_address_details')->nullable()->after('payment_gateway_response');
        });
        Schema::table('package_subscription_services', function (Blueprint $table) {
            $table->dropColumn('frequency');
            $table->dropColumn('frequency_unit');
            $table->integer('duration')->nullable()->after('used_count');
            $table->string('duration_unit')->nullable()->after('duration');
            $table->date('service_start_date')->nullable()->after('duration_unit');
            $table->date('next_service_start_date')->nullable()->after('service_start_date');
            $table->time('service_time')->nullable()->after('next_service_start_date');
            $table->boolean('is_expired')->default(0)->after('service_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->integer('duration')->nullable()->after('description');
            $table->string('duration_unit')->nullable()->after('duration');
        });
        Schema::table('package_services', function (Blueprint $table) {
            $table->dropColumn('duration');
            $table->dropColumn('duration_unit');
            $table->integer('frequency')->nullable()->after('service_count');
            $table->string('frequency_unit')->nullable()->after('frequency');
        });
        Schema::table('package_subscriptions', function (Blueprint $table) {
            $table->integer('duration')->nullable()->after('description');
            $table->string('duration_unit')->nullable()->after('duration');
            $table->date('start_date')->nullable()->after('price');
            $table->date('end_date')->nullable()->after('start_date');
            $table->dropColumn('customer_address_id');
            $table->dropColumn('unique_key');
            $table->dropColumn('razorpay_order_id');
            $table->dropColumn('razorpay_payment_id');
            $table->dropColumn('payment_status');
            $table->dropColumn('payment_method');
            $table->dropColumn('payment_gateway_response');
            $table->dropColumn('customer_address_details');
        });
        Schema::table('package_subscription_services', function (Blueprint $table) {
            $table->dropColumn('duration');
            $table->dropColumn('duration_unit');
            $table->dropColumn('service_start_date');
            $table->dropColumn('next_service_start_date');
            $table->dropColumn('service_time');
            $table->dropColumn('is_expired');
            $table->integer('frequency')->nullable()->after('used_count');
            $table->string('frequency_unit')->nullable()->after('frequency');
        });
    }
}
