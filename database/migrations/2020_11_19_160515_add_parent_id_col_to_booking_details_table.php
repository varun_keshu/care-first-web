<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentIdColToBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            $table->integer('parent_id')->after('id')->nullable();
        });
        Schema::table('booked_service_issues', function (Blueprint $table) {
            $table->integer('status')->after('images')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            $table->dropColumn('parent_id');
        });
        Schema::table('booked_service_issues', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
