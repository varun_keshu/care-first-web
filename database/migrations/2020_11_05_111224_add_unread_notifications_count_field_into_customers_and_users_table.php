<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnreadNotificationsCountFieldIntoCustomersAndUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('unread_notifications_count')->after('image')->default(0);
        });
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('unread_notifications_count')->after('image')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('unread_notifications_count');
        });
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('unread_notifications_count');
        });
    }
}
