<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterServiceStatusTableRelatedChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_status', function (Blueprint $table){
            $table->string('customer_title')->after('title')->nullable();
        });
        Schema::table('booking_status_history', function (Blueprint $table){
            $table->string('state')->after('service_status_id')->nullable();
        });
        Schema::table('booking_details', function (Blueprint $table){
            $table->boolean('is_cancelled_booking')->after('service_time')->default(0);
            $table->integer('generate_issue_status')->after('is_cancelled_booking')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_status', function (Blueprint $table){
            $table->dropColumn('customer_title');
        });
        Schema::table('booking_status_history', function (Blueprint $table){
            $table->dropColumn('state');
        });
        Schema::table('booking_details', function (Blueprint $table){
            $table->dropColumn('is_cancelled_booking');
            $table->dropColumn('generate_issue_status');
        });
    }
}
