<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_requests', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->integer('booking_service_id')->nullable();
            $table->integer('request_category')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('images')->nullable();
            $table->timestamps();
        });

        Schema::table('conversations', function (Blueprint $table){
            $table->renameColumn('booking_id', 'booking_service_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_requests');
        Schema::table('conversations', function (Blueprint $table){
            $table->renameColumn('booking_service_id', 'booking_id');
        });
    }
}
