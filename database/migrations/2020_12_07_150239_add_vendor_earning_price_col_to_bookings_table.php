<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVendorEarningPriceColToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->double('service_price', 8, 2)->after('priority')->default(0);
            $table->double('vendor_earning_price', 8, 2)->after('commission_fee')->default(0);
            $table->dropColumn('actual_received_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->double('actual_received_amount', 8, 2)->after('price')->default(0);
            $table->dropColumn('service_price');
            $table->dropColumn('vendor_earning_price');
        });
    }
}
