<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingCancellationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_cancellation_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('booking_id');
            $table->integer('booking_service_id');
            $table->integer('cancelled_by_user_id');
            $table->string('cancelled_by_user_type');
            $table->double('charges', 8, 2)->default(0);
            $table->boolean('is_paid')->default(0);
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table){
            $table->double('pending_charges', 8, 2)->default(0)->after('status');
        });

        Schema::table('booking_status_history', function (Blueprint $table){
            $table->string('user_type')->nullable()->after('service_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_cancellation_logs');

        Schema::table('users', function (Blueprint $table){
            $table->dropColumn('pending_charges');
        });

        Schema::table('booking_status_history', function (Blueprint $table){
            $table->dropColumn('user_type');
        });
    }
}
