<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_details', function (Blueprint $table) {
            $table->id();
            $table->integer('booking_id');
            $table->integer('customer_id');
            $table->integer('service_id');
            $table->integer('service_category_id');
            $table->integer('manager_id')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->integer('package_subscription_service_id')->nullable();
            $table->integer('customer_address_id');
            $table->integer('quantity')->nullable();
            $table->double('price', 8, 2)->default(0);
            $table->double('actual_received_amount', 8, 2)->nullable();
            $table->double('commission_fee', 8, 2)->default(0);
            $table->integer('service_status')->nullable();
            $table->boolean('is_vendor_paid')->default(0);
            $table->text('notes')->nullable();
            $table->text('address_details')->nullable();
            $table->integer('rating_value')->nullable();
            $table->text('review')->nullable();
            $table->date('service_date')->nullable();
            $table->time('service_time')->nullable();
            $table->date('warranty_expiration_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('vendor_id');
            $table->dropColumn('manager_id');
            $table->dropColumn('service_id');
            $table->dropColumn('service_category_id');
            $table->dropColumn('package_subscription_service_id');
            $table->dropColumn('customer_address_id');
            $table->dropColumn('quantity');
            $table->dropColumn('commission_value');
            $table->dropColumn('actual_received_amount');
            $table->dropColumn('service_status');
            $table->dropColumn('notes');
            $table->dropColumn('rating_value');
            $table->dropColumn('review');
            $table->dropColumn('service_date');
            $table->dropColumn('service_time');
            $table->dropColumn('warranty_expiration_date');

            $table->double('insurance_amount', 8, 2)->default(0)->after('price');
            $table->double('total_amount', 8, 2)->default(0)->after('insurance_amount');
        });

        Schema::table('booking_status_history', function (Blueprint $table) {
            $table->integer('booked_service_id')->after('booking_id');
        });

        Schema::table('booked_service_issues', function (Blueprint $table) {
            $table->integer('booked_service_id')->after('booking_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_details');

        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('service_id');
            $table->integer('service_category_id');
            $table->integer('manager_id')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->integer('package_subscription_service_id')->nullable();
            $table->integer('customer_address_id');
            $table->integer('quantity')->nullable();
            $table->double('actual_received_amount', 8, 2)->nullable();
            $table->double('commission_value', 8, 2)->default(0);
            $table->integer('service_status')->nullable();
            $table->text('notes')->nullable();
            $table->integer('rating_value')->nullable();
            $table->text('review')->nullable();
            $table->date('service_date')->nullable();
            $table->time('service_time')->nullable();
            $table->date('warranty_expiration_date')->nullable();

            $table->dropColumn('insurance_amount');
            $table->dropColumn('total_amount');
        });

        Schema::table('booking_status_history', function (Blueprint $table) {
            $table->dropColumn('booked_service_id');
        });

        Schema::table('booked_service_issues', function (Blueprint $table) {
            $table->dropColumn('booked_service_id');
        });
    }
}
