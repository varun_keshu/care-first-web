<?php

use Illuminate\Database\Seeder;

class AddDefaultDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        self::AddAdminsData();
        self::AddConfigurationsData();
        self::AddServiceStatusData();
    }

    private function AddAdminsData()
    {
        \App\Models\Admins::truncate();
        $admin = [
            'name' => 'Care First',
            'email' => 'meetmalay@gmail.com',
            'password' => '123456uuzz',
            'status' => 1,
            'activation_date' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ];
        \App\Models\Admins::create($admin);
    }

    private function AddConfigurationsData()
    {
        \App\Models\SiteConfigurations::truncate();
        $configurations_data = [
            [
                'title' => 'Fixed visiting charges if cancelled service before specific time of service.',
                'identifier' => 'fixed_visiting_charges',
                'value' => 10
            ],
            [
                'title' => 'Commission value by service.',
                'identifier' => 'commission_charges',
                'value' => 5
            ],
            [
                'title' => 'Service warranty days',
                'identifier' => 'service_warranty_days',
                'value' => 7
            ],
            [
                'title' => 'Specific time value fot taken cancellation service charges.',
                'identifier' => 'specific_time_before_taken_cancellation_charges',
                'value' => 30
            ],
            [
            'title' => 'Company Address.',
            'identifier' => 'company_address',
            'value' => 'Buttonwood, California Rosemead, CA 91770'
             ],
            [
                'title' => 'Contact',
                'identifier' => 'contact_number',
                'value' => '+1 253 565 2365'
            ],
            [
                'title' => 'Email',
                'identifier' => 'email',
                'value' => 'support@colorlib.com'
            ],

        ];
        foreach ($configurations_data as $data){
            \App\Models\SiteConfigurations::create([
                'title' => $data['title'],
                'identifier' => $data['identifier'],
                'value' => $data['value'],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }

    private function AddServiceStatusData()
    {
        \App\Models\ServiceStatus::truncate();
        $status_data = [
            [
                'title' => 'Booking Placed',
                'customer_title' => 'Your booking has been placed.',
                'state' => 'booked',
                'backend_state' => 'open',
                'update_completion_count' => 0,
                'update_issue_confirmation_count' => 0,
                'visible_to_customer' => 1
            ],
            [
                'title' => 'Booking Confirmed',
                'customer_title' => 'Your booking was confirmed.',
                'state' => 'confirmed',
                'backend_state' => 'open',
                'update_completion_count' => 0,
                'update_issue_confirmation_count' => 0,
                'visible_to_customer' => 1
            ],
            [
                'title' => 'Vendor Unassign',
                'customer_title' => 'Booking Cancelled by the vendor',
                'state' => 'booked',
                'backend_state' => 'open',
                'update_completion_count' => 0,
                'update_issue_confirmation_count' => 0,
                'visible_to_customer' => 0
            ],
            [
                'title' => 'Booking Cancelled by the owner',
                'customer_title' => 'You cancelled your booked service.',
                'state' => 'cancelled',
                'backend_state' => 'cancelled',
                'update_completion_count' => 0,
                'update_issue_confirmation_count' => 0,
                'visible_to_customer' => 1
            ],
            [
                'title' => 'Started',
                'customer_title' => 'Your booked service start.',
                'state' => 'in_process',
                'backend_state' => 'open',
                'update_completion_count' => 0,
                'update_issue_confirmation_count' => 0,
                'visible_to_customer' => 1
            ],
            [
                'title' => 'Completed',
                'customer_title' => 'Completed your booked service.',
                'state' => 'completed',
                'backend_state' => 'completed',
                'update_completion_count' => 1,
                'update_issue_confirmation_count' => 0,
                'visible_to_customer' => 1
            ],
            [
                'title' => 'Issue Submitted',
                'customer_title' => 'Issue Submitted',
                'state' => 'open_issue_request',
                'backend_state' => 'issued',
                'update_completion_count' => 0,
                'update_issue_confirmation_count' => 1,
                'visible_to_customer' => 1
            ],
            [
                'title' => 'Issue Request Approved',
                'customer_title' => 'Your issue request was approved and assign to vendor.',
                'state' => 'open_issue_request',
                'backend_state' => 'issued',
                'update_completion_count' => 0,
                'update_issue_confirmation_count' => 1,
                'visible_to_customer' => 1
            ],
            [
                'title' => 'Issue Request Rejected',
                'customer_title' => 'Your issue request was rejected.',
                'state' => 'rejected',
                'backend_state' => 'issued',
                'update_completion_count' => 0,
                'update_issue_confirmation_count' => 1,
                'visible_to_customer' => 1
            ],
            [
                'title' => 'Issue Resolved',
                'customer_title' => 'Issue Resolved',
                'state' => 'issue_resolved',
                'backend_state' => 'issued',
                'update_completion_count' => 1,
                'update_issue_confirmation_count' => 0,
                'visible_to_customer' => 1
            ],
        ];
        foreach ($status_data as $data){
            \App\Models\ServiceStatus::create([
                'title' => $data['title'],
                'customer_title' => $data['customer_title'],
                'state' => $data['state'],
                'backend_state' => $data['backend_state'],
                'update_completion_count' => $data['update_completion_count'],
                'update_issue_confirmation_count' => $data['update_issue_confirmation_count'],
                'visible_to_customer' => $data['visible_to_customer'],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
