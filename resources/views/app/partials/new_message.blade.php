<div class="@if($newMessageByUser == "sender") sent darker  @else replies @endif container1">
    <p>{{ $messageData['message'] }}
        <span class="time timeago" datetime="{{$messageData['created_at']}}"></span>
    </p>
</div>
