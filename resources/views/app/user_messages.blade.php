<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
    <meta name="description" content="Care First Messages Screen"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content=""/>
    <title>Care First</title>
    <link rel="stylesheet"
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('admin/css/app-message.css') }}">
    <!-- Jquery-2.2.4 JS -->
    <script type="text/javascript" language="javascript">
        var $app_url = "{{ url('/') }}";
    </script>
</head>
<body>


<div class="container-fluid chat-sction px-0">
    <div class="row mx-auto w-100">
        <?php
        $last_message_id = null;
        ?>
        <div class="col-lg-12 col-md-12 px-0 border">

            <div class="chat" id="messagesContainer">
                @if(isset($conversation->contactRequestDetail) && !empty($conversation->contactRequestDetail->images))
                    @php
                        $class = (count($conversation->contactRequestDetail->images) == 1) ? 'single-img' : ((count($conversation->contactRequestDetail->images) == 2) ? 'couple-img' : 'multi-img');
                    @endphp
                    <div class="@if($user_type == 'customer') text-right @else text-left @endif">
                        <a class="img-wraper @if($user_type == 'customer') right @else left @endif" data-toggle="modal" data-target="#galleryModal">
                            @foreach($conversation->contactRequestDetail->images as $image)
                                <img src="{{ showContactRequestImage($image) }}" class="{{ $class }}"/>
                            @endforeach
                        </a>
                    </div>
                    <!-- Image Gallery Modal-->
                    <div class="modal fade bd-example-modal-lg" id="galleryModal" tabindex="-1" role="dialog"
                         aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div id="modalSlider" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        @foreach($conversation->contactRequestDetail->images as $index => $image)
                                            <div class="carousel-item @if($index == 0) active @endif">
                                                <img class="d-block" src="{{ showContactRequestImage($image) }}" alt="First slide">
                                            </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#modalSlider" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#modalSlider" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @foreach($conversation['relatedMessages'] as $message)
                    @php
                        if ($message->sender_type == $user_type) {
                            $class = 'sent darker';
                        } else {
                            $class = 'replies';
                        }
                        $last_message_id = $message->id;
                    @endphp
                    <div class="{{ $class }} container1">
                        <p>
                            {!! $message->message !!}
                            <span class="time timeago" datetime="{{$message->created_at}}">
                            </span>
                        </p>
                    </div>
                @endforeach

            </div>
            <input type="hidden" id="last_message_id" value="{{ $last_message_id }}"/>
            <div class="meassage">
                @php
                    $receiver_id = ($user_type == "customer") ? $conversation->receiver_user_id : $conversation->customer_id;
                    $receiver_type = ($user_type == "customer") ? $conversation->userDetail->user_type : "customer";
                @endphp
                <form method="post" name="sendMessageForm" id="sendMessageForm">
                    {{ csrf_field() }}
                    <input type="hidden" name="login_user_type" id="login_user_type" value="{{ $user_type }}"/>
                    <input type="hidden" name="conversation_id" id="conversation_id" value="{{ $conversation->id }}"/>
                    <input type="hidden" name="receiver_id" id="receiver_id" value="{{ $receiver_id }}"/>
                    <input type="hidden" name="receiver_type" id="receiver_type" value="{{ $receiver_type }}"/>
                    <div class="form-group message-input">
                        <input name="message" id="new_message" autocomplete="off" class="form-control type_message"
                               placeholder="Reply a message"/>
                        <button type="submit" class="btn btn-primary submit">
                            <i class="lab la-telegram"></i>
                            {{--<i class="las la-camera"></i>--}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('admin/lib/jquery/jquery.min.js') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//timeago.yarp.com/jquery.timeago.js"></script>
<script src="{{ asset('admin/js/jquery-timeago-master/locales/jquery.timeago.en.js') }}"></script>
<script type="text/javascript">
    var conversation_id = $('#conversation_id').val();
    $(document).ready(function () {
        jQuery.timeago.settings.allowFuture = false;

        reloadTime();
        // Update the chat timer every 5 seconds
        setInterval(function () {
            reloadTime();
        }, 5000);

        $('#messagesContainer').scrollTop($('#messagesContainer')[0].scrollHeight);

        setInterval("reloadNewMessage();", 10000);

        $(window).on('keydown', function (e) {
            if (e.which == 13) {
                $('#sendMessageForm').submit();
                return false;
            }
        });

        $('#sendMessageForm').submit(function (e) {
            message = $("#new_message").val();
            if ($.trim(message) == '') {
                $("#new_message").focus();
                return false;
            }
            e.preventDefault();
            $("#sendMessageForm").find("button.submit").prop("disabled", true);
            $.ajax({
                headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
                url: $app_url + "/app-view/message/send",
                type: 'POST',
                cache: false,
                data: $("#sendMessageForm").serialize(),
                datatype: 'html',
                beforeSend: function () {
                    //something before send
                },
                success: function (data) {
                    $('#new_message').val(null);
                    $("#messagesContainer").append(data.html);
                    reloadTime();
                    $("#sendMessageForm").find("button.submit").prop("disabled", false);
                    $('#messagesContainer').scrollTop($('#messagesContainer')[0].scrollHeight);
                },
                error: function () {
                    alert('error');
                }
            });
        });
    });

    function reloadNewMessage() {
        var last_message_id = $('#last_message_id').val();
        var login_user_type = $('#login_user_type').val();
        if (last_message_id != null) {
            $.ajax({
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                url: $app_url + "/app-view/check-new-message",
                type: 'post',
                data: {
                    "conversation_id": conversation_id,
                    "last_message_id": last_message_id,
                    "login_user_type": login_user_type
                },
                datatype: 'json',
                success: function (data) {
                    if (data.last_message_id != null) {
                        $("#messagesContainer").append(data.html);
                        reloadTime();
                        $('#last_message_id').val(data.last_message_id);
                        $('#messagesContainer').scrollTop($('#messagesContainer')[0].scrollHeight);
                    }
                },
                error: function () {
                    alert('error');
                }
            });
        }
    }
    function reloadTime() {
        $(".time").each(function () {
            $(this).html(jQuery.timeago($(this).attr('datetime')));
        })
    }
</script>
</body>
</html>