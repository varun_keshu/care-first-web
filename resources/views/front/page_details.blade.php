@extends('front.layout.master')
@section('content')
    <div class="inner-pages-title-container">
        <div class="container">
            <!-- Section-tittle -->
            <div class="inner-pages-title">
                <h2>{!! $page->title ?? "No Record Found!" !!}</h2>
                <div class="app-shape app-shape1">
                    <img src="http://carefirst-local.com/front/img/shape/app-shape-top.png" alt="" class="app-shape-top heartbeat d-none d-lg-block">
                </div>
            </div>
        </div>
    </div>
    @if(isset($page->content))
        <section class="sample-text-area pb-50 pt-50">
            <div class="container box_1170 inner-pages-content">
                <div class="inner-pages-item">
                    {!! $page->content !!}
                </div>
            </div>
        </section>
    @endif
@endsection
