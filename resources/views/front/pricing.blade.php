@extends('front.layout.master')
@section('content')
    <section class="pricing-header">
        <div class="pricing-shape rotateme">&nbsp;</div>
        <div class="pricing-shape1 rotateme">&nbsp;</div>
        <div class="app-shape app-shape1">
            <img src="{{ url('front/img/shape/app-shape-top.png') }}" alt=""
                 class="app-shape-top heartbeat d-none d-lg-block">
        </div>
        <div class="container">
            @if(count($packages) > 0)
                <h1>Choose Your Very Best Pricing Plan.</h1>
            @else
                <h1>No Plan Available.</h1>
            @endif
        </div>
    </section>
    @if(count($packages) > 0)
        <div class="pricing-card-area-new pricing-card-area pt-5">
            <div class="container">
                <div class="row">
                    @foreach($packages as $package)
                        <div class="col-xl-3 col-lg-3 col-md-6">
                            <div class="single-card text-center  mb-5">
                                <div class="card-top">
                                    <span>{{ $package['duration'] }} {{ $package['duration_unit'] }}</span>
                                    <h4>{{ formatPrice($package['price']) }}</h4>
                                </div>
                                <div class="card-bottom">
                                    <ul>
                                        <li><h2>{{ ucfirst($package['title']) }}</h2></li>
                                        <li><h5>Service Category: {{ ucfirst($package['services']) }}</h5></li>
                                        <li><h6>{{ ucfirst($package['service_category']) }}</h6></li>
                                        <li><h6>Service Count : {{ $package['service_count'] }}</h6></li>
                                        <li><p>{{ ucfirst($package['description'] ?:"---") }}</p></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        <!-- Pricing Card End -->
            <!-- Available App  Start-->
            <div class="available-app-area  pt-50 pb-50">
                <div class="container">
                    <div class="row d-flex download-container justify-content-between">
                        <div class="col-xl-4 col-lg-4">
                            <div class="app-caption">
                                <div class="section-tittle section-tittle3">
                                    <h2>Our App Available For Any Device Download now</h2>
                                    <p>AVAILABLE ANYTIME, ANYWHERE.<br>
                                        Browse From iPhone , iPad , Android Mobile Devices Or Tablets</p>
                                    <div class="app-btn">
                                        <a href="#" class="app-btn1"><img
                                                    src="{{asset('front/img/shape/app_btn1.png')}}"
                                                    alt=""></a>
                                        <a href="#" class="app-btn2"><img
                                                    src="{{asset('front/img/shape/app_btn2.png')}}"
                                                    alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8">
                            <div class="platform-container">
                                <div class="platform-main">
                                    <div class="row">
                                        <div class="mobile">
                                            <div class="mobile-inner">
                                                <div class="mobile-item">
                                                    <img src="{{ url('front/img/home-screenshot.jpg') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="laptop">
                                            <div class="laptop-inner">
                                                <div class="laptop-item">
                                                    <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mack">
                                            <div class="mack-inner">
                                                <div class="mack-item">
                                                    <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Shape -->
                <div class="app-shape">
                    <img src="{{asset('front/img/shape/app-shape-top.png')}}" alt=""
                         class="app-shape-top heartbeat d-none d-lg-block">
                    <img src="{{asset('front/img/shape/app-shape-left.png')}}" alt=""
                         class="app-shape-left d-none d-xl-block">
                    <!-- <img src="assets/img/shape/app-shape-right.png" alt="" class="app-shape-right bounce-animate "> -->
                </div>
            </div>
            <!-- Available App End-->

            <!-- Say Something Start -->
            <div class="say-something-aera pt-90 pb-90 fix">
                <div class="container">
                    <div class="row justify-content-between align-items-center">
                        <div class="offset-xl-1 offset-lg-1 col-xl-5 col-lg-5">
                            <div class="say-something-cap">
                                <h2>Say Hello To The Collaboration Hub.</h2>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-3">
                            <div class="say-btn">
                                <a href="{{ route('contact') }}" class="btn radius-btn">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="say-shape">
                    <img src="{{asset('front/img/shape/say-shape-left.png')}}" alt=""
                         class="say-shape1 rotateme d-none d-xl-block">
                    <img src="{{asset('front/img/shape/say-shape-right.png')}}" alt=""
                         class="say-shape2 d-none d-lg-block">
                </div>
            </div>
        </div>
@endsection
