@extends('front.layout.master')
@section('content')
    <!-- Slider Area Start-->
    <div class="slider-area pb-50">
        <div class="slider-active">
            <div class="single-slider slider-height slider-padding d-flex align-items-center">
                <div class="container">
                    <div class="row d-flex align-items-center">
                        <div class="col-lg-5 col-md-9">
                            <div class="hero__caption">
                                <span data-animation="fadeInUp" data-delay=".4s">Care First Services</span>
                                <h4 data-animation="fadeInUp" data-delay=".6s"> On-Demand Home Services</h4>
                                <p data-animation="fadeInUp" data-delay=".8s">Repair, Cleaning, indoor, outdoor &
                                    More Services by Expert Professionals.Empowering millions of service professionals
                                    across India to deliver services at home like never seen before.</p>
                                <!-- Slider btn -->
                                <div class="slider-btns">
                                    <!-- Hero-btn -->
                                    <a data-animation="fadeInLeft" data-delay="1.0s" href="#"
                                       class="btn radius-btn custom-btn"><i class="fab fa-android" aria-hidden="true"></i> Android</a>
                                    <a data-animation="fadeInLeft" data-delay="1.0s" href="#"
                                       class="btn radius-btn custom-btn"><i class="fab fa-apple"></i> IOS</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="hero__img d-none d-lg-block f-right" data-animation="fadeInRight"
                                 data-delay="1s">
                                <img src="{{ url('front/img/hero/hero_right-new.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Slider Area End -->
    <!-- Best Features Start -->
    <section class="best-features-area section-padd4">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-xl-12 col-lg-12">
                    <!-- Section Tittle -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="section-tittle">
                                <h2 class="mb-3 text-center">Some of the best features Of Our App!</h2>
                            </div>
                        </div>
                    </div>
                    <!-- Section caption -->
                    <div class="row mt-4">
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="single-features mb-70">
                                <div class="features-icon">
                                    <span class="flaticon-support"></span>
                                </div>
                                <div class="features-caption">
                                    <h3>Easy to Customize</h3>
                                    <p>It should be easy for a customer to add or remove services as they desire.
                                        The customers should be able to find what they’re
                                        looking for easily and there should be clear filtering and sorting options.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="single-features mb-70">
                                <div class="features-icon">
                                    <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                </div>
                                <div class="features-caption">
                                    <h3>Ratings & Reviews</h3>
                                    <p>It must allow your user to rate & review the service provider. This feature is
                                        very beneficial as it acts as a feedback and
                                        helps other users to select service providers of higher ratings. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="single-features mb-70">
                                <div class="features-icon">
                                    <span><i class="fa fa-check-circle" aria-hidden="true"></i></span>
                                </div>
                                <div class="features-caption">
                                    <h3>Accept/Reject Requests</h3>
                                    <p>Once the service provider receives the requests, he must have the authority to
                                        review it.
                                        Once he reviews it ,he must accept it if he likes the job or reject it if he
                                        doesn’t.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="single-features mb-70">
                                <div class="features-icon">
                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                                <div class="features-caption">
                                    <h3>Real-time Request</h3>
                                    <p>It is imperative that the service-providers get the inquiry notifications in
                                        real-time.
                                        Faster the notification better your app and service will perform.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- Best Features End -->
    <!-- Services Area Start -->
    @if(count($service_categories) > 0)
        <section class="service-area light-grey section-padding2">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-tittle text-center">
                            <h2>How can we help you with Care First Services!</h2>
                        </div>
                    </div>
                </div>
                <!-- Section caption -->
                <div class="row">
                    @foreach($service_categories as $service_category)
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="services-caption text-center mb-30">
                                <div class="service-icon">
                                    {{--<span class="flaticon-businessman"></span>--}}
                                    <span><img src="{{ showServiceCategoryIconImage($service_category->icon_image) }}"
                                               alt="" height="60px"></span>
                                </div>
                                <div class="service-cap">
                                    <h4>
                                        <a href="{{ route('serviceCategory.detail', $service_category->id) }}">{{ $service_category->title }}</a>
                                    </h4>
                                    <p title="{{ $service_category->description }}">{{substr($service_category->description, 0, 40)}}
                                        ...</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
    <!-- Services Area End -->

    <!-- Applic App Start -->
    <div class="applic-apps section-padding2">
        <div class="container-fluid">
            <div class="row">
                <!-- slider Heading -->
                <div class="col-xl-4 col-lg-4 col-md-8">
                    <div class="single-cases-info mb-30">
                        <h3>Applic Apps Screenshot</h3>
                        <p>It caters to their exact needs, letting them manage multiple things in a single roof thereby
                            saving in a lot of time and costs.
                            In simple words, on-demand app development involves creating custom on demand mobile
                            applications on platforms such as Android and iOS,
                            which are then used by customers to deliver their serviceable needs. </p>
                    </div>
                </div>
                <!-- OwL -->
                <div class="col-xl-8 col-lg-8 col-md-col-md-7">
                    <div class="app-active owl-carousel">
                        <div class="single-cases-img">
                            <img src="{{ url('front/img/gallery/App1.png') }}" alt="">
                        </div>
                        <div class="single-cases-img">
                            <img src="{{ url('front/img/gallery/App2.png') }}" alt="">
                        </div>
                        <div class="single-cases-img">
                            <img src="{{ url('front/img/gallery/App3.png') }}" alt="">
                        </div>
                        <div class="single-cases-img">
                            <img src="{{ url('front/img/gallery/App4.png') }}" alt="">
                        </div>
                        <div class="single-cases-img">
                            <img src="{{ url('front/img/gallery/App1.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Applic App End -->
    <!-- Best Pricing Start -->
    @if(count($packages) > 0)
        <section class="best-pricing pricing-padding" data-background="front/img/gallery/best_pricingbg.jpg">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row d-flex justify-content-center my-3">
                    <div class="col-lg-6 col-md-8">
                        <div class="section-tittle section-tittle2 text-center">
                            <h2>Choose Your Very Best Pricing Plan</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Best Pricing End -->
        <!-- Pricing Card Start -->

        <div class="pricing-card-area">
            <div class="container">
                <div class="row">
                    @foreach($packages as $package)
                        <div class="col-xl-3 col-lg-3 col-md-6">
                            <div class="single-card text-center mb-30">
                                <div class="card-top">
                                    <span>{{ $package['duration'] }} {{ $package['duration_unit'] }}</span>
                                    <h4>{{ formatPrice($package['price']) }}</h4>
                                </div>
                                <div class="card-bottom">
                                    <ul>
                                        <li><h2>{{ ucfirst($package['title']) }}</h2></li>
                                        <li><h5>Service Category: {{ ucfirst($package['services']) }}</h5></li>
                                        <li><h6>{{ ucfirst($package['service_category']) }}</h6></li>
                                        <li><h6>Service Count : {{ $package['service_count'] }}</h6></li>
                                    </ul>
                                    {{--<a href="javascript:;" class="btn card-btn1">Get Started</a>--}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <!-- Pricing Card End -->
    <!-- Our Customer Start -->
    @if(count($testimonials) > 0)
        <div class="our-customer light-grey section-padd-top30">
            <div class="container-fluid">
                <div class="our-customer-wrapper">
                    <!-- Section Tittle -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-xl-8">
                            <div class="section-tittle text-center">
                                <h2>What Our Customers<br> Say About Us</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="customar-active dot-style d-flex dot-style">
                                @foreach($testimonials as $testimonial)
                                    <div class="single-customer mb-100">
                                        <div class="what-img">
                                            <img src="{{ showTestimonialImage($testimonial->image) }}"
                                                 class="rounded-circle" alt="" height="100px">
                                        </div>
                                        <div class="what-cap">
                                            <h4><a href="#">{{ $testimonial->title }}</a></h4>
                                            <p>{{ $testimonial->description }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- Our Customer End -->
    <!-- Available App  Start-->
    <div class="available-app-area pt-50 pb-50">
        <div class="container">
            <div class="row d-flex download-container justify-content-between">
                <div class="col-xl-4 col-lg-4">
                    <div class="app-caption">
                        <div class="section-tittle section-tittle3">
                            <h2>Our App Available For Any Device Download now</h2>
                            <p>AVAILABLE ANYTIME, ANYWHERE.<br>
                                Browse From iPhone , iPad , Android Mobile Devices Or Tablets</p>
                            <div class="app-btn">
                                <a href="#" class="app-btn1"><img src="{{ url('front/img/shape/app_btn1.png') }}"
                                                                  alt=""></a>
                                <a href="#" class="app-btn2"><img src="{{ url('front/img/shape/app_btn2.png') }}"
                                                                  alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8">
                   {{-- <div class="app-img">--}}
                        {{--<img src="{{ url('front/img/shape/available-app.png') }}" alt="">--}}
                        {{--</div>--}}
                        <div class="platform-container">
                            <div class="platform-main">
                                <div class="row">
                                    <div class="mobile">
                                        <div class="mobile-inner">
                                            <div class="mobile-item">
                                                <img src="{{ url('front/img/home-screenshot.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="laptop">
                                        <div class="laptop-inner">
                                            <div class="laptop-item">
                                                <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mack">
                                        <div class="mack-inner">
                                            <div class="mack-item">
                                                <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Shape -->
            <div class="app-shape">
                <img src="{{ url('front/img/shape/app-shape-top.png') }}" alt=""
                     class="app-shape-top heartbeat d-none d-lg-block">
                <img src="{{ url('front/img/shape/app-shape-left.png') }}" alt=""
                     class="app-shape-left d-none d-xl-block">
                <!-- <img src="front/img/shape/app-shape-right.png" alt="" class="app-shape-right bounce-animate "> -->
            </div>
        </div>
        <!-- Available App End-->
    </div>
    <!-- Say Something Start -->
    @if(count($clients) > 0)
        <div class="say-something-aera pt-50 pb-50">
            <div class="container">
                <div class="double-heading-tk pt-4">
                    <div class="row">
                        @foreach($clients as $client)
                            <div class="client-border-box col-md-3  col-lg-3 text mb-30 ">
                                <div class="service-img">
                                    <span><img src="{{ showClientImage($client->image) }}" alt="" height="60px"></span>
                                </div>
                                <div class="services mt-20 ">
                                    <h2>{{ $client->name }}</h2>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- Say Something End -->
@endsection
