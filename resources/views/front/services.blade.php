@extends('front.layout.master')
@section('css')
    <link href="{{ asset('front/css/service_category_detail.css') }}" rel="stylesheet"/>
@endsection
@section('content')
    @if(isset($service_category->banner_image))
        <section>
            <div class="#">
                <div class="category-hero">
                    <img src="{{ showServiceCategoryBannerImage($service_category->banner_image) }}"/>
                    <div class="img-content">
                        <h2>
                            {{ $service_category->title }}
                        </h2>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <!-- End Hero Slider Section -->
    @if(isset($service_category) && count($service_category->relatedservices) > 0)
        <div class="pick-service-sec light-grey pt-50 pb-50">
            <div class="container">
                <div class="section-tittle text-center">
                    <h2>Services</h2>
                </div>
                <div class="row">
                    @foreach($service_category->relatedservices as $service)
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="service-card">
                                <div class="service-img-wrap">
                                    <img src="{{showServiceImage($service->image) }}"
                                         class="img-fluid">
                                </div>
                                <div class="service-card-content">
                                    <h4 class="service-title">{{ $service->title }}</h4>
                                    <p class="service-description">
                                        {{$service->description}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <!-- End Choose Service Section -->
    @if(isset($service_category) && count($service_category->serviceProducts) > 0)
        <div class="service-info-sec pt-50 pb-50">
            <div class="container">
                <div class="section-tittle text-center">
                    <h2>Products We Use</h2>
                </div>
                <div class="owl-carousel productBrandSlider">
                    @foreach($service_category->serviceProducts as $index => $serviceProduct)
                        <div class="product-brand">
                            <img src="{{ showServiceCategoryProductImage($serviceProduct->image) }}">
                        </div>

                    @endforeach
                </div>
            </div>
        </div>

    @endif
    {{-- <!-- End Using Products Section -->
     @if(isset($service_category) && count($service_category->serviceInstructions) > 0)
         <div class="service-info-sec light-grey pt-50 pb-50">
             <div class="container">
                 <div class="section-tittle text-center">
                     <h2>Service Instructions</h2>
                 </div>
                 <div class="row">
                     <div class="col-md-12">
                         @foreach($service_category->serviceInstructions as $index => $serviceInstruction)
                             <div class="media service-info">
                                 <i class="ti-dropbox-alt"></i>
                                 <div class="media-body">
                                     <p>{{ $serviceInstruction->title}}
                                     </p>
                                 </div>
                             </div>
                         @endforeach
                     </div>
                 </div>
             </div>
         </div>
     @endif--}}
    <!-- Available App  Start-->
    <div class="available-app-area  pt-50 pb-50">
        <div class="container">
            <div class="row d-flex download-container justify-content-between">
                <div class="col-xl-4 col-lg-4">
                    <div class="app-caption">
                        <div class="section-tittle section-tittle3">
                            <h2>Our App Available For Any Device Download now</h2>
                            <p>AVAILABLE ANYTIME, ANYWHERE.<br>
                                Browse From iPhone , iPad , Android Mobile Devices Or Tablets</p>
                            <div class="app-btn">
                                <a href="#" class="app-btn1"><img src="{{asset('front/img/shape/app_btn1.png')}}"
                                                                  alt=""></a>
                                <a href="#" class="app-btn2"><img src="{{asset('front/img/shape/app_btn2.png')}}"
                                                                  alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8">
                    {{--<div class="app-img">--}}
                    {{--<img src="{{asset('front/img/shape/available-app.png')}}" alt="">--}}
                    {{--</div>--}}
                    <div class="platform-container">
                        <div class="platform-main">
                            <div class="row">
                                <div class="mobile">
                                    <div class="mobile-inner">
                                        <div class="mobile-item">
                                            <img src="{{ url('front/img/home-screenshot.jpg') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="laptop">
                                    <div class="laptop-inner">
                                        <div class="laptop-item">
                                            <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mack">
                                    <div class="mack-inner">
                                        <div class="mack-item">
                                            <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shape -->
        <div class="app-shape">
            <img src="{{asset('front/img/shape/app-shape-top.png')}}" alt=""
                 class="app-shape-top heartbeat d-none d-lg-block">
            <img src="{{asset('front/img/shape/app-shape-left.png')}}" alt="" class="app-shape-left d-none d-xl-block">
            <!-- <img src="assets/img/shape/app-shape-right.png" alt="" class="app-shape-right bounce-animate "> -->
        </div>
    </div>
    <!-- Available App End-->

    <!-- Say Something Start -->
    <div class="say-something-aera pt-90 pb-90 fix">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="offset-xl-1 offset-lg-1 col-xl-5 col-lg-5">
                    <div class="say-something-cap">
                        <h2>Say Hello To The Collaboration Hub.</h2>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3">
                    <div class="say-btn">
                        <a href="{{ route('contact') }}" class="btn radius-btn">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- shape -->
        <div class="say-shape">
            <img src="{{asset('front/img/shape/say-shape-left.png')}}" alt=""
                 class="say-shape1 rotateme d-none d-xl-block">
            <img src="{{asset('front/img/shape/say-shape-right.png')}}" alt="" class="say-shape2 d-none d-lg-block">
        </div>
    </div>
    <!-- Available App End-->
@endsection

