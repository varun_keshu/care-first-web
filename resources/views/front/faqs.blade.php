@extends('front.layout.master')
@section('css')
    <style>
        .side-categories .list-group-item.active {
            background-color: transparent;
            -webkit-background-color: transparent;
            border-color: rgba(0, 0, 0, 0.125);
            -webkit-border-color: rgba(0, 0, 0, 0.125);
            border-bottom-color: #003262;
            -webkit-border-bottom-color: #003262;
            border-bottom-width: 3px;
        }
    </style>
@endsection
@section('content')
    <div class="inner-pages-title-container">
        <div class="container">
            <!-- Section-tittle -->
            <div class="inner-pages-title">
                <h2>FAQs</h2>
                <div class="app-shape app-shape1">
                    <img src="http://carefirst-local.com/front/img/shape/app-shape-top.png" alt="" class="app-shape-top heartbeat d-none d-lg-block">
                </div>
            </div>
        </div>
    </div>
    <section class="sample-text-area">
        <div class="container box_1170">
            <div class="section-top-border">
                <div class="row">
                    <div class="col-md-3">
                        <div class="side-sub-categories side-categories mb-3">
                            <ul class="list-group" id="faq">
                                @foreach(\App\Models\Faqs::CATEGORIES as $key => $value)
                                    <li class="list-group-item p-2 rounded-0 questionsList">
                                        <a href="javascript:;" data-category="{{ $key }}"
                                           class="text-dark faqCategory">
                                            <div class="media">
                                                <i class="fa fa-chevron-right text-dark mt-1"></i>
                                                <div class="media-body pl-1">
                                                    {{ $value }}
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @php
                        $categories = [];
                    @endphp
                    <div class="col-md-9 mt-sm-20">

                        {{--<div class="widget-content">
                            <ul id="accordion">
                                <li>
                                    <h4>Collection <span class="plusminus">+</span></h4>
                                    <ul>
                                        <li><a href="#">Golf</a></li>
                                        <li><a href="#">Cricket</a></li>
                                        <li><a href="#">Football</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <h4>Technology <span class="plusminus">+</span></h4>
                                    <ul>
                                        <li><a href="#">Macbook</a></li>
                                        <li><a href="#">Ipad</a></li>
                                        <li><a href="#">Iphone</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <h4>Furniture <span class="plusminus">+</span></h4>
                                    <ul>
                                        <li><a href="#">Chairs</a></li>
                                        <li><a href="#">Tables</a></li>
                                        <li><a href="#">Storage</a></li>
                                        <li><a href="#">Office</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>--}}

                        @foreach($faqs_list as $category => $faqs)
                            <div id="accordion" class="{{ $category }} accordionDiv" style="display: none">
                                @foreach($faqs as $faq)
                                    <div class="card mb-3">
                                        <div class="card-header border-bottom-none">
                                            <a class="card-link text-dark" data-toggle="collapse"
                                               href="#collapse_{{ $faq->id }}">
                                                {{ $faq->question }}
                                            </a>
                                        </div>
                                        <div id="collapse_{{ $faq->id }}" class="collapse" data-parent="#accordion">
                                            <div class="card-body tab-container">
                                                {!! $faq->answer !!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @php
                                $categories[] = $category;
                            @endphp
                        @endforeach
                        @foreach(\App\Models\Faqs::CATEGORIES as $category_key => $cat_value)
                            @if(!in_array($category_key, $categories))
                                <div id="accordion" class="{{ $category_key }} accordionDiv" style="display: none">
                                    No Record Found!
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(".faqCategory").click(function () {
            $(".questionsList").removeClass("active");
            $(this).parent('.questionsList').addClass("active");
            var category = $(this).data('category');
            $(".accordionDiv").hide();
            $("." + category).show();
        });
        $(".faqCategory:first").trigger('click');

        $(document).ready(function () {
            $("#accordion li > h4").click(function () {

                if ($(this).next().is(':visible')) {
                    $(this).next().slideUp(300);
                    $(this).children(".plusminus").text('+');
                } else {
                    $(this).next("#accordion ul").slideDown(300);
                    $(this).children(".plusminus").text('-');
                }
            });
        });
    </script>

@endsection
