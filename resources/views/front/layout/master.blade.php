<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{url('images/favicon.ico')}}">
    <!-- Meta -->
    <meta name="description" content="Care First Website">
    <meta name="author" content="ThemePixels">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Care First</title>
    @include('front.layout.global.css')
    @yield('css')
</head>
<body>
<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="{{asset('front/img/logo/logo.png')}}" alt="">
            </div>
        </div>
    </div>
</div>
@include('front.layout.partials.header')
<main>
@yield('content')
</main>
@include('front.layout.partials.footer')
@include('front.layout.global.js')
@yield('footer_scripts')
</body>
</html>
