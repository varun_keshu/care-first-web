<div class="footer-main">
    <div class="footer-area light-grey pt-50 pb-50">
        <div class="container">
            <div class="row  justify-content-between">
                <div class="col-lg-3 col-md-4 col-sm-8">
                    <div class="single-footer-caption mb-30">
                        <!-- logo -->
                        <div class="footer-logo">
                            <a href="{{route('home')}}"><img src="{{asset('front/img/logo/logo2_footer.png')}}" alt=""></a>
                        </div>
                        <div class="footer-tittle">
                            <div class="footer-pera">
                                <p class="info1">Repair, Cleaning, Indoor, Outdoor &
                                    More Services By Expert Professionals.
                                    Empowering Millions Of Service Professionals Across India To Deliver Services At
                                    Home Like Never Seen Before.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-5">
                    <div class="single-footer-caption mb-50">
                        <div class="footer-tittle">
                            <h4>Quick Links</h4>
                            <ul>
                                <li><a href="{{ route('page_detail', 'about-us') }}">About Us</a></li>
                                <li><a href="{{ route('feature') }}">Features</a></li>
                                <li><a href="{{ route('price') }}">Pricing</a></li>
                                <li><a href="{{ route('service') }}">Services</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-7">
                    <div class="single-footer-caption mb-50">
                        <div class="footer-tittle">
                            <h4>Support</h4>
                            <ul>
                                <li><a href="{{ route('page_detail', 'privacy-policy') }}">Privacy Policy</a></li>
                                <li><a href="{{ route('page_detail', 'terms-and-conditions') }}">Terms & Conditions</a></li>
                                <li><a href="{{ route('faq') }}">FAQs</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Copy-Right -->
            <div class="row align-items-center">
                <div class="col-xl-12 ">
                    <div class="footer-copy-right">
                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
