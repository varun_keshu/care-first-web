<header>
    <div class="header-area header-transparrent ">
        <div class="header-sticky">
            <div class="main-header header-sticky">
                <div class="container">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-2 col-lg-2 col-md-2">
                            <div class="logo">
                                <a href="{{route('home')}}"> <img src="{{asset('front/img/logo/logo.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-10 col-md-10">
                            <!-- Main-menu -->
                            <div class="main-menu f-right d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li class="@if(Request::is('/')) active @endif">
                                            <a href="{{route('home')}}">Home</a></li>
                                        <li class="@if(Request::is('features')) active @endif">
                                            <a href="{{route('feature')}}" class="active">Features</a></li>
                                        <li class="@if(Request::is('services')) active @endif">
                                            <a href="{{route('service')}}">Services</a></li>
                                        <li class="@if(Request::is('prices')) active @endif">
                                            <a href="{{route('price')}}">Pricing</a></li>
                                        <li class="@if(Request::is('contact')) active @endif">
                                            <a href="{{ route('contact') }}">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
