<!-- All JS Custom Plugins Link Here here -->
<script src="{{ asset('front/js/modernizr-3.5.0.min.js') }}"></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="{{ asset('front/js/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('front/js/popper.min.js') }}"></script>
<script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
<!-- Jquery Mobile Menu -->
<script src="{{ asset('front/js/jquery.slicknav.min.js') }}"></script>
<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="{{ asset('front/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('front/js/slick.min.js') }}"></script>
<!-- Date Picker -->
<script src="{{ asset('front/js/gijgo.min.js') }}"></script>
<!-- One Page, Animated-HeadLin -->
<script src="{{ asset('front/js/wow.min.js') }}"></script>
<script src="{{ asset('front/js/animated.headline.js') }}"></script>
<script src="{{ asset('front/js/jquery.magnific-popup.js') }}"></script>
<!-- Scrollup, nice-select, sticky -->
<script src="{{ asset('front/js/jquery.scrollUp.min.js') }}"></script>
<script src="{{ asset('front/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('front/js/jquery.sticky.js') }}"></script>
<!-- contact js -->
<script src="{{ asset('front/js/contact.js') }}"></script>
<script src="{{ asset('front/js/jquery.form.js') }}"></script>
<script src="{{ asset('front/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('front/js/mail-script.js') }}"></script>
<script src="{{ asset('front/js/jquery.ajaxchimp.min.js') }}"></script>
<!-- Jquery Plugins, main Jquery -->
<script src="{{ asset('front/js/plugins.js') }}"></script>
<script src="{{ asset('front/js/main.js') }}"></script>

{{--<script src="{{ asset('front/js/jquery.paroller.min.js') }}"></script>--}}
{{--<script src="{{ asset('front/js/one-page-nav-min.js') }}"></script>--}}






