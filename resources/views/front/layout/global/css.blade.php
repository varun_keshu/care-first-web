<link href="{{ asset('front/css/bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/owl.carousel.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/flaticon.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/slicknav.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/animate.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/magnific-popup.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/fontawesome-all.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/themify-icons.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/slick.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/nice-select.css') }}" rel="stylesheet"/>
<link href="{{ asset('front/css/style.css') }}" rel="stylesheet"/>

