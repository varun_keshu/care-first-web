@extends('front.layout.master')
@section('content')
    <section class="category-slider-sec">
        <div class="owl-carousel categorySlider">
            <div class="category-hero">
                <img src="{{ url('images/services/Cleaning-Services.jpg') }}"/>
                <div class="img-content">
                    <h2>Cleaning & Disinfection</h2>
                </div>
            </div>
            <div class="category-hero">
                <img src="{{ url('images/services/Electrical.jpg') }}"/>
                <div class="img-content">
                    <h2>Electrical</h2>
                </div>
            </div>
            <div class="category-hero">
                <img src="{{ url('images/services/Ac-Service.jpg') }}"/>
                <div class="img-content">
                    <h2>AC Services</h2>
                </div>
            </div>
            <div class="category-hero">
                <img src="{{ url('images/services/plumbing.jpg') }}"/>
                <div class="img-content">
                    <h2>plumbing</h2>
                </div>
            </div>
            <div class="category-hero">
                <img src="{{ url('images/services/Ro-Service.jpg') }}"/>
                <div class="img-content">
                    <h2>Ro Service</h2>
                </div>
            </div>
            <div class="category-hero">
                <img src="{{ url('images/services/handyman.jpg') }}"/>
                <div class="img-content">
                    <h2>Handyman</h2>
                </div>
            </div>
            <div class="category-hero">
                <img src="{{ url('images/services/Pest-Control.jpg') }}"/>
                <div class="img-content">
                    <h2>Pest Control</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="how-it-works-chain-main light-grey pt-5 pb-5">
        <div class="container">
            <ul class="how-it-works-chain">
                <div class="double-heading-tk pt-4">
                    <div class="row">
                        @foreach($service_category as $data)
                            <div class="col-sm-12 col-md-4 col-lg-3 col-xl-3  text mb-30">
                                <div class="how-it-work-chain-box">
                                    <div class="service-img">
                                        <span><img src="{{showServiceCategoryImage($data->image) }}"
                                                   alt="" height="60px"></span>
                                    </div>
                                    <div class="services services-link mt-20">
                                        <h4><a href="{{ route('serviceCategory.detail', $data->id)}}">{{ $data->title }}</a></h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </ul>
        </div>
    </section>
    <div class="section-padding1 pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="feature-img col-lg-6 scat-col mrk-left wow fadeInRight" data-wow-duration="1000ms"
                     data-wow-delay="800ms">
                    <span class="img-hover-zoom img-hover-zoom--zoom-n-pan-v"><img
                                src="{{asset('images/services/Electrical.jpg') }}"></span>
                </div>
                <div class="feature-img col-lg-6 scat-col mrk-right scat1 wow fadeInLeft">
                    <h2>Electrical</h2>
                    <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>We have expertise in all electrical works, indoor and outdoor installations.</p>
                    <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>We do internal and external wiring, switch gear maintenance, panel boards, installation of ELCB and other electrical works.</p>
                    <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>We do all types of Maintenance Services for Domestic as well as Commercial Set up.</p>
                    <p><i class="fa fa-angle-double-right" aria-hidden="true"></i>We are enriched with a strong Team to Execute Energy Audits, power Saving suggestions and Referral one point contact for subordinate services.</p>
                    </div>
            </div>
        </div>
    </div>
    <div class="section-padding1 light-grey pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="feature-img services-ul col-lg-6 scat-col mrk-left scat1 wow fadeInDown">
                    <h2>AC Service</h2>
                    <p><span> <i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We have expertise in all Types of AC Maintenance and Repair.</font></p>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We are having a result oriented team for HVAC needs of Domestic as well as Industrial Customers.</font></p>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We are the turnkey project Executor In terms of HVAC Designing, Duct Fabrication, Insulation, Area qualification and Validation works.</font></p>
                        <ul>
                        <li><i class="fas fa-circle"></i>We are Expert in:</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><font>Monoblock Air Conditioners</font></li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><font>Monoblock Air Conditioners</font></li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><font>Monoblock Air Conditioners</font></li>

                     </ul>

                </div>
                <div class="feature-img col-lg-6 scat-col mrk-right wow fadeInDown" data-wow-duration="1000ms"
                     data-wow-delay="800ms">
                    <span class="img-hover-zoom img-hover-zoom--zoom-n-pan-v"><img
                                src="{{asset('images/services/Ac-Service.jpg') }}"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="section-padding1 pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="feature-img col-lg-6 scat-col mrk-left wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="800ms">
                    <span class="img-hover-zoom img-hover-zoom--zoom-n-pan-v"><img
                                src="{{asset('images/services/Cleaning-Services.jpg') }}"></span>
                </div>
                <div class="feature-img col-lg-6 scat-col mrk-right scat1 wow fadeInRight">
                    <h2>Cleaning Services</h2>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>A residential community/Commercial Building cleaning is based on a contract cleaning for the apartments/villas/Buildings of a community. This type of services is always scheduled and it has a planned frequency on a daily, weekly or monthly basis.</font></p>
                    <p><span> <i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Private home cleaning is based on a direct contract with the landlord, where our company is delivering weekly cleaning services with occasional one-time interventions.</font></p>
                    <p><span> <i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Cleaning services include floors, Carpets, Upholstery, Curtains, Sofa Sets, Windows, Furniture, Home appliances, Toiletry and all areas inside and outside your home.</font></p>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We are having our dedicated team for Pool Cleaning & Water storage bodies cleaning (Overhead/Underground Tank).</font></p>

                </div>
            </div>
        </div>
    </div>
    <div class="section-padding1 light-grey pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="feature-img col-lg-6 scat-col mrk-left scat1 wow fadeInLeft">
                    <h2>Maintenance</h2>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Preventive Maintenance: Our priority projects are preventive maintenance for public and private establishments. We believe that maintenance should be focused on solving problems before they occur.</font></p>
                    <p><span> <i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Routine Maintenance: We provide and willing to undertake any kind of daily maintenance operations in a most cost effective, competitive and creative performance and results.</font></p>
                    <p> <span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Civil Maintenance: Our civil maintenance includes tile lying, grouting, plumbing, and sanitary, carpentry, false ceiling, and other construction jobs.</font></p>

                </div>
                <div class="feature-img  col-lg-6 scat-col mrk-right wow fadeInDown" data-wow-duration="1000ms"
                     data-wow-delay="800ms">
                    <span class="img-hover-zoom img-hover-zoom--zoom-n-pan-v"><img src="{{asset('images/services/carpenter.jpg') }}"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="section-padding1 pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="feature-img col-lg-6 scat-col mrk-left wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="800ms">
                    <span class="img-hover-zoom img-hover-zoom--zoom-n-pan-v"><img
                                src="{{asset('images/services/plumbing.jpg') }}"></span>
                </div>
                <div class="feature-img col-lg-6 scat-col mrk-right scat1 wow fadeInRight">
                    <h2>Plumbing</h2>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We are ready to respond to all plumbing needs in a home or commercial Set up, from installing a new Geyser, dishwasher, redesigning your bathroom or fixing any leakages. </p>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font> Our specialists are focused on providing the best solution for your home/office to be water safe proofed and functional.</font> </p>

                </div>
            </div>
        </div>
    </div>
    <div class="section-padding1 light-grey pt-5 pb-5">

        <div class="container">
            <div class="row">
                <div class="feature-img col-lg-6 scat-col mrk-left scat1 wow fadeInLeft">
                    <h2>RO Services</h2>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We have expertise in all Types of RO Machines, be it a Domestic one or a Commercial RO.</font></p>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We are experts in AMC service of all type of RO machines and we also provide solutions for Hard water, Saline water etc.</font>
                    </p>
                </div>
                <div class="feature-img  col-lg-6 scat-col mrk-right wow fadeInDown" data-wow-duration="1000ms"
                     data-wow-delay="800ms">
                    <span class="img-hover-zoom img-hover-zoom--zoom-n-pan-v"><img
                                src="{{asset('images/services/Ro-Service.jpg') }}"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="section-padding1 pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="feature-img col-lg-6 scat-col mrk-left wow fadeInLeft" data-wow-duration="1000ms"
                     data-wow-delay="800ms">
                    <span class="img-hover-zoom img-hover-zoom--zoom-n-pan-v"><img
                                src="{{asset('images/services/handyman.jpg') }}"></span>
                </div>
                <div class="feature-img col-lg-6 scat-col mrk-right scat1 wow fadeInRight">
                    <h2>Handyman</h2>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Our handyman services cover everything from changing a light bulb to putting up shelves, fixing furniture or create customized solution for every challenge you find in your home.</font>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font> With this service we take care of the time consuming jobs so that you can enjoy your home stress free.</font>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-padding1 light-grey pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="feature-img col-lg-6 scat-col mrk-left scat1 wow fadeInLeft">
                    <h2>Pest Control</h2>
                    <p><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We handle all types of pest infections and we deliver solutions for your home & offices.</font></p>
                    <p> <span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>We have expertise in Pest elimination from kitchen cockroaches to mice and all in between.</font></p>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Our Pest control services include also a site assessment of your property because we believe prevention is the best solution in avoiding other pest infections.</font></p>
                    <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>All the pest controls are done with high care regarding the chemicals used, making sure the quantity of pesticides are kept at the minimum.</font></p>

                </div>
                <div class="feature-img  col-lg-6 scat-col mrk-right wow fadeInDown" data-wow-duration="1000ms"
                     data-wow-delay="800ms">
                    <span class="img-hover-zoom img-hover-zoom--zoom-n-pan-v"><img
                                src="{{asset('images/services/Pest-Control.jpg') }}"></span>
                </div>
            </div>
        </div>
    </div>
    <!-- Services Area End -->

    <!-- Available App  Start-->
    <div class="available-app-area  pt-50 pb-50">
        <div class="container">
            <div class="row d-flex download-container justify-content-between">
                <div class="col-xl-4 col-lg-4">
                    <div class="app-caption">
                        <div class="section-tittle section-tittle3">
                            <h2>Our App Available For Any Device Download now</h2>
                            <p>AVAILABLE ANYTIME, ANYWHERE.<br>
                                Browse From iPhone , iPad , Android Mobile Devices Or Tablets</p>
                            <div class="app-btn">
                                <a href="#" class="app-btn1"><img src="{{asset('front/img/shape/app_btn1.png')}}"
                                                                  alt=""></a>
                                <a href="#" class="app-btn2"><img src="{{asset('front/img/shape/app_btn2.png')}}"
                                                                  alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8">
                    {{--<div class="app-img">--}}
                    {{--<img src="{{asset('front/img/shape/available-app.png')}}" alt="">--}}
                    {{--</div>--}}
                    <div class="platform-container">
                        <div class="platform-main">
                            <div class="row">
                                <div class="mobile">
                                    <div class="mobile-inner">
                                        <div class="mobile-item">
                                            <img src="{{ url('front/img/home-screenshot.jpg') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="laptop">
                                    <div class="laptop-inner">
                                        <div class="laptop-item">
                                            <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mack">
                                    <div class="mack-inner">
                                        <div class="mack-item">
                                            <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shape -->
        <div class="app-shape">
            <img src="{{asset('front/img/shape/app-shape-top.png')}}" alt=""
                 class="app-shape-top heartbeat d-none d-lg-block">
            <img src="{{asset('front/img/shape/app-shape-left.png')}}" alt="" class="app-shape-left d-none d-xl-block">
            <!-- <img src="assets/img/shape/app-shape-right.png" alt="" class="app-shape-right bounce-animate "> -->
        </div>
    </div>
    <!-- Available App End-->

    <!-- Say Something Start -->
    <div class="say-something-aera pt-90 pb-90 fix">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="offset-xl-1 offset-lg-1 col-xl-5 col-lg-5">
                    <div class="say-something-cap">
                        <h2>Say Hello To The Collaboration Hub.</h2>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3">
                    <div class="say-btn">
                        <a href="{{ route('contact') }}" class="btn radius-btn">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- shape -->
        <div class="say-shape">
            <img src="{{asset('front/img/shape/say-shape-left.png')}}" alt=""
                 class="say-shape1 rotateme d-none d-xl-block">
            <img src="{{asset('front/img/shape/say-shape-right.png')}}" alt="" class="say-shape2 d-none d-lg-block">
        </div>
    </div>
    <!-- Say Something End -->


@endsection
