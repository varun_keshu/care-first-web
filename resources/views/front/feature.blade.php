@extends('front.layout.master')
@section('content')
   <section class="best-features-area best-features-area-new pb-50">
        <div class="container">
            <div class="app-shape app-shape1">
                <img src="../front/img/shape/app-shape-top.png" alt="" class="app-shape-top heartbeat d-none d-lg-block">
            </div>
            <div class="features-title">
                <h2>Some of the Best Features Of Our App!</h2>
           </div>
        <div class="row">
                <div class="col-sm-6 col-md-3 scat-col scat1 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="800ms">
                    <div class="fae scat-wrap feature-services-vh text-center feature-services">
                        <div class="feature-icon feature-icon-ani">
                            <span><i class="fa fa-server" aria-hidden="true"></i></span>
                        </div>
                        <h3>Service Providers </h3>
                        <p>Service providers will get a highly functional and interactive on-demand marketplace for home services, offering various features to improve the overall sales of your business.
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 scat-col scat1 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1000ms">
                    <div class="fae scat-wrap feature-services-vh text-center feature-services">
                        <div class="feature-icon">
                        <span><i class="fa fa-user-plus" aria-hidden="true"></i>
                             </span>
                        </div>
                        <h3>Admin </h3>
                        <p>A powerful centralized dashboard that manages and keeps track of all the aspects of your home services from assigning new requests, tracking service status to reviewing customer feedback.</p>

                    </div>
                </div>
                <div class="col-sm-6 col-md-3 scat-col scat1 wow fadeInDown " data-wow-duration="1000ms" data-wow-delay="1000ms">
                    <div class="fae scat-wrap feature-services-vh text-center feature-services">
                        <div class="feature-icon">
                        <span><i class="fa fa-user" aria-hidden="true"></i>
                             </span>
                        </div>
                         <h3>Customer</h3>
                        <p>Comprehensive uber-like solution for handyman services that provide your end-user with easy service request submission, response tracking, payment options and feedback of the total experience.</p>
                        </div>
                    </div>

                <div class="col-sm-6 col-md-3 scat-col scat1 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="800ms">
                    <div class="fae scat-wrap feature-services-vh text-center feature-services">
                        <div class="feature-icon">
                        <span><i class="fa fa-address-card" aria-hidden="true"></i>
                             </span>
                        </div>
                        <h3>Vendor Aggregator </h3>
                        <p>Service providers will get a highly functional and interactive on-demand marketplace for home services, offering various features to improve the overall sales of your business.
                        </p>
                    </div>
                </div>
        </div>
        </div>
    </section>

    <div class="text-left wow fadeInDown main-leads pt-50 pb-50">
        <div class="container feature-img " data-wow-duration="1000ms" data-wow-delay="800ms">
       <div class="features">
           <h2 class="feature-main-heading">Features of our on-demand marketplace for home services</h2>
        <p class="feature-lead">A comprehensive approach to build an on-demand handyman app like uber</p>
       </div><ul class="featuremaincatg">
            <li class="feature-ser">
                <span><i class="fa fa-address-card" aria-hidden="true"></i>
                </span>
                <h2>Booking Management</h2>
            </li>
            <li class="feature-ser">
                 <span><i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <h2>Scheduling </h2>
            </li>
            <li class="feature-ser">
                <span><i class="fa fa-users" aria-hidden="true"></i>
                </span>
                <h2>Customer Management</h2>
            </li>
            <li class="feature-ser">
                <span><i class="fa fa-star" aria-hidden="true"></i>
                </span>
                <h2>Review</h2>
            </li>
            <li class="feature-ser">
                 <span><i class="fa fa-envelope" aria-hidden="true"></i>
                </span>
                <h2>Help &amp; Support </h2>
            </li>
        </ul>
        </div>
    </div>

   <div class="light-grey pt-50 pb-50">
   <div class="container">
           <div class="row">
               <div class="feature-img col-lg-6 wow fadeInDown mrk-left animatedcol-sm-6 wow fadeInDown mrk-left animated" style="animation-name: fadeInLeft;">
                     <div class="wrap">
                         <img src="{{ url('front/img/gallery/Customer-App-features.png') }}" alt="On-Demand-Handyman-App-solution">
                     </div>
               </div>
                <div class="feature-img col-lg-6 wow fadeInDown mrk-right animated" style="visibility: visible; animation-name: fadeInRight;">
                 <div class="wrap featuremain">
               <h2>Customer App features</h2>
               <p>Create home services Care First app for your customers to book and schedule multiple types of services, such as house cleaning and repairing.</p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Easy Onboarding</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Manage Profile</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Search service</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Submit Request</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Schedule Service</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Notifications</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Seamless Payments</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Intuitive UI</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Rate Service</font></p>
                     <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Help &amp; Support</font></p>

             </div>
             </div>
           </div>
   </div>
   </div>
   <div class="pt-50 pb-50">
    <div class="container">
       <div class="row">
                <div class="feature-img col-sm-6 wow fadeInDown mrk-right animated" style="visibility: visible; animation-name: fadeInLeft;">
            <div class="wrap featuremain">
                <h2>Service Provider App </h2>
                <p>Service providers get a dedicated app to accept customer’s service requests, know their requirements, suitable quotations, secure payment options, and much more.</p>
                <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Instant Registration</font></p>
                <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Profile Approval</font></p>
                <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Accept Requests</font></p>
                <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Chat With Customer</font></p>
                <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Add Payment Details</font></p>
                <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Service History</font></p>
                <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Customer Feedback</font></p>
                <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Service Provider Support</font></p>

            </div>
            </div>
        <div class="feature-img col-sm-6 wow fadeInDown mrk-left animated" style="visibility: visible; animation-name: fadeInRight;">
            <div class="wrap">
                <img src="{{ url('front/img/gallery/Service-Provide-App.png') }}" alt="Home-Service-App-Development">
            </div>
        </div>
       </div>
   </div>
   </div>
   <div class="light-grey pt-50 pb-50">
       <div class="container">
           <div class="row">
               <div class="feature-img col-lg-6 wow fadeInDown mrk-left animatedcol-sm-6 wow fadeInDown mrk-left animated" style="animation-name: fadeInLeft;">
                   <div class="wrap">
                       <img src="{{ url('front/img/gallery/Vendor-App-Android.png') }}" alt="On-Demand-Handyman-App-solution">
                   </div>
               </div>
               <div class="feature-img col-lg-6 wow fadeInDown mrk-right animated" style="visibility: visible; animation-name: fadeInRight;">
                   <div class="wrap featuremain">
                       <h2>Vendor App (Android &amp; iOS)</h2>
                       <p>Vendors  get a dedicated app to accept customer’s service requests, know their requirements,Schedule Service </p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Quick Onboarding for Vendor</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Manage Profile</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Customer Order Management</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Accept And Reject Request</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Schedule Service</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Notifications</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Rewards/Discounts:</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Intuitive UI</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Help &amp; Support</font></p>

                   </div>
               </div>
           </div>
       </div>
   </div>
   <div class="pt-50 pb-50">
       <div class="container">
           <div class="row">
               <div class="feature-img col-sm-6 wow fadeInDown mrk-right animated" style="visibility: visible; animation-name: fadeInLeft;">
                   <div class="wrap featuremain">
                       <h2>Admin Panel <br> (Web Based)  </h2>
                       <p>A powerful centralized dashboard that manages and keeps track of all the aspects of your home services from assigning new requests, tracking service status to reviewing customer feedback.</p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Control Center</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Manage Service Providers</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Manage Service Requests</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Manage Partner Payments</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Manage Notifications</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Manage Reviews</font></p>
                       <p><span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><font>Reporting &amp; Analytics</font></p>

                   </div>
               </div>
               <div class="feature-img col-sm-6 wow fadeInDown mrk-left animated" style="visibility: visible; animation-name: fadeInRight;">
                   <div class="wrap">
                       <img src="{{asset('front/img/shape/available-app.png')}}" alt="">
                   </div>
               </div>
           </div>
       </div>
   </div>
    <!-- Best Features End -->
    <!-- Our Customer Start -->
    @if(count($testimonials) > 0)
        <div class="our-customer pt-50 pb-50 light-grey">
            <div class="container-fluid">
                <div class="our-customer-wrapper">
                    <!-- Section Tittle -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-xl-8">
                            <div class="section-tittle text-center">
                                <h2>What Our Customers<br> Have to Say</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="customar-active dot-style d-flex dot-style">
                                @foreach($testimonials as $testimonial)
                                    <div class="single-customer mb-100">
                                        <div class="what-img">
                                            <img src="{{ showTestimonialImage($testimonial->image) }}" class="rounded-circle" alt="" height="100px">
                                        </div>
                                        <div class="what-cap">
                                            <h4><a href="#">{{ $testimonial->title }}</a></h4>
                                            <p>{{ $testimonial->description }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- Our Customer End -->

    <!-- Available App  Start-->
    <div class="available-app-area  pt-50 pb-50">
        <div class="container">
            <div class="row d-flex download-container justify-content-between">
                <div class="col-xl-4 col-lg-4">
                    <div class="app-caption">
                        <div class="section-tittle section-tittle3">
                            <h2>Our App Available For Any Device Download now</h2>
                            <p>AVAILABLE ANYTIME, ANYWHERE.<br>
                                Browse From iPhone , iPad , Android Mobile Devices Or Tablets</p>
                            <div class="app-btn">
                                <a href="#" class="app-btn1"><img src="{{asset('front/img/shape/app_btn1.png')}}"
                                                                  alt=""></a>
                                <a href="#" class="app-btn2"><img src="{{asset('front/img/shape/app_btn2.png')}}"
                                                                  alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8">
                    {{--<div class="app-img">--}}
                        {{--<img src="{{asset('front/img/shape/available-app.png')}}" alt="">--}}
                    {{--</div>--}}
                    <div class="platform-container">
                        <div class="platform-main">
                            <div class="row">
                                <div class="mobile">
                                    <div class="mobile-inner">
                                        <div class="mobile-item">
                                            <img src="{{ url('front/img/home-screenshot.jpg') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="laptop">
                                    <div class="laptop-inner">
                                        <div class="laptop-item">
                                            <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mack">
                                    <div class="mack-inner">
                                        <div class="mack-item">
                                            <img src="{{ url('front/img/screen-desk.png') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shape -->
        <div class="app-shape">
            <img src="{{asset('front/img/shape/app-shape-top.png')}}" alt=""
                 class="app-shape-top heartbeat d-none d-lg-block">
            <img src="{{asset('front/img/shape/app-shape-left.png')}}" alt="" class="app-shape-left d-none d-xl-block">
            <!-- <img src="assets/img/shape/app-shape-right.png" alt="" class="app-shape-right bounce-animate "> -->
        </div>
    </div>
    <!-- Available App End-->

    <!-- Say Something Start -->
    <div class="say-something-aera pt-90 pb-90 fix">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="offset-xl-1 offset-lg-1 col-xl-5 col-lg-5">
                    <div class="say-something-cap">
                        <h2>Say Hello To The Collaboration Hub.</h2>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3">
                    <div class="say-btn">
                        <a href="{{ route('contact') }}" class="btn radius-btn">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- shape -->
        <div class="say-shape">
            <img src="{{asset('front/img/shape/say-shape-left.png')}}" alt=""
                 class="say-shape1 rotateme d-none d-xl-block">
            <img src="{{asset('front/img/shape/say-shape-right.png')}}" alt="" class="say-shape2 d-none d-lg-block">
        </div>
    </div>
</div>
@endsection
