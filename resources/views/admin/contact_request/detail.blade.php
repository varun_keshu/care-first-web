@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Contact Request</span>
                </div>
                <h2 class="az-content-title">Contact Request
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ $previous_url }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Customer</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $contact_request->customer->name ?? "- - -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Title</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $contact_request->title }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $contact_request->serviceDetails->title ?? "- -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Description</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $contact_request->description ?? "- -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items mg-b-20">
                        <div class="col-md-4">
                            <strong>Images</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            @if(isset($conact_request->images))
                                @foreach($contact_request->images as $img)
                                    <img src="{{ showContactRequestImage($img)}}" height="100px" width="auto">
                                @endforeach
                            @else
                                - -
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
