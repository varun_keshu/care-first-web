@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Contact_Request</span>
                </div>
                <h2 class="az-content-title">Contact Request
                </h2>
                <table id="dataTableList" class="display responsive nowrap">
                    <thead>
                    <tr>
                        <th>Customer Details</th>
                        <th>Service Category</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script src="{{ asset('admin/modules/contact_request/index.js') }}" type="text/javascript"></script>
@endsection
