<div class="btn-group" role="group">

    <a href="{{ route('contact_request.detail', $contact_request->id) }}" class="btn btn-secondary btn-sm">
        <i class="fa fa-eye"></i>
    </a>
</div>
