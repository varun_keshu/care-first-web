@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>My Profile</span>
                </div>
                <h2 class="az-content-title">Edit Profile</h2>
                <div class="card card-body pd-40">
                    <form role="form" method="post" action="{{ route('admin.update_profile') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Name</label>
                                    {{ Form::text('name', isset($admin) ? $admin->name : old('name') ,['class' => 'form-control','id'=>'name', 'placeholder' => 'Enter name',
                                            'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a name!"] ) }}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Email Address</label>
                                    {{ Form::email('email', isset($admin) ? $admin->email : old('email') ,['class' => 'form-control','id'=>'email', 'disabled'] ) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="ckbox">
                                        <input type="checkbox" name="change_password" id="change_password"><span>Change your password?</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="passwordChangeRow">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Enter password', 'data-validation'=>'required length', 'data-validation-depends-on' => 'change_password', 'data-validation-length' => 'min6',
                                            'data-validation-error-msg-required' => "Please enter a password!", 'data-validation-error-msg-length' => "The input value is shorter than 6 characters"]) !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Confirm Password</label>
                                {!! Form::password('password',['class'=>'form-control', 'data-validation' => 'confirmation', 'placeholder'=>'Enter confirm password', 'data-validation-depends-on' => 'change_password',
                                        'data-validation-error-msg' => "Your confirm password does not match with your password!"]) !!}
                            </div>
                        </div>
                        <button class="btn btn-az-primary btn-block">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
