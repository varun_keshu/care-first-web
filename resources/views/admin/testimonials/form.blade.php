<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('title', isset($testimonial) ? $testimonial->title : old('title') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
            <input type="file" accept="image/*" class="file-upload dropify"
                   data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                   @if(isset($testimonial) && $testimonial->image) data-default-file="{{ showTestimonialImage($testimonial->image) }}" @else data-validation="required"
                   @endif
                   id="image" name="image"/>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Description</label>
    {{ Form::textarea('description', isset($testimonial) ? $testimonial->description : old('description') ,['class' => 'form-control', 'data-validation' => 'required', 'rows' => 3] ) }}
</div>
<button class="btn btn-az-primary btn-block">{{ isset($testimonial) ? "Update" : "Submit" }}</button>