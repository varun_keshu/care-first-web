<div class="row instructionRowTemplate" style="display: none;">
    <div class="col-sm-5">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('service_instructions[0][title]', null ,['class' => 'form-control inputCloneField', 'data-name' => "title",
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Icon</label>
            <select name="service_instructions[0][icon]" class="form-control inputCloneField" data-name="icon" data-validation="required" data-validation-error-msg="Please select title!">
                <option value="">Select icon..</option>
                @foreach(\App\Models\ServiceInstructionByCategory::ICONS as $icon)
                    <option value="{{ $icon }}">{{ $icon }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-sm-2">
        <a href="javascript:;" class="btn btn-danger btn-sm removeInstructionRow">
            <i class="fas fa-trash"></i>
        </a>
    </div>
</div>
<div class="row productRowTemplate" style="display: none;">
    <div class="col-sm-5">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
            <input type="file" accept="image/*" class="file-upload dropifyImage inputCloneField"
                   data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                   data-validation="required" name="service_products[0][image]" data-name="image"/>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('service_products[0][title]', null ,['class' => 'form-control inputCloneField', 'data-name' => "title",
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
        </div>
    </div>
    <div class="col-sm-2">
        <a href="javascript:;" class="btn btn-danger btn-sm removeProductRow">
            <i class="fas fa-trash"></i>
        </a>
    </div>
</div>

<div class="col-sm-6 issueTitleRowTemplate mb-2" style="display: none;">
    <div class="form-group">
        <div class="input-group">
            {{ Form::text('issue_titles[]', null ,['class' => 'form-control', 'placeholder' => 'Enter issue title', 'data-name' => "title",
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter issue title!"] ) }}
            <span class="input-group-btn">
            <a href="javascript:;" class="removeIssueTitleRow btn btn-danger btn-sm "><i class="fas fa-trash"></i></a>
        </span>
        </div>
    </div>
</div>