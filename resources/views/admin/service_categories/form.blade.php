<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
    {{ Form::text('title', isset($service_category) ? $service_category->title : old('title') ,['class' => 'form-control','id'=>'title',
            'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Description</label>
    {{ Form::textarea('description', isset($service_category) ? $service_category->description : old('description') ,['class' => 'form-control','id'=>'description',
            'rows' => 3] ) }}
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
            <input type="file" accept="image/*" class="file-upload dropify"
                   data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                   @if(isset($service_category) && $service_category->image) data-default-file="{{ showServiceCategoryImage($service_category->image) }}"
                   @else data-validation="required"
                   @endif
                   id="image" name="image"/>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Icon Image</label>
            <input type="file" accept="image/*" class="file-upload dropify"
                   data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                   @if(isset($service_category) && $service_category->icon_image) data-default-file="{{ showServiceCategoryIconImage($service_category->icon_image) }}"
                   @else data-validation="required"
                   @endif
                   id="icon_image" name="icon_image"/>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Banner Image</label>
            <input type="file" accept="image/*" class="file-upload dropify"
                   data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                   @if(isset($service_category) && $service_category->banner_image) data-default-file="{{ showServiceCategoryBannerImage($service_category->banner_image) }}"
                   @else data-validation="required"
                   @endif
                   id="banner_image" name="banner_image"/>
        </div>
    </div>
</div>
<hr>
<div class="card">
    <div class="card-header">
        <h5 class="card-title tx-dark mg-b-10">Service Instructions
            <span class="pull-right d-inline-block float-right">
                <a href="javascript:;" id="addNewInstructions" class="btn btn-success btn-block">
                    Add New Instruction
                </a>
            </span>
        </h5>
    </div>
    <div class="card-body" id="instructionsRows">
        @if(isset($service_category) && count($service_category->serviceInstructions) > 0)
            @foreach($service_category->serviceInstructions as $index => $serviceInstruction)
                <div class="row instructionRowTemplate">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
                            {{ Form::text('service_instructions['.$index."][title]", $serviceInstruction->title ,['class' => 'form-control inputCloneField', 'data-name' => "title",
                                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Icon</label>
                            <select name="service_instructions[{{ $index }}][icon]" class="form-control inputCloneField" data-name="icon" data-validation="required" data-validation-error-msg="Please select title!">
                                <option value="">Select icon..</option>
                                @foreach(\App\Models\ServiceInstructionByCategory::ICONS as $icon)
                                    <option value="{{ $icon }}" @if($serviceInstruction->icon == $icon) selected @endif>{{ $icon }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:;" class="btn btn-danger btn-sm removeInstructionRow">
                            <i class="fas fa-trash"></i>
                        </a>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
<hr>
<div class="card">
    <div class="card-header">
        <h5 class="card-title tx-dark mg-b-10">Service Products
            <span class="pull-right d-inline-block float-right">
                <a href="javascript:;" id="addNewProduct" class="btn btn-success btn-block">
                    Add New Product
                </a>
            </span>
        </h5>
    </div>
    <div class="card-body" id="productsRows">
        @if(isset($service_category) && count($service_category->serviceProducts) > 0)
            @foreach($service_category->serviceProducts as $index => $serviceProduct)
                <div class="row productRowTemplate">
                    <input type="hidden" name="exist_service_products[{{ $index }}][id]" value="{{ $serviceProduct->id }}">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
                            <input type="file" accept="image/*" class="file-upload dropify inputCloneField"
                                   data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                                   data-default-file="{{ showServiceCategoryProductImage($serviceProduct->image) }}" name="exist_service_products[{{ $index }}][image]" data-name="image"/>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
                            {{ Form::text('exist_service_products['.$index."][title]", $serviceProduct->title ,['class' => 'form-control inputCloneField', 'data-name' => "title",
                                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:;" class="btn btn-danger btn-sm removeProductRow">
                            <i class="fas fa-trash"></i>
                        </a>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
<hr>
<div class="card">
    <div class="card-header">
        <h5 class="card-title tx-dark mg-b-10">Issue Titles
            <span class="pull-right d-inline-block float-right">
                <a href="javascript:;" id="addNewIssueTitle" class="btn btn-success btn-block">
                    Add New Issue Title
                </a>
            </span>
        </h5>
    </div>
    <div class="card-body">
        <div class="row" id="issueTitlesRows">
            @if(isset($service_category) && count($service_category->issueTitles) > 0)
                @foreach($service_category->issueTitles as $index => $issueTitle)
                    <div class="col-sm-6 issueTitleRowTemplate mb-2">
                        <input type="hidden" name="exist_issue_titles[{{ $index }}][id]" value="{{ $issueTitle->id }}">
                        <div class="form-group">
                            <div class="input-group">
                                {{ Form::text('exist_issue_titles['.$index.'][title]', $issueTitle->title ,['class' => 'form-control', 'placeholder' => 'Enter issue title', 'data-name' => "title",
                                        'data-validation' => 'required', 'data-validation-error-msg' => "Please enter issue title!"] ) }}
                                <span class="input-group-btn">
                                    <a href="javascript:;" class="removeIssueTitleRow btn btn-danger btn-sm "><i class="fas fa-trash"></i></a>
                                </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
<hr>
<button class="btn btn-az-primary btn-block">{{ isset($service_category) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script src="https://unpkg.com/feather-icons"></script>
    <script type="text/javascript">
        feather.replace();

        var instruction_count = $("#instructions_count").val();
        var product_count = $("#products_count").val();
        var issue_title_count = $("#issues_count").val();
        $(document).ready(function () {
            $(document).on('click', "#addNewInstructions", function () {
                var instructionRowCloned = $('.instructionRowTemplate').last().clone();
                instructionRowCloned.find('.inputCloneField').each(function () {
                    var self = $(this);
                    var name_value = self.data('name');
                    self.attr({name: "service_instructions[" + instruction_count + "][" + name_value + "]"});
                });
                instructionRowCloned.show();
                $("#instructionsRows").append(instructionRowCloned);
                instruction_count++;

                $('.select2').select2();
            });
            $(document).on('click', ".removeInstructionRow", function () {
                var self = $(this);
                self.parents(".instructionRowTemplate").remove();
            });

            /**/
            $(document).on('click', "#addNewProduct", function () {
                var productRowCloned = $('.productRowTemplate').last().clone();
                productRowCloned.find('.inputCloneField').each(function () {
                    var self = $(this);
                    var name_value = self.data('name');
                    self.attr({name: "service_products[" + product_count + "][" + name_value + "]"});
                });
                productRowCloned.show();
                productRowCloned.find('.dropifyImage').dropify();
                $("#productsRows").append(productRowCloned);
                product_count++;
            });
            $(document).on('click', ".removeProductRow", function () {
                var self = $(this);
                self.parents(".productRowTemplate").remove();
            });

            /**/
            $(document).on('click', "#addNewIssueTitle", function () {
                var issueTitleRowCloned = $('.issueTitleRowTemplate').last().clone();
                issueTitleRowCloned.show();
                $("#issueTitlesRows").append(issueTitleRowCloned);
                issue_title_count++;
            });
            $(document).on('click', ".removeIssueTitleRow", function () {
                var self = $(this);
                self.parents(".issueTitleRowTemplate").remove();
            });
        })
    </script>
@endsection
