@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Service Categories</span>
                </div>
                <h2 class="az-content-title">Edit Service Category
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('service_categories') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <input type="hidden" id="instructions_count" value="{{ count($service_category->serviceInstructions) }}">
                <input type="hidden" id="products_count" value="{{ count($service_category->serviceProducts) }}">
                <input type="hidden" id="issues_count" value="{{ count($service_category->issueTitles) }}">
                <div class="card card-body pd-40">
                    <form role="form" method="post" action="{{ route('service_categories.update', $service_category->id) }}" enctype="multipart/form-data" >
                        @csrf
                        @include('admin.service_categories.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('admin.service_categories.partials.static_html')
@endsection