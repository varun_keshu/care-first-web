<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('title', isset($slider) ? $slider->title : old('title') ,['class' => 'form-control','id'=>'title',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Slider Type</label>
            {{ Form::select('type', \App\Models\Sliders::TYPES , isset($slider) ? $slider->type : old('type') ,['class' => 'form-control select2-no-search','id'=>'sliderType', 'placeholder' => 'Select slider type..',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please select slider type!"] ) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
            <input type="file" accept="image/*" class="file-upload dropify"
                    data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                   @if(isset($slider) && $slider->image) data-default-file="{{ showSliderImage($slider->image) }}" @else data-validation="required"
                   @endif
                   id="image" name="image"/>
        </div>
    </div>
    <div class="col-sm-6" id="serviceCategoriesDiv">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Service Categories</label>
            {{ Form::select('service_category_id', $service_categories , isset($slider) ? $slider->service_category_id : old('service_category_id') ,['class' => 'form-control select2','id'=>'service_category_id', 'placeholder' => 'Select service category..',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please select service category!"] ) }}
        </div>
    </div>
</div>
<button class="btn btn-az-primary btn-block">{{ isset($slider) ? "Update" : "Submit" }}</button>

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#serviceCategoriesDiv").toggle($("#sliderType").val() == 'service_category_page');

            $(document).on('change', "#sliderType", function () {
                $("#serviceCategoriesDiv").toggle($(this).val() == 'service_category_page');
            })
        })
    </script>
@endsection