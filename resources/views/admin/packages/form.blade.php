<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('title', isset($package) ? $package->title : old('title') ,['class' => 'form-control','id'=>'title',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Package Price</label>
            {{ Form::number('price', isset($package) ? $package->price : old('price') ,['class' => 'form-control','id'=>'price',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a price!",'min'=>1] ) }}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Description</label>
    {{ Form::textarea('description' , isset($package) ? $package->description : old('description') ,['class' => 'form-control','rows'=>4,'id'=>'description',
            'placeholder' => 'Add Description...', 'data-validation' => 'required', 'data-validation-error-msg' => "Please add description!"] ) }}
</div>
<hr/>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header">
                Service Details
            </div>
            <div class="card-body service-packages">
                <div class="row service-package-row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Service</label>
                            {{ Form::select('service_id', $services ,isset($package)? $package->service_id : old('service_id') ,['class' => 'form-control select2-no-search',
                'data-validation' => 'required', 'data-validation-error-msg' => "Please Add Service Id!"] ) }}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Service Count</label>
                            {{ Form::number('service_count' ,isset($package)? $package->service_count : old('service_count') ,['class' => 'form-control',
                'data-validation' => 'required', 'data-validation-error-msg' => "Please Add Service Count!",'min'=>1] ) }}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Duration </label>
                            <div class="input-group mb-3">
                                {{ Form::number('duration', isset($package) ? $package->duration : old('duration') ,['class' => 'form-control',
                                    'data-validation' => 'required', 'data-validation-error-msg' => "Please Add Duration!",'min'=>1] ) }}
                                <div class="input-group-append">
                                    {{ Form::select('duration_unit', \App\Models\Package::DURATION_UNITS ,isset($package)? $package->duration_unit : old('duration_unit') ,['class' => 'form-control select2-no-search','min'=>1,
                                    'data-validation' => 'required', 'data-validation-error-msg' => "Please Add duration!"] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button class="btn btn-az-primary btn-block">{{ isset($package) ? "Update" : "Submit" }}</button>
