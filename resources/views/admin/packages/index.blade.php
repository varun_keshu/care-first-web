@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Services</span>
                </div>
                <h2 class="az-content-title">Packages List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('package.create') }}" class="btn btn-info btn-block"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                @if(count($packages) > 0)
                    <div class="table-responsive">
                        <table class="table table-striped mg-b-0">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Service</th>
                                <th>Service Count</th>
                                <th>Service Time</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($packages as $item)
                                <tr>
                                    <td>{{ ucfirst($item->title) ?? "- - -" }}</td>
                                    <td>{{ $item->serviceDetails->title ?? "- - -" }}</td>
                                    <td>{{ $item->service_count }}</td>
                                    <td>{{ $item->duration }} {{ \App\Models\Package::DURATION_UNITS[$item->duration_unit] }}</td>
                                    <td>{{ formatPrice($item->price) }}</td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="{{ route('package.show', $item->id) }}"
                                               class="btn btn-secondary btn-sm">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a href="{{ route('package.edit', $item->id) }}"
                                               class="btn btn-primary btn-sm mx-1">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0);"
                                               onclick="confirmDelete('{{ route("package.delete",$item->id) }}')"
                                               class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="tx-center pd-y-20 bg-gray-200">
                        <h5>No Records Found!</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
