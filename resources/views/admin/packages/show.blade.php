@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Packages</span>
                </div>
                <h2 class="az-content-title">Package Details
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('package') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Title</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $package->title }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Description</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $package->description ?? "- - -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $package->serviceDetails->title ?? "- - -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service Category</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $package->serviceCategoryDetails->title ?? "- - -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service Count</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $package->service_count }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Price</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ formatPrice($package->price) }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service Time Difference</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $package->duration }} {{ \App\Models\Package::DURATION_UNITS[$package->duration_unit] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
