<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('title', isset($service) ? $service->title : old('title') ,['class' => 'form-control','id'=>'title',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Service Categories</label>
            {{ Form::select('service_category_id', $service_categories , isset($service) ? $service->service_category_id : old('service_category_id') ,['class' => 'form-control select2','id'=>'service_category_id',
                    'placeholder' => 'Select service category...', 'data-validation' => 'required', 'data-validation-error-msg' => "Please select service category!"] ) }}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Description</label>
    {{ Form::textarea('description', isset($service) ? $service->description : old('description') ,['class' => 'form-control','id'=>'description',
            'rows' => 3] ) }}
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Time Period</label>
            <div class="input-group">
                {{ Form::number('time_period', isset($service) ? $service->time_period : old('time_period') ,['class' => 'form-control','id'=>'title',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter time period!"] ) }}
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">Hour</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="ckbox">
                <input type="checkbox" name="show_quantity_option"
                       @if(isset($service) && ($service->show_quantity_option == 1)) checked @endif><span>Show Quantity Option</span>
            </label>
        </div>
        <div class="form-group">
            <label class="ckbox">
                <input type="checkbox" name="is_insurance_applicable"
                       @if(isset($service) && ($service->is_insurance_applicable == 1)) checked @endif><span>Insurance Applicable?</span>
            </label>
        </div>
        <div class="form-group">
            <label class="ckbox">
                <input type="checkbox" name="estimation_request_available" id="estimation_request_available" value="uncheked"
                       @if(isset($service) && ($service->estimation_request_available == 1)) checked @endif><span>Estimation request available?</span>
            </label>
        </div>
        <div class="form-group">
            <label class="ckbox">
                <input type="checkbox" name="is_warranty_apply" id="is_warranty_apply" value="uncheked"
                       @if(isset($service) && ($service->is_warranty_apply == 1)) checked @endif><span>Is applicable for warranty?</span>
            </label>
        </div>
        <div class="row" id="warrantyExpirationDaysDiv">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Warranty Expiration Days</label>
                    {{ Form::number('warranty_expiration_days', isset($service) ? $service->warranty_expiration_days : old('warranty_expiration_days') ,['class' => 'form-control', 'data-validation' => 'required'] )}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
            <input type="file" accept="image/*" class="file-upload dropify"
                   data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                   @if(isset($service) && $service->image) data-default-file="{{ showServiceImage($service->image) }}"
                   @else data-validation="required"
                   @endif
                   id="image" name="image"/>
        </div>
    </div>
</div>
<hr>
<div class="card">
    <div class="card-header">
        <h5 class="card-title tx-dark mg-b-10">Price Details</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Default Service Price</label>
                    {{ Form::text('price_details[low]', isset($service) && isset($service->price_details) ? $service->price_details['low'] : old('price_details[low]') ,['class' => 'form-control',
                            'data-validation' => 'required number', 'data-validation-error-msg-required' => "Please enter service default price!", 'data-validation-error-msg-number' => "Please enter correct number value!"] ) }}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Medium Priority Service Price</label>
                    {{ Form::text('price_details[medium]', isset($service) && isset($service->price_details) ? $service->price_details['medium'] : old('price_details[medium]') ,['class' => 'form-control',
                            'data-validation' => 'required number', 'data-validation-error-msg-required' => "Please enter service medium priority price!", 'data-validation-error-msg-number' => "Please enter correct number value!"] ) }}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">High Priority Service Price</label>
                    {{ Form::text('price_details[high]', isset($service) && isset($service->price_details) ? $service->price_details['high'] : old('price_details[high]') ,['class' => 'form-control',
                            'data-validation' => 'required number', 'data-validation-error-msg-required' => "Please enter service high priority price!", 'data-validation-error-msg-number' => "Please enter correct number value!"] ) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Default Priority Service Price Discount</label>
                    {{ Form::text('discounted_price_details[low]', isset($service) && isset($service->discounted_price_details) ? $service->discounted_price_details['low'] : old('discounted_price_details[low]') ,['class' => 'form-control'] ) }}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Medium Priority Service Price Discount</label>
                    {{ Form::text('discounted_price_details[medium]', isset($service) && isset($service->discounted_price_details) ? $service->discounted_price_details['medium'] : old('discounted_price_details[medium]') ,['class' => 'form-control'] ) }}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">High Priority Service Price Discount</label>
                    {{ Form::text('discounted_price_details[high]', isset($service) && isset($service->discounted_price_details) ? $service->discounted_price_details['high'] : old('discounted_price_details[high]') ,['class' => 'form-control'] ) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Commission Value</label>
                    <div class="input-group">
                        {{ Form::text('commission_value', isset($service) ? $service->commission_value : old('commission_value') ,['class' => 'form-control'] ) }}
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">%</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Fixed Visiting Charges</label>
                    <div class="input-group">
                        {{ Form::text('visiting_charges', isset($service) ? $service->visiting_charges : old('visiting_charges') ,['class' => 'form-control'] ) }}
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">₹</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<button class="btn btn-az-primary btn-block">{{ isset($service) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#warrantyExpirationDaysDiv").toggle($("#is_warranty_apply").prop('checked'));

            $("#is_warranty_apply").on("click", function () {
                $("#warrantyExpirationDaysDiv").toggle(this.checked);
            });
        });

    </script>
@endsection
