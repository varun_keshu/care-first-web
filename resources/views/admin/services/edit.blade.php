@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Services</span>
                </div>
                <h2 class="az-content-title">Edit Service
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('services') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <form role="form" method="post" action="{{ route('services.update', $service->id) }}" enctype="multipart/form-data" >
                        @csrf
                        @include('admin.services.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

