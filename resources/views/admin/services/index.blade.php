@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Services</span>
                </div>
                <h2 class="az-content-title">Services List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('services.create') }}" class="btn btn-success btn-block"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                @if(count($services) > 0)
                    <table id="dataTableList" class="display responsive nowrap">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Service Category</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($services as $item)
                            <tr>
                                <td>{{ $item->title ?? "- - -" }}</td>
                                <td>{{ isset($item->serviceCategoryDetails) ? $item->serviceCategoryDetails->title : "- - -" }}</td>
                                <td>
                                    <img src="{{ showServiceImage($item->image) }}" class="img-fluid"
                                         style="height: 100px;">
                                </td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <a href="{{ route('services.edit', $item->id) }}"
                                           class="btn btn-primary btn-sm">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="javascript:;"
                                           onclick="confirmDelete('{{ route("services.delete",$item->id) }}')"
                                           class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
            @else
                <div class="tx-center pd-y-20 bg-gray-200">
                    <h5>No Records Found!</h5>
                </div>
            @endif
        </div>
    </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
        });
    </script>
@endsection