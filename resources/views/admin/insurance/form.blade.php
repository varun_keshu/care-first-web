<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">User Types</label>
            <select name="user_type" id="user_type" class="form-control select2-no-search" data-validation="required">
                <option></option>
                <option value="customer" @if(isset($insurance) && $insurance->user_type == "customer") selected @endif>Customers</option>
                <option value="vendor" @if(isset($insurance) && $insurance->user_type == "vendor") selected @endif>Vendors</option>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Insurance Amount</label>
            <div class="input-group">
                {{ Form::text('insurance_amount', isset($insurance) ? $insurance->insurance_amount : old('insurance_amount') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">₹</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Minimum Order Amount</label>
            <div class="input-group">
                {{ Form::text('minimum_bookings_value', isset($insurance) ? $insurance->minimum_bookings_value : old('minimum_bookings_value') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">₹</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Maximum Order Amount</label>
            <div class="input-group">
                {{ Form::text('maximum_bookings_value', isset($insurance) ? $insurance->maximum_bookings_value : old('maximum_bookings_value') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">₹</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">insurance Premium Amount</label>
            <div class="input-group">
                {{ Form::text('insurance_premium_value', isset($insurance) ? $insurance->insurance_premium_value : old('insurance_premium_value') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">₹</span>
                </div>
            </div>
        </div>
    </div>
</div>
<button class="btn btn-az-primary btn-block">{{ isset($insurance) ? "Update" : "Submit" }}</button>