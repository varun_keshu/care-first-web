@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Insurances</span>
                </div>
                <h2 class="az-content-title">Edit Insurance
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('insurances') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <form role="form" method="post" action="{{ route('insurance.update', $insurance->id) }}">
                        @csrf
                        @include('admin.insurance.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection