@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Insurances</span>
                </div>
                <h2 class="az-content-title">Insurance List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('insurance.create') }}" class="btn btn-success btn-block"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                @if(count($insurances) > 0)
                    <div class="table-responsive">
                        <table class="table table-striped mg-b-0">
                            <thead>
                            <tr>
                                <th>User Type</th>
                                <th>Insurance Amount</th>
                                <th>Minimum Value</th>
                                <th>Maximum Value</th>
                                <th>Premium Value</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($insurances as $item)
                                <tr>
                                    <td>{{ ucfirst($item->user_type) }}</td>
                                    <td>{{ formatPrice($item->insurance_amount) }}</td>
                                    <td>{{ formatPrice($item->minimum_bookings_value) }}</td>
                                    <td>{{ formatPrice($item->maximum_bookings_value) }}</td>
                                    <td>{{ formatPrice($item->insurance_premium_value) }}</td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="{{ route('insurance.edit', $item->id) }}" class="btn btn-primary btn-sm">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="javascript:;" onclick="confirmDelete('{{ route("insurance.delete",$item->id) }}')"
                                               class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="tx-center pd-y-20 bg-gray-200">
                        <h5>No Records Found!</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection