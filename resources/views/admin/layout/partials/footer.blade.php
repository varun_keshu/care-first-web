<div class="az-footer">
    <div class="container">
        <span>&copy; {{ date('Y') }} {{ env('APP_NAME') }}</span>
        <span>Developed by: <a href="http://www.urvam.com" target="_blank">Urvam Technologies</a></span>
    </div><!-- container -->
</div><!-- az-footer -->
