<div class="az-navbar">
    <div class="container">
        <a href="{{ route('dashboard') }}" class="az-logo">Care <span>First</span></a>
        <ul class="nav mx-auto">
            <li class="nav-label">Main Menu</li>
            <li class="nav-item @if(Request::is('admin/dashboard')) active show @endif">
                <a href="{{ route('dashboard') }}" class="nav-link"><i class="typcn typcn-clipboard"></i>Dashboard</a>
            </li>
            <li class="nav-item @if(Request::is('admin/bookings')) active show @endif">
                <a href="{{ route('bookings') }}" class="nav-link @if(Request::is('admin/bookings')) active @endif"><i class="typcn typcn-spanner"></i>Bookings</a>
            </li>

            <li class="nav-item @if(Request::is('admin/email-templates') || Request::is('admin/email-templates/*') || Request::is('admin/banners') || Request::is('admin/banners/*') ||
                        Request::is('admin/pages') || Request::is('admin/page/*') || Request::is('admin/faqs') || Request::is('admin/faq/*')
                        || Request::is('admin/site-configurations') || Request::is('admin/insurances') || Request::is('admin/insurance/*') || Request::is('admin/testimonials') || Request::is('admin/testimonial/*')
                        || Request::is('admin/contact_request') || Request::is('admin/contact_submission')) active show @endif">
                <a href="" class="nav-link with-sub"><i class="typcn typcn-document"></i>Modules</a>
                <nav class="nav-sub">
                    <a href="{{ route('site_configurations') }}" class="nav-sub-link @if(Request::is('admin/site-configurations')) active @endif">Site Configurations</a>
                    <a href="{{ route('email_templates') }}" class="nav-sub-link @if(Request::is('admin/email-templates') || Request::is('admin/email-templates/*')) active @endif">Email Templates</a>
                    <a href="{{ route('sliders') }}" class="nav-sub-link @if(Request::is('admin/banners') || Request::is('admin/banners/*')) active @endif">Banners</a>
                    <a href="{{ route('pages') }}" class="nav-sub-link @if(Request::is('admin/pages') || Request::is('admin/page/*')) active @endif">Pages</a>
                    <a href="{{ route('faqs') }}" class="nav-sub-link @if(Request::is('admin/faqs') || Request::is('admin/faq/*')) active @endif">FAQs</a>
                    <a href="{{ route('insurances') }}" class="nav-sub-link @if(Request::is('admin/insurances') || Request::is('admin/insurance/*')) active @endif">Insurance List</a>
                    <a href="{{ route('testimonials') }}" class="nav-sub-link @if(Request::is('admin/testimonials') || Request::is('admin/testimonial/*')) active @endif">Testimonials List</a>
                    <a href="{{ route('contact_request') }}" class="nav-sub-link @if(Request::is('admin/contact_request') || Request::is('admin/contact_request/*')) active @endif">Contact Request</a>
                    <a href="{{ route('contact_submission') }}" class="nav-sub-link @if(Request::is('admin/contact_submission') || Request::is('admin/contact_submission/*')) active @endif">Contact Submission</a>
                    <a href="{{ route('clients') }}" class="nav-sub-link @if(Request::is('clients') || Request::is('clients')) active @endif">Client</a>

                    <a href="{{ route('reward_program') }}" class="nav-sub-link @if(Request::is('admin/reward_programs') || Request::is('admin/reward_program/*')) active @endif">Reward Program</a>
                </nav>
            </li>
            <li class="nav-item @if(Request::is('admin/service-categories') || Request::is('admin/service-categories/*') || Request::is('admin/services') || Request::is('admin/services/*') ||
                    Request::is('admin/service-areas') || Request::is('admin/service-area/*') || Request::is('admin/service-statuses') || Request::is('admin/service-status/*')
                    || Request::is('admin/package*')|| Request::is('admin/package*')) active show @endif">
                <a href="" class="nav-link with-sub"><i class="typcn typcn-document"></i>Services</a>
                <nav class="nav-sub">
                    <a href="{{ route('service_categories') }}" class="nav-sub-link @if(Request::is('admin/service-categories') || Request::is('admin/service-categories/*')) active @endif">Service Categories</a>
                    <a href="{{ route('services') }}" class="nav-sub-link @if(Request::is('admin/services') || Request::is('admin/services/*')) active @endif">Services</a>
                    <a href="{{ route('service_areas') }}" class="nav-sub-link @if(Request::is('admin/service-areas') || Request::is('admin/service-area/*')) active @endif">Service Areas</a>
                    <a href="{{ route('service_status') }}" class="nav-sub-link @if(Request::is('admin/service-statuses') || Request::is('admin/service-status/*')) active @endif">Service Status List</a>
                    <a href="{{ route('package') }}" class="nav-sub-link @if(Request::is('admin/packages*')) active @endif">Packages</a>
                    <a href="{{ route('package-subscription') }}" class="nav-sub-link @if(Request::is('admin/package-subscription*')) active @endif">Package Subscriptions</a>
                </nav>
            </li>

            <li class="nav-item @if(Request::is('admin/report/by-vendor')  || Request::is('admin/report/by-service') ||
             Request::is('admin/report/by-category')|| Request::is('admin/payout-history')) active show @endif">
                <a href="" class="nav-link with-sub"><i class="typcn typcn-document"></i>Reports</a>
                <nav class="nav-sub">
                    <a href="{{ route('reportByVendor') }}" class="nav-sub-link @if(Request::is('admin/report/by-vendor') || Request::is('admin/report/by-vendor*')) active @endif">Booking By Vendor</a>
                    <a href="{{ route('reportByService') }}" class="nav-sub-link @if(Request::is('admin/report/by-service') || Request::is('admin/report/by-service/*')) active @endif">Booking By Service</a>
                    <a href="{{ route('reportByCategory') }}" class="nav-sub-link @if(Request::is('admin/report/by-category') || Request::is('admin/report/by-category/*')) active @endif">Booking By Category</a>
                    <a href="{{ route('payoutHistory') }}" class="nav-sub-link @if(Request::is('admin/payout-history') || Request::is('admin/payout-history/*')) active @endif">Payment History</a>

                </nav>
            </li>
            <li class="nav-item @if(Request::is('admin/admin-users') || Request::is('admin/admin-user/*') || Request::is('admin/customers') || Request::is('admin/customer/*') ||
                        Request::is('admin/managers') || Request::is('admin/manager/*') || Request::is('admin/vendors') || Request::is('admin/vendor/*')) active show @endif">
                <a href="" class="nav-link with-sub"><i class="typcn typcn-document"></i>Users</a>
                <nav class="nav-sub">
                    <a href="{{ route('admin.admin_users') }}" class="nav-sub-link @if(Request::is('admin/admin-users') || Request::is('admin/admin-user/*')) active @endif">Admin Users</a>
                    <a href="{{ route('customers') }}" class="nav-sub-link @if(Request::is('admin/customers') || Request::is('admin/customer/*')) active @endif">Customers</a>
                    <a href="{{ route('managers') }}" class="nav-sub-link @if(Request::is('admin/managers') || Request::is('admin/manager/*')) active @endif">Managers</a>
                    <a href="{{ route('vendors') }}" class="nav-sub-link @if(Request::is('admin/vendors') || Request::is('admin/vendor/*')) active @endif">Vendors</a>
                </nav>
            </li>
        </ul>
    </div>
</div>
