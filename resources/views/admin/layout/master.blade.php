<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{url('images/favicon.ico')}}">
    <!-- Meta -->
    <meta name="description" content="Care First Website">
    <meta name="author" content="ThemePixels">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Care First</title>
    @include('admin.layout.globals.css')
    @yield('css')
</head>
<body class="az-dashboard">
@include('admin.layout.partials.header')
@include('admin.layout.partials.navbar')
@yield('content')
@include('admin.layout.partials.footer')
@include('admin.layout.globals.js')
@yield('footer_scripts')
</body>
</html>
