<script src="{{ asset('admin/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('admin/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>

<script src="{{ asset('admin/lib/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('admin/lib/dropify/js/dropify.min.js') }}"></script>
<script src="{{ asset('admin/js/azia.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    var $app_url = "{{ url('/') }}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script src="{{ asset('admin/js/custom.js') }}"></script>
