<!-- vendor css -->
<link href="{{ asset('admin/lib/fontawesome-free/css/all.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/lib/typicons.font/typicons.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/lib/dropify/css/dropify.css') }}" rel="stylesheet"/>

<link href="{{ asset('admin/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">

<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
<!-- azia CSS -->
<link href="{{ asset('admin/css/azia.css') }}" rel="stylesheet"/>
