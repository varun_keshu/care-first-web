@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Vendor</span>
                </div>
                <h2 class="az-content-title">Vendors Details
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('vendors') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card bd-0">
                    <div class="card-header bg-gray-400 bd-b-0-f pd-b-0">
                        <nav class="nav nav-tabs">
                            <a class="nav-link active show" data-toggle="tab" href="#profile">Profile</a>
                            <a class="nav-link" data-toggle="tab" href="#services">Services</a>
                            <a class="nav-link" data-toggle="tab" href="#bookings">Bookings</a>
                            <a class="nav-link" data-toggle="tab" href="#workSchedule">Work Schedule</a>
                            <a class="nav-link" data-toggle="tab" href="#unavailability">Unavailability</a>
                        </nav>
                    </div>
                    <div class="card-body bd bd-t-0 tab-content">
                        <div id="profile" class="tab-pane active show">
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Profile Image</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    @if(isset($vendor->image))
                                        <img src="{{ showUserImage($vendor->image) }}" alt="Profile Image"
                                             style="height: 100px;">
                                    @else
                                        - - -
                                    @endif
                                </div>
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Name</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    {{ $vendor->name ?? "- - -" }}
                                </div>
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Contact Number</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    {{ $vendor->contact_number }}
                                </div>
                            </div>
                        </div>
                        <div id="services" class="tab-pane">
                            @forelse($vendor->userServiceAreas as $userServiceArea)
                                <div class="az-content-label mg-b-5">{{ $userServiceArea->serviceArea->title }}
                                    - {{ $userServiceArea->city->title }}</div>
                                <div class="row row-sm">
                                    <ul>
                                        @foreach($userServiceArea->userServices as $userService)
                                            <li>{{ $userService->serviceDetail->title }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @empty
                                <div class="tx-center pd-y-20 bg-gray-200">
                                    <h5>No Records Found!</h5>
                                </div>
                            @endforelse
                        </div>
                        <div id="bookings" class="tab-pane">
                            <div class="row row-sm">
                                <div class="col-sm-12">
                                    <table id="dataTableList" class="display responsive nowrap"
                                           style="width: 100% !important;">
                                        <thead>
                                        <tr>
                                            <th>Booking ID</th>
                                            <th>Customer</th>
                                            <th>Service Details</th>
                                            <th>Status</th>
                                            <th>Service Date</th>
                                            <th>Booking Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($vendor->vendorBookings as $booking)
                                            <tr>
                                                <td>{{ $booking->id }}</td>
                                                <td>{{ isset($booking->customer) ? $booking->customer->name : "- - -" }}</td>
                                                <td>{{ $booking->serviceDetails->title }}<br>
                                                    <b>Category : </b>{{ $booking->serviceCategoryDetails->title }}
                                                </td>
                                                <td>{{ \App\Models\ServiceStatus::STATES[$booking->serviceStatusDetail->state] }}</td>
                                                <td>{{ formatDate($booking->service_date.' '.$booking->service_time) }}</td>
                                                <td>{{ formatDate($booking->created_at) }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="workSchedule" class="tab-pane">
                            @foreach($vendor->vendorWorkSchedule as $vendorWorkSchedule)
                                <div class="row row-xs align-items-center mg-b-20">
                                    <div class="col-md-4">
                                        <strong>{{ \App\Models\VendorWorkSchedule::WEEK_DAYS[$vendorWorkSchedule->week_day] }}</strong>
                                    </div>
                                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                                        {{ ($vendorWorkSchedule->is_available == 1) ? formatDate($vendorWorkSchedule->start_time, "h:i A").' -- '.formatDate($vendorWorkSchedule->end_time, "h:i A") : "Not Available" }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div id="unavailability" class="tab-pane">
                            @if(count($vendor->vendorUnavailability) > 0)
                            <div class="table-responsive">
                                <table class="table table-striped mg-b-0">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Reason</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($vendor->vendorUnavailability as $vendorUnavailability)
                                        <tr>
                                            <td>{{ formatDate($vendorUnavailability->date, "d M, Y") }}</td>
                                            <td>{{ $vendorUnavailability->reason }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                                <div class="tx-center pd-y-20 bg-gray-200">
                                    <h5>No Records Found!</h5>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
        });
    </script>
@endsection