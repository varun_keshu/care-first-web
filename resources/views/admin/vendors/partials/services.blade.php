@if(!empty($user_service_categories))
    @foreach($user_service_categories as $userServiceArea)
        <div class="card mb-2" id="servicesDiv_{{ $userServiceArea['service_area_id'] }}">
            <div class="card-header">
                <h5 class="card-title tx-dark mg-b-10">{{ $userServiceArea['service_area_title'] }} - Services</h5>
            </div>
            <div class="card-body">
                @forelse($userServiceArea['services'] as $service_category_id => $services)
                    <div class="az-content-label mg-b-5">{{ $services[0]->serviceCategoryDetails->title }}
                        <input type="checkbox" class="selectCategory" data-category="{{ $service_category_id }}"
                               data-area="{{ $userServiceArea['service_area_id'] }}"></div>
                    <div class="row">
                        @foreach($services as $relatedService)
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="ckbox">
                                        <input type="checkbox"
                                               class="service_{{ $userServiceArea['service_area_id'] }}_{{ $service_category_id }}"
                                               name="services[{{ $userServiceArea['service_area_id'] }}][{{ $relatedService->id }}]"><span>{{ $relatedService->title }}</span>
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @empty
                    <div class="tx-center pd-y-20 bg-gray-200">
                        <h5>No Records Found!</h5>
                    </div>
                @endforelse
            </div>
        </div>
    @endforeach
@endif