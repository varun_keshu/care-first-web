@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Vendors</span>
                </div>
                <h2 class="az-content-title">Vendors List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('vendors.create') }}" class="btn btn-success btn-block"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <table id="dataTableList" class="display responsive nowrap">
                    <thead>
                    <tr>
                        <th>Contact number</th>
                        <th>Name</th>
                        <th>Manager</th>
                        <th>Registration Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vendors as $vendor)
                        <tr>
                            <td>{{ $vendor->contact_number }}</td>
                            <td>{{ $vendor->name ?? "- - -" }}</td>
                            <td>{{ isset($vendor->manager->name) ? $vendor->manager->name : $vendor->manager->contact_number }}</td>
                            <td>{{ formatDate($vendor->created_at) }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <a href="{{ route('vendors.show', $vendor->id) }}" class="btn btn-secondary btn-sm">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ route('vendors.edit', $vendor->id) }}" class="btn btn-primary btn-sm">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
        });
    </script>
@endsection
