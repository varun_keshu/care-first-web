@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Vendors</span>
                </div>
                <h2 class="az-content-title">Add New Vendor
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('vendors') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <form action="{{ route('vendors.store') }}" method="post">
                        @csrf
                        @include('admin.vendors.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

