<div id="total_number_of_customer_initiate_completed_bookings" style="display: none;">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Completed Services</label>
                {{ Form::text('criteria[completed_service_count]', (isset($reward_program) && ($reward_program->category == "total_number_of_customer_initiate_completed_bookings")) ? $reward_program->criteria['completed_service_count'] : null ,['class' => 'form-control', 'data-validation' => 'required number',
                'data-validation-error-msg-required' => "Please enter a total completed bookings count.", "data-validation-error-msg-number" => "The input value was not a correct number"] ) }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Free Service</label>
                {{ Form::select('criteria[free_service_id]', $services, (isset($reward_program) && ($reward_program->category == "total_number_of_customer_initiate_completed_bookings")) ? $reward_program->criteria['free_service_id'] : null ,['class' => 'form-control', 'data-validation' => 'required',
                'data-validation-error-msg' => "Please select a service..", "placeholder" => "Select service"] ) }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Service Expiration Days</label>
                {{ Form::text('criteria[expiration_days]', (isset($reward_program) && ($reward_program->category == "total_number_of_customer_initiate_completed_bookings")) ? $reward_program->criteria['expiration_days'] : null ,['class' => 'form-control', 'data-validation' => 'required number',
                'data-validation-error-msg-required' => "Please enter a expiration days.", "data-validation-error-msg-number" => "The input value was not a correct number"] ) }}
            </div>
        </div>
    </div>
</div>
