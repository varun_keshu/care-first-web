@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Reward Programs</span>
                </div>
                <h2 class="az-content-title">Create Reward Program
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('reward_program') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <form role="form" method="post" action="{{ route('reward_program.store') }}">
                        @csrf
                        @include('admin.reward_programs.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('admin.reward_programs.partials.category_actions')
@endsection