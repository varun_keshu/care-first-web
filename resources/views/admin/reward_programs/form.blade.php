<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('title', isset($reward_program) ? $reward_program->title : old('title') ,['class' => 'form-control','id'=>'title', 'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
        </div>
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Category</label>
            {{ Form::select('category', \App\Models\RewardProgram::CATEGORIES,  isset($reward_program) ? $reward_program->category : old('category') ,['class' => 'form-control select2','id'=>'category', 'data-validation' => 'required', 'data-validation-error-msg' => "Please select a category!", "placeholder" => ""] ) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Description</label>
            {{ Form::textarea('description', isset($reward_program) ? $reward_program->description : old('description') ,['class' => 'form-control','id'=>'description', 'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a description!", "rows" => 5] ) }}
        </div>
    </div>
</div>
<div id="criteriaDiv"></div>
<button class="btn btn-az-primary btn-block">{{ isset($reward_program) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script type="text/javascript">
        var editCategory = $("#category").val();
        $(document).ready(function () {
            if (editCategory != ""){
                var clonedHTML = $("#" + editCategory).html();
                $("#criteriaDiv").html(clonedHTML);
            }
            $(document).on('change', "#category", function () {
                $("#criteriaDiv").html("");
                var category = $(this).val();
                var cloneHTML = $("#" + category).html();
                $("#criteriaDiv").html(cloneHTML);
            });
        });
    </script>
@endsection