@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Package Subscriptions</span>
                </div>
                <h2 class="az-content-title">Package Subscription Details
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('package-subscription') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Customer</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->customer->contact_number }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Package Name</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->title ?? "- - -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Description</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->description ?? "- - -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->serviceDetails->title ?? "- - -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service Category</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->serviceCategoryDetails->title ?? "- - -" }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service Count</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->service_count }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Used Service Count</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->used_count }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Package Price</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ formatPrice($packageSubscription->price) }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Quantity</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->quantity }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Total Amount</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ formatPrice($packageSubscription->total_amount) }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service Time Difference</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ $packageSubscription->duration }} {{ \App\Models\Package::DURATION_UNITS[$packageSubscription->duration_unit] }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Service Start Date</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ formatDate($packageSubscription->service_date.' '.$packageSubscription->service_time, "h:i A") }}
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <strong>Next Service Date</strong>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            {{ isset($packageSubscription->next_service_start_date) ? formatDate($packageSubscription->next_service_start_date, "d M, Y") : "- - -" }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
