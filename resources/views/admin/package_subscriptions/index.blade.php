@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Package Subscriptions</span>
                </div>
                <h2 class="az-content-title">
                    Packages Subscriptions List
                </h2>
                @if(count($packageSubscriptions) > 0)
                    <div class="table-responsive">
                        <table class="table table-striped mg-b-0">
                            <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Title</th>
                                <th>Service</th>
                                <th>Package Price</th>
                                <th>Quantity</th>
                                <th>Expired ?</th>
                                <th class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($packageSubscriptions as $packageSubscription)
                                <tr>
                                    <td>{{ $packageSubscription->customer->contact_number ?? "- - -" }}</td>
                                    <td>{{ $packageSubscription->title ?? "- - -" }}</td>
                                    <td>{{ $packageSubscription->serviceDetails->title ?? "- - -" }}</td>
                                    <td>{{ formatPrice($packageSubscription->price) }}</td>
                                    <td>{{ $packageSubscription->quantity }}</td>
                                    <td>{{ checkCheckboxValue($packageSubscription->is_expired) }}</td>
                                    <td class="text-right">
                                        <div class="btn-group" role="group">
                                            <a href="{{ route('package-subscription.show', $packageSubscription->id) }}"
                                               class="btn btn-primary btn-sm">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="tx-center pd-y-20 bg-gray-200">
                        <h5>No Records Found!</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
