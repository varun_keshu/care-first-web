<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">City Name</label>
            {{ Form::text('title', isset($city) ? $city->title : old('title') ,['class' => 'form-control','id'=>'title',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a city name!"] ) }}
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h5 class="card-title tx-dark mg-b-10">City Service Area list
            <span class="pull-right d-inline-block float-right">
                <a href="javascript:;" id="addNewRow" class="btn btn-success btn-block">
                    Add New Area
                </a>
            </span>
        </h5>
    </div>
    <div class="card-body" id="areaRows">
        @if(isset($city) && (count($city->relatedServiceAreas) > 0))
            @foreach($city->relatedServiceAreas as $index => $serviceArea)
                <input type="hidden" name="exist_areas[{{ $index }}][id]" value="{{ $serviceArea->id }}">
                <div class="row areaRowTemplate">
                    <div class="col-sm-5">
                        <div class="form-group">
                            {{ Form::text('exist_areas['.$index.'][title]', $serviceArea->title ,['class' => 'form-control inputCloneField', 'data-name' => 'title',
                                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a area title!"] ) }}
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            {{ Form::text('exist_areas['.$index.'][zip_code]', $serviceArea->zip_code ,['class' => 'form-control inputCloneField', 'data-name' => 'zip_code',
                                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a zip code!"] ) }}
                        </div>
                    </div>
                    @if($index > 0)
                        <div class="col-sm-2">
                            <a href="javascript:;" class="btn btn-danger btn-sm removeRow">
                                <i class="fas fa-trash"></i>
                            </a>
                        </div>
                    @endif
                </div>
            @endforeach
        @else
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Area</label>
                        {{ Form::text('areas[0][title]', null ,['class' => 'form-control',
                                'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a area title!"] ) }}
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Zip Code</label>
                        {{ Form::text('areas[0][zip_code]', null ,['class' => 'form-control',
                                'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a zip code!"] ) }}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<button class="btn btn-az-primary btn-block">{{ isset($city) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script type="text/javascript">
        var index = $("#row_count").val();
        $(document).ready(function () {
            $(document).on('click', "#addNewRow", function () {
                var areaRowTemplateCloned = $('.areaRowTemplate').last().clone();
                areaRowTemplateCloned.find('.inputCloneField').each(function () {
                    var self = $(this);
                    var name_value = self.data('name');
                    self.attr({name: "areas[" + index + "][" + name_value + "]"});
                });
                areaRowTemplateCloned.show();
                $("#areaRows").append(areaRowTemplateCloned);
                index++;
            });
            $(document).on('click', ".removeRow", function () {
                var self = $(this);
                self.parents(".areaRowTemplate").remove();
            });
        })
    </script>
@endsection
