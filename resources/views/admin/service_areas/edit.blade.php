@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Service Areas</span>
                </div>
                <h2 class="az-content-title">Edit Service Area
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('service_areas') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <input type="hidden" id="row_count" value="{{ (count($city->relatedServiceAreas) > 0) ? count($city->relatedServiceAreas) : 1 }}">
                <div class="card card-body pd-40">
                    <form role="form" method="post" action="{{ route('service_areas.update', $city->id) }}" enctype="multipart/form-data" >
                        @csrf
                        @include('admin.service_areas.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row areaRowTemplate" style="display: none;">
        <div class="col-sm-5">
            <div class="form-group">
                {{ Form::text('areas[1][title]', null ,['class' => 'form-control inputCloneField', 'data-name' => 'title',
                        'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a area title!"] ) }}
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group">
                {{ Form::text('areas[1][zip_code]', null ,['class' => 'form-control inputCloneField', 'data-name' => 'zip_code',
                        'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a zip code!"] ) }}
            </div>
        </div>
        <div class="col-sm-2">
            <a href="javascript:;" class="btn btn-danger btn-sm removeRow">
                <i class="fas fa-trash"></i>
            </a>
        </div>
    </div>
@endsection