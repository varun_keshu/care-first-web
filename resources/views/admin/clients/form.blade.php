<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('name', isset($client) ? $client->name : old('name') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
            <input type="file" accept="image/*" class="file-upload dropify"
                   data-show-remove="false" data-allowed-file-extensions="png jpg jpeg"
                   @if(isset($client) && $client->image) data-default-file="{{ showClientImage($client->image) }}" @else data-validation="required"
                   @endif
                   id="image" name="image"/>
        </div>
    </div>
</div>

<button class="btn btn-az-primary btn-block">{{ isset($client) ? "Update" : "Submit" }}</button>
