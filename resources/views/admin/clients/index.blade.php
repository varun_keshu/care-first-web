@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Clients</span>
                </div>
                <h2 class="az-content-title">Client List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('clients.create') }}" class="btn btn-success btn-block"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                @if(count($clients) > 0)
                    <div class="table-responsive">
                        <table class="table table-striped mg-b-0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <img src="{{ showClientImage($item->image) }}" class="img-fluid"
                                             style="height: 100px;">
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="{{ route('clients.edit', $item->id) }}" class="btn btn-primary btn-sm">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="javascript:;" onclick="confirmDelete('{{ route("clients.delete",$item->id) }}')"
                                               class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="tx-center pd-y-20 bg-gray-200">
                        <h5>No Records Found!</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
