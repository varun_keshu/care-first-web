<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{url('images/favicon.ico')}}">
    <!-- Meta -->
    <meta name="description" content="Login Page For Care First Website">
    <meta name="author" content="ThemePixels">
    <title>Forgot Password Care First</title>
    <!-- azia CSS -->
    <link href="{{ asset('admin/css/azia.css') }}" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css"
          rel="stylesheet"/>
</head>
<body class="az-body">
<div class="az-signin-wrapper">
    <div class="az-card-signin">
        <h1 class="az-logo"><img src="{{asset('front/img/logo/logo.png')}}" alt="" height="50px;"></h1>
        @include('admin.layout.flash_messages')
        <div class="az-signin-header">
            <h2>Reset Your Password!</h2>
            <form action="{{ route('reset_admin_password') }}" method="post">
                @csrf
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" readonly value="{{ $result['email'] }}">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Enter password', 'data-validation'=>'required length', 'data-validation-length' => 'min6',
                            'data-validation-error-msg-required' => "Please enter a password!", 'data-validation-error-msg-length' => "The input value is shorter than 6 characters"]) !!}
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    {!! Form::password('password',['class'=>'form-control', 'data-validation' => 'confirmation', 'placeholder'=>'Enter confirm password',
                            'data-validation-error-msg' => "Your confirm password does not match with your password!"]) !!}
                </div>
                <button class="btn btn-az-primary btn-block">Submit</button>
            </form>
        </div>
        <div class="az-signin-footer">
            <p></p>
        </div>
    </div>
</div>

<script src="{{ asset('admin/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('admin/js/azia.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    $.validate({
        modules : 'security'
    });
</script>
</body>
</html>
