<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{url('images/favicon.ico')}}">
    <!-- Meta -->
    <meta name="description" content="Login Page For Care First Website">
    <meta name="author" content="ThemePixels">
    <title>Forgot Password Care First</title>
    <!-- azia CSS -->
    <link href="{{ asset('admin/css/azia.css') }}" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css"
          rel="stylesheet"/>
</head>
<body class="az-body">
<div class="az-signin-wrapper">
    <div class="az-card-signin">
        <h1 class="az-logo"><img src="{{asset('front/img/logo/logo.png')}}" alt="" height="50px;"></h1>
        @include('admin.layout.flash_messages')
        <div class="az-signin-header">
            <h2>Forgot Password!</h2>
            <form action="{{ route('sent_reset_password_link') }}" method="post">
                @csrf
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Enter your registered email" data-validation="email">
                </div>
                <button class="btn btn-az-primary btn-block">Send Mail</button>
            </form>
        </div>
        <div class="az-signin-footer">
            <p><a href="{{ route('login') }}">Back to Login</a></p>
        </div>
    </div>
</div>

<script src="{{ asset('admin/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('admin/js/azia.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    $.validate();
</script>
</body>
</html>
