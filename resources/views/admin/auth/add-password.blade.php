<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{url('images/favicon.ico')}}">
    <!-- Meta -->
    <meta name="description" content="Login Page For Care First Website">
    <meta name="author" content="ThemePixels">
    <title>Login Care First</title>
    <!-- azia CSS -->
    <link href="{{ asset('admin/css/azia.css') }}" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css"
          rel="stylesheet"/>
</head>
<body class="az-body">
<div class="az-signin-wrapper">
    <div class="az-card-signin">
        <h1 class="az-logo"><img src="{{asset('front/img/logo/logo.png')}}" alt="" height="50px;"></h1>
        @include('admin.layout.flash_messages')
        <div class="az-signin-header">
            <h2>Welcome {{ $user->name }}!</h2>
            <h4>Please set password to continue</h4>
            <form action="{{ route('admin-user.password.store',$user->id) }}" method="post" id="loginForm">
                @csrf
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Enter your email"
                           value="{{ $user->email }}"
                           data-validation="email" disabled>
                </div><!-- form-group -->
                <div class="form-group">
                    <label>Password</label>
                    {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Enter password', 'data-validation'=>'required length', 'data-validation-length' => 'min6',
                            'data-validation-error-msg-required' => "Please enter a password!", 'data-validation-error-msg-length' => "The input value is shorter than 6 characters"]) !!}
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    {!! Form::password('password',['class'=>'form-control', 'data-validation' => 'confirmation', 'placeholder'=>'Enter confirm password',
                            'data-validation-error-msg' => "Your confirm password does not match with your password!"]) !!}
                </div>
                <input type="hidden" name="activation_token" value="{{ $user->activation_token }}">
                <button class="btn btn-az-primary btn-block">Proceed</button>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('admin/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('admin/js/azia.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    $.validate({
        modules : 'security'
    });
</script>
</body>
</html>
