@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Customers</span>
                </div>
                <h2 class="az-content-title">Customer Detail
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('customers') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span></h2>
                <div class="card bd-0">
                    <div class="card-header bg-gray-400 bd-b-0-f pd-b-0">
                        <nav class="nav nav-tabs">
                            <a class="nav-link active show" data-toggle="tab" href="#profile">Profile</a>
                            <a class="nav-link" data-toggle="tab" href="#customerAddresses">Addresses</a>
                            <a class="nav-link" data-toggle="tab" href="#supportMessages">Support Messages</a>
                            <a class="nav-link" data-toggle="tab" href="#bookings">Bookings</a>
                        </nav>
                    </div>
                    <div class="card-body bd bd-t-0 tab-content">
                        <div id="profile" class="tab-pane active show">
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Profile Image</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    @if(isset($customer->image))
                                        <img src="{{ showCustomerImage($customer->image) }}" alt="Profile Image"
                                             style="height: 100px;">
                                    @else
                                        - - -
                                    @endif
                                </div>
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Name</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    {{ $customer->name ?? "- - -" }}
                                </div>
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Email Address</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    {{ $customer->email ?? "- - -" }}
                                </div>
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Contact Number</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    {{ $customer->contact_number }}
                                </div>
                            </div>
                        </div>
                        <div id="customerAddresses" class="tab-pane">
                            <div class="row row-sm">
                                @forelse($customer->relatedAddresses as $customerAddress)
                                    <div class="col-md-3">
                                        <div class="card bd-0">
                                            <div class="card-header tx-medium bd-0 tx-white bg-indigo">
                                                {{ $customerAddress->title }}
                                            </div>
                                            <div class="card-body bd bd-t-0">
                                                <p><b>Service Area
                                                        : </b>{{ $customerAddress->serviceArea->title ?? "- - -" }}</p>
                                                {{--<p class="mg-b-0">{{ $customerAddress->flat_or_house_number.', '.$customerAddress->address_line_1  }}</p>
                                                <p class="mg-b-0">{{ $customerAddress->landmark  }}</p>
                                                <p class="mg-b-0">{{ $customerAddress->city.' - '.$customerAddress->zip_code }}
                                                    .</p>--}}
                                                <b>Address : </b>
                                                <p>{{ getAddressString($customerAddress) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <div class="col-md-12">
                                        <div class="tx-center pd-y-20 bg-gray-200">
                                            <h5>No Records Found!</h5>
                                        </div>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                        <div id="supportMessages" class="tab-pane">
                            @if(count($customer->relatedConversations) > 0)
                                <div class="container">
                                    <div class="az-content-left az-content-left-chat">
                                        <div id="azChatList" class="az-chat-list">
                                            @foreach($customer->relatedConversations as $index => $relatedConversation)
                                                <div class="media conversationDiv"
                                                     @if($index == 0) id="firstConversation" @endif
                                                     data-conversation-id="{{ $relatedConversation->id }}">
                                                    <div class="media-body">
                                                        <div class="media-contact-name">
                                                            <span>{{ $relatedConversation->title }}</span>
                                                        </div>
                                                        <p>Booking ID : {{ $relatedConversation->booking_id }}</p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="az-content-body az-content-body-chat" id="supportConversationList">
                                    </div>
                                </div>
                            @else
                                <div class="tx-center pd-y-20 bg-gray-200">
                                    <h5>No Records Found!</h5>
                                </div>
                            @endif
                        </div>
                        <div id="bookings" class="tab-pane">
                            <div class="row row-sm">
                                <div class="col-sm-12">
                                    <table id="dataTableList" class="display responsive nowrap" style="width: 100% !important;">
                                        <thead>
                                        <tr>
                                            <th>Booking ID</th>
                                            <th>Manager</th>
                                            <th>Vendor</th>
                                            <th>Service Details</th>
                                            <th>Status</th>
                                            <th>Service Date</th>
                                            <th>Booking Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($customer->bookings as $booking)
                                            <tr>
                                                <td>{{ $booking->id }}</td>
                                                <td>{{ isset($booking->manager) ? $booking->manager->name : "- - -" }}</td>
                                                <td>{{ isset($booking->vendor) ? $booking->vendor->name : "- - -" }}</td>
                                                <td>{{ $booking->serviceDetails->title }}<br>
                                                    <b>Category : </b>{{ $booking->serviceCategoryDetails->title }}
                                                </td>
                                                <td>{{ \App\Models\ServiceStatus::STATES[$booking->serviceStatusDetail->state] }}</td>
                                                <td>{{ formatDate($booking->service_date.' '.$booking->service_time) }}</td>
                                                <td>{{ formatDate($booking->created_at) }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
        });
    </script>
@endsection