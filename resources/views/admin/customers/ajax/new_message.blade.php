<div class="media flex-row-reverse">
    <div class="media-body">
        <div class="az-msg-wrapper">
            {{ $message->message }}
        </div>
        <div><span class="time timeago" datetime="{{ $message->created_at }}"></span></div>
    </div>
</div>