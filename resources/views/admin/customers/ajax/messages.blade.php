<div class="az-chat-header">
    <div class="az-chat-msg-name">
        <h6>{{ $conversation->title }}</h6>
        <small>Booking ID : {{ $conversation->booking_id }}</small>
    </div>
</div>
@php $last_message_id = null  @endphp
<div id="azChatBody" class="az-chat-body">
    <div class="content-inner" id="supportMessagesContainer">
        @foreach($conversation->relatedMessages as $relatedMessage)
            @php $last_message_id = $relatedMessage['id']; @endphp
            <div class="media @if($relatedMessage->sender_type == "admin") flex-row-reverse @endif">
                <div class="media-body">
                    <div class="az-msg-wrapper">
                        {{ $relatedMessage->message }}
                    </div>
                    <div><span class="time timeago" datetime="{{ $relatedMessage->created_at }}"></span></div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="az-chat-footer">
    <input type="hidden" id="last_message_id" value="{{ $last_message_id }}"/>
    <input type="hidden" name="conversation_id" id="conversation_id" value="{{ $conversation->id }}"/>
    <input type="text" class="form-control" name="message" id="message"
           placeholder="Type your message here...">
    <a href="javascript:;" class="az-msg-send" id="submitButton"><i class="far fa-paper-plane"></i></a>
</div>