@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Customers</span>
                </div>
                <h2 class="az-content-title">Customers List</h2>
                <table id="customersList" class="display responsive nowrap">
                    <thead>
                    <tr>
                        <th>Contact number</th>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Registration Date</th>
                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customers as $customer)
                        <tr>
                            <td>{{ $customer->contact_number }}</td>
                            <td>{{ $customer->name ?? "- - -" }}</td>
                            <td>{{ $customer->email ?? "- - -" }}</td>
                            <td>{{ formatDate($customer->created_at) }}</td>
                            <td class="text-right">
                                <div class="btn-group" role="group">
                                    <a href="{{ route('customer.detail', $customer->id) }}" class="btn btn-secondary btn-sm">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#customersList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
        });
    </script>
@endsection
