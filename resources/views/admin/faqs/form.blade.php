<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Types</label>
            {{ Form::select('category', \App\Models\Faqs::CATEGORIES , isset($faq) ? $faq->category : old('category') ,['class' => 'form-control select2','id'=>'category', 'placeholder' => 'Select faq category..',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please select category!"] ) }}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Question</label>
    {{ Form::text('question', isset($faq) ? $faq->question : old('question') ,['class' => 'form-control','id'=>'question', 'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a question!"] ) }}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Answer</label>
    {{ Form::textarea('answer', isset($faq) ? $faq->answer : old('answer') ,['class' => 'form-control wysiwyg_editor','id'=>'answer',
            'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a answer!"] ) }}
</div>
<button class="btn btn-az-primary btn-block">{{ isset($faq) ? "Update" : "Submit" }}</button>