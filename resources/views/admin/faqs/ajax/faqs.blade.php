<div class="panel panel-default">
    <input type="hidden" value="1" id="is_checked">
    @if(count($faqs) > 0)
        <div class="panel-heading">
            <h3 class="panel-title">FAQS List</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <ul class="sort_menu list-group">
                        @foreach ($faqs as $faq)
                            <li class="list-group-item" data-id="{{ $faq->id }}">
                                <span class="handle"></span> {{ $faq->question }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <button id="updateOrder" type="button" class="btn btn-info btn-single">Save</button>
        </div>
    @else
        <div class="panel-heading">
            <h3 class="panel-title">FAQS List</h3>
        </div>
        <div class="panel-body">
            <h3 class="panel-title text-center">No Questions Found!</h3>
        </div>
    @endif
</div>
<style>
    .list-group-item {
        display: flex;
        align-items: center;
    }

    .highlight {
        background: #f7e7d3;
        min-height: 30px;
        list-style-type: none;
    }

    .handle {
        min-width: 18px;
        background: #607D8B;
        height: 15px;
        display: inline-block;
        cursor: move;
        margin-right: 10px;
    }
</style>