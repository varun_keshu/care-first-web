@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12" id="alertMessage">
                    </div>
                </div>
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Faqs</span>
                </div>
                <h2 class="az-content-title">Sort Order
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('faqs') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <div class="form-group">
                        @foreach(\App\Models\Faqs::CATEGORIES as $identifier => $value)
                            <div class="col-sm-10 categoryIdentifierDiv">
                                <label class="rdiobox">
                                    <input name="faq_category" type="radio" class="faqCategory" value="{{ $identifier }}">
                                    <span>{{ $value }}</span>
                                </label>
                                <div class="faqsList">
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', ".faqCategory", function () {
                var self = $(this);
                var isChecked = self.parents(".categoryIdentifierDiv").find("#is_checked").length;
                if (isChecked == 0){
                    $.ajax({
                        url: $app_url + "/admin/get-faqs-by-category/" + self.val(),
                        type: "get",
                        data: {},
                        dataType: "json",
                        beforeSend: function () {
                            $(".faqsList").html("");
                        },
                        success: function (data) {
                            if (data.success) {
                                self.parents(".categoryIdentifierDiv").find(".faqsList").html(data.html);
                                var target = $('.sort_menu');
                                target.sortable({
                                    handle: '.handle',
                                    placeholder: 'highlight',
                                    axis: "y"
                                })
                            }
                        }
                    })
                }else{
                    $(".faqsList").html("");
                    self.prop("checked", false);
                }
            });
        });

        $(document).on('click', '#updateOrder', function () {
            var category_title = $('input:radio[name="faq_category"]').val();
            var sortData = $('.sort_menu').sortable('toArray', {attribute: 'data-id'});
            var idString = sortData.join(',');
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}});
            $.ajax({
                url: $app_url + "/admin/faq-order/update-order",
                method: 'POST',
                data: {ids: idString, 'category_title': category_title},
                beforeSend: function () {
                    $('#alertMessage').html("");
                },
                success: function (data) {
                    if (data.success) {
                        var alertMsg = '<div class="alert alert-success">' +
                            '<button type="button" class="close" data-dismiss="alert">' +
                            '<span aria-hidden="true">×</span> <span class="sr-only">Close</span></button>' + data.message +
                            '</div>';
                        $('#alertMessage').html(alertMsg);
                    }
                }
            })
        });
    </script>
@endsection