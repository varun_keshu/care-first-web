@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Site Configurations</span>
                </div>
                <h2 class="az-content-title">Site Configurations
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('services') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <div class="row">
                        <div class="col-sm-12">
                            <form method="POST" action="{{ route('site_configurations.update') }}"
                                  accept-charset="UTF-8" style="display:inline">
                                {{ csrf_field() }}
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        @foreach($site_configurations as $item)
                                            <tr>
                                                <td>{{ $item->title }}</td>
                                                <td>
                                                    @if(in_array($item->identifier, ["specific_time_before_taken_cancellation_charges", "service_warranty_days"]))
                                                        <div class="input-group">
                                                            {!! Form::number('configuration_value['.$item->identifier."]",(isset($item->value) ? $item->value : ""),['class'=>'form-control', "min" => 1]) !!}
                                                            <div class="input-group-append">
                                                                @if($item->identifier == "specific_time_before_taken_cancellation_charges")
                                                                    <span class="input-group-text" id="basic-addon2">Minutes</span>
                                                                @elseif($item->identifier == "service_warranty_days")
                                                                    <span class="input-group-text" id="basic-addon2">Days</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @else
                                                        {!! Form::text('configuration_value['.$item->identifier."]",(isset($item->value) ? $item->value : ""),['class'=>'form-control']) !!}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <input type="submit" class="btn btn-info" value="Update">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection