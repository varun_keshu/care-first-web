<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" id="name"
                   value="{{ isset($manager) ? $manager->name : old('name') }}"
                   placeholder="Enter Name" data-validation="required"
                   data-validation-error-msg="Please enter a name!">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="contact_number">Contact Number</label>
            <input type="text" name="contact_number" class="form-control" id="contact_number"
                   value="{{ isset($manager) ? $manager->contact_number : old('contact_number') }}"
                   placeholder="Enter contact number"
                   data-validation="required number length"
                   data-validation-length="10"
                   data-validation-error-msg-required="Please enter your contact number!"
                   maxlength="10">
        </div>
    </div>
</div>
@php
   $checkedServiceAreas = [];
    if(isset($manager) && !empty($user_service_categories)){
        foreach ($user_service_categories as $userServiceArea){
            $checkedServiceAreas[] = $userServiceArea['service_area_id'];

        }
    }
@endphp
@foreach($service_areas as $city => $areas)
    <div class="az-content-label mg-b-5">{{ $city }} - Areas</div>
    <div class="row">
        @foreach($areas as $area_id => $title)
            <div class="col-md-3">
                <div class="form-group">
                    <label class="ckbox">
                        <input type="checkbox" @if(in_array($area_id, $checkedServiceAreas)) checked @endif
                        name="service_areas[{{ $area_id }}]" class="serviceArea"
                               data-value="{{ $area_id }}"><span>{{ $title }}</span>

                    </label>
                </div>
            </div>
        @endforeach
    </div>
@endforeach
<div id="serviceListDiv">
    @if(isset($manager) && !empty($user_service_categories))
        @foreach($user_service_categories as $userServiceArea)
            <div class="card mb-2" id="servicesDiv_{{ $userServiceArea['service_area_id'] }}">
                <div class="card-header">
                    <h5 class="card-title tx-dark mg-b-10">{{ $userServiceArea['service_area_title'] }} - Services</h5>
                </div>
                <div class="card-body">
                    @forelse($userServiceArea['services'] as $service_category_id => $services)
                        @php
                            $checked = true;
                            foreach ($services as $serviceData){
                                if (!$serviceData->is_checked){
                                    $checked = false;
                                    break;
                                }
                            }
                        @endphp
                        <div class="az-content-label mg-b-5">{{ $services[0]->serviceCategoryDetails->title }}
                            <input type="checkbox" class="selectCategory" data-category="{{ $service_category_id }}" @if($checked) checked @endif
                                   data-area="{{ $userServiceArea['service_area_id'] }}"></div>
                        <div class="row">
                            @foreach($services as $relatedService)
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="ckbox">
                                            <input type="checkbox" @if($relatedService->is_checked) checked @endif
                                                   class="service_{{ $userServiceArea['service_area_id'] }}_{{ $service_category_id }}"
                                                   name="services[{{ $userServiceArea['service_area_id'] }}][{{ $relatedService->id }}]"><span>{{ $relatedService->title }}</span>
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @empty
                        <div class="tx-center pd-y-20 bg-gray-200">
                            <h5>No Records Found!</h5>
                        </div>
                    @endforelse
                </div>
            </div>
        @endforeach
    @endif
</div>
<button class="btn btn-az-primary btn-block">Submit</button>
@section('footer_scripts')
    <script type="text/javascript">
        var user_id = "{{ isset($manager) ? $manager->id : "" }}";
        $(document).ready(function () {
            $(document).on('click', ".serviceArea", function () {
                var self = $(this);
                var area = self.data('value');
                var isChecked = self.is(":checked");
                if (isChecked) {
                    var dataValue = {};
                    if (user_id != ""){
                        dataValue = {'user_id' : user_id};
                    }
                    $.ajax({
                        url: $app_url + "/admin/manager/get-available-services/" + area,
                        data: dataValue,
                        type: "get",
                        success: function (data) {
                            if (data.success) {
                                $("#serviceListDiv").append(data.html);
                            }
                        }
                    })
                } else {
                    $("#servicesDiv_" + area).remove();
                }
            });

            $(document).on('click', ".selectCategory", function () {
                var self = $(this);
                var area = self.data('area');
                var category = self.data('category');
                var class_name = "service_" + area + "_" + category;
                var isChecked = self.is(":checked");
                if (isChecked) {
                    $("." + class_name).prop("checked", true);
                } else {
                    $("." + class_name).prop("checked", false);
                }
            })
        })
    </script>
@endsection

