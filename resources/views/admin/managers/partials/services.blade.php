<div class="card mb-2" id="servicesDiv_{{ $service_area_id }}">
    <div class="card-header">
        <h5 class="card-title tx-dark mg-b-10">{{ $service_area->title }} - Services</h5>
    </div>
    <div class="card-body">
        @forelse($service_categories as $service_category_id => $services)
            <div class="az-content-label mg-b-5">{{ $services[0]->serviceCategoryDetails->title }}
                <input type="checkbox" class="selectCategory" data-category="{{ $service_category_id }}" data-area="{{ $service_area_id }}"></div>
            <div class="row">
                @foreach($services as $relatedService)
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="ckbox">
                                <input type="checkbox" class="service_{{ $service_area_id }}_{{ $service_category_id }}"
                                       name="services[{{ $service_area_id }}][{{ $relatedService->id }}]"><span>{{ $relatedService->title }}</span>
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>
        @empty
            <div class="tx-center pd-y-20 bg-gray-200">
                <h5>No Records Found!</h5>
            </div>
        @endforelse
    </div>
</div>
