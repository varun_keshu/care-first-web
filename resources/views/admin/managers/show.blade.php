@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Managers</span>
                </div>
                <h2 class="az-content-title">Manager Detail
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('managers') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card bd-0">
                    <div class="card-header bg-gray-400 bd-b-0-f pd-b-0">
                        <nav class="nav nav-tabs">
                            <a class="nav-link active show" data-toggle="tab" href="#profile">Profile</a>
                            <a class="nav-link" data-toggle="tab" href="#services">Services</a>
                            <a class="nav-link" data-toggle="tab" href="#vendorList">Vendors List</a>
                        </nav>
                    </div>
                    <div class="card-body bd bd-t-0 tab-content">
                        <div id="profile" class="tab-pane active show">
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Profile Image</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    @if(isset($manager->image))
                                        <img src="{{ showUserImage($manager->image) }}" alt="Profile Image"
                                             style="height: 100px;">
                                    @else
                                        - - -
                                    @endif
                                </div>
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Name</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    {{ $manager->name ?? "- - -" }}
                                </div>
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <strong>Contact Number</strong>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    {{ $manager->contact_number }}
                                </div>
                            </div>
                        </div>
                        <div id="services" class="tab-pane">
                            @forelse($manager->userServiceAreas as $userServiceArea)
                                <div class="az-content-label mg-b-5">{{ $userServiceArea->serviceArea->title }}
                                    - {{ $userServiceArea->city->title }}</div>
                                <div class="row row-sm">
                                    <ul>
                                        @foreach($userServiceArea->userServices as $userService)
                                            <li>{{ $userService->serviceDetail->title }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @empty
                                <div class="tx-center pd-y-20 bg-gray-200">
                                    <h5>No Records Found!</h5>
                                </div>
                            @endforelse
                        </div>
                        <div id="vendorList" class="tab-pane">
                            <div class="row row-sm">
                                <div class="col-sm-12">
                                    <table id="dataTableList" class="display responsive nowrap"
                                           style="width: 100% !important;">
                                        <thead>
                                        <tr>
                                            <th>Contact Numbers</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($manager->vendors as $vendor)
                                            <tr>
                                                <td>{{ $vendor->contact_number }}</td>
                                                <td>{{ $vendor->name ?? "- - -" }}</td>
                                                <td>{{ formatDate($vendor->created_at) }}</td>
                                                <td>
                                                    <div class="btn-group" role="group">
                                                        <a href="{{ route('vendors.show', $vendor->id) }}" class="btn btn-secondary btn-sm">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
        });
    </script>
@endsection