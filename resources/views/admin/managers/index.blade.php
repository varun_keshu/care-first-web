@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Managers</span>
                </div>
                <h2 class="az-content-title">Managers List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('managers.create') }}" class="btn btn-success btn-block"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <table id="dataTableList" class="display responsive nowrap">
                    <thead>
                    <tr>
                        <th>Contact number</th>
                        <th>Name</th>
                        <th>Registration Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($managers as $manager)
                        <tr>
                            <td>{{ $manager->contact_number }}</td>
                            <td>{{ $manager->name ?? "- - -" }}</td>
                            <td>{{ formatDate($manager->created_at) }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <a href="{{ route('managers.show', $manager->id) }}" class="btn btn-secondary btn-sm">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ route('managers.edit', $manager->id) }}" class="btn btn-primary btn-sm">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
        });
    </script>
@endsection
