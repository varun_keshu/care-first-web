<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('title', isset($service_status) ? $service_status->title : old('title') ,['class' => 'form-control','id'=>'title',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Customer Title</label>
            {{ Form::text('customer_title', isset($service_status) ? $service_status->customer_title : old('customer_title') ,['class' => 'form-control','id'=>'customer_title',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a customer title!"] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">State</label>
            <div class="input-group">
                {{ Form::select('state', \App\Models\ServiceStatus::STATES, isset($service_status) ? $service_status->state : old('state') ,['class' => 'form-control select2-no-search','id'=>'state',
                        'placeholder' => 'Select..', 'data-validation' => 'required', 'data-validation-error-msg' => "Please select a state!"] ) }}
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">
                        <input name="state_color" data-show-alpha="true" type="text" class="form-control"
                               id="colorpicker" value="{{ isset($service_status) ? $service_status->state_color : '#000000' }}">
                    </span>
                </div>
            </div>
        </div>
    </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Backend State</label>
                {{ Form::select('backend_state', \App\Models\ServiceStatus::BACKEND_STATES, isset($service_status) ? $service_status->backend_state : old('backend_state') ,['class' => 'form-control select2-no-search','id'=>'backend_state',
                        'placeholder' => 'Select..', 'data-validation' => 'required', 'data-validation-error-msg' => "Please select a backend state!"] ) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label class="ckbox">
                    <input type="checkbox" name="update_completion_count"
                           @if(isset($service_status) && ($service_status->update_completion_count == 1)) checked @endif><span>Update completion count?</span>
                </label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label class="ckbox">
                    <input type="checkbox" name="update_issue_confirmation_count"
                           @if(isset($service_status) && ($service_status->update_issue_confirmation_count == 1)) checked @endif><span>Update issue confirmation count?</span>
                </label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label class="ckbox">
                    <input type="checkbox" name="visible_to_customer"
                           @if(isset($service_status) && ($service_status->visible_to_customer == 1)) checked @endif><span>Visible to customer?</span>
                </label>
            </div>
        </div>
    </div>
    <button class="btn btn-az-primary btn-block">{{ isset($service_status) ? "Update" : "Submit" }}</button>
    @section('css')
        <link href="{{ asset('admin/lib/spectrum-colorpicker/spectrum.css') }}" rel="stylesheet"/>
    @endsection
    @section('footer_scripts')
        <script src="{{ asset('admin/lib/spectrum-colorpicker/spectrum.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#colorpicker').spectrum({
                    showButtons: false,
                    preferredFormat: "hex",
                    showInput: true
                });
            })
        </script>
@endsection
