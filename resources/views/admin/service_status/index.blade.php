@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Service Statuses</span>
                </div>
                <h2 class="az-content-title">Service Status List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('service_status.create') }}" class="btn btn-success btn-block"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                @if(count($service_statuses) > 0)
                    <div class="table-responsive">
                        <table class="table table-striped mg-b-0">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>State</th>
                                <th>Visible to customer?</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($service_statuses as $item)
                                <tr>
                                    <td>{{ $item->title ?? "- - -" }}</td>
                                    <td>{{ \App\Models\ServiceStatus::STATES[$item->state] }}</td>
                                    <td>{{ checkCheckboxValue($item->visible_to_customer) }}</td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="{{ route('service_status.edit', $item->id) }}"
                                               class="btn btn-primary btn-sm">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="javascript:;"
                                               onclick="confirmDelete('{{ route("service_status.delete",$item->id) }}')"
                                               class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="tx-center pd-y-20 bg-gray-200">
                        <h5>No Records Found!</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection