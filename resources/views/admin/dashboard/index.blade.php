@extends('admin.layout.master')
@section('content')
    <div class="az-content az-content-dashboard">
        <div class="container">
            <div class="az-content-body">
                <div class="card card-body card-dashboard-twentyfive mg-b-20">
                    <h6 class="card-title">Bookings Statistics</h6>
                    <div class="row row-sm">
                        <div class="col-6 col-sm-4 col-lg">
                            <a href="{{ route('bookings')}}" class="card-label">
                                <label href="{{ route('bookings')}}" class="card-label">Total Booking</label>
                                <h6 class="card-value">{{$data['total']}}
                                </h6>
                            </a>
                            </h6>
                        </div><!-- col -->
                        <div class="col-6 col-sm-4 col-lg">
                            <a href="{{route('bookings',['service_state'=>'open'])}}" class="card-label">
                                <label class="card-label">Open</label>
                                <h6 class="card-value">{{$data['open']}}</h6>
                            </a>
                        </div><!-- col -->
                        <div class="col-6 col-sm-4 col-lg">
                            <a href="{{route('bookings',['service_state'=>'completed'])}}" class="card-label">
                                <label class="card-label">Completed</label>
                                <h6 class="card-value">{{$data['completed']}}</h6>
                            </a>
                        </div><!-- col -->
                        <div class="col-6 col-sm-4 col-lg mg-t-20 mg-sm-t-0">
                            <a href="{{route('bookings',['service_state'=>'cancelled'])}}" class="card-label">
                                <label class="card-label">Cancelled</label>
                                <h6 class="card-value">{{$data['cancelled']}}</h6>
                            </a>
                        </div><!-- col -->
                        <div class="col-6 col-sm-4 col-lg mg-t-20 mg-lg-t-0">
                            <a href="{{route('bookings',['service_state'=>'issue'])}}" class="card-label">
                                <label class="card-label">Issues</label>
                                <h6 class="card-value">{{$data['issue']}}</h6>
                            </a>
                        </div><!-- col -->
                    </div><!-- row -->
                </div>
                <div class="card card-body card-dashboard-twentyfive mg-b-20">
                       <h6 class="card-title">Earning Statistics</h6>
                    <div class="row row-sm">
                        <div class="col-6 col-sm-4 col-lg">
                            <a href="{{route('bookings',['payment_status'=>'total'])}}" class="card-label">
                                <label class="card-label">Total Earning Price</label>
                                <h6 class="card-value">{{formatPrice($earning_data['vendor_earning_price'])}}</h6>
                            </a>
                        </div>
                        <div class="col-6 col-sm-4 col-lg">
                            <a href="{{route('bookings',['payment_status'=>'pending'])}}" class="card-label">
                                <label class="card-label">Pending </label>
                                <h6 class="card-value">{{formatPrice($earning_data['pending'])}}</h6>
                            </a>
                        </div>
                        <div class="col-6 col-sm-4 col-lg">
                            <a href="{{route('bookings',['payment_status'=>'completed'])}}" class="card-label">
                                <label class="card-label">complete</label>
                                <h6 class="card-value">{{formatPrice($earning_data['complete'])}}</h6>
                            </a>
                        </div><!-- col -->
                    </div><!-- row -->
                </div>
                <div class="row row-sm mg-b-20 mg-lg-b-0">
                    <div class="az-content-label mg-b-5">Today Bookings List</div>
                    <div class="col-sm-12 text-center">
                        <div class="card bd-0">
                            <div class="card-header bg-gray-400 bd-b-0-f pd-b-0">
                                <nav class="nav nav-tabs">
                                    <a class="nav-link active show" data-toggle="tab" href="#open">Open</a>
                                    <a class="nav-link" data-toggle="tab" href="#complete">Complete</a>
                                    <a class="nav-link" data-toggle="tab" href="#issue">Issue</a>
                                </nav>
                            </div>
                            <div class="card-body bd bd-t-0 tab-content">
                                <div id="open" class="tab-pane active show">
                                    <table id="dataTableOpen" class="display responsive nowrap"
                                           style="width: 100% !important;">
                                        <thead>
                                        <tr role="row">
                                            <th>#ID</th>
                                            <th>Customer Details</th>
                                            <th>Manager</th>
                                            <th>Vendor</th>
                                            <th>Service Details</th>
                                            <th>Status</th>
                                            <th>Booking Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($booking['open'] as $data)
                                            <tr role="row" class="odd">
                                                <td>{{$data['id']}}</td>
                                                <td>{{$data['customer_contact']}}</td>
                                                <td>{{$data['manager'] }}</td>
                                                <td>{{$data['vendor_name'] ?? "- - -" }}<br></td>
                                                <td>{{$data['title'] }}<br><b>Category : </b>{{$data['category'] }}</td>
                                                <td>{{$data['status']}}<br></td>
                                                <td> {{$data['created_at'] }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="complete" class="tab-pane">
                                    <table id="dataTableComplete" class="display responsive nowrap"
                                           style="width: 100% !important;">
                                        <thead>
                                        <tr role="row">
                                            <th>#ID</th>
                                            <th>Customer Details</th>
                                            <th>Manager</th>
                                            <th>Vendor</th>
                                            <th>Service Details</th>
                                            <th>Status</th>
                                            <th>Booking Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($booking['complete'] as $data)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1" tabindex="0">{{$data['id']}}</td>
                                                <td>{{$data['customer_contact']}}</td>
                                                <td>{{$data['manager'] }}</td>
                                                <td>{{$data['vendor_name'] ?? "- - -" }}<br></td>
                                                <td>{{$data['title'] }}<br><b>Category : </b>{{$data['category'] }}</td>
                                                <td>{{$data['status']}}<br></td>
                                                <td> {{$data['created_at'] }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div id="issue" class="tab-pane">
                                    <table id="dataTableIssue" class="display responsive nowrap"
                                           style="width: 100% !important;">
                                        <thead>
                                        <tr role="row">
                                            <th>#ID</th>
                                            <th>Customer Details</th>
                                            <th>Manager</th>
                                            <th>Vendor</th>
                                            <th>Service Details</th>
                                            <th>Status</th>
                                            <th>Booking Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($booking['issue'] as $data)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1" tabindex="0">{{$data['id']}}</td>
                                                <td>{{$data['customer_contact']}}</td>
                                                <td>{{$data['manager'] }}</td>
                                                <td>{{$data['vendor_name'] ?? "- - -" }}</td>
                                                <td>{{$data['title'] }}<br><b>Category : </b>{{$data['category'] }}</td>
                                                <td>{{$data['status']}}<br></td>
                                                <td> {{$data['created_at'] }}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <div class="az-content-body">
                                <div class="row row-sm mg-b-20 mg-lg-b-0">
                                    <div class="az-content-label mg-b-5">Vendor Earning Details</div>
                                    <div class="col-sm-12 text-center">
                                        <div class="card bd-0">
                                            <div id="VendorList" class="tab-pane active show">
                                                <table id="dataTableVendor" class="display responsive nowrap"
                                                       style="width: 100% !important;">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>Vendor Name</th>
                                                        <th>Contact</th>
                                                        <th>Pending Charges</th>
                                                        <th>Payout</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($vendor_list as $vendorData)
                                                        <tr role="row" class="odd">
                                                            <td>{{ $vendorData['vendor_name'] }}</td>
                                                            <td>{{ $vendorData['contact_number'] }}</td>
                                                            <td>{{ formatPrice($vendorData['vendor_earning_price']) }}</td>
                                                            <td>{{ formatPrice($vendorData['payout']) }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_scripts')
    <script type="text/javascript">

        $(document).ready(function () {
            $('#dataTableOpen').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
            $('#dataTableComplete').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
            $('#dataTableIssue').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }

            });
            $('#dataTableVendor').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }

            });
        });
    </script>

@endsection
