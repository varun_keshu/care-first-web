<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
            {{ Form::text('title', isset($page) ? $page->title : old('title') ,['class' => 'form-control','id'=>'title',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Types</label>
            {{ Form::select('page_type', $page_types , isset($page) ? $page->page_type : old('page_type') ,['class' => 'form-control select2-no-search','id'=>'page_type', 'placeholder' => 'Select page type..',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please select page type!"] ) }}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Title</label>
    {{ Form::text('page_title', isset($page) ? $page->page_title : old('page_title') ,['class' => 'form-control','id'=>'page_title'] ) }}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Keywords</label>
    {{ Form::text('meta_keywords', isset($page) ? $page->meta_keywords : old('meta_keywords') ,['class' => 'form-control','id'=>'meta_keywords'] ) }}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Description</label>
    {{ Form::textarea('meta_description', isset($page) ? $page->meta_description : old('meta_description') ,['class' => 'form-control','id'=>'meta_description', 'rows' => 3] ) }}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Content</label>
    {{ Form::textarea('content', isset($page) ? $page->content : old('content') ,['class' => 'form-control wysiwyg_editor','id'=>'content',
            'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a content!"] ) }}
</div>
<button class="btn btn-az-primary btn-block">{{ isset($page) ? "Update" : "Submit" }}</button>