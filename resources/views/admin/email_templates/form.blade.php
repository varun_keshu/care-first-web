<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Email Actions</label>
            {{ Form::select('identifier', $identifiers , isset($email_template) ? $email_template->identifier : old('identifier') ,['class' => 'form-control select2','id'=>'identifier', 'placeholder' => 'Select action..',
                    'data-validation' => 'required', 'data-validation-error-msg' => "Please select identifier!"] ) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group" id="emailVariables">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Variables</th>
                    <th>Details</th>
                </tr>
                </thead>
                @if(!empty(config('emailvariables')))
                    @foreach(config('emailvariables') as $action => $details)
                        <tbody id="{{ $action }}" class="actionDiv">
                        @foreach($details as $key => $value)
                            <tr>
                                <td>{{ $key }}</td>
                                <td>{{ $value }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endforeach
                @endif
            </table>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Title</label>
    {{ Form::text('title', isset($email_template) ? $email_template->title : old('title') ,['class' => 'form-control','id'=>'title',
            'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a title!"] ) }}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Subject</label>
    {{ Form::text('subject', isset($email_template) ? $email_template->subject : old('subject') ,['class' => 'form-control','id'=>'subject',
            'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a subject!"] ) }}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Content</label>
    {{ Form::textarea('content', isset($email_template) ? $email_template->content : old('content') ,['class' => 'form-control wysiwyg_editor','id'=>'content',
            'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a content!"] ) }}
</div>
<button class="btn btn-az-primary btn-block">{{ isset($email_template) ? "Update" : "Submit" }}</button>

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            getAction($('#identifier').val());

            $(document).on('change', "#identifier", function () {
                getAction($('#identifier').val());
            });
        });
        function getAction(selected_action) {
            $("#emailVariables").toggle(selected_action != "");
            $(".actionDiv").hide();
            if (selected_action != ""){
                $('#'+selected_action).show();
            }
        }
    </script>
@endsection