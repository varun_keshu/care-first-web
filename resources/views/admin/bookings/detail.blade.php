@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Bookings</span>
                </div>
                <h2 class="az-content-title">Booking Details
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ $previous_url }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <div class="card card-invoice">
                        <div class="card-body">
                            <div class="invoice-header">
                                <h1 class="invoice-title">Invoice</h1>
                                @if(isset($booking->vendor_id))
                                    <div class="billed-from">
                                        <h6>{{ $booking->vendor->name }}</h6>
                                        <p><b>Contact No :</b> {{ $booking->vendor->contact_number }}</p>
                                    </div><!-- billed-from -->
                                @endif
                            </div><!-- invoice-header -->

                            <div class="row mg-t-20">
                                <div class="col-md">
                                    <label class="tx-gray-600">Customer Address</label>
                                    <div class="billed-to">
                                        <h6>{{ $booking->customer->name }}</h6>
                                        <p>{{ getAddressString($booking->address_details) }}<br>
                                            <b>Contact No :</b> {{ $booking->customer->contact_number }}</p>
                                    </div>
                                </div><!-- col -->
                                <div class="col-md">
                                    <label class="tx-gray-600">Invoice Information</label>
                                    <p class="invoice-info-row">
                                        <span>Invoice No</span>
                                        <span>#{{ $booking->id }}</span>
                                    </p>
                                    <p class="invoice-info-row">
                                        <span>Booking ID</span>
                                        <span>{{ $booking->id }}</span>
                                    </p>
                                    <p class="invoice-info-row">
                                        <span>Service Date</span>
                                        <span>{{ formatDate(\Carbon\Carbon::parse($booking->service_date), "d M, Y") }} {{ formatDate(\Carbon\Carbon::parse($booking->service_time), "h:i A") }}</span>
                                    </p>
                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="table-responsive mg-t-40">
                                <table class="table table-invoice">
                                    <thead>
                                    <tr>
                                        <th class="wd-20p">Service Category</th>
                                        <th class="wd-40p">Service</th>
                                        <th class="tx-center">QNTY</th>
                                        <th class="tx-right">Unit Price</th>
                                        <th class="tx-right">Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{ $booking->serviceCategoryDetails->title }}</td>
                                        <td class="tx-12">{{ $booking->serviceDetails->title }}</td>
                                        <td class="tx-center">{{ isset($booking->quantity) ? $booking->quantity : "- -" }}</td>
                                        <td class="tx-right">{{ formatPrice($booking->service_price) }}</td>
                                        <td class="tx-right">{{ formatPrice($booking->price) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="4" class="valign-middle">
                                            @if(isset($booking->notes))
                                                <div class="invoice-notes">
                                                    <label class="az-content-label tx-13">Notes</label>
                                                    <p>{{ $booking->notes }}</p>
                                                </div><!-- invoice-notes -->
                                            @endif
                                        </td>
                                        <td class="tx-right">Commission</td>
                                        <td colspan="2"
                                            class="tx-right">{{ formatPrice($booking->commission_fee) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="tx-right">Insurance Amount</td>
                                        <td colspan="2"
                                            class="tx-right">{{ formatPrice($booking->insurance_premium_amount) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="tx-right">Previous Charges</td>
                                        <td colspan="2"
                                            class="tx-right">{{ formatPrice($booking->previous_pending_charges) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="tx-right tx-uppercase tx-bold tx-inverse">Total Due</td>
                                        <td colspan="2" class="tx-right"><h4
                                                    class="tx-primary tx-bold">{{ formatPrice($booking->total_amount) }}</h4>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr class="mg-b-40">
                            <div class="az-content-label mg-b-5">Status History</div>
                            <div class="table-responsive mg-t-40">
                                <table class="table table-invoice">
                                    <thead>
                                    <tr>
                                        <th class="wd-20p">Status</th>
                                        <th class="wd-20p">State</th>
                                        <th class="wd-40p">Message</th>
                                        <th class="tx-center">Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($status_history as $bookingStatusHistory)
                                        <tr>
                                            <td>{{ $bookingStatusHistory->serviceStatusDetail->title ?? "- - -" }}</td>
                                            <td>{{ \App\Models\ServiceStatus::STATES[$bookingStatusHistory->state] }}</td>
                                            <td>{{ $bookingStatusHistory->notes }}
                                                @if(isset($bookingStatusHistory->reason))
                                                    <br><b>Reason : </b>{{ $bookingStatusHistory->reason }}
                                                @endif
                                            </td>
                                            <td>{{ formatDate($bookingStatusHistory->created_at) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection