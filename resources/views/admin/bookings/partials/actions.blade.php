<div class="btn-group" role="group">
    @if(!isset($booking_detail->vendor_id) && ($booking_detail->is_cancelled_booking == 0))
        <a href="{{ route('booking.getAvailableVendorsList', $booking_detail->id) }}" class="btn btn-warning btn-sm" title="Assign To Vendor">
            <i class="fa fa-paper-plane"></i>
        </a>
    @endif
    <a href="{{ route('booking.detail', $booking_detail->id) }}" class="btn btn-secondary btn-sm">
        <i class="fa fa-eye"></i>
    </a>
</div>