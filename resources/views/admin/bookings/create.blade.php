@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Bookings</span>
                </div>
                <h2 class="az-content-title">Create Booking
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('bookings') }}" class="btn btn-warning btn-block" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    <form action="{{ route('booking.store') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Customer</label>
                                    {!! Form::select('customer_id', [], null, ['id' => "customer_id", "class" => "form-control", "placeholder" => "",
                                                'data-validation' => "required"]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Customer Addresses</label>
                                    {!! Form::select('customer_address_id', [], null, ['id' => "customer_address_id", "class" => "form-control", 'data-validation' => "required"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Services</label>
                                    {!! Form::select('service_id', $services, null, ['id' => "service_id", "class" => "form-control select2", "placeholder" => "",
                                                'data-validation' => "required"]) !!}
                                </div>
                            </div>
                            <div class="col-md-6" id="managerList">
                                <div class="form-group">
                                    <label for="name">Managers</label>
                                    {!! Form::select('manager_id', [], null, ['id' => "manager_id", "class" => "form-control", "placeholder" => "",
                                                'data-validation' => "required"]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                                            </div>
                                        </div>
                                        {!! Form::text('service_date', old('service_date'), ['class' => "form-control fc-datepicker", "placeholder" => "MM/DD/YYYY", 'data-validation' => "required"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Time</label>
                                    <input type="text" name="service_time" class="form-control timepicker"
                                           data-template="dropdown" data-show-seconds="true"
                                           data-default-time="{{ formatDate(\Carbon\Carbon::now(), "h:i A") }}"
                                           data-show-meridian="true" data-validation="required"
                                           data-minute-step="15" data-second-step="30"
                                           value="{{ old('service_time') }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Notes</label>
                                    {!! Form::textarea('notes', old('notes'), ['class' => "form-control", "placeholder" => "Enter some notes..", 'data-validation' => "required", 'rows' => 3]) !!}
                                </div>
                            </div>
                        </div>
                        <div id="priceDetails">
                        </div>
                        <button class="btn btn-az-primary btn-block">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.css"
          integrity="sha512-E4kKreeYBpruCG4YNe4A/jIj3ZoPdpWhWgj9qwrr19ui84pU5gvNafQZKyghqpFIHHE4ELK7L9bqAv7wfIXULQ=="
          crossorigin="anonymous"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="{{ asset('admin/lib/widgets/datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"
            integrity="sha512-2xXe2z/uA+2SyT/sTSt9Uq4jDKsT0lV4evd3eoE/oxKih8DSAsOF6LUb+ncafMJPAimWAXdu9W+yMXGrCVOzQA=="
            crossorigin="anonymous"></script>
    <script src="{{ asset('admin/modules/bookings/create.js') }}" type="text/javascript"></script>
@endsection
