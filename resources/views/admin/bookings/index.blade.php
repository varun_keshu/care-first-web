@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Bookings</span>
                </div>
                <h2 class="az-content-title">Booking List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('booking.create') }}" class="btn btn-success btn-block" title="Add new">
                            <i class="fa fa-plus" aria-hidden="true"></i> Create Booking
                        </a>
                    </span>
                </h2>
                {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                <div class="pd-20 pd-sm-20 bg-gray-200 wd-xl-100p mb-3">
                    <div class="row row-xs">
                        <div class="col-md-2">
                            <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                        </div>
                        <div class="col-md-3">
                            {!! Form::select('service_status', $status_list, $selectedStatuses,['id' => 'service_status', 'class' => 'form-control service-status-select2' , 'multiple' ]) !!}
                        </div>
                        <div class="col-md-3">
                            <input name="booking_date" id="booking_date" type="text" class="form-control"
                                   autocomplete="off" placeholder="Select Date">
                        </div>
                        <div class="col-md mg-t-10 mg-md-t-0">
                            <button type="submit" class="btn btn-az-success btn-success">Search</button>
                            @if(request('service_state') === null)
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset</button>
                            @else
                                <a href="{{ route('bookings') }}" class="btn btn-az-warning btn-warning">Reset</a>
                            @endif
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <table id="dataTableList" class="display responsive">
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Customer</th>
                        <th>Manager</th>
                        <th>Vendor</th>
                        <th>Service Details</th>
                        <th>Status</th>
                        <th>Service Date</th>
                        <th>Booking Date</th>
                        <th>Amount</th>
                        <th>System Commission</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
        var  payment_status = "{{request('payment_status')}}"

    </script>

    <script src="{{ asset('admin/modules/bookings/index.js') }}" type="text/javascript"></script>
@endsection
