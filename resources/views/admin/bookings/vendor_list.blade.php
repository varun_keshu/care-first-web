@extends('admin.layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layout.flash_messages')
                <div class="az-content-breadcrumb">
                    <span>Dashboard</span>
                    <span>Bookings</span>
                </div>
                <h2 class="az-content-title">Assign To Vendor</h2>
                <div class="card card-body pd-40">
                    @if(count($vendors) > 0)
                        <form action="{{ route('booking.assignVendor', $booking_id) }}" method="post">
                            @csrf
                            @foreach($vendors as $vendor)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="rdiobox">
                                                <input name="vendor_id" value="{{ $vendor->id }}" type="radio">
                                                <span>{{ $vendor->name }}</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <button class="btn btn-az-primary pd-x-30 mg-r-5">Assign</button>
                        </form>
                    @else
                        No available vendors for this service time period.
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection