<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Select Service Priority</label>
            <select name="priority" class="form-control" id="priority" data-validation="required">
                <option></option>
                <option value="low">Low</option>
                <option value="medium">Medium</option>
                <option value="high">High</option>
            </select>
        </div>
    </div>
</div>
<input type="hidden" name="price" id="price">
@foreach($service->price_details as $priority => $price)
    <div class="pd-sm-40 bg-gray-200 wd-xl-50p mg-b-20 allPriorityDiv" id="{{ $priority }}" style="display:none;">
        <div class="row row-xs align-items-center mg-b-20">
            <div class="col-md-4">
                <label class="form-label mg-b-0">Service Amount</label>
            </div>
            <div class="col-md-8 mg-t-5 mg-md-t-0">
                <input type="hidden" class="amount"
                       value="{{ isset($service->discounted_price_details[$priority]) ? $service->discounted_price_details[$priority] : $price }}">
                {{ isset($service->discounted_price_details[$priority]) ? formatPrice($service->discounted_price_details[$priority]) : formatPrice($price) }}
                @if(isset($service->discounted_price_details[$priority]))
                    <del style="color: red">{{ formatPrice($price) }}</del>
                @endif
            </div>
        </div>
    </div>
@endforeach