$(document).ready(function () {
    $('#dataTableList').DataTable({
        serverSide: true,
        responsible:true,
        searching:false,
        ordering: true,
        ajax: ({
            url: $app_url + "/admin/contact_submission/ajax",
            type: "get",
            data: {
                _token: '{{ csrf_token() }}'
            }
        }),
        "columns": [
            {data: 'email', 'sortable': false, 'width': "10%"},
            {data: 'message', 'sortable': false, 'width': "20%"}
        ]
    });
});
