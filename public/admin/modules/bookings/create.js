$(document).ready(function () {
    $("#customer_id").select2({
        placeholder: "",
        ajax: {
            url: $app_url + "/admin/booking/get-customers-list",
            type: "get",
            dataType: 'json',
            data: function (params) {
                var searchTerm = params.term;
                return {
                    searchTerm: searchTerm
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });

    $('.fc-datepicker').datepicker({
        minDate: 1
    });

    $('.timepicker').timepicker();

    $(document).on('change', "#customer_id", function () {
        $("#customer_address_id").val("").trigger('change');
    });

    $(document).on('change', "#priority", function () {
        var val = $(this).val();
        $("#low").toggle(val == "low");
        $("#medium").toggle(val == "medium");
        $("#high").toggle(val == "high");
        var amount = $("#"+val).find(".amount").val();

        $("#price").val(amount);
    });

    $(document).on('change', "#service_id", function () {
        $("#manager_id").val("").trigger('change');

        $.ajax({
            url : $app_url + "/admin/booking/get-service-price-details/" + $("#service_id").val(),
            type : 'get',
            data : {},
            beforeSend : function () {
                $("#priceDetails").html("");
            },
            success : function (data) {
                if (data.success){
                    $("#priceDetails").html(data.html);

                    $('#priority').select2({
                        placeholder: "",
                        minimumResultsForSearch: Infinity
                    });
                }
            }
        })
    });

    $(document).on('change', "#customer_address_id", function () {
        $("#manager_id").val("").trigger('change');
    });

    $("#customer_address_id").select2({
        placeholder: "",
        ajax: {
            url: $app_url + "/admin/booking/get-customer-addresses",
            type: "get",
            dataType: 'json',
            data: function (params) {
                var searchTerm = params.term;
                return {
                    searchTerm: searchTerm,
                    customerId: $("#customer_id").val()
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });

    $("#manager_id").select2({
        placeholder: "",
        ajax: {
            url: $app_url + "/admin/booking/get-managers-by-service",
            type: "get",
            dataType: 'json',
            data: function (params) {
                var searchTerm = params.term;
                return {
                    searchTerm: searchTerm,
                    serviceId: $("#service_id").val(),
                    addressId: $("#customer_address_id").val()
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });
});