$(document).ready(function () {
    $('#dataTableList').DataTable({
        serverSide: true,
        responsible:true,
        searching:false,
        ordering: true,
        ajax: ({
            url: $app_url + "/admin/contact_request/ajax",
            type: "get",
            data: {
                _token: '{{ csrf_token() }}'
            }
        }),
        "columns": [
            {data: 'customer_details', 'sortable': false, 'width': "10%"},
            {data: 'service_categories', 'sortable': false, 'width': "20%"},
            {data: 'title', 'sortable': false, 'width': "10%"},
            {data: 'action', 'orderable': false, 'width': "10%"}
        ]
    });
});
