$(document).ready(function () {
    $('#service_category_id').select2({
        placeholder: "All Categories",
        minimumResultsForSearch: Infinity
    });

    $('#booking_date').daterangepicker({
        autoUpdateInput: false,
        endDate: moment().endOf('day'),
        locale: {
            cancelLabel: 'Clear'
        },
        ranges: {
            'Today': [moment(), moment()],
            'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Next 7 Days': [moment().add(6, 'days'), moment()],
            // 'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            // 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
    $('#booking_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });
    $('#booking_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    if ($('#dataTableList').length) {
        var $dataTableList = $('#dataTableList').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            bFilter: false,
            lengthMenu: [[50, 100, 500, -1], [50, 100, 500, "All"]],
            "aaSorting": [[0, 'asc']],
            ajax: {
                url: $app_url + '/admin/report/by-category/ajax',

                data: function (d) {
                    d.service_category_id = $('#service_category_id').val();
                    d.booking_date = $('#booking_date').val();


                }
            },
            "columns": [
                {data: 'id', 'sortable': true, 'width': "5%"},
                {data: 'customer', 'sortable': false, 'width': "10%"},
                {data: 'manager', 'sortable': false, 'width': "10%"},
                {data: 'vendor', 'orderable': false, 'width': "10%"},
                {data: 'service_details', 'orderable': false, 'width': "15%"},
                {data: 'status', 'orderable': false, 'width': "10%"},
                {data: 'service_at', 'orderable': false, 'width': "10%"},
                {data: 'booking_date', 'orderable': false, 'width': "10%"},
                {data: 'action', 'orderable': false, 'width': "15%"}

            ],
            select: true,
            bStateSave: true,
            fnStateSave: function (settings, data) {
                localStorage.setItem("dataTables_state", JSON.stringify(data));
            },
            fnStateLoad: function (settings) {
                return JSON.parse(localStorage.getItem("dataTables_state"));
            }
        });

        $('#search-form').on('submit', function (e) {
            $dataTableList.draw();
            e.preventDefault();
        });

        $('#reset-filters').on('click', function () {

            $("#booking_date").val(null);
            $("#service_category_id").val(null).trigger('change');
            $dataTableList.draw();
        });
    }
});
