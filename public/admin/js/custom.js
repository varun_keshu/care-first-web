$(document).ready(function () {
    $.validate({
        modules: 'file, security, logic'
    });

    $(".dropify").dropify();

    // Binding common event on all the elements with wysiwyg_editor class
    if ($('.wysiwyg_editor').length > 0) {
        $('.wysiwyg_editor').summernote({
            tabsize: 4,
            height: 250
        });
    }

    $('.select2').select2({
        placeholder: ""
    });

    $('.select2-no-search').select2({
        placeholder: "",
        minimumResultsForSearch: Infinity
    });
});

function confirmDelete(delete_url) {
    bootbox.confirm({
        message: "Are you sure you want to delete ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: 'btn-success'
            },
            cancel: {
                label: "No",
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result === true) {
                window.location = delete_url;
            }
        }
    });
}
